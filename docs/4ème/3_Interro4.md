---
author: Anthony Salze
title: Interrogations écrites
hide :
    -footer
---
# <span style="color:Tomato"> Évaluations

## <span style="color:LightSalmon">Interrogations écrites
??? tip "Interrogation écrite <span style=color:red> :material-file-pdf-box: <span style=color:blue>:material-text-box-edit-outline: "
    === "IE 01"
        [PDF](a_telecharger/4_C00b/4_IE_01_Nombres_relatifs.pdf){:target="_blank"}   
        [LaTeX](a_telecharger/4_C00b/4_IE_01_Nombres_relatifs.tex)  
    === "IE 02"
        [PDF](a_telecharger/4_C00b/4_IE_02_Translations_frises_pavages.pdf){:target="_blank"}   
        [LaTeX](a_telecharger/4_C00b/4_IE_02_Translations_frises_pavages.tex)  
    === "IE 03"
        [PDF](a_telecharger/4_C00b/4_IE_03_Transformations.pdf){:target="_blank"}   
        [LaTeX](a_telecharger/4_C00b/4_IE_03_Transformations.tex)  
    === "IE 04"
        [PDF](a_telecharger/4_C00b/4_IE_04_Calcul_littéral_2.pdf){:target="_blank"}   
        [LaTeX](a_telecharger/4_C00b/4_IE_04_Calcul_littéral_2.tex)  
        
    <span style=color:CornflowerBlue>_Dernière mise à jour : le 29/09/24_


