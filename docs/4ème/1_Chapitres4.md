---
author: Anthony Salze
title: Leçons et exercices
hide :
    -footer
---
# <span style="color:Tomato"> Leçons et exercices

## <span style="color:LightSalmon">01 : Les nombres relatifs : multiplication et division
???+ info "Contenu"
    - Rappels sur l'addition et la soustraction avec des nombres relatifs.
    - Règles pour multiplier et diviser des nombres relatifs.

??? tip "Fichiers à télécharger <span style=color:red> :material-file-pdf-box: <span style=color:blue>:material-text-box-edit-outline: "
    === "Leçon"
        [Leçon : 01 - Nombres relatifs : multiplication et division](a_telecharger/4_C01/4_C01_Nombres_relatifs_mult_div.pdf){:target="_blank"}   
    === "Exercices"
        [Exercices](a_telecharger/4_C01/4_C01_Exercices_énoncé.pdf){:target="_blank"}   
    === "Exercices + correction"
        [Exercices + correction](a_telecharger/4_C01/4_C01_Exercices_correction.pdf){:target="_blank"} 
    === "LaTeX"
        - [Leçon](a_telecharger/4_C01/4_C01_Nombres_relatifs_mult_div.tex) 
        - [Leçon Beamer](a_telecharger/4_C01/4_C01_Présentation.tex) 
        - [Exercices](a_telecharger/4_C01/4_C01_Exercices.tex) 
    <span style=color:CornflowerBlue>_Dernière mise à jour : le 03/12/23_

## <span style="color:LightSalmon">02 : Translations, frises et pavages
???+ info "Contenu"
    - Nouvelle transformation : translation.
    - Propriétés de la translation.
    - Identifier une translation dans une frise ou un pavage.

??? tip "Fichiers à télécharger <span style=color:red> :material-file-pdf-box: <span style=color:blue>:material-text-box-edit-outline: "
    === "Leçon"
        [Leçon : 02 - Translations, frises et pavages](a_telecharger/4_C02/4_C02_Translations.pdf){:target="_blank"}   
    === "Exercices"
        [Exercices](a_telecharger/4_C02/4_C02_Exercices_énoncé.pdf){:target="_blank"}   
    === "Exercices + correction"
        [Exercices + correction](a_telecharger/4_C02/4_C02_Exercices_correction.pdf){:target="_blank"} 
    === "LaTeX"
        - [Leçon](a_telecharger/4_C02/4_C02_Translations.tex) 
        - [Leçon Beamer](a_telecharger/4_C02/4_C02_Présentation.tex) 
        - [Exercices](a_telecharger/4_C02/4_C02_Exercices.tex) 
    <span style=color:CornflowerBlue>_Dernière mise à jour : le 03/12/23_


## <span style="color:LightSalmon">03 : Calcul littéral : simple distributivité
???+ info "Contenu"
    - Rappels sur les simplifications d'écriture.
    - Simple distributivité.
    - Réduire une expression littérale.

??? tip "Fichiers à télécharger <span style=color:red> :material-file-pdf-box: <span style=color:blue>:material-text-box-edit-outline: "
    === "Leçon"
        [Leçon : 03 - Calcul littéral : simple distributicité](a_telecharger/4_C03/4_C03_Calcul_littéral.pdf){:target="_blank"}   
    === "Exercices"
        [Exercices](a_telecharger/4_C03/4_C03_Exercices_énoncé.pdf){:target="_blank"}   
    === "Exercices + correction"
        [Exercices + correction](a_telecharger/4_C03/4_C03_Exercices_correction.pdf){:target="_blank"} 
    === "LaTeX"
        - [Leçon](a_telecharger/4_C03/4_C03_Calcul_littéral_1.tex) 
        - [Leçon Beamer](a_telecharger/4_C03/4_C03_Présentation.tex) 
        - [Exercices](a_telecharger/4_C03/4_C03_Exercices.tex) 
    <span style=color:CornflowerBlue>_Dernière mise à jour : le 03/12/23_


## <span style="color:LightSalmon">04 : Rotations, frises et pavages
???+ info "Contenu"
    - Nouvelle transformation : rotation.
    - Propriétés de la rotation.
    - Identifier une rotation dans une frise ou un pavage.
    - Bilan de toutes les transformations connues : symétries axiale et centrale, translation et rotation.

??? tip "Fichiers à télécharger <span style=color:red> :material-file-pdf-box: <span style=color:blue>:material-text-box-edit-outline: "
    === "Leçon"
        [Leçon : 04 - Rotations, frises et pavages](a_telecharger/4_C04/4_C04_Rotations.pdf){:target="_blank"}   
    === "Exercices"
        [Exercices](a_telecharger/4_C04/4_C04_Exercices_énoncé.pdf){:target="_blank"}   
    === "Exercices + correction"
        Non disponible
    === "LaTeX"
        - [Leçon](a_telecharger/4_C04/4_C04_Rotations.tex) 
        - [Exercices](a_telecharger/4_C04/4_C04_Exercices.tex) 
    <span style=color:CornflowerBlue>_Dernière mise à jour : le 03/12/23_


## <span style="color:LightSalmon">05 : Calcul littéral : double distributivité
???+ info "Contenu"
    - Double distributivité.
    - Identités remarquables : formules et démonstrations.
    - Résolution de problèmes, lien avec la géométrie (calculs de périmètres, d'aires).

??? tip "Fichiers à télécharger <span style=color:red> :material-file-pdf-box: <span style=color:blue>:material-text-box-edit-outline: "
    === "Leçon"
        [Leçon : 05 - Calcul littéral : double distributicité](a_telecharger/4_C05/4_C05_Calcul_littéral_2.pdf){:target="_blank"}   
    === "Exercices"
        [Exercices](a_telecharger/4_C05/4_C05_Exercices_énoncé.pdf){:target="_blank"}   
    === "Exercices + correction"
        [Exercices + correction](a_telecharger/4_C05/4_C05_Exercices_correction.pdf){:target="_blank"}   
    === "LaTeX"
        - [Leçon](a_telecharger/4_C05/4_C05_Calcul_littéral_2.tex) 
        - [Exercices](a_telecharger/4_C05/4_C05_Exercices.tex) 
    <span style=color:CornflowerBlue>_Dernière mise à jour : le 21/12/23_

## <span style="color:LightSalmon">06 : Triangles isométriques
???+ info "Contenu"
    - Définition de triangles isométriques.
    - Propriétés pour démontrer que deux triangles sont isométriques.

??? tip "Fichiers à télécharger <span style=color:red> :material-file-pdf-box: <span style=color:blue>:material-text-box-edit-outline: "
    === "Leçon"
        [Leçon : 06 - Triangles isométriques](a_telecharger/4_C06/4_C06_Triangles_isométriques.pdf){:target="_blank"}   
    === "Exercices"
        [Exercices](a_telecharger/4_C06/4_C06_Exercices_énoncé.pdf){:target="_blank"}   
    === "Exercices + correction"
        Non disponible
    === "LaTeX"
        - [Leçon](a_telecharger/4_C06/4_C06_Triangles_isométriques.tex) 
        - [Exercices](a_telecharger/4_C06/4_C06_Exercices.tex) 
    <span style=color:CornflowerBlue>_Dernière mise à jour : le 20/12/23_

## <span style="color:LightSalmon">07 : Additions et soustractions de fractions
???+ info "Contenu"
    - Rappels sur les additions et soustractions de fractions.
    - Utilisation du produit en croix pour montrer que deux quotients sont égaux.

??? tip "Fichiers à télécharger <span style=color:red> :material-file-pdf-box: <span style=color:blue>:material-text-box-edit-outline: "
    === "Leçon"
        [Leçon : 07 - Additions et soustractions de fractions](a_telecharger/4_C07/4_C07_Additions_et_soustractions_de_fractions.pdf){:target="_blank"}   
    === "Exercices"
        [Exercices](a_telecharger/4_C07/4_C07_Exercices_énoncé.pdf){:target="_blank"}   
    === "Exercices + correction"
        [Exercices + correction](a_telecharger/4_C07/4_C07_Exercices_correction.pdf){:target="_blank"}   
    === "LaTeX"
        - [Leçon](a_telecharger/4_C07/4_C07_Additions_et_soustractions_de_fractions.tex) 
        - [Exercices](a_telecharger/4_C07/4_C07_Exercices.tex) 
    <span style=color:CornflowerBlue>_Dernière mise à jour : le 17/01/24_


## <span style="color:LightSalmon">08 : Théorème de Pythagore
???+ info "Contenu"
    - Notion de racine carrée.
    - Utilisation de la calculatrice pour calculer la racine carrée d'un nombre.
    - Utilisation du théorème de Pythagore pour calculer une longueur manquante dans un triangle rectangle connaissant la longueur des deux autres côtés.

??? tip "Fichiers à télécharger <span style=color:red> :material-file-pdf-box: <span style=color:blue>:material-text-box-edit-outline: "
    === "Leçon"
        [Leçon : 08 - Théorème de Pythagore](a_telecharger/4_C08/4_C08_Pythagore_1.pdf){:target="_blank"}   
    === "Exercices"
        [Exercices](a_telecharger/4_C08/4_C08_Exercices_énoncé.pdf){:target="_blank"}   
    === "Exercices + correction"
        [Exercices + correction](a_telecharger/4_C08/4_C08_Exercices_correction.pdf){:target="_blank"}   
    === "LaTeX"
        - [Leçon](a_telecharger/4_C08/4_C08_Pythagore_1.tex) 
        - [Exercices](a_telecharger/4_C08/4_C08_Exercices.tex) 
    <span style=color:CornflowerBlue>_Dernière mise à jour : le 15/03/24_


## <span style="color:LightSalmon">09 : Multiplications et divisions de fractions
???+ info "Contenu"
    - Multiplication de plusieurs fractions.
    - Inverse d'un nombre et plus particulièrement d'une fraction.
    - Division de deux fractions.

??? tip "Fichiers à télécharger <span style=color:red> :material-file-pdf-box: <span style=color:blue>:material-text-box-edit-outline: "
    === "Leçon"
        [Leçon : 09 - Additions et soustractions de fractions](a_telecharger/4_C09/4_C09_Fractions_2.pdf){:target="_blank"}   
    === "Exercices"
        [Exercices](a_telecharger/4_C09/4_C09_Exercices_énoncé.pdf){:target="_blank"}   
    === "Exercices + correction"
        [Exercices + correction](a_telecharger/4_C09/4_C09_Exercices_correction.pdf){:target="_blank"}   
    === "LaTeX"
        - [Leçon](a_telecharger/4_C09/4_C09_Fractions_2.tex) 
        - [Exercices](a_telecharger/4_C09/4_C09_Exercices.tex) 
        - [Présentation](a_telecharger/4_C09/4_C09_Présentation.tex) 
    <span style=color:CornflowerBlue>_Dernière mise à jour : le 15/03/24_


## <span style="color:LightSalmon">10 : Réciproque et contraposée du théorème de Pythagore
???+ info "Contenu"
    - Savoir si un triangle est rectangle connaissant les longueurs de ses côtés.

??? tip "Fichiers à télécharger <span style=color:red> :material-file-pdf-box: <span style=color:blue>:material-text-box-edit-outline: "
    === "Leçon"
        [Leçon : 10 - Réciproque et contraposée du théorème de Pythagore](a_telecharger/4_C10/4_C10_Pythagore_2.pdf){:target="_blank"}   
    === "Exercices"
        [Exercices](a_telecharger/4_C10/4_C10_Exercices_énoncé.pdf){:target="_blank"}   
    === "Exercices + correction"
        [Exercices + correction](a_telecharger/4_C10/4_C10_Exercices_correction.pdf){:target="_blank"}   
    === "LaTeX"
        - [Leçon](a_telecharger/4_C10/4_C10_Pythagore_2.tex) 
        - [Exercices](a_telecharger/4_C10/4_C10_Exercices.tex) 
        - [Présentation](a_telecharger/4_C10/4_C10_Présentation.tex) 
    <span style=color:CornflowerBlue>_Dernière mise à jour : le 15/03/24_


## <span style="color:LightSalmon">11 : Puissances
???+ info "Contenu"
    - Savoir si un triangle est rectangle connaissant les longueurs de ses côtés.

??? tip "Fichiers à télécharger <span style=color:red> :material-file-pdf-box: <span style=color:blue>:material-text-box-edit-outline: "
    === "Leçon"
        [Leçon : 11 - Puissances](a_telecharger/4_C11/4_C11_Puissances.pdf){:target="_blank"}   
    === "Exercices"
        [Exercices](a_telecharger/4_C11/4_C11_Exercices_énoncé.pdf){:target="_blank"}   
    === "Exercices + correction"
        Non disponible  
    === "LaTeX"
        - [Leçon](a_telecharger/4_C11/4_C11_Puissances.tex) 
        - [Exercices](a_telecharger/4_C11/4_C11_Exercices.tex) 
    <span style=color:CornflowerBlue>_Dernière mise à jour : le 18/04/24_



## <span style="color:LightSalmon">12 : Pyramides et cônes
???+ info "Contenu"
    - Savoir si un triangle est rectangle connaissant les longueurs de ses côtés.

??? tip "Fichiers à télécharger <span style=color:red> :material-file-pdf-box: <span style=color:blue>:material-text-box-edit-outline: "
    === "Leçon"
        [Leçon : 12 - Pyramides et cônes](a_telecharger/4_C12/4_C12_Pyramides_cônes.pdf){:target="_blank"}   
    === "Exercices"
        [Exercices](a_telecharger/4_C12/4_C12_Exercices_énoncé.pdf){:target="_blank"}   
    === "Exercices + correction"
        Non disponible  
    === "LaTeX"
        - [Leçon](a_telecharger/4_C12/4_C12_Pyramides_cônes.tex) 
        - [Exercices](a_telecharger/4_C12/4_C12_Exercices.tex) 
    <span style=color:CornflowerBlue>_Dernière mise à jour : le 18/04/24_


## <span style="color:LightSalmon">13 : Proportionnalité
???+ info "Contenu"
    - Produit en croix.
    - Quatrième proportionnelle.
    - Lien entre proportionnalité et représentation graphique.
    - Pourcentage, augmentation et réduction.

??? tip "Fichiers à télécharger <span style=color:red> :material-file-pdf-box: <span style=color:blue>:material-text-box-edit-outline: "
    === "Leçon"
        [Leçon : 13 - Proportionnalité](a_telecharger/4_C13/4_C13_Proportionnalité.pdf){:target="_blank"}   
    === "Exercices"
        Non disponible
    === "Exercices + correction"
        Non disponible  
    === "LaTeX"
        - [Leçon](a_telecharger/4_C13/4_C13_Proportionnalité.tex) 
        - Non disponible
    <span style=color:CornflowerBlue>_Dernière mise à jour : le 19/04/24_