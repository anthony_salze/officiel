\documentclass[dvipsnames, a4paper, 12pt]{report}

\usepackage{lmodern} %police standard latin moderne
\usepackage[frenchb]{babel} %langue française
\usepackage[utf8]{inputenc} %pour l'utf-8
\usepackage[T1]{fontenc} %pour les césures et accentuations
\usepackage{geometry} %pour changer les marges d'un document
\usepackage{graphicx}
\usepackage{fancybox}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{fontawesome5}
\usepackage{ProfCollege}
\usepackage{luacode}
\usepackage{xlop}
\usepackage{comment}

\usepackage[explicit]{titlesec} %Permet de modifier les différentes parties, chapitres, sous-parties,...

\usepackage{colortbl} %permet de mettre des cellules de tableau en couleur

\usepackage{multicol}

\usepackage{cellspace} %pour définir les espaces en haut et en bas des cellules d'un tableau
\setlength{\cellspacebottomlimit}{5pt}
\setlength{\cellspacetoplimit}{5pt}
\setlength{\parindent}{0pt}

\usepackage{xcolor} %permet de mettre du texte en couleur

\usepackage{array} %gestion des tableaux
\usepackage{tcolorbox} %créer des boîtes de couleurs

\usepackage{hyperref} %permet de gérer le sommaire et la page de garde et les options du PDF

\geometry{top=2cm, bottom=2cm, right=2cm, left=2cm}   
\titlespacing{\chapter}{0pt}{0pt}{5pt}

\setcounter{chapter}{3}

\usepackage{lastpage}
\usepackage{fancyhdr} %définition des en-têtes et des bas de page
\fancyhf{}
\fancypagestyle{main}{
	\renewcommand\headrulewidth{5pt}
	\fancyhead[L]{4\ieme\, - Chapitre \arabic{chapter} - Calcul littéral}
	\fancyhead[R]{Collège}
	\fancyfoot[C]{\thepage/\pageref{LastPage}}
	\fancyfoot[R]{\tiny{Anthony Salze}}
	}
\pagestyle{main}
%gestion des entêtes
\makeatletter\@addtoreset{section}{chapter}\makeatother % RAZ des numéros de section après un chapitre
\renewcommand{\thepart}{\Alph{part} -} % Pour mettre des I, II, etc. aux parties
\renewcommand{\thesection}{\color{Green} \Roman{section} -}% Idem pour les sections et avoir le numéro de chapitre
\renewcommand{\thesubsection}{\arabic{subsection} -}

\begin{document}
%format du titre des chapitres placé après Sommaire sinon cela bugue
\titleformat{\chapter}[frame] {\normalfont \bfseries \Large}{\textcolor{red} {\chaptertitlename\ \thechapter\,-\,#1}}{0pt}{\Huge}

\begin{luacode}
	d=1
	p=1
	t=1
	m=1
\end{luacode}

\setcounter{chapter}{2}

\chapter{Calcul littéral - Simple distributivité}

{\color{blue}\underline{\textbf{Prérequis}}
\begin{itemize}
	\item Introduction au calcul littéral faite en 5\ieme ;
	\item Vérifier si une égalité est vraie pour une valeur donnée.
\end{itemize}~~

\underline{\textbf{Objectifs}}
\begin{itemize}
	\item Simple distributivité ;
	\item Factorisation ;
\end{itemize}}~~

\textbf{\underline{Rappels}} : QCM (une seule réponse possible)\\\footnotesize{}
\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom=, AlphT, Largeur=3cm]{%
Le résultat de $x-3$ lorsque $x=8$ est \ldots  & $8$ & $-3$ & $5$ &3,%
L'écriture la plus simple de l'expression littérale $x \times x-5 \times \left(2-x \right)$ est \ldots & $x^2-5\left(2-x \right)$ & $2x-5\times \left(2-x\right)$ & $x^2-5\times \left(2-x\right)$ &1,%
Le résultat de $5y-3z$ pour $y=9$ et $z=-5$ est \ldots & $-52$ & $60$ & $30$ & 2,%
L'égalité $9x-5=7x+3$ est vérifiée pour \ldots & $x=3$ & $x=4$ & $x=5$ & 2,% 
On souhaite compléter la feuille de calcul ci-dessous pour obtenir les valeurs prises par l'expression littérale $9x-2$ dans la colonne B. Quelle formule $\text{,}$ qu'on étirera vers le bas$\text{,}$ peut-on taper dans la cellule B2 ? & =9*A2-2 & =9*1-2 & 9*A2-2 &1 %
}
\begin{center}
\begin{Tableur}[Bandeau=false, LargeurUn=40, Largeur=40, Colonnes=2, Cellule=B2, Colonne=2, Ligne=2]
	Valeur choisie pour $x$ & Résultat de $9x-2$ \\
	1 & \\
	1,2 & \\
	1,4 & \\
	1,6 & \\
	1,8 & \\
	2 & \\
	2,2 & \\
\end{Tableur}
\end{center}


\color{Green}
\section{Rappels : simplification d'écriture}
\color{black}
\begin{tcolorbox}[colframe=Black!20, colback=black!0, boxrule=2pt, arc=6pt, title={\textbf{Règle \arabic{chapter}.\directlua{tex.print(m)}}}, coltitle=black]
En règle générale, on n'écrit pas le signe \guillemetleft \,$\times$ \guillemetright\, lorsqu'il est suivi d'une lettre ou d'une parenthèse.
\end{tcolorbox}
\begin{luacode}
	m=m+1
\end{luacode}

\textbf{\underline{Exemples}}
\begin{itemize}[label=\textbullet] \vspace{-0.3cm}
	\begin{multicols}{2}
		\item $5\times a=5a$
		\item $3\times (x-2)=3(x-2)$
	\end{multicols}
\end{itemize}

\begin{tcolorbox}[colframe=Black!20, colback=black!0, boxrule=2pt, arc=6pt, title={\textbf{Règle \arabic{chapter}.\directlua{tex.print(m)}}}, coltitle=black]
$x$ et $y$ désignent des nombres relatifs.
\begin{itemize}[label=\textbullet] \vspace{-0.3cm}
	\begin{multicols}{2}
 		\item $(-x)\times y=x\times (-y)=-xy$
		\item $(-x)\times (-y)=x\times y=xy$
		\item $x\times x=x^2$
		\item $x\times x\times x=x^3$ et ainsi de suite.
	\end{multicols}
\end{itemize}
\end{tcolorbox}
\begin{luacode}
	m=m+1
\end{luacode}

\textbf{\underline{Exemples}}
\begin{itemize}[label=\textbullet] \vspace{-0.3cm}
	\begin{multicols}{3}
		\item $(-1)\times x=-1x=-x$
		\item $(-x)\times (-4)=4x$
		\item $3x \times (-4x)=-12x^2$
	\end{multicols}
\end{itemize}

\color{Green}
\section{Réduire et développer (simple distributivité)}
\color{black}
\begin{tcolorbox}[colframe=Green!20, colback=Green!0, boxrule=2pt, arc=6pt, title={\textbf{Définition \arabic{chapter}.\directlua{tex.print(d)}}}, coltitle=black]
Réduire une expression littérale, c'est regrouper les puissances de \guillemetleft \,$x$ \guillemetright\, ensemble.
\end{tcolorbox}
\begin{luacode}
	d=d+1
\end{luacode}

\textbf{\underline{Exemple}} \vspace{-0.3cm}
\begin{flalign*}
	A&=\color{red}25x\color{blue}+3x^2\color{Green}-6x^3\color{black}+25\color{red}-15x\color{black}+8\color{blue}-9x^2\color{Green}+25x^3&\\
	A&=\color{Green}-6x^3+25x^3\color{blue}+3x^2-9x^2\color{red}+25x-15x\color{black}+25+8\\
	A&=\color{Green}19x^3\color{blue}-6x^2\color{red}+10x\color{black}+33
\end{flalign*}


\begin{tcolorbox}[colframe=Green!20, colback=Green!0, boxrule=2pt, arc=6pt, title={\textbf{Définition \arabic{chapter}.\directlua{tex.print(d)}}}, coltitle=black]
	Développer, c'est transformer un produit en une somme (ou une différence).
\end{tcolorbox}
\begin{luacode}
	d=d+1
\end{luacode}

\begin{tcolorbox}[colframe=blue!20, colback=blue!0, boxrule=2pt, arc=6pt, title={\textbf{Propriété \arabic{chapter}.\directlua{tex.print(p)}}}, coltitle=black]
	$b$, $c$ et $d$ désignent des nombres relatifs.
\begin{itemize}[label=\textbullet]
	\begin{multicols}{2}
		\item $\Distri[Cours, Fleches, CouleurFH=Fuchsia]{0}{1}{1}{1}$
		\item $\Distri[Cours, Fleches, CouleurFH=Fuchsia]{0}{1}{1}{-1}$
	\end{multicols}
\end{itemize}
On dit que $b$ est le facteur commun aux termes $b\times c$ et $b \times d$  (ou $b \times (-d)$).
\end{tcolorbox}
\begin{luacode}
	p=p+1
\end{luacode}

\textbf{\underline{Exemples}}\\
Développer les expressions suivantes : $A = \Distri{0}{25}{1}{2}$ et $B=\Distri[Echange=2]{0}{3}{-3}{7}$.\\
\begin{minipage}{0.5\textwidth}
	\begin{align*}
		A & = \Distri[Fleches, CouleurFH=Fuchsia]{0}{25}{1}{2}\\
		A & = \Distri[Etape=2]{0}{25}{1}{2}\\
		A & = \Distri[Etape=3]{0}{25}{1}{2}
	\end{align*}
\end{minipage}
\begin{minipage}{0.5\textwidth}
	\begin{align*}	
		B & = \Distri[Fleches, CouleurFH=Fuchsia,Echange=2]{0}{3}{-3}{7}\\
		B & = \Distri[Etape=2,Echange=2]{0}{3}{-3}{7}\\
		B & = -9+21x
	\end{align*}
\end{minipage}
~~\\

\begin{tcolorbox}[colframe=blue!20, colback=blue!0, boxrule=2pt, arc=6pt, title={\textbf{Propriété \arabic{chapter}.\directlua{tex.print(p)}}}, coltitle=black]
\begin{itemize}[label=\textbullet]
	\item Si le signe $+$ se trouve devant une parenthèse, on peut les retirer.
	\item Si le signe $-$ se trouve devant une parenthèse, on peut enlever les parenthèses à condition d'inverser tous les signes des nombres à l'intérieur de ces parenthèses.
\end{itemize}
\end{tcolorbox}
\begin{luacode}
	p=p+1
\end{luacode}

\textbf{\underline{Exemples}}\\
Simplifier les expressions suivantes : $A=25x+(5+19x)$ et $B=37x^2-(-7+x^2)$.\\
\begin{minipage}{0.5\textwidth}
	\begin{flalign*}
		A&=25x+(5+19x)&\\
		A&=25x+5+19x\\
		A&=44x+5
	\end{flalign*}
\end{minipage}
\begin{minipage}{0.5\textwidth}
	\begin{flalign*}
		B&=37x^2-(-7+x^2)&\\
		B&=37x^2+7-x^2 \text{\hspace{1.5cm} Les signes sont inversés.}\\
		B&=36x^2+7
	\end{flalign*}
\end{minipage}



\color{Green}
\section{Factoriser}
\color{black}

\begin{tcolorbox}[colframe=Green!20, colback=Green!0, boxrule=2pt, arc=6pt, title={\textbf{Définition \arabic{chapter}.\directlua{tex.print(d)}}}, coltitle=black]
	Factoriser, c'est transformer une somme (ou une différence) en un produit.
\end{tcolorbox}
\begin{luacode}
	d=d+1
\end{luacode}

\begin{tcolorbox}[colframe=blue!20, colback=blue!0, boxrule=2pt, arc=6pt, title={\textbf{Propriété \arabic{chapter}.\directlua{tex.print(p)}}}, coltitle=black]
$\color{red}b$, $c$ et $d$ désignent des nombres relatifs.
\begin{multicols}{2}
\begin{enumerate}[label=\textbullet]
	\item $\textcolor{red}bc+\textcolor{red}bd=\textcolor{red}b(c+d)$
	\item $\textcolor{red}bc-\textcolor{red}bd=\textcolor{red}b(c-d)$\end{enumerate}
\end{multicols}
\end{tcolorbox}
\begin{luacode}
	p=p+1
\end{luacode}

\textbf{\underline{Exemples}}\\
\textbf{Factoriser les expressions suivantes :}
\begin{enumerate}[label=\alph*{)}]
	\begin{multicols}{2}
		\item \textbf{$2x+3x^2$}
		\item \textbf{$6x-12$}
	\end{multicols}
\end{enumerate}
\begin{enumerate}[label=\alph*{)}]
	\begin{multicols}{2}
		\color{red}\item $\begin{aligned}[t] 2x+3x^2&=2 \times \ovalbox{$x$} + 3x \times \ovalbox{$x$}&\\
		&=\ovalbox{$x$}\times (2+3x)
		\end{aligned}$
	\item $\begin{aligned}[t] 6x-12&=\ovalbox{6} \times x - \ovalbox{6} \times 2&\\
	&=\ovalbox{6} \times (x-2)
		\end{aligned}$
	\end{multicols}
\end{enumerate}


\color{Green}
\section{Bilan}
\color{black}
QCM (une seule réponse possible)\\\footnotesize{}
\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom=, AlphT, Largeur=3cm]{%
Le développement de l'expression suivante $4x(2x+5)$ est \ldots  & $8x+20$ & $8x^2+5$ & $8x^2+20x$ &3,%
L'expression suivante $48x-(27+5x)$ est égale à \ldots & $43x-27$ & $43x-27$ & $53x+27$ &1,%
La factorisation de l'expression suivante $-81x+72$ est \ldots & $9(-9x+8)$ & $-9(-9x+8)$ & On ne peut pas factoriser. &1,%
La factorisation de l'expression suivante $36x-6x^2+6$ est \ldots & $6(6x-x^2+1)$ & $6x(6-6x+1)$ & On ne peut pas factoriser. &1,%
Un facteur commun (le plus grand possible) de l'expression suivante
$-4x^2+2x$ est \ldots & $x^2$ & $2$ & $2x$ &3,%
L'expression réduite de \vspace{-0.3cm} \[4a+9-5a-27+4a^2-89a^2 \text{ est \ldots} \vspace{-0.3cm}\] & $93a^2+9a+36$  & $-86a^2-18$ & $-85a^2-a-18$ &3%
}


\color{Green}
\section{Auto-évaluation}
\color{black}

\begin{tabular}{|>{\columncolor{darkgray}\color{white}}m{8cm}|>{\color{white}}m{0.6cm}|m{0.6cm}|m{0.6cm}|m{5cm}|} \hline

\rowcolor{darkgray}\centering{} Compétences & \centering{}\color{white} Oui & \centering{}\color{white}\scriptsize{}\rotatebox{45}{Moyen} & \centering{}\color{white}Non & \centering{}\color{white}Pour réviser \tabularnewline \hline \hline

\cellcolor{gray}Je connais les règles pour simplifier des écritures. & & & & \textbf{\underline{Leçon}} : Partie I\smallskip \newline  \textbf{\underline{Exercices}} :  \tabularnewline \hline \hline

\cellcolor{gray}Je sais développer une expression littérale (simple distributivité). & & & & \textbf{\underline{Leçon}} : Partie II \smallskip \newline  \textbf{\underline{Exercices}} : \tabularnewline \hline\hline

\cellcolor{gray}Je sais réduire une expression. & & & & \textbf{\underline{Leçon}} : Partie II \smallskip \newline  \textbf{\underline{Exercices}} :  \tabularnewline \hline\hline

\cellcolor{gray}Je sais factoriser une expression. & & & &\textbf{\underline{Leçon}} : Partie III \smallskip \newline  \textbf{\underline{Exercices}} :  \tabularnewline \hline
\end{tabular}

%\begin{comment}
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------- Correction QCM -----------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\newpage
\setcounter{page}{1}
\footnotesize{}
\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom=, AlphT, Largeur=3cm, Solution, Couleur=cyan!20]{%
Le résultat de $x-3$ lorsque $x=8$ est \ldots  & $8$ & $-3$ & $5$ &3,%
L'écriture la plus simple de l'expression littérale $x \times x-5 \times \left(2-x \right)$ est \ldots & $x^2-5\left(2-x \right)$ & $2x-5\times \left(2-x\right)$ & $x^2-5\times \left(2-x\right)$ &1,%
Le résultat de $5y-3z$ pour $y=9$ et $z=-5$ est \ldots & $-52$ & $60$ & $30$ & 2,%
L'égalité $9x-5=7x+3$ est vérifiée pour \ldots & $x=3$ & $x=4$ & $x=5$ & 2,% 
On souhaite compléter la feuille de calcul ci-dessous pour obtenir les valeurs prises par l'expression littérale $9x-2$ dans la colonne B. Quelle formule $\text{,}$ qu'on étirera vers le bas$\text{,}$ peut-on taper dans la cellule B2 ? & =9*A2-2 & =9*1-2 & 9*A2-2 &1 %
}
\begin{center}
\begin{Tableur}[Bandeau=false, LargeurUn=40, Largeur=40, Colonnes=2, Cellule=B2, Colonne=2, Ligne=2]
	Valeur choisie pour $x$ & Résultat de $9x-2$ \\
	1 & \\
	1,2 & \\
	1,4 & \\
	1,6 & \\
	1,8 & \\
	2 & \\
	2,2 & \\
\end{Tableur}
\end{center}

\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom=, AlphT, Largeur=3cm, Solution, Couleur=cyan!20]{%
Le développement de l'expression suivante $4x(2x+5)$ est \ldots  & $8x+20$ & $8x^2+5$ & $8x^2+20x$ &3,%
L'expression suivante $48x-(27+5x)$ est égale à \ldots & $43x-27$ & $43x-27$ & $53x+27$ &1,%
La factorisation de l'expression suivante $-81x+72$ est \ldots & $9(-9x+8)$ & $-9(-9x+8)$ & On ne peut pas factoriser. &1,%
La factorisation de l'expression suivante $36x-6x^2+6$ est \ldots & $6(6x-x^2+1)$ & $6x(6-6x+1)$ & On ne peut pas factoriser. &1,%
Un facteur commun (le plus grand possible) de l'expression suivante
$-4x^2+2x$ est \ldots & $x^2$ & $2$ & $2x$ &3,%
L'expression réduite de \vspace{-0.3cm} \[4a+9-5a-27+4a^2-89a^2 \text{ est \ldots} \vspace{-0.3cm}\] & $93a^2+9a+36$  & $-86a^2-18$ & $-85a^2-a-18$ &3%
}
\normalsize{}

%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%---------------------------------------------------------------------- Leçon à trous -----------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\newpage
\setcounter{page}{1}
%format du titre des chapitres placé après Sommaire sinon cela bugue
\titleformat{\chapter}[frame] {\normalfont \bfseries \Large}{\textcolor{red} {\chaptertitlename\ \thechapter\,-\,#1}}{0pt}{\Huge}

\begin{luacode}
	d=1
	p=1
	t=1
	m=1
\end{luacode}

\setcounter{chapter}{2}

\chapter{\pointilles[10cm]}

{\color{blue}\underline{\textbf{Prérequis}}
\begin{itemize}
	\item Introduction au \pointilles[13cm] ;
	\item Vérifier si une \pointilles[7cm] pour une \pointilles[5cm].
\end{itemize}~~

\underline{\textbf{Objectifs}}
\begin{itemize}
	\item \pointilles[7cm] ;
	\item \pointilles[5cm] ;
\end{itemize}}~~

\textbf{\underline{Rappels}} : QCM (une seule réponse possible)\\\footnotesize{}
\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom=, AlphT, Largeur=3cm]{%
Le résultat de $x-3$ lorsque $x=8$ est \ldots  & $8$ & $-3$ & $5$ &3,%
L'écriture la plus simple de l'expression littérale $x \times x-5 \times \left(2-x \right)$ est \ldots & $x^2-5\left(2-x \right)$ & $2x-5\times \left(2-x\right)$ & $x^2-5\times \left(2-x\right)$ &1,%
Le résultat de $5y-3z$ pour $y=9$ et $z=-5$ est \ldots & $-52$ & $60$ & $30$ & 2,%
L'égalité $9x-5=7x+3$ est vérifiée pour \ldots & $x=3$ & $x=4$ & $x=5$ & 2,% 
On souhaite compléter la feuille de calcul ci-dessous pour obtenir les valeurs prises par l'expression littérale $9x-2$ dans la colonne B. Quelle formule $\text{,}$ qu'on étirera vers le bas$\text{,}$ peut-on taper dans la cellule B2 ? & =9*A2-2 & =9*1-2 & 9*A2-2 &1 %
}
\begin{center}
\begin{Tableur}[Bandeau=false, LargeurUn=40, Largeur=40, Colonnes=2, Cellule=B2, Colonne=2, Ligne=2]
	Valeur choisie pour $x$ & Résultat de $9x-2$ \\
	1 & \\
	1,2 & \\
	1,4 & \\
	1,6 & \\
	1,8 & \\
	2 & \\
	2,2 & \\
\end{Tableur}
\end{center}


\color{Green}
\section{Rappels : \pointilles[10cm]}
\color{black}
\begin{tcolorbox}[colframe=Black!20, colback=black!0, boxrule=2pt, arc=6pt, title={\textbf{Règle \arabic{chapter}.\directlua{tex.print(m)}}}, coltitle=black]
On peut ne pas écrire le signe \pointilles[2cm] lorsqu'il est suivi d'une \pointilles[7cm].
\end{tcolorbox}
\begin{luacode}
	m=m+1
\end{luacode}

\textbf{\underline{Exemples}}
\begin{itemize}[label=\textbullet] \vspace{-0.3cm}
	\begin{multicols}{2}
		\item $5\times a=\pointilles[1cm]$
		\item $3\times (x-2)=\pointilles[2cm]$
	\end{multicols}
\end{itemize}

\begin{tcolorbox}[colframe=Black!20, colback=black!0, boxrule=2pt, arc=6pt, title={\textbf{Règle \arabic{chapter}.\directlua{tex.print(m)}}}, coltitle=black]
$x$ et $y$ désignent des \pointilles[7cm].
\begin{itemize}[label=\textbullet] \vspace{-0.3cm}
	\begin{multicols}{2}
 		\item $(-x)\times y=\pointilles[2cm]=\pointilles[1cm]$
		\item $(-x)\times (-y)=\pointilles[2cm]=\pointilles[1cm]$
		\item $x\times x=\pointilles[1cm]$
		\item $x\times x\times x=\pointilles[1cm]$ et ainsi de suite.
	\end{multicols}
\end{itemize}
\end{tcolorbox}
\begin{luacode}
	m=m+1
\end{luacode}

\textbf{\underline{Exemples}}
\begin{itemize}[label=\textbullet] \vspace{-0.3cm}
	\begin{multicols}{3}
		\item $(-1)\times x=-1x=\pointilles[1cm]$
		\item $(-x)\times (-4)=\pointilles[1cm]$
		\item $3x \times (-4x)=\pointilles[1cm]$
	\end{multicols}
\end{itemize}

\color{Green}
\section{\pointilles[15cm]}
\color{black}

\begin{tcolorbox}[colframe=Green!20, colback=Green!0, boxrule=2pt, arc=6pt, title={\textbf{Définition \arabic{chapter}.\directlua{tex.print(d)}}}, coltitle=black]
Réduire une expression littérale, c'est \pointilles[8cm].
\end{tcolorbox}
\begin{luacode}
	d=d+1
\end{luacode}

\textbf{\underline{Exemple}}
\begin{flalign*}
	A&=\color{red}25x\color{blue}+3x^2\color{Green}-6x^3\color{black}+25\color{red}-15x\color{black}+8\color{blue}-9x^2\color{Green}+25x^3&\\
	A&= \pointilles[7cm]\\
	A&= \pointilles[7cm]
\end{flalign*}

\begin{tcolorbox}[colframe=Green!20, colback=Green!0, boxrule=2pt, arc=6pt, title={\textbf{Définition \arabic{chapter}.\directlua{tex.print(d)}}}, coltitle=black]
	Développer, c'est \pointilles[13cm].
\end{tcolorbox}
\begin{luacode}
	d=d+1
\end{luacode}

\begin{tcolorbox}[colframe=blue!20, colback=blue!0, boxrule=2pt, arc=6pt, title={\textbf{Propriété \arabic{chapter}.\directlua{tex.print(p)}}}, coltitle=black]
	$b$, $c$ et $d$ désignent des \pointilles[7cm]
\begin{itemize}[label=\textbullet]
	\begin{multicols}{2}
		\item \pointilles[6.5cm]
		\item \pointilles[6.5cm]
	\end{multicols}
\end{itemize}
On dit que $b$ est le \pointilles[7cm] aux termes \pointilles[10cm].
\end{tcolorbox}
\begin{luacode}
	p=p+1
\end{luacode}

\textbf{\underline{Exemples}}\\
Développer les expressions suivantes : $A = \Distri{0}{25}{1}{2}$ et $B=\Distri[Echange=2]{0}{3}{-3}{7}$.\\
\begin{minipage}{0.5\textwidth}
	\begin{align*}
		A & = \Distri{0}{25}{1}{2}\\
		A & = \pointilles[5cm]\\
		A & = \pointilles[5cm]
	\end{align*}
\end{minipage}
\begin{minipage}{0.5\textwidth}
	\begin{align*}	
		B & = \Distri[Echange=2]{0}{3}{-3}{7}\\
		B & = \pointilles[5cm]\\
		B & = \pointilles[5cm]
	\end{align*}
\end{minipage}
~~\\

\begin{tcolorbox}[colframe=blue!20, colback=blue!0, boxrule=2pt, arc=6pt, title={\textbf{Propriété \arabic{chapter}.\directlua{tex.print(p)}}}, coltitle=black]
\begin{itemize}[label=\textbullet]
	\item Si le signe $+$ \pointilles[13cm]
	\item Si le signe $-$ \pointilles[13cm]\\
	\pointilles[15cm]
\end{itemize}
\end{tcolorbox}
\begin{luacode}
	p=p+1
\end{luacode}

\textbf{\underline{Exemples}}\\
Simplifiier les expressions suivantes : $A=25x+(5+19x)$ et $B=37x^2-(-7+x^2)$.\\
\begin{minipage}{0.5\textwidth}
	\begin{flalign*}
		A&=25x+(5+19x)&\\
		A&=\pointilles[3cm]\\
		A&=\pointilles[3cm]
	\end{flalign*}
\end{minipage}
\begin{minipage}{0.5\textwidth}
	\begin{flalign*}
		B&=37x^2-(-7+x^2)&\\
		B&=\pointilles[3cm] \text{\hspace{1.5cm}\pointilles[4cm]}\\
		B&=\pointilles[3cm]
	\end{flalign*}
\end{minipage}



\color{Green}
\section{\pointilles[5cm]}
\color{black}\vspace{-0.5cm}

\begin{tcolorbox}[colframe=Green!20, colback=Green!0, boxrule=2pt, arc=6pt, title={\textbf{Définition \arabic{chapter}.\directlua{tex.print(d)}}}, coltitle=black]
	Factoriser, c'est \pointilles[10cm].
\end{tcolorbox}
\begin{luacode}
	d=d+1
\end{luacode}

\begin{tcolorbox}[colframe=blue!20, colback=blue!0, boxrule=2pt, arc=6pt, title={\textbf{Propriété \arabic{chapter}.\directlua{tex.print(p)}}}, coltitle=black]
$\color{red}b$, $c$ et $d$ désignent des  \pointilles[5cm].
\begin{multicols}{2}
\begin{enumerate}[label=\textbullet]
	\item $\textcolor{red}bc+\textcolor{red}bd= \pointilles[3cm]$
	\item $\textcolor{red}bc-\textcolor{red}bd= \pointilles[3cm]$\end{enumerate}
\end{multicols}
\end{tcolorbox}
\begin{luacode}
	p=p+1
\end{luacode}

\textbf{\underline{Exemples}}\\
\textbf{Factoriser les expressions suivantes :}
\begin{enumerate}[label=\alph*{)}]
	\begin{multicols}{2}
		\item \textbf{$2x+3x^2$}
		\item \textbf{$6x-12$}
	\end{multicols}
\end{enumerate}
\begin{enumerate}[label=\alph*{)}]
	\begin{multicols}{2}
		\color{red}\item $\begin{aligned}[t] 2x+3x^2&= \pointilles[3cm]&\\
		&= \pointilles[3cm]
		\end{aligned}$
	\item $\begin{aligned}[t] 6x-12&= \pointilles[3cm]&\\
	&= \pointilles[3cm]
		\end{aligned}$
	\end{multicols}
\end{enumerate}
\vspace{-0.5cm}

\color{Green}
\section{Bilan}
\color{black}\vspace{-0.5cm}
QCM (une seule réponse possible)\\\footnotesize{}
\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom=, AlphT, Largeur=3cm]{%
Le développement de l'expression suivante $4x(2x+5)$ est \ldots  & $8x+20$ & $8x^2+5$ & $8x^2+20x$ &3,%
L'expression suivante $48x-(27+5x)$ est égale à \ldots & $43x-27$ & $43x-27$ & $53x+27$ &1,%
La factorisation de l'expression suivante $-81x+72$ est \ldots & $9(-9x+8)$ & $-9(-9x+8)$ & On ne peut pas factoriser. &1,%
La factorisation de l'expression suivante $36x-6x^2+6$ est \ldots & $6(6x-x^2+1)$ & $6x(6-6x+1)$ & On ne peut pas factoriser. &1,%
Un facteur commun (le plus grand possible) de l'expression suivante
$-4x^2+2x$ est \ldots & $x^2$ & $2$ & $2x$ &3,%
L'expression réduite de \vspace{-0.3cm} \[4a+9-5a-27+4a^2-89a^2 \text{ est \ldots} \vspace{-0.3cm}\] & $93a^2+9a+36$  & $-86a^2-18$ & $-85a^2-a-18$ &3%
}
\normalsize{}
\vspace{-0.5cm}
\color{Green}
\section{Auto-évaluation}
\color{black}\vspace{-0.5cm}

\begin{tabular}{|>{\columncolor{darkgray}\color{white}}m{8cm}|>{\color{white}}m{0.6cm}|m{0.6cm}|m{0.6cm}|m{5cm}|} \hline

\rowcolor{darkgray}\centering{} Compétences & \centering{}\color{white} Oui & \centering{}\color{white}\scriptsize{}\rotatebox{45}{Moyen} & \centering{}\color{white}Non & \centering{}\color{white}Pour réviser \tabularnewline \hline \hline

\cellcolor{gray}Je connais les règles pour simplifier des écritures. & & & & \textbf{\underline{Leçon}} : Partie I\smallskip \newline  \textbf{\underline{Exercices}} :  \tabularnewline \hline \hline

\cellcolor{gray}Je sais développer une expression littérale (simple distributivité). & & & & \textbf{\underline{Leçon}} : Partie II \smallskip \newline  \textbf{\underline{Exercices}} : \tabularnewline \hline\hline

\cellcolor{gray}Je sais réduire une expression. & & & & \textbf{\underline{Leçon}} : Partie II \smallskip \newline  \textbf{\underline{Exercices}} :  \tabularnewline \hline\hline

\cellcolor{gray}Je sais factoriser une expression. & & & &\textbf{\underline{Leçon}} : Partie III \smallskip \newline  \textbf{\underline{Exercices}} :  \tabularnewline \hline 
\end{tabular}

%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------- Impression -----------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\newpage
\textbf{\underline{Rappels}} : QCM (une seule réponse possible)\\\footnotesize{}
\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom=, AlphT, Largeur=3cm]{%
Le résultat de $x-3$ lorsque $x=8$ est \ldots  & $8$ & $-3$ & $5$ &3,%
L'écriture la plus simple de l'expression littérale $x \times x-5 \times \left(2-x \right)$ est \ldots & $x^2-5\left(2-x \right)$ & $2x-5\times \left(2-x\right)$ & $x^2-5\times \left(2-x\right)$ &1,%
Le résultat de $5y-3z$ pour $y=9$ et $z=-5$ est \ldots & $-52$ & $60$ & $30$ & 2,%
L'égalité $9x-5=7x+3$ est vérifiée pour \ldots & $x=3$ & $x=4$ & $x=5$ & 2,% 
On souhaite compléter la feuille de calcul ci-dessous pour obtenir les valeurs prises par l'expression littérale $9x-2$ dans la colonne B. Quelle formule $\text{,}$ qu'on étirera vers le bas$\text{,}$ peut-on taper dans la cellule B2 ? & =9*A2-2 & =9*1-2 & 9*A2-2 &1 %
}
\begin{center}
\begin{Tableur}[Bandeau=false, LargeurUn=40, Largeur=40, Colonnes=2, Cellule=B2, Colonne=2, Ligne=2]
	Valeur choisie pour $x$ & Résultat de $9x-2$ \\
	1 & \\
	1,2 & \\
	1,4 & \\
	1,6 & \\
	1,8 & \\
	2 & \\
	2,2 & \\
\end{Tableur}
\end{center}

QCM (une seule réponse possible)\\\footnotesize{}
\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom=, AlphT, Largeur=3cm]{%
Le développement de l'expression suivante $4x(2x+5)$ est \ldots  & $8x+20$ & $8x^2+5$ & $8x^2+20x$ &3,%
L'expression suivante $48x-(27+5x)$ est égale à \ldots & $43x-27$ & $43x-27$ & $53x+27$ &1,%
La factorisation de l'expression suivante $-81x+72$ est \ldots & $9(-9x+8)$ & $-9(-9x+8)$ & On ne peut pas factoriser. &1,%
La factorisation de l'expression suivante $36x-6x^2+6$ est \ldots & $6(6x-x^2+1)$ & $6x(6-6x+1)$ & On ne peut pas factoriser. &1,%
Un facteur commun (le plus grand possible) de l'expression suivante
$-4x^2+2x$ est \ldots & $x^2$ & $2$ & $2x$ &3,%
L'expression réduite de \vspace{-0.3cm} \[4a+9-5a-27+4a^2-89a^2 \text{ est \ldots} \vspace{-0.3cm}\] & $93a^2+9a+36$  & $-86a^2-18$ & $-85a^2-a-18$ &3%
}
\normalsize{}

\newpage

\begin{tabular}{|>{\columncolor{darkgray}\color{white}}m{8cm}|>{\color{white}}m{0.6cm}|m{0.6cm}|m{0.6cm}|m{5cm}|} \hline

\rowcolor{darkgray}\centering{} Compétences & \centering{}\color{white} Oui & \centering{}\color{white}\scriptsize{}\rotatebox{45}{Moyen} & \centering{}\color{white}Non & \centering{}\color{white}Pour réviser \tabularnewline \hline \hline

\cellcolor{gray}Je connais les règles pour simplifier des écritures. & & & & \textbf{\underline{Leçon}} : Partie I\smallskip \newline  \textbf{\underline{Exercices}} :  \tabularnewline \hline \hline

\cellcolor{gray}Je sais développer une expression littérale (simple distributivité). & & & & \textbf{\underline{Leçon}} : Partie II \smallskip \newline  \textbf{\underline{Exercices}} : \tabularnewline \hline\hline

\cellcolor{gray}Je sais réduire une expression. & & & & \textbf{\underline{Leçon}} : Partie II \smallskip \newline  \textbf{\underline{Exercices}} :  \tabularnewline \hline\hline

\cellcolor{gray}Je sais factoriser une expression. & & & &\textbf{\underline{Leçon}} : Partie III \smallskip \newline  \textbf{\underline{Exercices}} :  \tabularnewline \hline 
\end{tabular}
\\
\\
\\
\begin{tabular}{|>{\columncolor{darkgray}\color{white}}m{8cm}|>{\color{white}}m{0.6cm}|m{0.6cm}|m{0.6cm}|m{5cm}|} \hline

\rowcolor{darkgray}\centering{} Compétences & \centering{}\color{white} Oui & \centering{}\color{white}\scriptsize{}\rotatebox{45}{Moyen} & \centering{}\color{white}Non & \centering{}\color{white}Pour réviser \tabularnewline \hline \hline

\cellcolor{gray}Je connais les règles pour simplifier des écritures. & & & & \textbf{\underline{Leçon}} : Partie I\smallskip \newline  \textbf{\underline{Exercices}} :  \tabularnewline \hline \hline

\cellcolor{gray}Je sais développer une expression littérale (simple distributivité). & & & & \textbf{\underline{Leçon}} : Partie II \smallskip \newline  \textbf{\underline{Exercices}} : \tabularnewline \hline\hline

\cellcolor{gray}Je sais réduire une expression. & & & & \textbf{\underline{Leçon}} : Partie II \smallskip \newline  \textbf{\underline{Exercices}} :  \tabularnewline \hline\hline

\cellcolor{gray}Je sais factoriser une expression. & & & &\textbf{\underline{Leçon}} : Partie III \smallskip \newline  \textbf{\underline{Exercices}} :  \tabularnewline \hline 
\end{tabular}
\\
\\
\\
\begin{tabular}{|>{\columncolor{darkgray}\color{white}}m{8cm}|>{\color{white}}m{0.6cm}|m{0.6cm}|m{0.6cm}|m{5cm}|} \hline

\rowcolor{darkgray}\centering{} Compétences & \centering{}\color{white} Oui & \centering{}\color{white}\scriptsize{}\rotatebox{45}{Moyen} & \centering{}\color{white}Non & \centering{}\color{white}Pour réviser \tabularnewline \hline \hline

\cellcolor{gray}Je connais les règles pour simplifier des écritures. & & & & \textbf{\underline{Leçon}} : Partie I\smallskip \newline  \textbf{\underline{Exercices}} :  \tabularnewline \hline \hline

\cellcolor{gray}Je sais développer une expression littérale (simple distributivité). & & & & \textbf{\underline{Leçon}} : Partie II \smallskip \newline  \textbf{\underline{Exercices}} : \tabularnewline \hline\hline

\cellcolor{gray}Je sais réduire une expression. & & & & \textbf{\underline{Leçon}} : Partie II \smallskip \newline  \textbf{\underline{Exercices}} :  \tabularnewline \hline\hline

\cellcolor{gray}Je sais factoriser une expression. & & & &\textbf{\underline{Leçon}} : Partie III \smallskip \newline  \textbf{\underline{Exercices}} :  \tabularnewline \hline 
\end{tabular}
\\
\\
\\
\begin{tabular}{|>{\columncolor{darkgray}\color{white}}m{8cm}|>{\color{white}}m{0.6cm}|m{0.6cm}|m{0.6cm}|m{5cm}|} \hline

\rowcolor{darkgray}\centering{} Compétences & \centering{}\color{white} Oui & \centering{}\color{white}\scriptsize{}\rotatebox{45}{Moyen} & \centering{}\color{white}Non & \centering{}\color{white}Pour réviser \tabularnewline \hline \hline

\cellcolor{gray}Je connais les règles pour simplifier des écritures. & & & & \textbf{\underline{Leçon}} : Partie I\smallskip \newline  \textbf{\underline{Exercices}} :  \tabularnewline \hline \hline

\cellcolor{gray}Je sais développer une expression littérale (simple distributivité). & & & & \textbf{\underline{Leçon}} : Partie II \smallskip \newline  \textbf{\underline{Exercices}} : \tabularnewline \hline\hline

\cellcolor{gray}Je sais réduire une expression. & & & & \textbf{\underline{Leçon}} : Partie II \smallskip \newline  \textbf{\underline{Exercices}} :  \tabularnewline \hline\hline

\cellcolor{gray}Je sais factoriser une expression. & & & &\textbf{\underline{Leçon}} : Partie III \smallskip \newline  \textbf{\underline{Exercices}} :  \tabularnewline \hline 
\end{tabular}



%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%--------------------------------------------------------------- Histoire des mathématiques -------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\newpage
\begin{tcolorbox}[colframe=black, colback=blue!10, boxrule=2pt, arc=6pt]
	\centering{}\LARGE{\textbf{Histoire des mathématiques \faBook}}
\end{tcolorbox}
\begin{tcolorbox}[colframe=black!30, colback=blue!0, boxrule=2pt, arc=6pt, title=Utilisation des symboles en mathématiques, coltitle=black]
	\begin{minipage}{0.3\textwidth}
	\includegraphics[width=4.5cm]{Figure2.jpg}
\end{minipage}
\begin{minipage}{0.7\textwidth}
Dans le livre d'algèbre \textbf{d'Al-Khwarizmi, mathématicien perse ($\approx 780$ - $\approx 850$)}, aucun symbole n'apparaît. Les équations et les calculs étaient écrits sous forme de phrases.\\\\
C'est au \textsc{xv}\ieme et \textsc{xvi}\ieme ~siècles que les premiers symboles commencent à apparaître et plus particulièrement à la Renaissance. Chaque mathématicien possédait ses propres symboles. C'est au siècle suivant que l'uniformisation se fait.\\
\\
Les premiers symboles ont été créés pour représenter les opérations arithmétiques.
\end{minipage}\\\\
\begin{minipage}{0.7\textwidth}
Par exemple, ce sont les \textbf{Allemands} qui utilisèrent en premier les symboles actuels $+$ et $-$ vers la fin du \textsc{xv}\ieme ~siècle.\\\\
\textbf{Jérôme Cardan, mathématicien italien (1501 - 1576)}, utilise la lettre $p$ pour l'addition et la lettre $m$ pour la soustraction dans son \oe{}uvre \textit{Ars Magna} (1545). \\
\\
Des symboles sont également utilisés pour noter les inconnues et les puissances. Par exemple, en 1577, \textbf{Guillaume Gosselin, mathématicien français ( \ldots - $\approx 1590$)}, utilise l'abréviation de certains mots : $L$ (longueur) pour désigner une inconnue et $Q$ (quarré) pour désigner son carré.
\end{minipage}
\begin{minipage}{0.25\textwidth}
	\includegraphics[width=4.5cm]{Figure4.jpg}
\end{minipage}\\\\\\
\begin{minipage}{0.30\textwidth}
	\includegraphics[width=4.5cm]{Figure3.jpg}
\end{minipage}
\begin{minipage}{0.70\textwidth}
\textbf{Raphaël Bombelli, mathématicien italien (1526 - 1572)} et \textbf{Albert Girard, mathématicien barrois francophone (1595 - 1632)}, proposent des symboles numériques pour noter les inconnues et les puissances.\\
\\
Ce n'est que vers 1600 que, \textbf{François Viète, mathématicien français (1540 - 1603)} introduit le symbole littéral pour désigner des inconnues mais aussi des paramètres.\\
\\
C'est en 1631 que le symbole $=$ est apparu dans les écrits de \textbf{Thomas Harriot, mathématicien anglais (1560 - 1621)}.\\
\end{minipage}
\end{tcolorbox}


\textbf{\underline{Sources}}
\begin{itemize}[label=\textbullet]
	\item Hauchecorne, B. et Suratteau, D. (2008), \textit{Des Mathématiciens de A à Z} ($3^e$ édition), Ellipses
	\item Dahan-Dalmedico, A. et Peiffer, J. (1986), \textit{Une histoire des mathématiques : Routes et dédales}, Points
\end{itemize}





%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%-------------------------------------------------------------------------- Fiche de route ------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\newpage
\begin{tcolorbox}[colframe=black, colback=Khaki!50, boxrule=2pt, arc=6pt]
	\centering{}\LARGE{\textbf{Fiche de route}}
\end{tcolorbox}
\begin{itemize}[label=\textbullet]
	\item \color{Tomato} \textbf{Introduction} \color{black} : Titre, prérequis, objectifs, rappels
	\item \color{RoyalBlue}\underline{Leçon} \color{black} : Partie I : 
	\item \color{ForestGreen}\underline{Exercices}  \color{black} : 
	\item \color{RoyalBlue}\underline{Leçon}  \color{black} : Parties IV et V : Bilan + Auto-évaluation
	\item \color{ForestGreen}\underline{Exercices bilan}  \color{black} : 
	\item \color{Coral} \underline{Histoire des mathématiques}
\end{itemize}

\setcounter{page}{4}
%\end{comment}
\end{document}
