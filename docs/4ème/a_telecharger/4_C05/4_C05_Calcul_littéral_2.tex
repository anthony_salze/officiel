\documentclass[dvipsnames, a4paper, 12pt]{report}

\usepackage{lmodern} %police standard latin moderne
\usepackage[frenchb]{babel} %langue française
\usepackage[utf8]{inputenc} %pour l'utf-8
\usepackage[T1]{fontenc} %pour les césures et accentuations
\usepackage{geometry} %pour changer les marges d'un document
\usepackage{graphicx}
\usepackage{fancybox}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{fontawesome5}
\usepackage{ProfCollege}
\usepackage{luacode}
\usepackage{xlop}
\usepackage{comment}

\usepackage[explicit]{titlesec} %Permet de modifier les différentes parties, chapitres, sous-parties,...

\usepackage{colortbl} %permet de mettre des cellules de tableau en couleur

\usepackage{multicol}

\usepackage{cellspace} %pour définir les espaces en haut et en bas des cellules d'un tableau
\setlength{\cellspacebottomlimit}{5pt}
\setlength{\cellspacetoplimit}{5pt}
\setlength{\parindent}{0pt}

\usepackage{xcolor} %permet de mettre du texte en couleur

\usepackage{array} %gestion des tableaux
\usepackage{tcolorbox} %créer des boîtes de couleurs

\usepackage{hyperref} %permet de gérer le sommaire et la page de garde et les options du PDF

\geometry{top=2cm, bottom=2cm, right=2cm, left=2cm}   
\titlespacing{\chapter}{0pt}{0pt}{5pt}

\setcounter{chapter}{5}

\usepackage{lastpage}
\usepackage{fancyhdr} %définition des en-têtes et des bas de page
\fancyhf{}
\fancypagestyle{main}{
	\renewcommand\headrulewidth{5pt}
	\fancyhead[L]{4\ieme\, - Chapitre \arabic{chapter} - Calcul littéral 2}
	\fancyhead[R]{Collège}
	\fancyfoot[C]{\thepage/\pageref{LastPage}}
	\fancyfoot[R]{\tiny{}Anthony Salze}
	}
\pagestyle{main}
%gestion des entêtes
\makeatletter\@addtoreset{section}{chapter}\makeatother % RAZ des numéros de section après un chapitre
\renewcommand{\thepart}{\Alph{part} -} % Pour mettre des I, II, etc. aux parties
\renewcommand{\thesection}{\color{Green} \Roman{section} -}% Idem pour les sections et avoir le numéro de chapitre
\renewcommand{\thesubsection}{\arabic{subsection} -}

\begin{document}
%format du titre des chapitres placé après Sommaire sinon cela bugue
\titleformat{\chapter}[frame] {\normalfont \bfseries \Large}{\textcolor{red} {\chaptertitlename\ \thechapter\,-\,#1}}{0pt}{\Huge}

\begin{luacode}
	d=1
	p=1
	t=1
	m=1
\end{luacode}

\setcounter{chapter}{4}

\chapter{Calcul littéral - Double distributivité}

{\color{blue}\underline{\textbf{Prérequis}}
\begin{itemize}
	\item Chapitre $3$ : Calcul littéral - Distributivité simple
\end{itemize}~~

\underline{\textbf{Objectifs}}
\begin{itemize}
	\item Utiliser la double distributivité pour développer des produits ;
	\item Découvrir les identités remarquables ;
	\item Montrer que deux expressions littérales sont égales.
\end{itemize}}~~

\textbf{\underline{Rappels}} : QCM (une seule réponse possible)\\\footnotesize{}
\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom=, AlphT, Largeur=3cm]{%
Pour $x=5 \text{,}$ que vaut l'expression littérale $4x^2-3x+7$\,? & $122$ & $12$ & $92$ &3,%
$3(x+7)= \ldots$  & $3x+7$ & $3x+21$ & $3x+10$ & 2,%
$-4x(2x-5)=\ldots$ & $-8x^2+20x$ & $-8x^2-20x$ & $-8x^2+45x$ & 1,%
Un facteur commun de $-8x^2+4x+2$ est \ldots & $2$ & $4$ & $8$ & 1,%
$x^2-3x+2x^2-7x+5-12x^2= \ldots$ & $15x^2-10x+5$ & $-9x^2+10x+5$ & $-9x^2-10x+5$ &3%
}



\color{Green}
\section{Double distributivité}
\color{black}
\color{blue}
\subsection{Cas général}
\color{black}
\begin{tcolorbox}[colframe=blue!20, colback=blue!0, boxrule=2pt, arc=6pt, title={\textbf{Propriété \arabic{chapter}.\directlua{tex.print(p)}}}, coltitle=black]
Soient $a$, $b$, $c$ et $d$ quatre nombres relatifs quelconques :\\
\begin{center}
	\Distri[Cours,Fleches]{2}{3}{4}{5}$=ac+ad+bc+bd$
\end{center}
\end{tcolorbox}
\begin{luacode}
	p=p+1
\end{luacode}

\begin{tcolorbox}[colframe=VioletRed!20, colback=MediumPurple!0, boxrule=2pt, arc=6pt, title={\textbf{Démonstration \arabic{chapter}.\directlua{tex.print(m)}}}, coltitle=black]
En utilisant la simple distributivité :
\begin{flalign*}
	(a+b)(c+d)&=a(c+d) + b(c+d)&\\
	&=ac+ad+bc+bd
\end{flalign*}
\end{tcolorbox}
\begin{luacode}
	m=m+1
\end{luacode}

\textbf{\underline{Exemples}}\\
Développer et réduire les expressions suivantes :
\begin{multicols}{2}
$A = \Distri[Echange=3]{3}{4}{5}{2}$\\
$B = \Distri[Echange=2]{-5}{2}{-4}{3}$\\
$C = \Distri[Etape=1]{7}{2}{5}{-2}+\Distri[Etape=1]{2}{-3}{-9}{-7}$
\end{multicols}

\begin{minipage}{0.5\textwidth}
\begin{flalign*}
A & = \Distri[Fleches,Echange=3]{3}{4}{5}{2}\\\\
A & = \Distri[Echange=3,Etape=2]{3}{4}{5}{2}\\
A & = \Distri[Echange=3,Etape=3]{3}{4}{5}{2}\\
A & = \Distri[Echange=3,Etape=4]{3}{4}{5}{2}
\end{flalign*}
\end{minipage}
\begin{minipage}{0.5\textwidth}
\begin{flalign*}
B & = \Distri[Fleches,Echange=2]{-5}{2}{-4}{3}\\\\
B & = \Distri[Echange=2,Etape=2]{-5}{2}{-4}{3}\\
B & = \Distri[Echange=2,Etape=3]{-5}{2}{-4}{3}\\
B & = \Distri[Echange=2,Etape=4]{-5}{2}{-4}{3}
\end{flalign*}
\end{minipage}

\begin{flalign*}
C & =\Distri[RAZ,Etape=1]{7}{2}{5}{-2}+\Distri[Etape=1]{2}{-3}{-9}{-7}&\\
C &=\Distri[Etape=2]{7}{2}{5}{-2}+\Distri[Etape=2]{2}{-3}{-9}{-7}\\
C &=\Distri[Etape=3]{7}{2}{5}{-2}+\Distri[Etape=3]{2}{-3}{-9}{-7}\\
C &=\Distri[Somme,Etape=4]{7}{2}{5}{-2}+\Distri[Somme,Etape=4]{2}{-3}{-9}{-7}\\
C &=\Resultat
\end{flalign*}


\color{blue}
\subsection{Cas particuliers : identités remarquables}
\color{black}

\begin{tcolorbox}[colframe=blue!20, colback=blue!0, boxrule=2pt, arc=6pt, title={\textbf{Propriétés \arabic{chapter}.\directlua{tex.print(p)}}}, coltitle=black]
Soient $a$, $b$ deux nombres relatifs quelconques :
\begin{center}
	\begin{itemize}[label=\textbullet]
		\item $(a+b)^2=a^2+2ab+b^2$
		\item $(a-b)^2=a^2-2ab+b^2$
		\item $(a+b)(a-b)=a^2-b^2$
	\end{itemize}
\end{center}
\end{tcolorbox}
\begin{luacode}
	p=p+1
\end{luacode}

\begin{tcolorbox}[colframe=VioletRed!20, colback=MediumPurple!0, boxrule=2pt, arc=6pt, title={\textbf{Démonstrations \arabic{chapter}.\directlua{tex.print(m)}}}, coltitle=black]
\begin{flalign*}
	\left(a+b\right)^2&=(a+b)(a+b)&\\
	&=a \times a + a \times b + b \times a + b \times b\\
	&=a^2+ab+ab+b^2\\
	&=a^2+2ab+b^2
\end{flalign*}
\begin{flalign*}
	\left(a-b\right)^2&=(a-b)(a-b)&\\
	&=a \times a - a \times b -b \times a -b \times (-b)\\
	&=a^2-ab-ab+b^2\\
	&=a^2-2ab+b^2
\end{flalign*}
\begin{flalign*}
	(a+b)(a-b)&=a \times a + a \times (-b) + b \times a + b \times (-b)&\\
	&=a^2-ab+ab-b^2\\
	&=a^2-b^2
\end{flalign*}
\end{tcolorbox}
\begin{luacode}
	m=m+1
\end{luacode}

\textbf{\underline{Exemples}}\\
Développer et réduire les expressions suivantes en utilisant les identités remarquables.
\begin{multicols}{3}
	$E=\Distri[Remarquable]{3}{5}{}{}$\\
	$F=\Distri[Remarquable]{7}{-2}{}{}$\\
	$G=\Distri[Remarquable]{6}{-9}{6}{9}$
\end{multicols}
\begin{minipage}{0.33\textwidth}
	\begin{flalign*}
		E&=\Distri[Remarquable]{3}{5}{}{}&\\
		E&=\Distri[Remarquable, Etape=2]{3}{5}{}{}\\
		E&=\Distri[Remarquable, Etape=3]{3}{5}{}{}
	\end{flalign*}
\end{minipage}
\begin{minipage}{0.33\textwidth}
	\begin{flalign*}
		F&=\Distri[Remarquable]{7}{-2}{}{}&\\
		F&=\Distri[Remarquable, Etape=2]{7}{-2}{}{}\\
		F&=\Distri[Remarquable, Etape=3]{7}{-2}{}{}
	\end{flalign*}
\end{minipage}
\begin{minipage}{0.33\textwidth}
	\begin{flalign*}
		G&=\Distri[Remarquable]{6}{-9}{6}{9}&\\
		G&=\Distri[Remarquable, Etape=2]{6}{9}{6}{-9}\\
		G&=\Distri[Remarquable, Etape=3]{6}{9}{6}{-9}
	\end{flalign*}
\end{minipage}


\color{Green}
\section{Expressions littérales égales}
\color{black}
\begin{tcolorbox}[colframe=Green!20, colback=Green!0, boxrule=2pt, arc=6pt, title={\textbf{Définition \arabic{chapter}.\directlua{tex.print(d)}}}, coltitle=black]
	On dit que deux expressions littérales sont égales si elles le sont pour toutes les valeurs de $x$.
\end{tcolorbox}
\begin{luacode}
	d=d+1
\end{luacode}

\textbf{\underline{Exemple}}\\
On considère les expressions littérales $A=\Distri[Echange=2]{2}{3}{5}{-1} +10$ et $B=25 + x(7-2x)$.\\
Montrer que $A=B$.\\
\begin{minipage}{0.45\textwidth}
\begin{flalign*}
	A&=(2x+3)(5-x)+10&\\
	A&=2x \times 5 + 2x \times (-x) + 3 \times 5 + 3 \times (-x)+10\\
	A&=10x-2x^2+15-3x+10\\
	A&=-2x^2+7x+25
\end{flalign*}
\end{minipage} \hspace{0.05cm} \vrule \hspace{0.05cm}
\begin{minipage}{0.5\textwidth}
\begin{flalign*}
	B&=25+x(7-2x)&\\
	B&=25+x \times 7 + x \times (-2x)\\
	B&=25+7x-2x^2
\end{flalign*}
\end{minipage}
~~\\\\
Donc $A=B$ quelque soit la valeur de $x$.
\\
\begin{tcolorbox}[colframe=blue!20, colback=blue!0, boxrule=2pt, arc=6pt, title={\textbf{Propriété \arabic{chapter}.\directlua{tex.print(p)}}}, coltitle=black]
	Pour montrer que deux expressions littérales ne sont pas égales, il suffit de trouver un contre-exemple.
\end{tcolorbox}
\begin{luacode}
	p=p+1
\end{luacode}

\textbf{\underline{Exemple}}\\
Les expressions littérales $K=(x-1)(2x+5)$ et $L=(-x+1)(x-1)$ sont-elles égales\,?\\
\\
On calcule chacune des deux expressions lorsque $x=0$.\\
\begin{minipage}{0.45\textwidth}
	\begin{flalign*}
		K&=(0-1)(2 \times 0 +5)&\\
		K&=-1 \times 5\\
		K&=-5
	\end{flalign*}
\end{minipage} \vrule \hspace{0.3cm}
\begin{minipage}{0.5\textwidth}	
	\begin{flalign*}
		L&=(0+1)(0-1)&\\
		L&=1 \times (-1)\\
		L&=-1
	\end{flalign*}
\end{minipage}\\
\\\\
Pour $x=0$, $K \neq L$ donc les deux expressions littérales ne sont pas égales.




\color{Green}
\section{Bilan}
\color{black}
QCM (une seule réponse possible)\\\footnotesize{}
\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom=, AlphT, Largeur=3cm]{%
$7(3x-2)$ est égal à \ldots & $21x-2$ & $21x-14$ & $10x-9$ & 2,%
$5x+2x$ est égal à \ldots & $7x$ & $7x^2$ & $14x$ &1,%
$3x+18$ est égal à \ldots & $3(x+6)$ & $21x$ & $3(x+18)$ &1,%
$(x+3)(x-2)$ est égal à \ldots & $x^2+x-6$ & $x^2+5x+6$ & $2x-6$ &1,%
$(x+5)^2$ est égal à \ldots & $x^2+25$ & $x^2+5x+25$ & $x^2+10x+25$ &3,%
$(x-4)^2$ est égal à \ldots & $x^2-16$ & $x^2-8x+16$ & $x^2-8x-16$ &2,%
$(x-3)(x+3)$ est égal à \ldots &  $x^2-9$ & $x^2-6x+9$ & $2x-9$ &1%
}


\color{Green}
\section{Auto-évaluation}
\color{black}

\begin{tabular}{|>{\columncolor{darkgray}\color{white}}m{8cm}|>{\color{white}}m{0.6cm}|m{0.6cm}|m{0.6cm}|m{5cm}|} \hline

\rowcolor{darkgray}\centering{} Compétences & \centering{}\color{white} Oui & \centering{}\color{white}\scriptsize{}\rotatebox{45}{Moyen} & \centering{}\color{white}Non & \centering{}\color{white}Pour réviser \tabularnewline \hline \hline

\cellcolor{gray}Je suis capable d'utiliser la double distributivité pour développer une expression littérale. & & & & \textbf{\underline{Leçon}} : Partie I \smallskip \newline  \textbf{\underline{Exercices}} :  \tabularnewline \hline \hline

\cellcolor{gray}Je suis capable de montrer que deux expressions littérales sont égales. & & & & \textbf{\underline{Leçon}} : Partie II \smallskip \newline  \textbf{\underline{Exercices}} : \tabularnewline \hline
\end{tabular}

\begin{comment}
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------- Correction QCM -----------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\newpage
\setcounter{page}{1}
\footnotesize{}
\textbf{\underline{Rappels}} : QCM (une seule réponse possible)\\\footnotesize{}
\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom=, AlphT, Largeur=3cm, Solution,Couleur=cyan!20]{%
Pour $x=5 \text{,}$ que vaut l'expression littérale $4x^2-3x+7$\,? & $122$ & $12$ & $92$ &3,%
$3(x+7)= \ldots$  & $3x+7$ & $3x+21$ & $3x+10$ & 2,%
$-4x(2x-5)=\ldots$ & $-8x^2+20x$ & $-8x^2-20x$ & $-8x^2+45x$ & 1,%
Un facteur commun de $-8x^2+4x+2$ est \ldots & $2$ & $4$ & $8$ & 1,%
$x^2-3x+2x^2-7x+5-12x^2= \ldots$ & $15x^2-10x+5$ & $-9x^2+10x+5$ & $-9x^2-10x+5$ &3%
}
\\
\\

QCM (une seule réponse possible)\\\footnotesize{}
\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom=, AlphT, Largeur=3cm, Solution,Couleur=cyan!20]{%
$7(3x-2)$ est égal à \ldots & $21x-2$ & $21x-14$ & $10x-9$ & 2,%
$5x+2x$ est égal à \ldots & $7x$ & $7x^2$ & $14x$ &1,%
$3x+18$ est égal à \ldots & $3(x+6)$ & $21x$ & $3(x+18)$ &1,%
$(x+3)(x-2)$ est égal à \ldots & $x^2+x-6$ & $x^2+5x+6$ & $2x-6$ &1,%
$(x+5)^2$ est égal à \ldots & $x^2+25$ & $x^2+5x+25$ & $x^2+10x+25$ &3,%
$(x-4)^2$ est égal à \ldots & $x^2-16$ & $x^2-8x+16$ & $x^2-8x-16$ &2,%
$(x-3)(x+3)$ est égal à \ldots &  $x^2-9$ & $x^2-6x+9$ & $2x-9$ &1%
}
\normalsize{}

%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%---------------------------------------------------------------------- Leçon à trous -----------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\newpage
%format du titre des chapitres placé après Sommaire sinon cela bugue
\titleformat{\chapter}[frame] {\normalfont \bfseries \Large}{\textcolor{red} {\chaptertitlename\ \thechapter\,-\,#1}}{0pt}{\Huge}

\begin{luacode}
	d=1
	p=1
	t=1
	m=1
\end{luacode}

\setcounter{chapter}{4}

\chapter{\pointilles[12cm]}

{\color{blue}\underline{\textbf{Prérequis}}
\begin{itemize}
	\item \pointilles[16cm]
\end{itemize}~~

\underline{\textbf{Objectifs}}
\begin{itemize}
	\item \pointilles[16cm]
	\item \pointilles[16cm]
	\item \pointilles[16cm]
\end{itemize}}~~

\textbf{\underline{Rappels}} : QCM (une seule réponse possible)\\\footnotesize{}
\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom=, AlphT, Largeur=3cm]{%
Pour $x=5 \text{,}$ que vaut l'expression littérale $4x^2-3x+7$\,? & $122$ & $12$ & $92$ &3,%
$3(x+7)= \ldots$  & $3x+7$ & $3x+21$ & $3x+10$ & 2,%
$-4x(2x-5)=\ldots$ & $-8x^2+20x$ & $-8x^2-20x$ & $-8x^2+45x$ & 1,%
Un facteur commun de $-8x^2+4x+2$ est \ldots & $2$ & $4$ & $8$ & 1,%
$x^2-3x+2x^2-7x+5-12x^2= \ldots$ & $15x^2-10x+5$ & $-9x^2+10x+5$ & $-9x^2-10x+5$ &3%
}



\color{Green}
\section{Double distributivité}
\color{black}
\color{blue}
\subsection{Cas général}
\color{black}
\begin{tcolorbox}[colframe=blue!20, colback=blue!0, boxrule=2pt, arc=6pt, title={\textbf{Propriété \arabic{chapter}.\directlua{tex.print(p)}}}, coltitle=black]
Soient $a$, $b$, $c$ et $d$ quatre nombres relatifs quelconques :\\

\pointilles[16cm]

\end{tcolorbox}
\begin{luacode}
	p=p+1
\end{luacode}

\begin{tcolorbox}[colframe=VioletRed!20, colback=MediumPurple!0, boxrule=2pt, arc=6pt, title={\textbf{Démonstration \arabic{chapter}.\directlua{tex.print(m)}}}, coltitle=black]
En utilisant la simple distributivité :\\
\pointilles[16cm]\\
\pointilles[16cm]
\end{tcolorbox}
\begin{luacode}
	m=m+1
\end{luacode}

\textbf{\underline{Exemples}}\\
Développer et réduire les expressions suivantes :
\begin{multicols}{2}
$A = \Distri[Echange=3]{3}{4}{5}{2}$\\
$B = \Distri[Echange=2]{-5}{2}{-4}{3}$\\
$C = \Distri[Etape=1]{7}{2}{5}{-2}+\Distri[Etape=1]{2}{-3}{-9}{-7}$
\end{multicols}

\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]


\color{blue}
\subsection{Cas particuliers : identités remarquables}
\color{black}

\begin{tcolorbox}[colframe=blue!20, colback=blue!0, boxrule=2pt, arc=6pt, title={\textbf{Propriétés \arabic{chapter}.\directlua{tex.print(p)}}}, coltitle=black]
Soient $a$, $b$ deux nombres relatifs quelconques :\\
\pointilles[16cm]\\
\pointilles[16cm]\\
\pointilles[16cm]\\
\end{tcolorbox}
\begin{luacode}
	p=p+1
\end{luacode}

\begin{tcolorbox}[colframe=VioletRed!20, colback=MediumPurple!0, boxrule=2pt, arc=6pt, title={\textbf{Démonstrations \arabic{chapter}.\directlua{tex.print(m)}}}, coltitle=black]
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]
\end{tcolorbox}
\begin{luacode}
	m=m+1
\end{luacode}

\textbf{\underline{Exemples}}\\
Développer et réduire les expressions suivantes en utilisant les identités remarquables.
\begin{multicols}{3}
	$E=\Distri[Remarquable]{3}{5}{}{}$\\
	$F=\Distri[Remarquable]{7}{-2}{}{}$\\
	$G=\Distri[Remarquable]{6}{-9}{6}{9}$
\end{multicols}

\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\


\color{Green}
\section{Expressions littérales égales}
\color{black}
\begin{tcolorbox}[colframe=Green!20, colback=Green!0, boxrule=2pt, arc=6pt, title={\textbf{Définition \arabic{chapter}.\directlua{tex.print(d)}}}, coltitle=black]
	\pointilles[16cm]\\
	\pointilles[16cm]
\end{tcolorbox}
\begin{luacode}
	d=d+1
\end{luacode}

\textbf{\underline{Exemple}}\\
On considère les expressions littérales $A=\Distri[Echange=2]{2}{3}{5}{-1} +10$ et $B=25 + x(7-2x)$.\\
Montrer que $A=B$.\\

\newpage
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\

\begin{tcolorbox}[colframe=blue!20, colback=blue!0, boxrule=2pt, arc=6pt, title={\textbf{Propriété \arabic{chapter}.\directlua{tex.print(p)}}}, coltitle=black]
	\pointilles[16cm]\\
	\pointilles[16cm]
\end{tcolorbox}
\begin{luacode}
	p=p+1
\end{luacode}

\textbf{\underline{Exemple}}\\
Les expressions littérales $K=(x-1)(2x+5)$ et $L=(-x+1)(x-1)$ sont-elles égales\,?\\
\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\




\color{Green}
\section{Bilan}
\color{black}
QCM (une seule réponse possible)\\\footnotesize{}
\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom=, AlphT, Largeur=3cm]{%
$7(3x-2)$ est égal à \ldots & $21x-2$ & $21x-14$ & $10x-9$ & 2,%
$5x+2x$ est égal à \ldots & $7x$ & $7x^2$ & $14x$ &1,%
$3x+18$ est égal à \ldots & $3(x+6)$ & $21x$ & $3(x+18)$ &1,%
$(x+3)(x-2)$ est égal à \ldots & $x^2+x-6$ & $x^2+5x+6$ & $2x-6$ &1,%
$(x+5)^2$ est égal à \ldots & $x^2+25$ & $x^2+5x+25$ & $x^2+10x+25$ &3,%
$(x-4)^2$ est égal à \ldots & $x^2-16$ & $x^2-8x+16$ & $x^2-8x-16$ &2,%
$(x-3)(x+3)$ est égal à \ldots &  $x^2-9$ & $x^2-6x+9$ & $2x-9$ &1%
}

\color{Green}
\section{Auto-évaluation}
\color{black}

\begin{tabular}{|>{\columncolor{darkgray}\color{white}}m{8cm}|>{\color{white}}m{0.6cm}|m{0.6cm}|m{0.6cm}|m{5cm}|} \hline

\rowcolor{darkgray}\centering{} Compétences & \centering{}\color{white} Oui & \centering{}\color{white}\scriptsize{}\rotatebox{45}{Moyen} & \centering{}\color{white}Non & \centering{}\color{white}Pour réviser \tabularnewline \hline \hline

\cellcolor{gray}Je suis capable d'utiliser la double distributivité pour développer une expression littérale. & & & & \textbf{\underline{Leçon}} : Partie I \smallskip \newline  \textbf{\underline{Exercices}} :  \tabularnewline \hline \hline

\cellcolor{gray}Je suis capable de montrer que deux expressions littérales sont égales. & & & & \textbf{\underline{Leçon}} : Partie II \smallskip \newline  \textbf{\underline{Exercices}} : \tabularnewline \hline
\end{tabular}


%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------- Grande Taille -----------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\newpage
%format du titre des chapitres placé après Sommaire sinon cela bugue
\titleformat{\chapter}[frame] {\normalfont \bfseries \Large}{\textcolor{red} {\chaptertitlename\ \thechapter\,-\,#1}}{0pt}{\Huge}

\begin{luacode}
	d=1
	p=1
	t=1
	m=1
\end{luacode}

\setcounter{chapter}{4}

\chapter{\pointilles[12cm]}

\Large{}

{\color{blue}\underline{\textbf{Prérequis}}
\begin{itemize}
	\item \pointilles[16cm]
\end{itemize}~~

\underline{\textbf{Objectifs}}
\begin{itemize}
	\item \pointilles[16cm]
	\item \pointilles[16cm]
	\item \pointilles[16cm]
\end{itemize}}~~

\textbf{\underline{Rappels}} : QCM (une seule réponse possible)\\\Large{}
\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom=, AlphT, Largeur=3cm]{%
Pour $x=5 \text{,}$ que vaut l'expression littérale $4x^2-3x+7$\,? & $122$ & $12$ & $92$ &3,%
$3(x+7)= \ldots$  & $3x+7$ & $3x+21$ & $3x+10$ & 2,%
$-4x(2x-5)=\ldots$ & $-8x^2+20x$ & $-8x^2-20x$ & $-8x^2+45x$ & 1,%
Un facteur commun de $-8x^2+4x+2$ est \ldots & $2$ & $4$ & $8$ & 1,%
\large{}$x^2-3x+2x^2-7x+5-12x^2= \ldots$ & \large{}$15x^2-10x+5$ & \large{}$-9x^2+10x+5$ & \large{}$-9x^2-10x+5$ &3%
}

\Large{}
\newpage
\color{Green}
\section{\LARGE{Double distributivité}}
\color{black}
\color{blue}
\subsection{\Large{Cas général}}
\color{black}
\begin{tcolorbox}[colframe=blue!20, colback=blue!0, boxrule=2pt, arc=6pt, title={\textbf{Propriété \arabic{chapter}.\directlua{tex.print(p)}}}, coltitle=black]
Soient $a$, $b$, $c$ et $d$ quatre nombres relatifs quelconques :\\

\pointilles[16cm]

\end{tcolorbox}
\begin{luacode}
	p=p+1
\end{luacode}

\begin{tcolorbox}[colframe=VioletRed!20, colback=MediumPurple!0, boxrule=2pt, arc=6pt, title={\textbf{Démonstration \arabic{chapter}.\directlua{tex.print(m)}}}, coltitle=black]
En utilisant la simple distributivité :\\
\pointilles[16cm]\\
\pointilles[16cm]
\end{tcolorbox}
\begin{luacode}
	m=m+1
\end{luacode}

\textbf{\underline{Exemples}}\\
Développer et réduire les expressions suivantes : \vspace{-0.5cm}
\begin{multicols}{2}
$A = \Distri[Echange=3]{3}{4}{5}{2}$\\
$B = \Distri[Echange=2]{-5}{2}{-4}{3}$\\
$C = \Distri[Etape=1]{7}{2}{5}{-2}+\Distri[Etape=1]{2}{-3}{-9}{-7}$
\end{multicols}
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]

\newpage
\color{blue}
\subsection{\Large{Cas particuliers : identités remarquables}}
\color{black}

\begin{tcolorbox}[colframe=blue!20, colback=blue!0, boxrule=2pt, arc=6pt, title={\textbf{Propriétés \arabic{chapter}.\directlua{tex.print(p)}}}, coltitle=black]
Soient $a$, $b$ deux nombres relatifs quelconques :\\
\pointilles[16cm]\\
\pointilles[16cm]\\
\pointilles[16cm]\\
\end{tcolorbox}
\begin{luacode}
	p=p+1
\end{luacode}

\begin{tcolorbox}[colframe=VioletRed!20, colback=MediumPurple!0, boxrule=2pt, arc=6pt, title={\textbf{Démonstrations \arabic{chapter}.\directlua{tex.print(m)}}}, coltitle=black]
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]
\end{tcolorbox}
\begin{luacode}
	m=m+1
\end{luacode}

\textbf{\underline{Exemples}}\\
Développer et réduire les expressions suivantes en utilisant les identités remarquables.
\begin{multicols}{3}
	$E=\Distri[Remarquable]{3}{5}{}{}$\\
	$F=\Distri[Remarquable]{7}{-2}{}{}$\\
	$G=\Distri[Remarquable]{6}{-9}{6}{9}$
\end{multicols}

\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\


\color{Green}
\section{Expressions littérales égales}
\color{black}
\begin{tcolorbox}[colframe=Green!20, colback=Green!0, boxrule=2pt, arc=6pt, title={\textbf{Définition \arabic{chapter}.\directlua{tex.print(d)}}}, coltitle=black]
	\pointilles[16cm]\\
	\pointilles[16cm]
\end{tcolorbox}
\begin{luacode}
	d=d+1
\end{luacode}

\textbf{\underline{Exemple}}\\
On considère les expressions littérales $A=\Distri[Echange=2]{2}{3}{5}{-1} +10$ et $B=25 + x(7-2x)$.\\
Montrer que $A=B$.\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\

\begin{tcolorbox}[colframe=blue!20, colback=blue!0, boxrule=2pt, arc=6pt, title={\textbf{Propriété \arabic{chapter}.\directlua{tex.print(p)}}}, coltitle=black]
	\pointilles[16cm]\\
	\pointilles[16cm]
\end{tcolorbox}
\begin{luacode}
	p=p+1
\end{luacode}

\textbf{\underline{Exemple}}\\
Les expressions littérales $K=(x-1)(2x+5)$ et $L=(-x+1)(x-1)$ sont-elles égales\,?\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\




\color{Green}
\section{\LARGE{Bilan}}
\color{black}
QCM (une seule réponse possible)\\\Large{}
\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom=, AlphT, Largeur=3cm]{%
$7(3x-2)$ est égal à \ldots & $21x-2$ & $21x-14$ & $10x-9$ & 2,%
$5x+2x$ est égal à \ldots & $7x$ & $7x^2$ & $14x$ &1,%
$3x+18$ est égal à \ldots & $3(x+6)$ & $21x$ & $3(x+18)$ &1,%
$(x+3)(x-2)$ est égal à \ldots & $x^2+x-6$ & $x^2+5x+6$ & $2x-6$ &1,%
$(x+5)^2$ est égal à \ldots & $x^2+25$ & $x^2+5x+25$ & $x^2+10x+25$ &3,%
$(x-4)^2$ est égal à \ldots & $x^2-16$ & $x^2-8x+16$ & $x^2-8x-16$ &2,%
$(x-3)(x+3)$ est égal à \ldots &  $x^2-9$ & $x^2-6x+9$ & $2x-9$ &1%
}

\color{Green}
\section{\LARGE{Auto-évaluation}}
\color{black}

\begin{tabular}{|>{\columncolor{darkgray}\color{white}}m{8cm}|>{\color{white}}m{0.6cm}|m{0.6cm}|m{0.6cm}|m{5cm}|} \hline

\rowcolor{darkgray}\centering{} Compétences & \centering{}\color{white} Oui & \centering{}\color{white}\scriptsize{}\rotatebox{45}{Moyen} & \centering{}\color{white}Non & \centering{}\color{white}Pour réviser \tabularnewline \hline \hline

\cellcolor{gray}Je suis capable d'utiliser la double distributivité pour développer une expression littérale. & & & & \textbf{\underline{Leçon}} : Partie I \smallskip \newline  \textbf{\underline{Exercices}} :  \tabularnewline \hline \hline

\cellcolor{gray}Je suis capable de montrer que deux expressions littérales sont égales. & & & & \textbf{\underline{Leçon}} : Partie II \smallskip \newline  \textbf{\underline{Exercices}} : \tabularnewline \hline
\end{tabular}




%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------- Impression -----------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\newpage
\footnotesize{}
\textbf{\underline{Rappels}} : QCM (une seule réponse possible)\\\footnotesize{}
\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom=, AlphT, Largeur=3cm]{%
Pour $x=5 \text{,}$ que vaut l'expression littérale $4x^2-3x+7$\,? & $122$ & $12$ & $92$ &3,%
$3(x+7)= \ldots$  & $3x+7$ & $3x+21$ & $3x+10$ & 2,%
$-4x(2x-5)=\ldots$ & $-8x^2+20x$ & $-8x^2-20x$ & $-8x^2+45x$ & 1,%
Un facteur commun de $-8x^2+4x+2$ est \ldots & $2$ & $4$ & $8$ & 1,%
$x^2-3x+2x^2-7x+5-12x^2= \ldots$ & $15x^2-10x+5$ & $-9x^2+10x+5$ & $-9x^2-10x+5$ &3%
}
\\
\\
\\
\\
\\
QCM (une seule réponse possible)\\\footnotesize{}
\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom=, AlphT, Largeur=3cm]{%
$7(3x-2)$ est égal à \ldots & $21x-2$ & $21x-14$ & $10x-9$ & 2,%
$5x+2x$ est égal à \ldots & $7x$ & $7x^2$ & $14x$ &1,%
$3x+18$ est égal à \ldots & $3(x+6)$ & $21x$ & $3(x+18)$ &1,%
$(x+3)(x-2)$ est égal à \ldots & $x^2+x-6$ & $x^2+5x+6$ & $2x-6$ &1,%
$(x+5)^2$ est égal à \ldots & $x^2+25$ & $x^2+5x+25$ & $x^2+10x+25$ &3,%
$(x-4)^2$ est égal à \ldots & $x^2-16$ & $x^2-8x+16$ & $x^2-8x-16$ &2,%
$(x-3)(x+3)$ est égal à \ldots &  $x^2-9$ & $x^2-6x+9$ & $2x-9$ &1%
}
\normalsize{}
\\
\\
\\
\\
\\
\begin{tabular}{|>{\columncolor{darkgray}\color{white}}m{8cm}|>{\color{white}}m{0.6cm}|m{0.6cm}|m{0.6cm}|m{5cm}|} \hline

\rowcolor{darkgray}\centering{} Compétences & \centering{}\color{white} Oui & \centering{}\color{white}\scriptsize{}\rotatebox{45}{Moyen} & \centering{}\color{white}Non & \centering{}\color{white}Pour réviser \tabularnewline \hline \hline

\cellcolor{gray}Je suis capable d'utiliser la double distributivité pour développer une expression littérale. & & & & \textbf{\underline{Leçon}} : Partie I \smallskip \newline  \textbf{\underline{Exercices}} :  \tabularnewline \hline \hline

\cellcolor{gray}Je suis capable de montrer que deux expressions littérales sont égales. & & & & \textbf{\underline{Leçon}} : Partie II \smallskip \newline  \textbf{\underline{Exercices}} : \tabularnewline \hline
\end{tabular}

%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%--------------------------------------------------------------- Histoire des mathématiques -------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\newpage
\begin{tcolorbox}[colframe=black, colback=blue!10, boxrule=2pt, arc=6pt]
	\centering{}\LARGE{\textbf{Histoire des mathématiques \faBook}}
\end{tcolorbox}
\begin{tcolorbox}[colframe=black!30, colback=blue!0, boxrule=2pt, arc=6pt, title=Utilisation des symboles en mathématiques, coltitle=black]
	
\end{tcolorbox}


\textbf{\underline{Sources}}
\begin{itemize}[label=\textbullet]
	\item Hauchecorne, B. et Suratteau, D. (2008), \textit{Des Mathématiciens de A à Z} ($3^e$ édition), Ellipses
	\item Dahan-Dalmedico, A. et Peiffer, J. (1986), \textit{Une histoire des mathématiques : Routes et dédales}, Points
\end{itemize}





%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%-------------------------------------------------------------------------- Fiche de route ------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\newpage
\begin{tcolorbox}[colframe=black, colback=Khaki!50, boxrule=2pt, arc=6pt]
	\centering{}\LARGE{\textbf{Fiche de route}}
\end{tcolorbox}
\begin{itemize}[label=\textbullet]
	\item \color{Tomato} \textbf{Introduction} \color{black} : Titre, prérequis, objectifs, rappels
	\item \color{RoyalBlue}\underline{Leçon} \color{black} : Partie I : 
	\item \color{ForestGreen}\underline{Exercices}  \color{black} : 
	\item \color{RoyalBlue}\underline{Leçon}  \color{black} : Parties IV et V : Bilan + Auto-évaluation
	\item \color{ForestGreen}\underline{Exercices bilan}  \color{black} : 
	\item \color{Coral} \underline{Histoire des mathématiques}
\end{itemize}

%\setcounter{page}{4}
\end{comment}
\end{document}
