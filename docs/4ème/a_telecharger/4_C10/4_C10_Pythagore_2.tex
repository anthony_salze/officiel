\documentclass[dvipsnames, a4paper, 12pt]{report}

\usepackage{lmodern} %police standard latin moderne
\usepackage[frenchb]{babel} %langue française
\usepackage[utf8]{inputenc} %pour l'utf-8
\usepackage[T1]{fontenc} %pour les césures et accentuations
\usepackage{geometry} %pour changer les marges d'un document
\usepackage{graphicx}
\usepackage{fancybox}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{fontawesome5}
\usepackage{ProfCollege}
\usepackage{luacode}
\usepackage{xlop}

\usepackage[explicit]{titlesec} %Permet de modifier les différentes parties, chapitres, sous-parties,...

\usepackage{colortbl} %permet de mettre des cellules de tableau en couleur

\usepackage{multicol}

\usepackage{cellspace} %pour définir les espaces en haut et en bas des cellules d'un tableau
\setlength{\cellspacebottomlimit}{5pt}
\setlength{\cellspacetoplimit}{5pt}
\setlength{\parindent}{0pt}

\usepackage{xcolor} %permet de mettre du texte en couleur

\usepackage{array} %gestion des tableaux
\usepackage{tcolorbox} %créer des boîtes de couleurs

\usepackage{hyperref} %permet de gérer le sommaire et la page de garde et les options du PDF

\geometry{top=2cm, bottom=2cm, right=2cm, left=2cm}   
\titlespacing{\chapter}{0pt}{0pt}{5pt}

\setcounter{chapter}{10}

\usepackage{lastpage}
\usepackage{fancyhdr} %définition des en-têtes et des bas de page
\fancyhf{}
\fancypagestyle{main}{
	\renewcommand\headrulewidth{5pt}
	\fancyhead[L]{4\ieme\, - Chapitre \arabic{chapter} - Réciproque et contraposée du théorème de Pythagore}
	\fancyhead[R]{Collège}
	\fancyfoot[C]{\thepage/\pageref{LastPage}}
	\fancyfoot[R]{\tiny{Anthony Salze}}
	}
\pagestyle{main}

%gestion des entêtes
\makeatletter\@addtoreset{section}{chapter}\makeatother % RAZ des numéros de section après un chapitre
\renewcommand{\thepart}{\Alph{part} -} % Pour mettre des I, II, etc. aux parties
\renewcommand{\thesection}{\color{Green} \Roman{section} -}% Idem pour les sections et avoir le numéro de chapitre
\renewcommand{\thesubsection}{\arabic{subsection} -}

\begin{document}
%format du titre des chapitres placé après Sommaire sinon cela bugue
\titleformat{\chapter}[frame] {\normalfont \bfseries \large}{\textcolor{red} {\chaptertitlename\ \thechapter\,-\,#1}}{0pt}{\Large}

\setcounter{chapter}{9}

\chapter{Réciproque et contraposée du théorème de Pythagore}
\begin{luacode}
	d=1
	p=1
	t=1
	m=1
\end{luacode}

\color{blue}\underline{\textbf{Prérequis}}
\begin{itemize}
	\item Chapitre $8$ - Théorème de Pythagore.
\end{itemize}~~

\underline{\textbf{Objectif}}
\begin{itemize}
	\item \^Etre capable d'utiliser la réciproque (ou la contraposée) du théorème de Pythagore pour montrer qu'un triangle est (ou n'est pas) rectangle.
\end{itemize}
\color{black}

\color{Green}
\section{Réciproque du théorème de Pythagore}
\color{black}
\normalsize{}
\begin{tcolorbox}[colframe=red!20, colback=red!0, boxrule=2pt, arc=6pt, title={\textbf{Réciproque \arabic{chapter}.\directlua{tex.print(d)}}}, coltitle=black]
	Dans un triangle, si le carré de la longueur du plus grand côté \textbf{\underline{est égal}} à la somme des carrés des longueurs des deux autres côtés, alors le \textbf{\underline{triangle est rectangle}}.
\end{tcolorbox}
\begin{luacode}
	d=d+1
\end{luacode}

\textbf{\underline{Exemple}}\\
\
\textbf{On considère le triangle LIT tel que $LI = \Lg[m]{4.5}, IT = \Lg[m]{6}$ et $LT = \Lg[m]{7.5}$. Est-il rectangle\,?}\\
\\
$[LT]$ est le plus grand côté du triangle.\\
\\
\begin{minipage}{0.45\textwidth}
	\underline{D'une part}
	\begin{flalign*}
		LT^2&=7,5^2&\\
		&=56,25\\
	\end{flalign*}
\end{minipage}
\hspace{0.05cm} \vrule \hspace{0.05cm}
\begin{minipage}{0.45\textwidth}
\underline{D'autre part}
\begin{flalign*}
		LI^2+IT^2&=4,5^2+6^2&\\
                  &=20,25+36\\
                  &=56,25
	\end{flalign*}
\end{minipage}
\\
Comme $LT^2=LI^2+IT^2$, alors d'après la réciproque du théorème de Pythagore, le triangle LIT est rectangle en I.

\color{Green}
\section{Contraposée du théorème de Pythagore}
\color{black}
\normalsize{}
\begin{tcolorbox}[colframe=red!20, colback=red!0, boxrule=2pt, arc=6pt, title={\textbf{Contraposée \arabic{chapter}.\directlua{tex.print(p)}}}, coltitle=black]
	Dans un triangle, si le carré de la longueur du plus grand côté \textbf{\underline{n'est pas égal}} à la somme des carrés des longueurs des deux autres côtés, alors le \textbf{\underline{triangle n'est pas rectangle}}.
\end{tcolorbox}
\begin{luacode}
	p=p+1
\end{luacode}

\textbf{\underline{Exemple}}\\
\textbf{On considère le triangle TUW tel que $TU = \Lg{5.24}, TW = \Lg{3.9}$ et $UW=\Lg{6.5}$. Est-il rectangle\,?}\\
\\
$[WU]$ est le plus grand côté du triangle.\\
\\
\begin{minipage}{0.45\textwidth}
	\underline{D'une part}
	\begin{flalign*}
		UW^2&=6,5^2&\\
		&=42,25\\
	\end{flalign*}
\end{minipage}
\hspace{0.05cm} \vrule \hspace{0.05cm}
\begin{minipage}{0.45\textwidth}
\underline{D'autre part}
\begin{flalign*}
		TW^2+TU^2&=3,9^2+5,24^2&\\
                  &=15,21+27,4576\\
                  &=42,6676
	\end{flalign*}
\end{minipage}
\\
Comme $UW^2\neq TW^2+TU^2$, alors d'après la contraposée du théorème de Pythagore, le triangle TUW n'est pas rectangle.

\color{Green}
\section{Bilan}
\color{black}
QCM (une seule réponse possible)\\\footnotesize{}
\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom=, AlphT, Largeur=3cm]{%
$IJK$ est un triangle tel que : $IJ = \Lg{8.2}\text{, } JK = \Lg{1.8}$ et $IK = \Lg{8}$. Alors ce triangle \ldots & n'est pas rectangle. & est rectangle en $K$. & est rectangle en $J$. &2,%
$ABC$ est un triangle tel que : $AB = \Lg{6.3}\text{, } AC = \Lg{7.8}$ et $BC = \Lg{10}$. Alors ce triangle \ldots & n'est pas rectangle. & est rectangle en $A$. & est rectangle en $C$. &1%
}

\normalsize{}

\color{Green}
\section{Auto-évaluation}
\color{black}

\begin{tabular}{|>{\columncolor{darkgray}\color{white}}m{8cm}|>{\color{white}}m{0.6cm}|m{0.6cm}|m{0.6cm}|m{5cm}|} \hline

\rowcolor{darkgray}\centering{} Compétences & \centering{}\color{white} Oui & \centering{}\color{white}\scriptsize{}\rotatebox{45}{Moyen} & \centering{}\color{white}Non & \centering{}\color{white}Pour réviser \tabularnewline \hline \hline
\cellcolor{gray}Je sais utiliser la réciproque du théorème de Pythagore pour montrer qu'un triangle est rectangle. & & & & \textbf{\underline{Leçon}} : Partie I\smallskip \newline  \textbf{\underline{Exercices}} : 44 p197 - 46 p198 - Exercices 1-2-4-5-6 \tabularnewline \hline \hline
\cellcolor{gray}Je sais utiliser la contraposée du théorème de Pythagore pour montrer qu'un triangle n'est pas rectangle. & & & & \textbf{\underline{Leçon}} : Partie II \smallskip \newline  \textbf{\underline{Exercices}} : 45 p197 - Exercices 3  \tabularnewline \hline
\end{tabular}

%\begin{comment}
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------- Correction QCM -----------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\newpage
\footnotesize{}
\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom=, AlphT, Largeur=3cm, Solution, Couleur=cyan!20]{%
$IJK$ est un triangle tel que : $IJ = \Lg{8.2}\text{, } JK = \Lg{1.8}$ et $IK = \Lg{8}$. Alors ce triangle \ldots & n'est pas rectangle. & est rectangle en $K$. & est rectangle en $J$. &2,%
$ABC$ est un triangle tel que : $AB = \Lg{6.3}\text{, } AC = \Lg{7.8}$ et $BC = \Lg{10}$. Alors ce triangle \ldots & n'est pas rectangle. & est rectangle en $A$. & est rectangle en $C$. &1%
}
\normalsize{}
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%---------------------------------------------------------------------- Leçon à trous -----------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\newpage
%format du titre des chapitres placé après Sommaire sinon cela bugue
\titleformat{\chapter}[frame] {\normalfont \bfseries \Large}{\textcolor{red} {\chaptertitlename\ \thechapter\,-\,#1}}{0pt}{\Huge}

\setcounter{chapter}{9}

\chapter{\pointilles[12.5cm]}
\begin{luacode}
	d=1
	p=1
	t=1
	m=1
\end{luacode}

\color{blue}\underline{\textbf{Prérequis}}
\begin{itemize}
	\item Chapitre $8$- \pointilles[7cm].
\end{itemize}~~

\underline{\textbf{Objectif}}
\begin{itemize}
	\item \^Etre capable d'utiliser la \pointilles[5cm] (ou la \pointilles[5cm]) du \pointilles[7cm] pour montrer qu'un triangle est (ou n'est pas) \pointilles[3cm].
\end{itemize}
\color{black}

\color{Green}
\section{\pointilles[13cm]}
\color{black}
\normalsize{}
\begin{tcolorbox}[colframe=red!20, colback=red!0, boxrule=2pt, arc=6pt, title={\textbf{Réciproque \arabic{chapter}.\directlua{tex.print(d)}}}, coltitle=black]
	Dans un triangle, si \pointilles[12cm]\\
	\pointilles[16cm]\\
	\pointilles[16cm]
\end{tcolorbox}
\begin{luacode}
	d=d+1
\end{luacode}

\textbf{\underline{Exemple}}\\
\
\textbf{On considère le triangle LIT tel que $LI = \Lg[m]{4.5}, IT = \Lg[m]{6}$ et $LT = \Lg[m]{7.5}$. Est-il rectangle\,?}\\
\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]

\color{Green}
\section{\pointilles[13cm]}
\color{black}
\normalsize{}
\begin{tcolorbox}[colframe=red!20, colback=red!0, boxrule=2pt, arc=6pt, title={\textbf{Contraposée \arabic{chapter}.\directlua{tex.print(p)}}}, coltitle=black]
	Dans un triangle, si \pointilles[12cm]\\
	\pointilles[16cm]\\
	\pointilles[16cm]
\end{tcolorbox}
\begin{luacode}
	p=p+1
\end{luacode}

\textbf{\underline{Exemple}}\\
\textbf{On considère le triangle TUW tel que $TU = \Lg{5.24}, TW = \Lg{3.9}$ et $UW=\Lg{6.5}$. Est-il rectangle\,?}\\
\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]

\color{Green}
\section{Bilan}
\color{black}
QCM (une seule réponse possible)\\\footnotesize{}
\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom=, AlphT, Largeur=3cm]{%
$IJK$ est un triangle tel que : $IJ = \Lg{8.2}\text{, } JK = \Lg{1.8}$ et $IK = \Lg{8}$. Alors ce triangle \ldots & n'est pas rectangle. & est rectangle en $K$. & est rectangle en $J$. &2,%
$ABC$ est un triangle tel que : $AB = \Lg{6.3}\text{, } AC = \Lg{7.8}$ et $BC = \Lg{10}$. Alors ce triangle \ldots & n'est pas rectangle. & est rectangle en $A$. & est rectangle en $C$. &1%
}

\color{Green}
\section{Auto-évaluation}
\color{black}

\begin{tabular}{|>{\columncolor{darkgray}\color{white}}m{8cm}|>{\color{white}}m{0.6cm}|m{0.6cm}|m{0.6cm}|m{5cm}|} \hline

\rowcolor{darkgray}\centering{} Compétences & \centering{}\color{white} Oui & \centering{}\color{white}\scriptsize{}\rotatebox{45}{Moyen} & \centering{}\color{white}Non & \centering{}\color{white}Pour réviser \tabularnewline \hline \hline
\cellcolor{gray}Je sais utiliser la réciproque du théorème de Pythagore pour montrer qu'un triangle est rectangle. & & & & \textbf{\underline{Leçon}} : Partie I\smallskip \newline  \textbf{\underline{Exercices}} : 44 p197 - 46 p198 - Exercices 1-2-4-5-6 \tabularnewline \hline \hline
\cellcolor{gray}Je sais utiliser la contraposée du théorème de Pythagore pour montrer qu'un triangle n'est pas rectangle. & & & & \textbf{\underline{Leçon}} : Partie II \smallskip \newline  \textbf{\underline{Exercices}} : 45 p197 - Exercices 3  \tabularnewline \hline
\end{tabular}


%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------- Impression -----------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\newpage

%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%--------------------------------------------------------------- Histoire des mathématiques -------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\newpage
\begin{tcolorbox}[colframe=black, colback=blue!10, boxrule=2pt, arc=6pt]
	\centering{}\LARGE{\textbf{Histoire des mathématiques \faBook}}\\
	Euclide
\end{tcolorbox}
\begin{tcolorbox}[colframe=black!30, colback=blue!0, boxrule=2pt, arc=6pt, title=Euclide ($\approx -330\, ;\, \approx -275$), coltitle=black]
	Comme vu au précédent chapitre sur le théorème de Pythagore, Euclide a également donné la démonstration de la réciproque du théorème de Pythagore dans le livre I des \textit{\'Eléments}, propriété XLVIII dont l'extrait est donné ci-dessous.
\end{tcolorbox}
\begin{tcolorbox}[colframe=black!30, colback=blue!0, boxrule=2pt, arc=6pt, title=Byrne O. (2022) \textit{The first six books of the elements of Euclid} Taschen p. 50, coltitle=black]
	\begin{center}
		\includegraphics[width=6cm]{4_C10_Euclide.png}	
	\end{center}
\end{tcolorbox}

\textbf{\underline{Sources}}
\begin{itemize}[label=\textbullet]
	\item Hauchecorne, B. et Suratteau, D. (2008), \textit{Des Mathématiciens de A à Z} ($3^e$ édition), Ellipses
	\item Byrne, O. (2022), \textit{The first six books of the elements of Euclid}, Taschen
\end{itemize}

%\end{comment}

\end{document}

