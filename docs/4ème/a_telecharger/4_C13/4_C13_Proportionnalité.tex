\documentclass[dvipsnames, a4paper, 12pt]{report}

\usepackage{lmodern} %police standard latin moderne
\usepackage[frenchb]{babel} %langue française
\usepackage[utf8]{inputenc} %pour l'utf-8
\usepackage[T1]{fontenc} %pour les césures et accentuations
\usepackage{geometry} %pour changer les marges d'un document
\usepackage{graphicx}
\usepackage{fancybox}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{fontawesome5}
\usepackage{ProfCollege}
\usepackage{luacode}
\usepackage{xlop}


\usepackage[explicit]{titlesec} %Permet de modifier les différentes parties, chapitres, sous-parties,...

\usepackage{colortbl} %permet de mettre des cellules de tableau en couleur

\usepackage{multicol}

\usepackage{cellspace} %pour définir les espaces en haut et en bas des cellules d'un tableau
\setlength{\cellspacebottomlimit}{5pt}
\setlength{\cellspacetoplimit}{5pt}
\setlength{\parindent}{0pt}

\usepackage{xcolor} %permet de mettre du texte en couleur

\usepackage{array} %gestion des tableaux
\usepackage{tcolorbox} %créer des boîtes de couleurs

\usepackage{hyperref} %permet de gérer le sommaire et la page de garde et les options du PDF

\geometry{top=2cm, bottom=2cm, right=2cm, left=2cm}   
\titlespacing{\chapter}{0pt}{0pt}{5pt}

\setcounter{chapter}{13}

\usepackage{lastpage}
\usepackage{fancyhdr} %définition des en-têtes et des bas de page
\fancyhf{}
\fancypagestyle{main}{
	\renewcommand\headrulewidth{5pt}
	\fancyhead[L]{4\ieme\, - Chapitre \arabic{chapter} - Proportionnalité}
	\fancyhead[R]{Collège}
	\fancyfoot[C]{\thepage/\pageref{LastPage}}
	\fancyfoot[R]{\footnotesize{Anthony Salze}}
	}
\pagestyle{main}

%gestion des entêtes
\makeatletter\@addtoreset{section}{chapter}\makeatother % RAZ des numéros de section après un chapitre
\renewcommand{\thepart}{\Alph{part} -} % Pour mettre des I, II, etc. aux parties
\renewcommand{\thesection}{\color{Green} \Roman{section} -}% Idem pour les sections et avoir le numéro de chapitre
\renewcommand{\thesubsection}{\arabic{subsection} -}

\begin{document}
%format du titre des chapitres placé après Sommaire sinon cela bugue
\titleformat{\chapter}[frame] {\normalfont \bfseries \Large}{\textcolor{red} {\chaptertitlename\ \thechapter\,-\,#1}}{0pt}{\Huge}

\setcounter{chapter}{12}

\chapter{Proportionnalité}
\begin{luacode}
	d=1
	p=1
	t=1
	m=1
\end{luacode}

\color{blue}\underline{\textbf{Prérequis}}
\begin{itemize}
	\item Produit en croix.
	\item Quatrième proportionnelle.
	\item Pourcentages.
\end{itemize}~~

\underline{\textbf{Objectifs}}
\begin{itemize}
	\item Calculer une quatrième proportionnelle à l'aide du produit en croix.
	\item Reconnaître une situation de proportionnalité graphiquement.
	\item Appliquer un pourcentage d'augmentation ou de réduction.
\end{itemize}
\color{black}


\color{Green}
\section{Rappels}
\color{black}
\normalsize{}
\begin{tcolorbox}[colframe=blue!20, colback=blue!0, boxrule=2pt, arc=6pt, title={\textbf{Propriété \arabic{chapter}.\directlua{tex.print(p)}}}, coltitle=black]
	\begin{minipage}{0.75\textwidth}
		Dans le tableau ci-contre, les grandeurs A et B sont proportionnelles. L'égalité du produit en croix permet d'écrire : $\textcolor{blue}a \times \textcolor{blue}d = \textcolor{red}c \times \textcolor{red}b$.
	\end{minipage}
	\begin{minipage}{0.2\textwidth}
	\begin{center}
		\begin{tabular}{|c|c|c|} \hline
			Grandeur A & \textcolor{blue}a & \textcolor{red}b \tabularnewline \hline
			Grandeur B & \textcolor{red}c & \textcolor{blue}d \tabularnewline \hline
		\end{tabular}
	\end{center}
	\end{minipage}
\end{tcolorbox}
\begin{luacode}
	p=p+1
\end{luacode}

\begin{tcolorbox}[colframe=Green!20, colback=Green!0, boxrule=2pt, arc=6pt, title={\textbf{Définition \arabic{chapter}.\directlua{tex.print(d)} : quatrième proportionnelle}}, coltitle=black]
Dans un tableau de proportionnalité, si on connaît trois valeurs sur quatre, il est possible de calculer la quatrième valeur.
Cette valeur est appelée la quatrième proportionnelle.
\end{tcolorbox}
\begin{luacode}
	d=d+1
\end{luacode}


\textbf{\underline{\'Enoncé}}\\
On sait que 64 cartes Pokémon pèsent 108,8 g. Quelle est la masse de 723 cartes ?
\\\\
\textbf{\underline{Solution}}\\
On a le tableau de proportionnalité suivant : \vspace{-0.5cm}
\begin{center}
	\Propor[Math, GrandeurA=Nombre de cartes, GrandeurB=Masse (en \Masse{})]{64/\num{108.8}, 723/M}
\end{center}
\FlechePCB{2}{1}
\textbf{Masse de 723 cartes Pokémon}\\
Soit $M$ la masse recherchée, le produit en croix permet d'écrire $64 \times M = 108,8 \times 723$.\\\\
Donc : $M = \dfrac{723 \times 108,8}{64} = \num{1229.1}$\\\\
La masse de 723 cartes Pokémon est de 1 229,1 g.


\color{Green}
\section{Représentation graphique}
\color{black}
\begin{tcolorbox}[colframe=blue!20, colback=blue!0, boxrule=2pt, arc=6pt, title={\textbf{Propriété \arabic{chapter}.\directlua{tex.print(p)}}}, coltitle=black]
	\begin{itemize}[label=\textbullet]
		\item Dans un repère, toute situation de proportionnalité se réprésente graphiquement par des points alignés avec l'origine du repère.
		\item Inversement, tout graphique dont les points sont alignés avec l'origine du repère représente une situation de proportionnalité.
	\end{itemize}
\end{tcolorbox}
\begin{luacode}
	p=p+1
\end{luacode}

\textbf{\underline{Exemple 1}}\\
\begin{minipage}{0.5\textwidth}
	\begin{center}
\Fonction[Trace,Calcul={0.5*x},Bornea={-5,-5},Borneb={5,5},CouleurTrace={red},Origine={(0,0)},Xmin=0,Xmax=4,Ymin=0,Ymax=3,Grille,
PasGrilleX=1,PasGrilleY=1,Graduations, LabelX=Temps (min), LabelY=Hauteur (\Lg{})]{}
	\end{center}
\end{minipage}
\begin{minipage}{0.5\textwidth}
	Ce graphique est une droite passant par l'origine du repère, c'est donc une situation de proportionnalité. Ainsi, la hauteur est proportionnelle au temps.
\end{minipage}
~~\\

\textbf{\underline{Exemple 2}}
\begin{center}
\Fonction[Trace,Calcul={0.5x+2},Bornea={-5,-5},Borneb={5,5},CouleurTrace={red},Origine={(0,0)},Xmin=0,Xmax=4,Ymin=0,Ymax=5,Grille,
PasGrilleX=1,PasGrilleY=1,Graduations,LabelX=Temps (min), LabelY=Hauteur (\Lg{})]{}
\Fonction[Trace,Calcul={exp(0.5*x)},Bornea={-5,-5},Borneb={5,5},CouleurTrace={red},Origine={(0,0)},Xmin=0,Xmax=4,Ymin=0,Ymax=5,Grille,
PasGrilleX=1,PasGrilleY=1,Graduations,LabelX=Temps (min), LabelY=Hauteur (\Lg{})]{}
\end{center}
Le premier graphique représente une droite qui ne passe pas par l'origine du repère et le deuxième graphique n'est pas une droite. Ce ne sont pas des situations de proportionnalité. Ainsi, la hauteur et le temps ne sont pas proportionnels.

\color{Green}
\section{Augmentation et réduction, pourcentages}
\color{black}

\begin{tcolorbox}[colframe=blue!20, colback=blue!0, boxrule=2pt, arc=6pt, title={\textbf{Propriété \arabic{chapter}.\directlua{tex.print(p)}}}, coltitle=black]
	Un pourcentage de $t$ \% traduit une situation de proportionnalité de coefficient $\dfrac{t}{100}$.\\
	Donc appliquer un taux de $t$ \% revient à multiplier par $\dfrac{t}{100}$.
\end{tcolorbox}
\begin{luacode}
	p=p+1
\end{luacode}

\textbf{\underline{Exemple}}\\
Dans une classe de $30$ élèves, $60$ \% des élèves pratiquent un sport.\\\\
$30 \times \dfrac{60}{100} = 18$. Il y a donc 18 élèves sportifs dans cette classe.
\\

\begin{tcolorbox}[colframe=Green!20, colback=Green!0, boxrule=2pt, arc=6pt, title={\textbf{Définition \arabic{chapter}.\directlua{tex.print(d)} : quatrième proportionnelle}}, coltitle=black]
Déterminer un pourcentage, c'est déterminer une proportion écrite sous forme d'une écriture fractionnaire dont le dénominateur est $100$.
\end{tcolorbox}
\begin{luacode}
	d=d+1
\end{luacode}

\textbf{\underline{Exemple}}\\
Sur 550 élèves, 231 sont externes. On a le tableau de proportionnalité suivant :
\begin{center}
	\Propor[Math, GrandeurA=Nombre d'élèves externes, GrandeurB=Nombre total d'élèves]{231/550, N/100}
\end{center}
\FlechePCH{2}{1}
D'après l'égalité des produits en croix, on a : $231 \times 100 = 550 \times N$.\\\\
Ainsi, $N = \dfrac{231 \times 100}{550}=42$. Il y a donc $42$ \% d'externes dans ce collège.\\


\begin{tcolorbox}[colframe=blue!20, colback=blue!0, boxrule=2pt, arc=6pt, title={\textbf{Propriété \arabic{chapter}.\directlua{tex.print(p)}}}, coltitle=black]
	\begin{itemize}[label=\textbullet]
		\item Augmenter une valeur de $t$ \% revient à multiplier cette valeur par $1 + \dfrac{t}{100}$.
		\item Diminuer une valeur de $t$ \% revient à multiplier cette valeur par $1 - \dfrac{t}{100}$.
	\end{itemize}
\end{tcolorbox}
\begin{luacode}
	p=p+1
\end{luacode}

\textbf{\underline{\'Enoncé 1}}\\
Un pull coûte $33$ \texteuro. Le vendeur décide d'augmenter son prix de $15$ \%. Quel prix le vendeur doit-il afficher ?\\

\textbf{\underline{Réponse}}\\
\Pourcentage[Augmenter,Formule]{15}{33}\\
Le nouveau prix du pull est $37,95$ \texteuro.\\
\\

\textbf{\underline{\'Enoncé 2}}\\
Une paire de chaussures coûte $95$ \texteuro. Le vendeur applique une réduction de $30$ \% sur son prix. Quel est le nouveau prix ?\\

\textbf{\underline{Réponse}}\\
\Pourcentage[Reduire,Formule]{30}{95}\\
La paire de chaussures après réduction coûte $66,5$ \texteuro.







\begin{comment}
%----------------------------------------- CORRECTION QCM
\newpage


\newpage
%---------------------------------------------------------------------------- DYS ------------------------------------------------------------
%format du titre des chapitres placé après Sommaire sinon cela bugue
\titleformat{\chapter}[frame] {\normalfont \bfseries \Large}{\textcolor{red} {\chaptertitlename\ \thechapter\,-\,#1}}{0pt}{\Huge}

\setcounter{chapter}{2}

\chapter{\pointilles[10cm]}
\begin{luacode}
	d=1
	p=1
	t=1
	m=1
\end{luacode}

\color{blue}\underline{\textbf{Prérequis}}
\begin{itemize}
	\item \pointilles[10cm]
	\item Opérations de base avec les \pointilles[10cm].
\end{itemize}~~

\underline{\textbf{Objectifs}}
\begin{itemize}
	\item Être capable de savoir si un tableau représente une \pointilles[7cm].
	\item Calculer la \pointilles[10cm].
	\item Notion de \pointilles[7cm].
\end{itemize}
\color{black}


\color{Green}
\section{Tableau de proportionnalité}
\color{black}
\normalsize{}
\begin{tcolorbox}[colframe=blue!20, colback=blue!0, boxrule=2pt, arc=6pt, title={\textbf{Propriété \arabic{chapter}.\directlua{tex.print(p)}}}, coltitle=black]
	Si le tableau ci-dessous est un \pointilles[7cm], l'\pointilles[5cm] des \pointilles[7cm] permet d'écrire \pointilles[5cm].
	\begin{center}
		\begin{tabular}{|c|c|} \hline
			a & b \tabularnewline \hline
			c & d \tabularnewline \hline
		\end{tabular}
	\end{center}
\end{tcolorbox}
\begin{luacode}
	p=p+1
\end{luacode}

\begin{tcolorbox}[colframe=Green!20, colback=Green!0, boxrule=2pt, arc=6pt, title={\textbf{Définition \arabic{chapter}.\directlua{tex.print(d)}}}, coltitle=black]
Pour savoir si un tableau représente une situation de proportionnalité, on peut utiliser la méthode du \pointilles[7cm].
\begin{itemize}[label=\textbullet]
	\item Si l'égalité est \pointilles[3cm], le tableau représente une \pointilles[7cm].
	\item \pointilles[3cm], le tableau ne représente pas une \pointilles[7cm].
\end{itemize}
\end{tcolorbox}
\begin{luacode}
	d=d+1
\end{luacode}

\textbf{\underline{Exemple}}\\
\begin{minipage}{0.5\textwidth}
Les quantités de pommes de terre et de carottes pour réaliser une purée sont synthétisées dans le tableau ci-dessous :
\begin{center}
	\Propor[GrandeurA=\begin{tabular}{c}Masse carottes \\ (en g) \end{tabular}, GrandeurB=\begin{tabular}{c}Masse PDT \\ (en g) \end{tabular}]{50/75,100/150}%
\end{center}
\end{minipage}
\hspace{0.05cm} \vrule \hspace{0.05cm}
\begin{minipage}{0.45\textwidth}
\pointilles[8cm]\\
\pointilles[8cm]\\
\pointilles[8cm]\\
\pointilles[8cm] \\
\pointilles[8cm]\\
\pointilles[8cm]
\end{minipage}



\color{Green}
\section{Quatrième proportionnelle : produit en croix}
\color{black}

\begin{tcolorbox}[colframe=Green!20, colback=Green!0, boxrule=2pt, arc=6pt, title={\textbf{Définition \arabic{chapter}.\directlua{tex.print(d)} : quatrième proportionnelle}}, coltitle=black]
Dans un tableau de proportionnalité, si on connaît \pointilles[10cm], il est possible de calculer la \pointilles[7cm]. Cette valeur est appelée la \pointilles[7cm].
\end{tcolorbox}
\begin{luacode}
	d=d+1
\end{luacode}


\textbf{\underline{\'Enoncé}}\\
On sait que 64 cartes Pokémon pèsent 108,8 g. Quelle est la masse de 723 cartes ?
\\\\
\textbf{\underline{Solution}}\\
On a le tableau de proportionnalité suivant : \vspace{-0.5cm}
\begin{center}
	\Propor[Math, GrandeurA=Nombre de cartes, GrandeurB=Masse (en g)]{\dots/\dots, \dots/\dots}
\end{center}\vspace{-0.5cm}
\textbf{Masse de 723 cartes Pokémon}\\
Soit $M$ la masse recherchée, le produit en croix permet d'écrire \pointilles[5cm].\\\\
\pointilles[17cm]\\\\
\pointilles[10cm]

\color{Green}
\section{Notion de pourcentage}
\color{black}

\begin{tcolorbox}[colframe=blue!20, colback=blue!0, boxrule=2pt, arc=6pt, title={\textbf{Propriété \arabic{chapter}.\directlua{tex.print(p)}}}, coltitle=black]
	Un pourcentage de \pointilles[2cm] traduit une \pointilles[7cm] de coefficient \pointilles[2cm].\\
	Donc appliquer un taux de \pointilles[2cm] revient à multiplier par\pointilles[2cm].
\end{tcolorbox}
\begin{luacode}
	p=p+1
\end{luacode}

\textbf{\underline{Exemple}}\\
Dans une classe de $30$ élèves, $60$ \% des élèves pratiquent un sport.\\\\
\pointilles[17cm]
\\


\begin{tcolorbox}[colframe=Green!20, colback=Green!0, boxrule=2pt, arc=6pt, title={\textbf{Définition \arabic{chapter}.\directlua{tex.print(d)} : quatrième proportionnelle}}, coltitle=black]
Déterminer un \pointilles[5cm], c'est déterminer une \pointilles[5cm] écrite sous forme d'une \pointilles[7cm] dont le \pointilles[7cm].
\end{tcolorbox}
\begin{luacode}
	d=d+1
\end{luacode}

\textbf{\underline{Exemple}}\\
Sur 550 élèves, 231 sont externes. On a le tableau de proportionnalité suivant :
\begin{center}
	\begin{tabular}{|c|c|c|} \hline
		\cellcolor{black!10}Nombre d'externes & \ldots & \ldots \tabularnewline \hline
		\cellcolor{black!10}Nombre d'élèves & \ldots &  \ldots \tabularnewline \hline
	\end{tabular}
\end{center}
D'après l'égalité des produits en croix, on a :\pointilles[5cm].\\\\
\pointilles[17cm]



%----------------------------------------------------------------------------- IMPRESSION
\newpage

\end{comment}
\end{document}
