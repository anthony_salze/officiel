---
author: Anthony Salze
title: Sources
hide :
    -navigation
    -toc
    -footer
---
# <span style="color:Tomato"> Sources
!!! info "Sources"
    === "Histoire des mathématiques"
        * Dahan-Dalmedico, A. & Peiffer, J. (mars 1986). *Une histoire des mathématiques. Routes et dédales.* Points.
        * Hauchecorne, B. & Suratteau, D. (2008). *Des mathématiciens de A à Z.* (3ème édition). Ellipses.
        * Houlou-Garcia, A. (avril 2019). *Mathematikos*. Belles Lettres.
        * Collectif Tangente. (2005). *Mille ans d'histoire des mathématiques*. (Bibliothèque Tangente HS n°10). Pole.
        * Collectif Tangente. (2015). *Histoire des mathématiques de l'Antiquité à l'an Mil*. (Bibliothèque Tangente HS n°30). Pole.
        * Barbin, E. (2019). *Faire des mathématiques avec l'histoire au lycée*. Ellipses.
        * Collectif APMEP. (2017-2024). *Au fil des maths*. APMEP.
        * Association Sciences en Seine et Patrimoine. *ASSP*. ASSP Rouen. [http://assprouen.free.fr/](http://assprouen.free.fr/){:target="_blank"} 
    === "Exercices"
        * Mission Indigo 6è. (2021). *Mission Indigo 6è Maths*. Hachette Education.
        * Mission Indigo 5è. (2020). *Mission Indigo 5è Maths*. Hachette Education.
        * Mission Indigo 4è. (2018). *Mission Indigo 4è Maths*. Hachette Education.
        * Collection Tam. (2020). *Maths 3è*. Hatier.
        * Le livre scolaire. (2022). *Maths 4è*. Le livre scolaire.
        * Le livre scolaire. (2021). *Maths 3è*. Le livre scolaire.
        * Transmaths 6è. (2022). *Transmaths 6è*. Nathan.
        * Transmaths 5è. (2022). *Transmaths 5è*. Nathan.
        * Transmaths 4è. (2021). *Transmaths 4è*. Nathan.
        * Transmaths 3è. (2021). *Transmaths 3è*. Nathan.
        * Transmaths 6è. (2022). *Le cahier Transmaths 6è*. Nathan.
        * Transmaths 5è. (2022). *Le cahier Transmaths 5è*. Nathan.
        * Transmaths 4è. (2021). *Le cahier Transmaths 4è*. Nathan.
        * Transmaths 3è. (2021). *Le cahier Transmaths 3è*. Nathan.
        * Collection Myriade. (2021). *Myriade maths 6è*. Bordas.
        * Collection Myriade. (2021). *Myriade maths 5è*. Bordas.
        * Collection Myriade. (2021). *Myriade maths 4è*. Bordas.
        * Collection Myriade. (2021). *Myriade maths 3è*. Bordas.
        * Collection Sésamath. (2021). *Les cahiers Sésamaths 6è*. Magnard.
        * Collection Delta. (2021). *Delta Maths 6è*. Magnard.
        * Collection Delta. (2014). *Delta Maths 5è*. Magnard.
        * Collection Delta. (2014). *Delta Maths 4è*. Magnard.
        * Collection Delta. (2014). *Delta Maths 3è*. Magnard.
    === "Sites"
        * Groupe Lifelong Kindergarten (MIT Media Lab). *Scratch*. Scratch, Imagine, Program, Share. [https://scratch.mit.edu/](https://scratch.mit.edu/){:target="_blank"} 
        * Association APMEP. *APMEP*. APMEP : Association des Professeurs de Mathématiques de l'Enseignement Public. [https://www.apmep.fr/](https://www.apmep.fr/){:target="_blank"}  
        * Collectif GeoGebra. *GeoGebra*. GeoGebra : Applications mathématiques gratuites. [https://www.geogebra.org/](https://www.geogebra.org/){:target="_blank"}  
        * CoopMaths. *MathAléa*. MathAléa. [https://coopmaths.fr/alea/](https://coopmaths.fr/alea/){:target="_blank"} 
        * MathsMentales. *MathsMentales*. MathMentales : Automatismes et calcul mental en mathématiques. [https://mathsmentales.net/](https://mathsmentales.net/){:target="_blank"} 
        * Christophe Poulain. *ProfCollege*. LaTeX. [https://ctan.org/pkg/profcollege](https://ctan.org/pkg/profcollege){:target="_blank"}
        * Vincent Joly. *PyBlock*. PyBlock Code. [https://mathematiques-medias.discipline.ac-lille.fr/PyBlock/](https://mathematiques-medias.discipline.ac-lille.fr/PyBlock/){:target="_blank"} 
        * Anthony Salze. *Mathêma*. Mathêma. [https://anthony_salze.forge.apps.education.fr/mathema/](https://anthony_salze.forge.apps.education.fr/mathema/){:target="_blank"} 
        * Laboratoire mathématique Alexandre Grothendieck du collège Félicien Joly de Fresnes-sur-Escaut (59). [https://college-felicien-joly.forge.apps.education.fr/laboratoire-grothendieck/](https://college-felicien-joly.forge.apps.education.fr/laboratoire-grothendieck/){:target="_blank"} 