---
author: Anthony Salze
title: Interrogations écrites
hide :
    -footer
---
# <span style="color:Tomato"> Évaluations

## <span style="color:LightSalmon">Interrogations écrites
??? tip "Interrogations écrites <span style=color:red> :material-file-pdf-box: <span style=color:blue>:material-text-box-edit-outline: "
    === "IE 00 - Révisions (numérique - fiches 01 à 05)"
        [PDF](a_telecharger/3_IE/3_IE_00_Révisions_numérique.pdf){:target="_blank"}  
        [LaTeX](a_telecharger/3_IE/3_IE_00_Révisions_numérique.tex)
        [LaTeX_main](a_telecharger/3_IE/3_IE_00_Révisions_num.tex)
    === "IE 00 - Révisions (géométrie - fiches 06 et 07)"
        [PDF](a_telecharger/3_IE/3_IE_00_Révisions_géométrie.pdf){:target="_blank"}    
        [LaTeX](a_telecharger/3_IE/3_IE_00_Révisions_géométrie.tex)
        [LaTeX_main](a_telecharger/3_IE/3_IE_00_Révisions_géo.tex)
    === "IE 01 - Calcul littéral"
        [PDF](a_telecharger/3_IE/3_IE_01_Calcul_littéral.pdf){:target="_blank"}    
        [LaTeX](a_telecharger/3_IE/3_IE_01_Calcul_littéral.tex)  
        [LaTeX_main](a_telecharger/3_IE/3_IE_01_Calcul_lit.tex) 
    === "IE 02 - Homothéties / Calcul littéral"
        [PDF](a_telecharger/3_IE/3_IE_02_Transformations_Calcul_litteral.pdf){:target="_blank"}    
        [LaTeX](a_telecharger/3_IE/3_IE_02_Transformations_Calcul_litteral.tex)
        [LaTeX_main](a_telecharger/3_IE/3_IE_02_Transformations_Calc_litt.tex)
    === "IE 03 - Équations"
        [PDF (prochainement)](){:target="_blank"}    
        [LaTeX (prochainement)]() 
        [LaTeX_main (prochainement)]()
        
    <span style=color:CornflowerBlue>_Dernière mise à jour : le 29/11/24_