---
author: Anthony Salze
title: Pour s'entraîner
hide :
    -footer
---
# <span style="color:Tomato"> S'entraîner avec des exercices interactifs

???+ info "Exercices interactifs"
    - [01 : Calcul littéral](https://coopmaths.fr/alea/?uuid=603a8&id=3L10&i=1&uuid=815eb&id=3L10-1&i=1&uuid=db2e0&id=3L11&i=1&uuid=4197c&id=3L11-1&i=1&uuid=f6853&id=3L11-2&i=1&uuid=82313&id=3L11-3&i=1&uuid=5f5a6&id=3L11-4&i=1&uuid=51360&id=3L11-6&i=1&uuid=7cf81&id=3L11-7&i=1&uuid=be157&id=3L12-1&i=1&uuid=cd2f2&id=3L13-4&i=1&uuid=231d2&id=3L15&i=1&uuid=22412&id=3L13-3&i=1&uuid=1802d&id=3L13-1&i=1&uuid=5a02b&id=3L13-0&i=1&uuid=f239f&id=3L13&i=1&v=eleve&es=1110001&title=Calcul+litt%C3%A9ral){:target="_blank"}  
    - [02a : Homothéties](https://coopmaths.fr/alea/?uuid=d4088&id=3G10-1&i=1&uuid=442e0&id=3G12&i=1&uuid=034f1&id=3G12-1&i=1&uuid=6f383&id=3G13&i=1&v=eleve&es=1110001&title=Transformations){:target="_blank"}  
    - [02b : Transformations](https://coopmaths.fr/alea/?uuid=cf7ce&id=4G10&i=1&uuid=7b40c&id=4G10-1&i=1&uuid=6ddc5&id=4G10-3&i=1&uuid=48253&id=4G11-1&i=1&uuid=4ffdb&id=4G12&i=1&uuid=8ac93&id=4G12-1&i=1&v=eleve&es=1110001&title=Transformations+2){:target="_blank"}  
    - [03 : Puissances](https://coopmaths.fr/alea/?uuid=eb865&id=3C10-1&n=6&d=10&s=3&i=1&cd=1&cols=4&uuid=379cd&id=3C10-2&i=1&uuid=31bd7&id=3C10-3&i=1&uuid=a9001&id=3C10-4&i=1&uuid=b0b3c&id=4C30-4&i=1&v=eleve&es=1110001&title=Puissances){:target="_blank"}  
    - [04 : Pythagore](https://coopmaths.fr/alea/?uuid=bd660&id=4G20&i=1&uuid=40c47&id=4G20-1&i=1&uuid=ab5d4&id=4G21&i=1&uuid=b18e8&id=4G22&i=1&v=eleve&es=1110001&title=Pythagore){:target="_blank"}  
    - [05 : Scratch](https://coopmaths.fr/alea/?uuid=8cbd6&id=3I1&uuid=2ecd9&id=3I10-1&i=1&uuid=39a32&id=3I12-1&uuid=defeb&id=3I12-2&uuid=52c97&id=3I12-3&uuid=9a1a9&id=3I12-4&v=eleve&es=1110001&title=Scratch){:target="_blank"}  
    - [06 : Périmètres, aires, volumes](https://coopmaths.fr/alea/?uuid=e26ca&id=5M20&n=5&d=10&s=1&s3=2&s4=5&i=1&cd=1&v=eleve&es=1110001&title=Volumes){:target="_blank"}  
    
    <span style=color:CornflowerBlue>_Dernière mise à jour : le 14/11/24_

???+ info "Type brevet"   
    - [06a : Volumes](https://coopmaths.fr/alea/?uuid=dnb_2013_12_caledonie_6&alea=YdYh&v=eleve&es=1110001&title=Volumes+2){:target="_blank"}  

    <span style=color:CornflowerBlue>_Dernière mise à jour : le 14/11/24_

