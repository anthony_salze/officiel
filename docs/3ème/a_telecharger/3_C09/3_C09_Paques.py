import numpy as np

def jour(annee):
    """
    Calcule le jour de Pâques en demandant à l'utilisateur l'année
    """
    annee=int(annee)
    R = annee % 4
    S = annee % 7
    T = annee % 19
    B = 19 * T + 24
    M = B % 30
    C = 2 * R + 4 * S + 6 * M + 5
    N = C % 7
    P = M + N
    if P < 10:
        P=P+22
        print('Le jour de Pâques sera le ',P,' mars ', annee)
    else:
        P=P-9
        print('Le jour de Pâques sera le ',P,' avril ', annee)



annee=input("Pour quelle année voulez-vous calculer le jour de Pâques ? ")
jour(annee)
