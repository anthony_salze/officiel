\documentclass[dvipsnames, a4paper, 12pt]{report}

\usepackage{lmodern} %police standard latin moderne
\usepackage[frenchb]{babel} %langue française
\usepackage[utf8]{inputenc} %pour l'utf-8
\usepackage[T1]{fontenc} %pour les césures et accentuations
\usepackage{geometry} %pour changer les marges d'un document
\usepackage{graphicx}
\usepackage{fancybox}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{fontawesome5}
\usepackage{ProfCollege}
\usepackage{luacode}
\usepackage{xlop}

\usepackage[explicit]{titlesec} %Permet de modifier les différentes parties, chapitres, sous-parties,...

\usepackage{colortbl} %permet de mettre des cellules de tableau en couleur

\usepackage{multicol}

\usepackage{cellspace} %pour définir les espaces en haut et en bas des cellules d'un tableau
\setlength{\cellspacebottomlimit}{5pt}
\setlength{\cellspacetoplimit}{5pt}
\setlength{\parindent}{0pt}

\usepackage{xcolor} %permet de mettre du texte en couleur

\usepackage{array} %gestion des tableaux
\usepackage{tcolorbox} %créer des boîtes de couleurs

\usepackage{hyperref} %permet de gérer le sommaire et la page de garde et les options du PDF

\geometry{top=2cm, bottom=2cm, right=2cm, left=2cm}   
\titlespacing{\chapter}{0pt}{0pt}{5pt}

\setcounter{chapter}{1}

\usepackage{lastpage}
\usepackage{fancyhdr} %définition des en-têtes et des bas de page
\fancyhf{}
\fancypagestyle{main}{
	\renewcommand\headrulewidth{5pt}
	\fancyhead[L]{3\ieme\, - Chapitre \arabic{chapter} - Calcul littéral}
	\fancyhead[R]{Collège}
	\fancyfoot[C]{\thepage/\pageref{LastPage}}
	\fancyfoot[R]{\footnotesize{Anthony SALZE}}
	}
\pagestyle{main}

%gestion des entêtes
\makeatletter\@addtoreset{section}{chapter}\makeatother % RAZ des numéros de section après un chapitre
\renewcommand{\thepart}{\Alph{part} -} % Pour mettre des I, II, etc. aux parties
\renewcommand{\thesection}{\color{Green} \Roman{section} -}% Idem pour les sections et avoir le numéro de chapitre
\renewcommand{\thesubsection}{\arabic{subsection} -}

\begin{document}
%format du titre des chapitres placé après Sommaire sinon cela bugue
\titleformat{\chapter}[frame] {\normalfont \bfseries \Large}{\textcolor{red} {\chaptertitlename\ \thechapter\,-\,#1}}{0pt}{\Huge}

\setcounter{chapter}{0}

\chapter{Calcul littéral}
\begin{luacode}
	d=1
	p=1
	t=1
	m=1
\end{luacode}

\color{blue}\underline{\textbf{Prérequis}}
\begin{itemize}
	\item Distributivités simple et double ;
	\item Factorisation.
\end{itemize}~~

\underline{\textbf{Objectifs}}
\begin{itemize}
	\item Double distributivité et identités remarquables ;
	\item Démontrer que deux expressions littérales sont (ou ne sont pas) égales ;
	\item Factorisation.
\end{itemize}
\color{black}
~~\\
\textbf{\underline{Rappels}} : QCM (une seule réponse possible)\\\footnotesize{}
\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom= , AlphT, Largeur=3cm]{%
Pour $x=5\text{,}$ que vaut l'expression littérale $4x^2-3x+7$ ?  & $122$ & $12$ & $92$ &3,%
$3(x+7)=$ \pointilles[1cm] & $3x+7$ & $3x+21$ & $3x+10$ &2,%
Le plus grand facteur entier commun de $-8x^2+4x+2$ est \pointilles[1cm] & $2$ & $4$ & $8$ &1,%
$-4x(2x-5)=$ \pointilles[1cm] & $-8x^2+20x$ & $-8x^2-20x$ & $-8x^2+45x$ &1,%
$x^2-3x+2x^2-7x+5-12x^2=$ \pointilles[1cm] & $15x^2-10x+5$ & $-9x^2+10x+5$ & $-9x^2-10x+5$ &3%
}
\normalsize{}

\color{Green}
\section{Simple et double distributivités}
\color{black}
\normalsize{}

\begin{tcolorbox}[colframe=blue!20, colback=blue!0, boxrule=2pt, arc=6pt, title={\textbf{Propriété \arabic{chapter}.\directlua{tex.print(p)}}}, coltitle=black]
Voir Fiche de révisions 4 - Exercice 3
\end{tcolorbox}
\begin{luacode}
	p=p+1
\end{luacode}

\textbf{\underline{Exemple 1}}\\
\textbf{Développer et réduire l'expression suivante.} \vspace{-0.5cm}
\begin{flalign*}
	A&=\color{red}-3x\color{black}(4-7x)&\\
	A&=\color{red}-3x \color{black} \times 4 \color{red}-3x \color{black} \times (-7x)\\
	A&=-12x+21x^2
\end{flalign*}


\textbf{\underline{Exemple 2}}\\
\textbf{Développer et réduire les expressions suivantes.}\\ \vspace{-0.5cm}
\begin{minipage}{0.45\textwidth}
	\begin{flalign*}
		B&=(4+3x)(2+5x)&\\
		B&=4 \times 2 + 4 \times 5x + 3x \times 2 + 3x \times 5x\\
		B&=8+20x+6x+15x^2\\
		B&=8+26x+15x^2
	\end{flalign*}
\end{minipage}\vrule \hspace{0.3cm}
\begin{minipage}{0.45\textwidth}
	\begin{flalign*}
		C&=(-5x+2)(3-4x)&\\
		C&=-5x \times 3 -5x \times (-4x) + 2 \times 3 + 2 \times (-4x)\\
		C&=-15x+20x^2+6-8x\\
		C&=20x^2-23x+6
	\end{flalign*}
\end{minipage}


\color{Green}
\section{Identités remarquables}
\color{black}
\begin{tcolorbox}[colframe=blue!20, colback=blue!0, boxrule=2pt, arc=6pt, title={\textbf{Propriétés \arabic{chapter}.\directlua{tex.print(p)} - \arabic{chapter}.\directlua{tex.print(p+1)} - \arabic{chapter}.\directlua{tex.print(p+2)}}}, coltitle=black]
	\begin{multicols}{3}
		$\left(a+b\right)^2=a^2+2ab+b^2$\\
		$\left(a-b\right)^2=a^2-2ab+b^2$\\
		$(a+b)(a-b)=a^2-b^2$
	\end{multicols}
\end{tcolorbox}
\begin{luacode}
	p=p+3
\end{luacode}

\begin{tcolorbox}[colframe=VioletRed!20, colback=MediumPurple!0, boxrule=2pt, arc=6pt, title={\textbf{Démonstrations \arabic{chapter}.\directlua{tex.print(m+1)} - \arabic{chapter}.\directlua{tex.print(m+2)} - \arabic{chapter}.\directlua{tex.print(m+3)}}}, coltitle=black]
	\begin{minipage}{0.4\textwidth}
	\begin{flalign*}
		\left(a+b\right)^2&=(a+b)(a+b)&\\
		&=a \times a + a \times b + b \times a + b \times b\\
		&=a^2+ab+ab+b^2\\
		&=a^2+2ab+b^2
	\end{flalign*}
	\end{minipage} \hspace{0.1cm}\vrule\hspace{0.1cm}
	\begin{minipage}{0.5\textwidth}
	\begin{flalign*}
		\left(a-b\right)^2&=(a-b)(a-b)&\\
		&=a \times a - a \times b -b \times a -b \times (-b)\\
		&=a^2-ab-ab+b^2\\
		&=a^2-2ab+b^2
	\end{flalign*}
	\end{minipage}\\
	\begin{flalign*}
		(a+b)(a-b)&=a \times a + a \times (-b) + b \times a + b \times (-b)&\\
		&=a^2-ab+ab-b^2\\
		&=a^2-b^2
	\end{flalign*}
\end{tcolorbox}
\begin{luacode}
	m=m+4
\end{luacode}


\textbf{\underline{Exemples}}\\
\textbf{Développer et réduire les expressions suivantes en utilisant les identités remarquables.}\\
\begin{minipage}{0.33\textwidth}
	\begin{flalign*}
		E&=\left(3x+5\right)^2&\\
		E&=(3x)^2+2 \times 3x \times 5 + 5^2\\
		E&=9x^2+30x+25
	\end{flalign*}
\end{minipage}\vrule \hspace{0.3cm}
\begin{minipage}{0.33\textwidth}
	\begin{flalign*}
		F&=(7x-2)^2&\\
		F&=(7x)^2-2 \times 7x \times 2 +2^2\\
		F&=49x^2-28x+4
	\end{flalign*}
\end{minipage}\vrule \hspace{0.3cm}
\begin{minipage}{0.33\textwidth}
	\begin{flalign*}
		G&=(6x-9)(6x+9)&\\
		G&=(6x)^2-9^2\\
		G&=36x^2-81
	\end{flalign*}
\end{minipage}

\color{Green}
\section{Factorisation}
\color{black}

\begin{tcolorbox}[colframe=Green!20, colback=Green!0, boxrule=2pt, arc=6pt, title={\textbf{Définition \arabic{chapter}.\directlua{tex.print(d)}}}, coltitle=black]
Factoriser permet de transformer une somme (ou une différence) en produit.
\end{tcolorbox}
\begin{luacode}
	d=d+1
\end{luacode}

\begin{tcolorbox}[colframe=blue!20, colback=blue!0, boxrule=2pt, arc=6pt, title={\textbf{Propriétés \arabic{chapter}.\directlua{tex.print(p)} - \arabic{chapter}.\directlua{tex.print(p+1)}}}, coltitle=black]
	\begin{itemize}[label=\textbullet]
		\item Soit $k$, $a$ et $b$ trois nombres quelconques.\\
		$\textcolor{Crimson}{k} \times \textcolor{MediumSeaGreen}{a\,+\,} \textcolor{Crimson}{k} \times \textcolor{MediumSeaGreen}{b} = \textcolor{Crimson}{k}(\textcolor{MediumSeaGreen}{a+b})$
		\item Soit $a$, $b$, $c$, $d$, $e$ et $f$ six nombres quelconques.\\
		$\textcolor{Crimson}{(a+b)}\textcolor{MediumSeaGreen}{(c+d)} \textcolor{MediumSeaGreen}{\,+\,} \textcolor{MediumSeaGreen}{(e+f)}\textcolor{Crimson}{(a+b)}=\textcolor{Crimson}{(a+b)}(\textcolor{MediumSeaGreen}{(c+d)+(e+f)})$
	\end{itemize}
\end{tcolorbox}
\begin{luacode}
	p=p+2
\end{luacode}

\textbf{\underline{Remarque}}\\
Les nombres $\textcolor{Crimson}{k}$ et $\textcolor{Crimson}{(a+b)}$ sont appelés facteurs communs.\\


\textbf{\underline{Exemples}}\\
\textbf{1)} Factoriser les expressions suivantes.
\begin{enumerate}[label=\textbf{\alph*)}]
	\begin{multicols}{2}
		\item $C=(3x+5)-(4x+2)(3x+5)$
		\item $D=(7x+2)(5x-3)+(5x-3)(-9x-7)$
	\end{multicols}
\end{enumerate}

\begin{minipage}{0.45\textwidth}
	\begin{flalign*}
		C&=(3x+5)-(4x+2)(3x+5)&\\
		C&=\textcolor{Crimson}{(3x+5)} \times \textcolor{MediumSeaGreen}{1\,-\,} \textcolor{Crimson}{(3x+5)}\textcolor{MediumSeaGreen}{(4x+2)}\\
		C&=\textcolor{Crimson}{(3x+5)}(\textcolor{MediumSeaGreen}{1-(4x+2)})\\
		C&=\textcolor{Crimson}{(3x+5)}(\textcolor{MediumSeaGreen}{1-4x-2})\\
		C&=\textcolor{Crimson}{(3x+5)}(\textcolor{MediumSeaGreen}{-4x-1)})
	\end{flalign*}
\end{minipage} \vrule \hspace{0.3cm}
\begin{minipage}{0.45\textwidth}
	\begin{flalign*}
		D&=\textcolor{MediumSeaGreen}{(7x+2)}\textcolor{Crimson}{(5x-3)}+\textcolor{Crimson}{(5x-3)}\textcolor{MediumSeaGreen}{(-9x-7)}&\\
		D&=\textcolor{Crimson}{(5x-3)}(\textcolor{MediumSeaGreen}{(7x+2)+(-9x-7)})\\
		D&=\textcolor{Crimson}{(5x-3)}(\textcolor{MediumSeaGreen}{7x+2-9x-7})\\
		D&=\textcolor{Crimson}{(5x-3)}(\textcolor{MediumSeaGreen}{-2x-5})
	\end{flalign*}
\end{minipage}
~~\\\\\\
\textbf{2)} Factoriser les expressions suivantes en utilisant les identités remarquables.
\begin{enumerate}[label=\textbf{\alph*)}]
	\begin{multicols}{3}
		\item $H=4x^2+12x+9$
		\item $I=81x^2-90x+25$
		\item $J=49-36x^2$
	\end{multicols}
\end{enumerate}
\begin{minipage}{0.33\textwidth}
	\begin{flalign*}
		H&=\textcolor{MediumSeaGreen}{a}^2 \textcolor{Magenta}{\,+\,} 2\textcolor{MediumSeaGreen}{a}\textcolor{Crimson}{b} + \textcolor{Crimson}{b}^2&\\
		H&=4x^2 \textcolor{Magenta}{\,+\,} 12x + 9\\
		H&=(\textcolor{MediumSeaGreen}{2x})^2 \textcolor{Magenta}{\,+\,} 2 \times \textcolor{MediumSeaGreen}{2x} \times \textcolor{Crimson}{3} + \textcolor{Crimson}{3}^2\\
		H&=(\textcolor{MediumSeaGreen}{2x} \textcolor{Magenta}{\,+\,} \textcolor{Crimson}{3})^2
	\end{flalign*}
\end{minipage}\vrule \hspace{0.3cm}
\begin{minipage}{0.33\textwidth}
	\begin{flalign*}
		I&=\textcolor{MediumSeaGreen}{a}^2 \textcolor{Magenta}{\,-\,} 2\textcolor{MediumSeaGreen}{a}\textcolor{Crimson}{b} + \textcolor{Crimson}{b}^2&\\
		I&=81x^2 \textcolor{Magenta}{\,-\,} 90x+25&\\
		I&=(\textcolor{MediumSeaGreen}{9x})^2 \textcolor{Magenta}{\,-\,} 2 \times \textcolor{MediumSeaGreen}{9x} \times \textcolor{Crimson}{5} + \textcolor{Crimson}{5}^2\\
		I&=(\textcolor{MediumSeaGreen}{9x} \textcolor{Magenta}{\,-\,} \textcolor{Crimson}{5})^2
	\end{flalign*}
\end{minipage}\vrule \hspace{0.3cm}
\begin{minipage}{0.33\textwidth}
	\begin{flalign*}
		J&=\textcolor{MediumSeaGreen}{a}^2 \textcolor{Magenta}{\,-\,} \textcolor{Crimson}{b}^2&\\
		J&=49 \textcolor{Magenta}{\,-\,} 36x^2\\
		J&=\textcolor{MediumSeaGreen}{7}^2 \textcolor{Magenta}{\,-\,} (\textcolor{Crimson}{6x})^2\\
		J&=(\textcolor{MediumSeaGreen}{7}-\textcolor{Crimson}{6x})(\textcolor{MediumSeaGreen}{7}+\textcolor{Crimson}{6x})
	\end{flalign*}
\end{minipage}



\color{Green}
\section{Montrer que deux expressions littérales sont égales}
\color{black}

\begin{tcolorbox}[colframe=Green!20, colback=Green!0, boxrule=2pt, arc=6pt, title={\textbf{Définition \arabic{chapter}.\directlua{tex.print(d)}}}, coltitle=black]
	On dit que deux expressions littérales sont égales si elles le sont pour toutes les valeurs de $x$.
\end{tcolorbox}
\begin{luacode}
	d=d+1
\end{luacode}

\textbf{\underline{Exemple}}\\
Soit $A=(2x+3)(5-x)+10$ et $B=47+x(7-2x)-22$. Montrer que $A=B$ quelque soit la valeur de $x$ choisie.\\

On développe et on réduit chacune des expressions littérales, puis on les compare termes à termes.\\
\begin{minipage}{0.45\textwidth}
	\begin{flalign*}
		A&=(2x+3)(5-x)+10&\\
		A&=10x-2x^2+15-3x+10\\
		A&=-2x^2+7x+25
	\end{flalign*}
\end{minipage} \hspace{0.05cm} \vrule \hspace{0.05cm}
\begin{minipage}{0.45\textwidth}
	\begin{flalign*}
		B&=47+x(7-2x)-22&\\
		B&=47+7x-2x^2-22\\
		B&=-2x^2+7x+25
	\end{flalign*}
\end{minipage}
~~\\

On remarque que $A=B$. Les deux expressions littérales sont bien égales quelque soit la valeur de $x$ choisie.

\begin{tcolorbox}[colframe=blue!20, colback=blue!0, boxrule=2pt, arc=6pt, title={\textbf{Propriété \arabic{chapter}.\directlua{tex.print(p)}}}, coltitle=black]
	Pour montrer que deux expressions littérales ne sont pas égales, il suffit de trouver un contre-exemple.
\end{tcolorbox}
\begin{luacode}
	p=p+2
\end{luacode}

\textbf{\underline{Exemple}}\\
Soit $K=(x-1)(2x+5)$ et $L=(-x+1)(x-1)$. Ces deux expressions littérales sont-elles égales ?\\

On choisit arbitrairement $x=0$. On calcule chacune des expressions avec la valeur de $x$ choisie puis on compare les résultats obtenus.\\
\begin{minipage}{0.45\textwidth}
	\begin{flalign*}
		K&=(0-1)(2 \times 0 +5)&\\
		K&=-1 \times 5\\
		K&=-5
	\end{flalign*}
\end{minipage} \vrule \hspace{0.3cm}
\begin{minipage}{0.5\textwidth}	
	\begin{flalign*}
		L&=(0+1)(0-1)&\\
		L&=1 \times (-1)\\
		L&=-1
	\end{flalign*}
\end{minipage}
~~\\
Pour $x=0$, on a : $K \neq L$ donc les deux expressions littérales ne sont pas égales.\\

\textbf{\underline{Remarque}}\\
\textcolor{Crimson}{\faExclamationTriangle}\,Ce n'est pas parce que deux expressions littérales sont égales pour une valeur de $x$ choisie, qu'elles le sont pour toutes les valeurs de $x$.

\color{Green}
\section{Bilan}
\color{black}

\textbf{\underline{Rappels}} : QCM (une seule réponse possible)\\\footnotesize{}
\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom= , AlphT, Largeur=3cm]{%
$7(3x-2)$ est égal à \pointilles[1cm] &$21x-2$ & $21x-14$ & $10x-9$ &2,%
$5x+2x$ est égal à \pointilles[1cm] & $7x$ & $7x^2$ & $14x$ &1,%
$3x+18$ est égal à \pointilles[1cm] & $3(x+6)$ & $21x$ &$3(x+18)$ &1,%
$(x+3)(x-2)$ est égal à \pointilles[1cm] & $x^2+x-6$ & $x^2+5x+6$ & $2x-6$ &1,%
$(x+5)^2$ est égal à \pointilles[1cm] &  $x^2+25$ &$x^2+5x+25$ & $x^2+10x+25$ &3,%
$(x-4)^2$ est égal à \pointilles[1cm] & $x^2-16$ & $x^2-8x+16$ & $x^2-8x-16$ &2,%
$(x-3)(x+3)$ est égal à \pointilles[1cm] & $x^2-9$ & $x^2-6x+9$ & $2x-9$ &1,%
$4x^2-49$ est égal à \pointilles[1cm] & $(2x-7)^2$ & $4(x^2-9)$ &$(2x+7)(2x-7)$ &3,%
$(x-3)(2x+7)-(4x+9)(2x+7)$ est égal à \pointilles[1cm] & $(2x+7)(-3x+6)$ & $(2x+7)(-3x-12)$ & $(2x+7)(5x+6)$ &2%
}
\normalsize{}

\color{Green}
\section{Auto-évaluation}
\color{black}

\begin{tabular}{|>{\columncolor{darkgray}\color{black}}m{8cm} |>{\color{black}}m{0.6cm}|m{0.6cm}|m{0.6cm}|m{5cm}|} \hline

\rowcolor{Lavender}\centering{} \color{black} Compétences / Savoir-faire & \centering{}\color{MediumSeaGreen} \faCheck & \centering{}\color{DarkOrange}\faWater & \centering{}\color{Crimson}\faTimes & \centering{}\color{black}Pour réviser \tabularnewline \hline \hline

\cellcolor{Lavender}Je suis capable d'utiliser la simple et double distributivités pour développer une expression littérale. & & & & \textbf{\underline{Leçon}} : Partie I \smallskip \newline  \textbf{\underline{Exercices}} :  \tabularnewline \hline \hline

\cellcolor{Lavender}Je connais les 3 identités remarquables et je sais les utiliser. & & & &\textbf{\underline{Leçon}} : Partie II \smallskip \newline  \textbf{\underline{Exercices}} :  \tabularnewline \hline\hline

\cellcolor{Lavender}Je suis capable de factoriser une expression littérale. & & & & \textbf{\underline{Leçon}} : Partie III \smallskip \newline  \textbf{\underline{Exercices}} :  \tabularnewline \hline\hline

\cellcolor{Lavender}Je suis capable de montrer que deux expressions littérales sont (ou ne sont pas) égales. & & & & \textbf{\underline{Leçon}} : Partie IV \smallskip \newline  \textbf{\underline{Exercices}} :  \tabularnewline \hline
\end{tabular}



%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------- Correction QCM -----------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\newpage
%\setcounter{page}{1}
\footnotesize{}
\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom= , AlphT, Largeur=3cm, Solution, Couleur=LightSkyBlue]{%
Pour $x=5\text{,}$ que vaut l'expression littérale $4x^2-3x+7$ ?  & $122$ & $12$ & $92$ &3,%
$3(x+7)=$ \pointilles[1cm] & $3x+7$ & $3x+21$ & $3x+10$ &2,%
Le plus grand facteur entier commun de $-8x^2+4x+2$ est \pointilles[1cm] & $2$ & $4$ & $8$ &1,%
$-4x(2x-5)=$ \pointilles[1cm] & $-8x^2+20x$ & $-8x^2-20x$ & $-8x^2+45x$ &1,%
$x^2-3x+2x^2-7x+5-12x^2=$ \pointilles[1cm] & $15x^2-10x+5$ & $-9x^2+10x+5$ & $-9x^2-10x+5$ &3%
}
\normalsize{}
\\
\\
\\
\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom= , AlphT, Largeur=3cm, Solution, Couleur=LightSkyBlue]{%
$7(3x-2)$ est égal à \pointilles[1cm] &$21x-2$ & $21x-14$ & $10x-9$ &2,%
$5x+2x$ est égal à \pointilles[1cm] & $7x$ & $7x^2$ & $14x$ &1,%
$3x+18$ est égal à \pointilles[1cm] & $3(x+6)$ & $21x$ &$3(x+18)$ &1,%
$(x+3)(x-2)$ est égal à \pointilles[1cm] & $x^2+x-6$ & $x^2+5x+6$ & $2x-6$ &1,%
$(x+5)^2$ est égal à \pointilles[1cm] &  $x^2+25$ &$x^2+5x+25$ & $x^2+10x+25$ &3,%
$(x-4)^2$ est égal à \pointilles[1cm] & $x^2-16$ & $x^2-8x+16$ & $x^2-8x-16$ &2,%
$(x-3)(x+3)$ est égal à \pointilles[1cm] & $x^2-9$ & $x^2-6x+9$ & $2x-9$ &1,%
$4x^2-49$ est égal à \pointilles[1cm] & $(2x-7)^2$ & $4(x^2-9)$ &$(2x+7)(2x-7)$ &3,%
$(x-3)(2x+7)-(4x+9)(2x+7)$ est égal à \pointilles[1cm] & $(2x+7)(-3x+6)$ & $(2x+7)(-3x-12)$ & $(2x+7)(5x+6)$ &2%
}
\normalsize{}
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%---------------------------------------------------------------------- Leçon à trous -----------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\newpage
%\setcounter{page}{1}


%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------- Impression -----------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\newpage
\textbf{\underline{Rappels}} : QCM (une seule réponse possible)\\\footnotesize{}
\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom= , AlphT, Largeur=3cm]{%
Pour $x=5\text{,}$ que vaut l'expression littérale $4x^2-3x+7$ ?  & $122$ & $12$ & $92$ &3,%
$3(x+7)=$ \pointilles[1cm] & $3x+7$ & $3x+21$ & $3x+10$ &2,%
Le plus grand facteur entier commun de $-8x^2+4x+2$ est \pointilles[1cm] & $2$ & $4$ & $8$ &1,%
$-4x(2x-5)=$ \pointilles[1cm] & $-8x^2+20x$ & $-8x^2-20x$ & $-8x^2+45x$ &1,%
$x^2-3x+2x^2-7x+5-12x^2=$ \pointilles[1cm] & $15x^2-10x+5$ & $-9x^2+10x+5$ & $-9x^2-10x+5$ &3%
}
\normalsize{}
\\\\\\\\
\textbf{\underline{Rappels}} : QCM (une seule réponse possible)\\\footnotesize{}
\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom= , AlphT, Largeur=3cm]{%
$7(3x-2)$ est égal à \pointilles[1cm] &$21x-2$ & $21x-14$ & $10x-9$ &2,%
$5x+2x$ est égal à \pointilles[1cm] & $7x$ & $7x^2$ & $14x$ &1,%
$3x+18$ est égal à \pointilles[1cm] & $3(x+6)$ & $21x$ &$3(x+18)$ &1,%
$(x+3)(x-2)$ est égal à \pointilles[1cm] & $x^2+x-6$ & $x^2+5x+6$ & $2x-6$ &1,%
$(x+5)^2$ est égal à \pointilles[1cm] &  $x^2+25$ &$x^2+5x+25$ & $x^2+10x+25$ &3,%
$(x-4)^2$ est égal à \pointilles[1cm] & $x^2-16$ & $x^2-8x+16$ & $x^2-8x-16$ &2,%
$(x-3)(x+3)$ est égal à \pointilles[1cm] & $x^2-9$ & $x^2-6x+9$ & $2x-9$ &1,%
$4x^2-49$ est égal à \pointilles[1cm] & $(2x-7)^2$ & $4(x^2-9)$ &$(2x+7)(2x-7)$ &3,%
$(x-3)(2x+7)-(4x+9)(2x+7)$ est égal à \pointilles[1cm] & $(2x+7)(-3x+6)$ & $(2x+7)(-3x-12)$ & $(2x+7)(5x+6)$ &2%
}
\normalsize{}
\\\\\\\\
\begin{tabular}{|>{\columncolor{darkgray}\color{black}}m{8cm} |>{\color{black}}m{0.6cm}|m{0.6cm}|m{0.6cm}|m{5cm}|} \hline

\rowcolor{Lavender}\centering{} \color{black} Compétences / Savoir-faire & \centering{}\color{MediumSeaGreen} \faCheck & \centering{}\color{DarkOrange}\faWater & \centering{}\color{Crimson}\faTimes & \centering{}\color{black}Pour réviser \tabularnewline \hline \hline

\cellcolor{Lavender}Je suis capable d'utiliser la simple et double distributivités pour développer une expression littérale. & & & & \textbf{\underline{Leçon}} : Partie I \smallskip \newline  \textbf{\underline{Exercices}} :  \tabularnewline \hline \hline

\cellcolor{Lavender}Je connais les 3 identités remarquables et je sais les utiliser. & & & &\textbf{\underline{Leçon}} : Partie II \smallskip \newline  \textbf{\underline{Exercices}} :  \tabularnewline \hline\hline

\cellcolor{Lavender}Je suis capable de factoriser une expression littérale. & & & & \textbf{\underline{Leçon}} : Partie III \smallskip \newline  \textbf{\underline{Exercices}} :  \tabularnewline \hline\hline

\cellcolor{Lavender}Je suis capable de montrer que deux expressions littérales sont (ou ne sont pas) égales. & & & & \textbf{\underline{Leçon}} : Partie IV \smallskip \newline  \textbf{\underline{Exercices}} :  \tabularnewline \hline
\end{tabular}


\newpage
\begin{tabular}{|>{\columncolor{darkgray}\color{black}}m{8cm} |>{\color{black}}m{0.6cm}|m{0.6cm}|m{0.6cm}|m{5cm}|} \hline

\rowcolor{Lavender}\centering{} \color{black} Compétences / Savoir-faire & \centering{}\color{MediumSeaGreen} \faCheck & \centering{}\color{DarkOrange}\faWater & \centering{}\color{Crimson}\faTimes & \centering{}\color{black}Pour réviser \tabularnewline \hline \hline

\cellcolor{Lavender}Je suis capable d'utiliser la simple et double distributivités pour développer une expression littérale. & & & & \textbf{\underline{Leçon}} : Partie I \smallskip \newline  \textbf{\underline{Exercices}} :  \tabularnewline \hline \hline

\cellcolor{Lavender}Je connais les 3 identités remarquables et je sais les utiliser. & & & &\textbf{\underline{Leçon}} : Partie II \smallskip \newline  \textbf{\underline{Exercices}} :  \tabularnewline \hline\hline

\cellcolor{Lavender}Je suis capable de factoriser une expression littérale. & & & & \textbf{\underline{Leçon}} : Partie III \smallskip \newline  \textbf{\underline{Exercices}} :  \tabularnewline \hline\hline

\cellcolor{Lavender}Je suis capable de montrer que deux expressions littérales sont (ou ne sont pas) égales. & & & & \textbf{\underline{Leçon}} : Partie IV \smallskip \newline  \textbf{\underline{Exercices}} :  \tabularnewline \hline
\end{tabular}
~~\\\\\\
\begin{tabular}{|>{\columncolor{darkgray}\color{black}}m{8cm} |>{\color{black}}m{0.6cm}|m{0.6cm}|m{0.6cm}|m{5cm}|} \hline

\rowcolor{Lavender}\centering{} \color{black} Compétences / Savoir-faire & \centering{}\color{MediumSeaGreen} \faCheck & \centering{}\color{DarkOrange}\faWater & \centering{}\color{Crimson}\faTimes & \centering{}\color{black}Pour réviser \tabularnewline \hline \hline

\cellcolor{Lavender}Je suis capable d'utiliser la simple et double distributivités pour développer une expression littérale. & & & & \textbf{\underline{Leçon}} : Partie I \smallskip \newline  \textbf{\underline{Exercices}} :  \tabularnewline \hline \hline

\cellcolor{Lavender}Je connais les 3 identités remarquables et je sais les utiliser. & & & &\textbf{\underline{Leçon}} : Partie II \smallskip \newline  \textbf{\underline{Exercices}} :  \tabularnewline \hline\hline

\cellcolor{Lavender}Je suis capable de factoriser une expression littérale. & & & & \textbf{\underline{Leçon}} : Partie III \smallskip \newline  \textbf{\underline{Exercices}} :  \tabularnewline \hline\hline

\cellcolor{Lavender}Je suis capable de montrer que deux expressions littérales sont (ou ne sont pas) égales. & & & & \textbf{\underline{Leçon}} : Partie IV \smallskip \newline  \textbf{\underline{Exercices}} :  \tabularnewline \hline
\end{tabular}
~~\\\\\\
\begin{tabular}{|>{\columncolor{darkgray}\color{black}}m{8cm} |>{\color{black}}m{0.6cm}|m{0.6cm}|m{0.6cm}|m{5cm}|} \hline

\rowcolor{Lavender}\centering{} \color{black} Compétences / Savoir-faire & \centering{}\color{MediumSeaGreen} \faCheck & \centering{}\color{DarkOrange}\faWater & \centering{}\color{Crimson}\faTimes & \centering{}\color{black}Pour réviser \tabularnewline \hline \hline

\cellcolor{Lavender}Je suis capable d'utiliser la simple et double distributivités pour développer une expression littérale. & & & & \textbf{\underline{Leçon}} : Partie I \smallskip \newline  \textbf{\underline{Exercices}} :  \tabularnewline \hline \hline

\cellcolor{Lavender}Je connais les 3 identités remarquables et je sais les utiliser. & & & &\textbf{\underline{Leçon}} : Partie II \smallskip \newline  \textbf{\underline{Exercices}} :  \tabularnewline \hline\hline

\cellcolor{Lavender}Je suis capable de factoriser une expression littérale. & & & & \textbf{\underline{Leçon}} : Partie III \smallskip \newline  \textbf{\underline{Exercices}} :  \tabularnewline \hline\hline

\cellcolor{Lavender}Je suis capable de montrer que deux expressions littérales sont (ou ne sont pas) égales. & & & & \textbf{\underline{Leçon}} : Partie IV \smallskip \newline  \textbf{\underline{Exercices}} :  \tabularnewline \hline
\end{tabular}
~~\\\\\\
\begin{tabular}{|>{\columncolor{darkgray}\color{black}}m{8cm} |>{\color{black}}m{0.6cm}|m{0.6cm}|m{0.6cm}|m{5cm}|} \hline

\rowcolor{Lavender}\centering{} \color{black} Compétences / Savoir-faire & \centering{}\color{MediumSeaGreen} \faCheck & \centering{}\color{DarkOrange}\faWater & \centering{}\color{Crimson}\faTimes & \centering{}\color{black}Pour réviser \tabularnewline \hline \hline

\cellcolor{Lavender}Je suis capable d'utiliser la simple et double distributivités pour développer une expression littérale. & & & & \textbf{\underline{Leçon}} : Partie I \smallskip \newline  \textbf{\underline{Exercices}} :  \tabularnewline \hline \hline

\cellcolor{Lavender}Je connais les 3 identités remarquables et je sais les utiliser. & & & &\textbf{\underline{Leçon}} : Partie II \smallskip \newline  \textbf{\underline{Exercices}} :  \tabularnewline \hline\hline

\cellcolor{Lavender}Je suis capable de factoriser une expression littérale. & & & & \textbf{\underline{Leçon}} : Partie III \smallskip \newline  \textbf{\underline{Exercices}} :  \tabularnewline \hline\hline

\cellcolor{Lavender}Je suis capable de montrer que deux expressions littérales sont (ou ne sont pas) égales. & & & & \textbf{\underline{Leçon}} : Partie IV \smallskip \newline  \textbf{\underline{Exercices}} :  \tabularnewline \hline
\end{tabular}

%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%--------------------------------------------------------------- Histoire des mathématiques -------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\newpage
\begin{tcolorbox}[colframe=black, colback=blue!10, boxrule=2pt, arc=6pt]
	\centering{}\LARGE{\textbf{Histoire des mathématiques \faBook}}
\end{tcolorbox}
\begin{tcolorbox}[colframe=black!30, colback=blue!0, boxrule=2pt, arc=6pt, title=?, coltitle=black]

\end{tcolorbox}

\begin{tcolorbox}[colframe=black!10, colback=blue!0, boxrule=1pt, arc=1pt, title=Quelques anecdotes, coltitle=black]

\end{tcolorbox}

\textbf{Pythagore ($\approx -580$ - $\approx -495$)} n'est pas seulement connu pour le théorème qui porte son nom. Les grecs décrivaient tous les calculs qu'ils faisaient de manière géométrique. Par exemple, $2^2$ pouvait s'écrire : \begin{tabular}{|ScSc|}\hline 
1&1 \tabularnewline
1&1 \tabularnewline \hline
\end{tabular}. Si on ajoute tous les $1$ de ce carré, on trouve $4$. On a donc bien $2^2=4$. Avec cette méthode, Pythagore avait commencé à esquisser un premier travail sur les identités remarquables. Par exemple, cherchons le résultat de $(3+5)^2$.\\\\
\begin{minipage}{0.45\textwidth}
\begin{center}
\begin{tabular}{|ccc|ccccc|} \hline
1 & 1 & 1 &   &   &   &   &   \tabularnewline
1 & 1 & 1 &   &   &   &   &   \tabularnewline
1 & 1 & 1 &   &   &   &   &   \tabularnewline \hline
  &   &   & 1 & 1 & 1 & 1 & 1 \tabularnewline
  &   &   & 1 & 1 & 1 & 1 & 1 \tabularnewline
  &   &   & 1 & 1 & 1 & 1 & 1 \tabularnewline   
  &   &   & 1 & 1 & 1 & 1 & 1 \tabularnewline
  &   &   & 1 & 1 & 1 & 1 & 1 \tabularnewline \hline
\end{tabular}
\end{center}
\end{minipage} \hspace{0.1cm} \vrule \hspace{0.1cm}
\begin{minipage}{0.45\textwidth}
\begin{center}
\begin{tabular}{|ccc|ccccc|} \hline
1 & 1 & 1 & \color{red}1 & \color{red}1 & \color{red}1 & \color{red}1 & \color{red}1 \tabularnewline
1 & 1 & 1 & \color{red}1 & \color{red}1 & \color{red}1 & \color{red}1 & \color{red}1 \tabularnewline
1 & 1 & 1 & \color{red}1 & \color{red}1 & \color{red}1 & \color{red}1 & \color{red}1 \tabularnewline \hline
\color{red}1 & \color{red}1 & \color{red}1 & 1 & 1 & 1 & 1 & 1 \tabularnewline
\color{red}1 & \color{red}1 & \color{red}1 & 1 & 1 & 1 & 1 & 1 \tabularnewline
\color{red}1 & \color{red}1 & \color{red}1 & 1 & 1 & 1 & 1 & 1 \tabularnewline   
\color{red}1 & \color{red}1 & \color{red}1 & 1 & 1 & 1 & 1 & 1 \tabularnewline
\color{red}1 & \color{red}1 & \color{red}1 & 1 & 1 & 1 & 1 & 1 \tabularnewline \hline
\end{tabular}
\end{center}
\end{minipage}\\\\\\
\begin{minipage}{0.45\textwidth}
Le carré en haut à gauche représente le nombre $3^2=9$. Le carré en bas à droite représente le nombre $5^2=25$.\\\\
Pour trouver le reste du calcul, il suffit de compléter les deux rectangles blancs avec des $1$.
\end{minipage}
\hspace{0.1cm} \vrule \hspace{0.1cm}
\begin{minipage}{0.45\textwidth}
Les deux rectangles représentent donc le calcul $3 \times 5$. On retrouve bien alors le résultat de l'identité remarquable :\\ $(3+5)^2=3^2+2 \times \color{red}3 \times 5\color{black} +5^2$.\\
Si on effectue le calcul, on retombe bien sur le carré de 8 :\\ $3^2+2 \times 3 \times 5 +5^2=9+30+25=64$.
\end{minipage}~~\\\\\\
\textbf{François Viète (1540 - 1603)} est un mathématicien français. C'est lui qui a mis en place la notation des inconnues par une lettre. Avant, les résolutions d'équations étaient résolues sous forme de phrases.\\\\
\textbf{René Descartes (1596 - 1650)} est un mathématicien, physicien et philosophe français. Il a poursuivi les travaux de François Viète au niveau de l'écriture en mettant en place la notation de puissance. Par conséquent, $xx$ devient $x^2$, $xxxx$ devient $x^4$,...\\\\
\textbf{Blaise Pascal (1623 - 1662)} est un mathématicien, physicien, philosophe français. Il est connu pour avoir créer la première machine à calculer à 19 ans : la pascaline. Cette première calculatrice permet de faire des additions et des soustractions. Blaise Pascal a également contribuer au calcul littéral avec son triangle qui permet de calculer les coefficients binomiaux.
\begin{center}
\begin{tabular}{|c|c|c|c|c|c|m{8cm}|} \hline
1 &   &   &   &   &   & \textcolor{white}. \tabularnewline \hline
1 & 1 &   &   &   &   & $(a+b)^1=a^1+b^1=a+b$ \tabularnewline \hline
1 & \color{red}2 & 1 &   &   &   & $(a+b)^2=a^2+\textcolor{red}2ab+b^2$ \tabularnewline \hline
1 & \color{red}3 & \color{blue}3 & 1 &   &   & $(a+b)^3=a^3+\textcolor{red}3a^2b+\textcolor{blue}3ab^2+b^3$ \tabularnewline \hline
1 & \textcolor{red}4 & \textcolor{blue}6 & \textcolor{Green}4 & 1 &   & $(a+b)^4=a^4+\textcolor{red}4a^3b+\textcolor{blue}6a^2b^2+\textcolor{Green}4ab^3+b^4$ \tabularnewline \hline
\color{white}1 & \color{white}5 & \color{white}10 & \color{white}10 & \color{white}5 & \color{white}1 &  $(a+b)^5=\color{white}a^5+5a^4b+10a^3b^2+10a^2b^3+5ab^4+b^5$ \tabularnewline \hline
\end{tabular}
\end{center}


\textbf{\underline{Sources}}
\begin{itemize}[label=\textbullet]
	\item Hauchecorne, B. et Suratteau, D. (2008), \textit{Des Mathématiciens de A à Z} ($3^e$ édition), Ellipses
	\item Byrne, O. (2022), \textit{The first six books of the elements of Euclid}, Taschen
	\item Houlou-Garcia, A. (2019), \textit{Mathematikos} (2\ieme\, tirage), Belles lettres
	\item Barbin, E. (2019), \textit{Faire des mathématiques avec l'histoire au lycée}, Ellipses
	\item Dahan-Dalmedico, A. et Peiffer, J. (1986), \textit{Une histoire des mathématiques : Routes et dédales}, Points
	\item Freiberger, M. et Thomas, R. (2018), \textit{Dans le secret des nombres}, Ekho
\end{itemize}

%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%-------------------------------------------------------------------------- Fiche de route ------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\newpage
\begin{tcolorbox}[colframe=black, colback=Khaki!50, boxrule=2pt, arc=6pt]
	\centering{}\LARGE{\textbf{Fiche de route}}
\end{tcolorbox}
\begin{itemize}[label=\textbullet]
	\item \color{Tomato} \textbf{Introduction} \color{black} : Titre, prérequis, objectifs, rappels
	\item \color{RoyalBlue}\underline{Leçon} \color{black} : Partie I : 
	\item \color{ForestGreen}\underline{Exercices}  \color{black} : 
	\item \color{RoyalBlue}\underline{Leçon}  \color{black} : Parties IV et V : Bilan + Auto-évaluation
	\item \color{ForestGreen}\underline{Exercices bilan}  \color{black} : 
	\item \color{Coral} \underline{Histoire des mathématiques}
\end{itemize}

%\setcounter{page}{4}


\end{document}
