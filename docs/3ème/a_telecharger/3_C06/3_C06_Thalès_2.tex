\documentclass[dvipsnames, a4paper, 12pt]{report}

\usepackage{lmodern} %police standard latin moderne
\usepackage[frenchb]{babel} %langue française
\usepackage[utf8]{inputenc} %pour l'utf-8
\usepackage[T1]{fontenc} %pour les césures et accentuations
\usepackage{geometry} %pour changer les marges d'un document
\usepackage{graphicx}
\usepackage{fancybox}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{fontawesome5}
\usepackage{ProfCollege}
\usepackage{luacode}
\usepackage{xlop}

\usepackage[explicit]{titlesec} %Permet de modifier les différentes parties, chapitres, sous-parties,...

\usepackage{colortbl} %permet de mettre des cellules de tableau en couleur

\usepackage{multicol}

\usepackage{cellspace} %pour définir les espaces en haut et en bas des cellules d'un tableau
\setlength{\cellspacebottomlimit}{5pt}
\setlength{\cellspacetoplimit}{5pt}
\setlength{\parindent}{0pt}

\usepackage{xcolor} %permet de mettre du texte en couleur

\usepackage{array} %gestion des tableaux
\usepackage{tcolorbox} %créer des boîtes de couleurs

\usepackage{hyperref} %permet de gérer le sommaire et la page de garde et les options du PDF

\geometry{top=2cm, bottom=2cm, right=2cm, left=2cm}   
\titlespacing{\chapter}{0pt}{0pt}{5pt}

\setcounter{chapter}{6}

\usepackage{lastpage}
\usepackage{fancyhdr} %définition des en-têtes et des bas de page
\fancyhf{}
\fancypagestyle{main}{
	\renewcommand\headrulewidth{5pt}
	\fancyhead[L]{3\ieme\, - Chapitre \arabic{chapter} - Réciproque et contraposée du théorème Thalès}
	\fancyhead[R]{Collège}
	\fancyfoot[C]{\thepage/\pageref{LastPage}}
	\fancyfoot[R]{\footnotesize{Anthony SALZE}}
	}
\pagestyle{main}

%gestion des entêtes
\makeatletter\@addtoreset{section}{chapter}\makeatother % RAZ des numéros de section après un chapitre
\renewcommand{\thepart}{\Alph{part} -} % Pour mettre des I, II, etc. aux parties
\renewcommand{\thesection}{\color{Green} \Roman{section} -}% Idem pour les sections et avoir le numéro de chapitre
\renewcommand{\thesubsection}{\arabic{subsection} -}

\begin{document}
%format du titre des chapitres placé après Sommaire sinon cela bugue
\titleformat{\chapter}[frame] {\normalfont \bfseries \large}{\textcolor{red} {\chaptertitlename\ \thechapter\,-\,#1}}{0pt}{\Huge}

\setcounter{chapter}{5}

\chapter{Réciproque et contraposée - Théorème de Thalès}
\begin{luacode}
	d=1
	p=1
	t=1
	m=1
\end{luacode}

{\color{blue}\underline{\textbf{Prérequis}}
\begin{itemize}
	\item Chapitre $4$ : Théorème de Thalès ;
	\item Proportionnalité ;
	\item \'Egalité de quotients.
\end{itemize}~~

\underline{\textbf{Objectif}}
\begin{itemize}
	\item Démontrer que deux droites sont (ne sont pas) parallèles en utilisant la réciproque (la contraposée) du théorème de Thalès.
\end{itemize}	
\color{black}
~~\\

Rappels : \url{https://bref.jeduque.net/teete7}

\normalsize{}
\color{Green}
\section{Réciproque et contraposée}
\color{black}
\begin{tcolorbox}[colframe=Crimson!20, colback=Crimson!0, boxrule=2pt, arc=6pt, title={\textbf{Théorème \arabic{chapter}.\directlua{tex.print(t)}}}, coltitle=black]
	\begin{minipage}{0.5\textwidth}
		\begin{center}
			\begin{Geometrie}[CoinHD={u*(5,4)}]
				pair A,B,C,M,N;
				B=u*(1,1);
				C=u*(4,1);
				A=u*(3,3);
				trace droite(A,B);
				trace droite(A,C);
				trace droite(B,C) dashed evenly withcolor red;
				M=projection(C,A,B);
				N=parallele(B,C,M) intersectionpoint droite(A,C);
				trace droite(M,N) dashed evenly withcolor blue;
				label.ulft(btex \textcolor{red}B etex, B);
				label.urt(btex \textcolor{red}C etex, C);
				label.top(btex \textcolor{Green}A etex, A);
				label.ulft(btex \textcolor{blue}M etex, M);
				label.urt(btex \textcolor{blue}N etex, N);
			\end{Geometrie}
		\end{center}
	\end{minipage}
	\begin{minipage}{0.5\textwidth}
		\begin{center}
			\begin{Geometrie}[CoinHD={u*(5,4)}]
				pair A,B,C,M,N;
				M=u*(1,1);
				A=u*(1.75,2);
				N=u*(0.75,2.5);
				trace droite(M,A);
				trace droite(N,A);
				trace droite(M,N) dashed evenly withcolor blue;
				C=perpendiculaire(M,N,M) intersectionpoint droite(N,A);
				B=parallele(M,N,C) intersectionpoint droite(A,M);
				trace droite(B,C) dashed evenly withcolor red;
				label.rt(btex \textcolor{red}B etex, B);
				label.urt(btex \textcolor{red}C etex, C);
				label.top(btex \textcolor{Green}A etex, A);
				label.ulft(btex \textcolor{blue}M etex, M);
				label.llft(btex \textcolor{blue}N etex, N);
			\end{Geometrie}
		\end{center}
	\end{minipage}
	Les figures ci-dessus représentent les deux configurations étudiées dans le chapitre $4$. On suppose connues les longueurs $AM$, $AN$, $AB$ et $AC$.\\\\
	En utilisant ces longueurs et \textbf{\underline{UNIQUEMENT}} celles-ci, on peut démontrer que les droites $(MN)$ et $(BC)$ sont (ne sont pas) parallèles.\\
	\begin{itemize}
		\item Si les rapports de longueurs $\dfrac{A\textcolor{blue}M}{A\textcolor{red}B}$ et $\dfrac{A\textcolor{blue}N}{A\textcolor{red}C}$ sont égaux alors d'après la \textbf{\underline{RÉCIPROQUE}} du théorème de Thalès, les droites $\textcolor{blue}{(MN)}$ et $\textcolor{red}{(BC)}$ sont parallèles.
		\item Si les rapports de longueurs $\dfrac{A\textcolor{blue}M}{A\textcolor{red}B}$ et $\dfrac{A\textcolor{blue}N}{A\textcolor{red}C}$ \textbf{\underline{NE}} sont \textbf{\underline{PAS}} égaux alors d'après la \textbf{\underline{CONTRAPOSÉE}} du théorème de Thalès, les droites $\textcolor{blue}{(MN)}$ et $\textcolor{red}{(BC)}$ \textbf{\underline{NE}} sont \textbf{\underline{PAS}} parallèles.
	\end{itemize}
\end{tcolorbox}
\begin{luacode}
	t=t+1
\end{luacode}

\textbf{\underline{Exemple 1 : Triangles emboîtés}}\\
\begin{minipage}{0.4\textwidth}
	\begin{center}
		\begin{Geometrie}[TypeTrace="MainLevee", CoinHD={u*(5,4)}]
				pair K,L,P,F,J;
				F=u*(1,0.75);
				J=u*(4,0.5);
				K=u*(3,3.25);
				trace droite(F,K);
				trace droite(J,K);
				trace droite(F,J);
				P=projection(F,J,K);
				L=parallele(F,J,P) intersectionpoint droite(F,K);
				trace droite(L,P);
				label.lrt(btex F etex, F);
				label.llft(btex J etex, J);
				label.top(btex K etex, K);
				label.llft(btex P etex, P);
				label.lrt(btex L etex, L);
				trace cotationmil(L,K,5mm,15,btex \footnotesize{}\Lg{3} etex);
				trace cotationmil(K,P,5mm,15,btex \footnotesize{}\Lg{4} etex);
				trace cotationmil(F,K,10mm,20,btex \footnotesize{}\Lg{4} etex);
				trace cotationmil(K,J,10mm,20,btex \footnotesize{}\Lg{6} etex);
			\end{Geometrie}
	\end{center}
\end{minipage} \hspace{0.05cm}
\begin{minipage}{0.6\textwidth}
\textbf{Les droites $(LP)$ et $(FJ)$ sont-elles parallèles ?}\\
\\
$\begin{cases}
	\dfrac{KL}{KF}=\dfrac{3}{4}=\dfrac{3\times 3}{4\times 3}=\dfrac{9}{12}\\\\
	\dfrac{KP}{KJ}=\dfrac{4}{6}=\dfrac{4\times 2}{6\times 2}=\dfrac{8}{12}
\end{cases}$\\
On remarque que $\dfrac{KL}{KF}\neq\dfrac{KP}{KJ}$, d'après la contraposée du théorème de Thalès, les droites $(LP)$ et $(FJ)$ ne sont pas parallèles.
\end{minipage}
~~
\\
\\
\textbf{\underline{Exemple 2 : Papillon / Sablier}}\\
\begin{minipage}{0.45\textwidth}
	\begin{center}
		\begin{Geometrie}[TypeTrace="MainLevee", CoinHD={u*(7.5,5)}]
			pair F,P,E,S,L;
			F=u*(1,0.5);
			E=u*(3,2);
			P=u*(0.75,2.5);
			trace droite(F,E);
			trace droite(P,E);
			trace droite(F,P);
			S=perpendiculaire(F,P,F) intersectionpoint droite(P,E);
			L=parallele(F,P,S) intersectionpoint droite(F,E);
			trace droite(L,S);
			trace appelation(P,E,3mm,btex \footnotesize{}\Lg{2} etex);
			trace appelation(E,S,-3mm,btex \footnotesize{}\Lg{2.5} etex);
			trace appelation(F,E,-3mm,btex \footnotesize{}\Lg{4} etex);
			trace appelation(E,L,3mm,btex \footnotesize{}\Lg{5} etex);
			label.ulft(btex L etex, L);
			label.urt(btex S etex, S);
			label.top(btex E etex, E);
			label.ulft(btex F etex, F);
			label.llft(btex P etex, P);
		\end{Geometrie}
	\end{center}
\end{minipage} \hspace{0.05cm}
\begin{minipage}{0.55\textwidth}
\textbf{Les droites $(PF)$ et $(LS)$ sont-elles parallèles ?}\\
\\
$\begin{cases}
	\dfrac{PE}{ES}=\dfrac{2}{2,5}=\dfrac{2\times 2}{2,5\times 2}=\dfrac{4}{5}\\\\
	\dfrac{FE}{EL}=\dfrac{4}{5}
\end{cases}$\\
On remarque que $\dfrac{PE}{ES}=\dfrac{FE}{EL}=\dfrac{4}{5}$, d'après la réciproque du théorème de Thalès, les droites $(PF)$ et $(LS)$ sont parallèles.
\end{minipage}


\color{Green}
\section{Auto-évaluation}
\color{black}

\begin{tabular}{|>{\columncolor{darkgray}\color{black}}m{8cm} |>{\color{black}}m{0.6cm}|m{0.6cm}|m{0.6cm}|m{5cm}|} \hline

\rowcolor{Lavender}\centering{} \color{black} \textbf{Compétences / Savoir-faire} & \centering{}\color{MediumSeaGreen} \faCheck & \centering{}\color{DarkOrange}\faWater & \centering{}\color{Crimson}\faTimes & \centering{}\color{black}Pour réviser \tabularnewline \hline \hline

\cellcolor{Lavender}Je suis capable de montrer que deux droites sont parallèles en utilisant la réciproque du théorème de Thalès. & & & & \textbf{\underline{Leçon}} : Partie I\smallskip \newline  \textbf{\underline{Exercices}} :  \tabularnewline \hline\hline

\cellcolor{Lavender}Je suis capable de montrer que deux droites ne sont pas parallèles en utilisant la contraposée du théorème de Thalès. & & & & \textbf{\underline{Leçon}} : Partie I\smallskip \newline  \textbf{\underline{Exercices}} :  \tabularnewline \hline
\end{tabular}

%\begin{comment}
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------- Correction QCM -----------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\newpage
\normalsize{}

%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%---------------------------------------------------------------------- Leçon à trous -----------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\newpage
\setcounter{chapter}{5}
\chapter{\pointilles[13cm]}
\begin{luacode}
	d=1
	p=1
	t=1
	m=1
\end{luacode}

{\color{blue}\underline{\textbf{Prérequis}}
\begin{itemize}
	\item \pointilles[16cm]
	\item \pointilles[16cm]
	\item \pointilles[16cm]
\end{itemize}~~

\underline{\textbf{Objectif}}
\begin{itemize}
	\item \pointilles[16cm]\\
	\pointilles[16cm]
\end{itemize}	
\color{black}
~~\\

Rappels : \url{https://bref.jeduque.net/teete7}

\normalsize{}
\color{Green}
\section{\pointilles[5cm]}
\color{black}
\begin{tcolorbox}[colframe=Crimson!20, colback=Crimson!0, boxrule=2pt, arc=6pt, title={\textbf{Théorème \arabic{chapter}.\directlua{tex.print(t)}}}, coltitle=black]
	\begin{minipage}{0.5\textwidth}
		\begin{center}
			\begin{Geometrie}[CoinHD={u*(5,4)}]
				pair A,B,C,M,N;
				B=u*(1,1);
				C=u*(4,1);
				A=u*(3,3);
				trace droite(A,B);
				trace droite(A,C);
				trace droite(B,C) dashed evenly withcolor red;
				M=projection(C,A,B);
				N=parallele(B,C,M) intersectionpoint droite(A,C);
				trace droite(M,N) dashed evenly withcolor blue;
				label.ulft(btex \textcolor{red}B etex, B);
				label.urt(btex \textcolor{red}C etex, C);
				label.top(btex \textcolor{Green}A etex, A);
				label.ulft(btex \textcolor{blue}M etex, M);
				label.urt(btex \textcolor{blue}N etex, N);
			\end{Geometrie}
		\end{center}
	\end{minipage}
	\begin{minipage}{0.5\textwidth}
		\begin{center}
			\begin{Geometrie}[CoinHD={u*(5,4)}]
				pair A,B,C,M,N;
				M=u*(1,1);
				A=u*(1.75,2);
				N=u*(0.75,2.5);
				trace droite(M,A);
				trace droite(N,A);
				trace droite(M,N) dashed evenly withcolor blue;
				C=perpendiculaire(M,N,M) intersectionpoint droite(N,A);
				B=parallele(M,N,C) intersectionpoint droite(A,M);
				trace droite(B,C) dashed evenly withcolor red;
				label.rt(btex \textcolor{red}B etex, B);
				label.urt(btex \textcolor{red}C etex, C);
				label.top(btex \textcolor{Green}A etex, A);
				label.ulft(btex \textcolor{blue}M etex, M);
				label.llft(btex \textcolor{blue}N etex, N);
			\end{Geometrie}
		\end{center}
	\end{minipage}
	~~\\
	\pointilles[16cm]\\
	\pointilles[16cm]\\
	\pointilles[16cm]\\
	\pointilles[16cm]\\
	\pointilles[16cm]\\
	\pointilles[16cm]\\
	\pointilles[16cm]\\
	\pointilles[16cm]\\
	\pointilles[16cm]\\
	\pointilles[16cm]\\
	\pointilles[16cm]\\
	\pointilles[16cm]\\
	\pointilles[16cm]\\
	\pointilles[16cm]
	
\end{tcolorbox}
\begin{luacode}
	t=t+1
\end{luacode}
\newpage
\textbf{\underline{Exemple 1 : Triangles emboîtés}}\\
\begin{minipage}{0.4\textwidth}
	\begin{center}
		\begin{Geometrie}[TypeTrace="MainLevee", CoinHD={u*(5,4)}]
				pair K,L,P,F,J;
				F=u*(1,0.75);
				J=u*(4,0.5);
				K=u*(3,3.25);
				trace droite(F,K);
				trace droite(J,K);
				trace droite(F,J);
				P=projection(F,J,K);
				L=parallele(F,J,P) intersectionpoint droite(F,K);
				trace droite(L,P);
				label.lrt(btex F etex, F);
				label.llft(btex J etex, J);
				label.top(btex K etex, K);
				label.llft(btex P etex, P);
				label.lrt(btex L etex, L);
				trace cotationmil(L,K,5mm,15,btex \footnotesize{}\Lg{3} etex);
				trace cotationmil(K,P,5mm,15,btex \footnotesize{}\Lg{4} etex);
				trace cotationmil(F,K,10mm,20,btex \footnotesize{}\Lg{4} etex);
				trace cotationmil(K,J,10mm,20,btex \footnotesize{}\Lg{6} etex);
			\end{Geometrie}
	\end{center}
\end{minipage} \hspace{0.05cm}
\begin{minipage}{0.6\textwidth}
\pointilles[10cm]\\
\pointilles[10cm]\\
\pointilles[10cm]\\
\pointilles[10cm]\\
\pointilles[10cm]\\
\pointilles[10cm]\\
\pointilles[10cm]\\
\pointilles[10cm]\\
\pointilles[10cm]
\end{minipage}
~~
\\
\\
\textbf{\underline{Exemple 2 : Papillon / Sablier}}\\
\begin{minipage}{0.45\textwidth}
	\begin{center}
		\begin{Geometrie}[TypeTrace="MainLevee", CoinHD={u*(7.5,5)}]
			pair F,P,E,S,L;
			F=u*(1,0.5);
			E=u*(3,2);
			P=u*(0.75,2.5);
			trace droite(F,E);
			trace droite(P,E);
			trace droite(F,P);
			S=perpendiculaire(F,P,F) intersectionpoint droite(P,E);
			L=parallele(F,P,S) intersectionpoint droite(F,E);
			trace droite(L,S);
			trace appelation(P,E,3mm,btex \footnotesize{}\Lg{2} etex);
			trace appelation(E,S,-3mm,btex \footnotesize{}\Lg{2.5} etex);
			trace appelation(F,E,-3mm,btex \footnotesize{}\Lg{4} etex);
			trace appelation(E,L,3mm,btex \footnotesize{}\Lg{5} etex);
			label.ulft(btex L etex, L);
			label.urt(btex S etex, S);
			label.top(btex E etex, E);
			label.ulft(btex F etex, F);
			label.llft(btex P etex, P);
		\end{Geometrie}
	\end{center}
\end{minipage} \hspace{0.05cm}
\begin{minipage}{0.55\textwidth}
\pointilles[9cm]\\
\pointilles[9cm]\\
\pointilles[9cm]\\
\pointilles[9cm]\\
\pointilles[9cm]\\
\pointilles[9cm]\\
\pointilles[9cm]\\
\pointilles[9cm]\\
\pointilles[9cm]\\
\pointilles[9cm]
\end{minipage}


\color{Green}
\section{Auto-évaluation}
\color{black}

\begin{tabular}{|>{\columncolor{darkgray}\color{black}}m{8cm} |>{\color{black}}m{0.6cm}|m{0.6cm}|m{0.6cm}|m{5cm}|} \hline

\rowcolor{Lavender}\centering{} \color{black} \textbf{Compétences / Savoir-faire} & \centering{}\color{MediumSeaGreen} \faCheck & \centering{}\color{DarkOrange}\faWater & \centering{}\color{Crimson}\faTimes & \centering{}\color{black}Pour réviser \tabularnewline \hline \hline

\cellcolor{Lavender}Je suis capable de montrer que deux droites sont parallèles en utilisant la réciproque du théorème de Thalès. & & & & \textbf{\underline{Leçon}} : Partie I\smallskip \newline  \textbf{\underline{Exercices}} :  \tabularnewline \hline\hline

\cellcolor{Lavender}Je suis capable de montrer que deux droites ne sont pas parallèles en utilisant la contraposée du théorème de Thalès. & & & & \textbf{\underline{Leçon}} : Partie I\smallskip \newline  \textbf{\underline{Exercices}} :  \tabularnewline \hline
\end{tabular}


%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------- Impression -----------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\newpage


%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%--------------------------------------------------------------- Histoire des mathématiques -------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\newpage
\begin{tcolorbox}[colframe=black, colback=blue!10, boxrule=2pt, arc=6pt]
	\centering{}\LARGE{\textbf{Histoire des mathématiques \faBook}}
\end{tcolorbox}
\begin{tcolorbox}[colframe=black!30, colback=blue!0, boxrule=2pt, arc=6pt, title=?, coltitle=black]
\textbf{Thalès de Milet} ($\approx -620$ ; $\approx -545$), est un philosophe et mathématicien grec, né à Milet (des vestiges de la ville sont encore visibles en Turquie). Il existe très peu d'écrits sur l'existence de Thalès. On sait qu'il a principalement travaillé sur la géométrie.\\
\\
On lui doit quelques propriétés étudiées au collège comme par exemple : \guillemotleft\,Deux angles opposés par le sommet sont égaux \guillemotright.\\

En France, le théorème de Thalès consiste à trouver une longueur manquante à partir des longueurs de deux triangles semblables. La légende voudrait que Thalès ait réussi à mesurer la hauteur de la Grande Pyramide d'\'Egypte grâce à ce théorème.\\
\begin{minipage}{0.7\textwidth}
Cependant, ce théorème n'est pas le même dans tous les pays. Par exemple, en Suisse ou en Allemagne, le théorème de Thalès est énoncé comme suit : \guillemotleft Si un triangle est inscrit dans un cercle et que son plus grand côté est un diamètre de ce cercle alors c'est un triangle rectangle.\,\guillemotright.
\end{minipage}
\begin{minipage}{0.3\textwidth}
	\includegraphics[width=5cm]{4_C08_Histoire.png}
\end{minipage}
\end{tcolorbox}

\begin{tcolorbox}[colframe=black!10, colback=blue!0, boxrule=1pt, arc=1pt, title=Quelques anecdotes, coltitle=black]

\end{tcolorbox}

\textbf{\underline{Sources}}
\begin{itemize}[label=\textbullet]
	\item Hauchecorne, B. et Suratteau, D. (2008), \textit{Des Mathématiciens de A à Z} ($3^e$ édition), Ellipses
	\item Byrne, O. (2022), \textit{The first six books of the elements of Euclid}, Taschen
	\item Houlou-Garcia, A. (2019), \textit{Mathematikos} (2\ieme\, tirage), Belles lettres
	\item Barbin, E. (2019), \textit{Faire des mathématiques avec l'histoire au lycée}, Ellipses
	\item Dahan-Dalmedico, A. et Peiffer, J. (1986), \textit{Une histoire des mathématiques : Routes et dédales}, Points
	\item Freiberger, M. et Thomas, R. (2018), \textit{Dans le secret des nombres}, Ekho
\end{itemize}

%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%-------------------------------------------------------------------------- Fiche de route ------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\newpage
\begin{tcolorbox}[colframe=black, colback=Khaki!50, boxrule=2pt, arc=6pt]
	\centering{}\LARGE{\textbf{Fiche de route}}
\end{tcolorbox}
\begin{itemize}[label=\textbullet]
	\item \color{Tomato} \textbf{Introduction} \color{black} : Titre, prérequis, objectifs, rappels
	\item \color{RoyalBlue}\underline{Leçon} \color{black} : Partie I : 
	\item \color{ForestGreen}\underline{Exercices}  \color{black} : 
	\item \color{RoyalBlue}\underline{Leçon}  \color{black} : Parties IV et V : Bilan + Auto-évaluation
	\item \color{ForestGreen}\underline{Exercices bilan}  \color{black} : 
	\item \color{Coral} \underline{Histoire des mathématiques}
\end{itemize}

\setcounter{page}{4}
%\end{comment}
\end{document}
