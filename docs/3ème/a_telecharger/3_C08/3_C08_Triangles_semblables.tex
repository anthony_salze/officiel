\documentclass[dvipsnames, a4paper, 12pt]{report}

\usepackage{lmodern} %police standard latin moderne
\usepackage[frenchb]{babel} %langue française
\usepackage[utf8]{inputenc} %pour l'utf-8
\usepackage[T1]{fontenc} %pour les césures et accentuations
\usepackage{geometry} %pour changer les marges d'un document
\usepackage{graphicx}
\usepackage{fancybox}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{fontawesome5}
\usepackage{ProfCollege}
\usepackage{luacode}
\usepackage{xlop}

\usepackage[explicit]{titlesec} %Permet de modifier les différentes parties, chapitres, sous-parties,...

\usepackage{colortbl} %permet de mettre des cellules de tableau en couleur

\usepackage{multicol}

\usepackage{cellspace} %pour définir les espaces en haut et en bas des cellules d'un tableau
\setlength{\cellspacebottomlimit}{5pt}
\setlength{\cellspacetoplimit}{5pt}
\setlength{\parindent}{0pt}

\usepackage{xcolor} %permet de mettre du texte en couleur

\usepackage{array} %gestion des tableaux
\usepackage{tcolorbox} %créer des boîtes de couleurs

\usepackage{hyperref} %permet de gérer le sommaire et la page de garde et les options du PDF

\geometry{top=2cm, bottom=2cm, right=2cm, left=2cm}   
\titlespacing{\chapter}{0pt}{0pt}{5pt}

\setcounter{chapter}{8}

\usepackage{lastpage}
\usepackage{fancyhdr} %définition des en-têtes et des bas de page
\fancyhf{}
\fancypagestyle{main}{
	\renewcommand\headrulewidth{5pt}
	\fancyhead[L]{3\ieme\, - Chapitre \arabic{chapter} - Triangles semblables}
	\fancyhead[R]{Collège}
	\fancyfoot[C]{\thepage/\pageref{LastPage}}
	\fancyfoot[R]{\footnotesize{Anthony SALZE}}
	}
\pagestyle{main}

%gestion des entêtes
\makeatletter\@addtoreset{section}{chapter}\makeatother % RAZ des numéros de section après un chapitre
\renewcommand{\thepart}{\Alph{part} -} % Pour mettre des I, II, etc. aux parties
\renewcommand{\thesection}{\color{Green} \Roman{section} -}% Idem pour les sections et avoir le numéro de chapitre
\renewcommand{\thesubsection}{\arabic{subsection} -}

\begin{document}
%format du titre des chapitres placé après Sommaire sinon cela bugue
\titleformat{\chapter}[frame] {\normalfont \bfseries \Large}{\textcolor{red} {\chaptertitlename\ \thechapter\,-\,#1}}{0pt}{\Huge}

\setcounter{chapter}{7}

\chapter{Triangles semblables}
\begin{luacode}
	d=1
	p=1
	t=1
	m=1
\end{luacode}

\color{blue}\underline{\textbf{Prérequis}}
\begin{itemize}
	\item Proportionnalité ;
	\item Propriétés des triangles ;
	\item Égalité de quotients.
\end{itemize}
~~\\
\underline{\textbf{Objectifs}}
\begin{itemize}
	\item Connaître la définition de triangles semblables ;
	\item Prouver que deux triangles sont semblables en utilisant les propriétés.
\end{itemize}
\color{black}
~~\\
\textbf{\underline{Rappels}}
\begin{enumerate}[label=\textbf{\arabic*)}]
	\item \textbf{Dans le triangle $ABC$ ci-dessous, calculer la mesure de l'angle $\widehat{ACB}$.}\\
	\begin{minipage}{0.45\textwidth}
		\begin{center}
			\begin{Geometrie}
				pair A,B,C,D,E;
				A=u*(1,1);
				B=u*(6,0.5);
				D=rotation(A,B,-48);
				E=rotation(B,A,37);
				C=demidroite(B,D) intersectionpoint demidroite(A,E);
				trace polygone(A,B,C);
				label.llft(btex A etex,A);
				label.lrt(btex B etex, B);
				label.top(btex C etex, C);
				trace Codeangle(B,A,C,0,btex \ang{37} etex);
				trace Codeangle(C,B,A,1,btex \ang{48} etex);
			\end{Geometrie}
		\end{center}
	\end{minipage}
	\begin{minipage}{0.45\textwidth}
		\begin{flalign*}
			\widehat{ACB}&=180^\circ-(\widehat{ABC}+\widehat{BAC})&\\
			&=180^\circ-(48^\circ+37^\circ)\\
			&=180^\circ-85^\circ\\
			&=95^\circ
		\end{flalign*}
	\end{minipage}
	\item \textbf{Le tableau ci-contre est-il un tableau de proportionnalité ?}
		\begin{tabular}{|c|c|} \hline
			$4,2$ & $7,7$ \tabularnewline \hline
			$21$ & $38,5$ \tabularnewline \hline
		\end{tabular}~~\\
		$\begin{cases}
			\dfrac{21}{4,2} =5\\
			\dfrac{38,5}{7,7}=5
		\end{cases}$
		On remarque que $\dfrac{21}{4,2}=\dfrac{38,5}{7,7}=5$.\\
		Donc ce tableau représente bien une situation de proportionnalité.
	\item \textbf{Les quotients $\dfrac{35}{28}$ et $\dfrac{47}{39}$ sont-ils égaux ?}\\
	On utilise la propriété du produit en croix.\\
	$\begin{cases}
		35 \times 39 = 1365\\
		28 \times 47=1316
	\end{cases}$
	Comme $35 \times 39 \neq 28 \times 47$ alors $\dfrac{35}{28} \neq \dfrac{47}{39}$.
\end{enumerate}

\color{Green}
\section{Triangles semblables et angles}
\color{black}
\normalsize{}
\begin{tcolorbox}[colframe=Green!20, colback=Green!0, boxrule=2pt, arc=6pt, title={\textbf{Définition \arabic{chapter}.\directlua{tex.print(d)}}}, coltitle=black]
	Des triangles semblables sont des triangles qui ont leurs angles deux à deux de même mesure. 
\end{tcolorbox}
\begin{luacode}
	d=d+1
\end{luacode}

\textbf{\underline{Exemple}}
\begin{center}
	\begin{Geometrie}
		%Triangle ABC
		pair A,B,C,D,E;
		A=u*(1,1);
		B=u*(6,1);
		D=rotation(A,B,-60);
		E=rotation(B,A,40);
		C=demidroite(B,D) intersectionpoint demidroite(A,E);
		trace polygone(A,B,C);
		label.llft(btex A etex,A);
		label.lrt(btex B etex, B);
		label.top(btex C etex, C);
		trace Codeangle(B,A,C,1,btex \ang{40} etex);
		trace Codeangle(C,B,A,0,btex \ang{60} etex);
		
		%Triangle IJK
		pair I,J,K,D,E;
		I=u*(7,0.5);
		J=u*(7.5,3);
		D=rotation(I,J,80);
		E=rotation(J,I,-40);
		K=demidroite(J,D) intersectionpoint demidroite(I,E);
		trace polygone(I,J,K);
		label.llft(btex I etex,I);
		label.ulft(btex J etex, J);
		label.rt(btex K etex, K);
		trace Codeangle(K,I,J,1,btex \ang{40} etex);
		trace Codeangle(I,J,K,0,btex \ang{80} etex);
	\end{Geometrie}
\end{center}
$\widehat{ACB}=180^\circ-(\widehat{CBA} + \widehat{CAB})=180^\circ-(60^\circ+40^\circ)=80^\circ=\widehat{IJK}$\\
Par un calcul similaire, on trouve que : $\widehat{JKI}=\widehat{CBA}=60^\circ$.\\
\begin{minipage}{0.35\textwidth}
	Ainsi, $\begin{cases}
		\widehat{ACB}=\widehat{IJK}=80^\circ\\
		\widehat{CBA}=\widehat{JKI}=60^\circ\\
		\widehat{CAB}=\widehat{JIK}=40^\circ
	\end{cases}$
\end{minipage}
\begin{minipage}{0.65\textwidth}
	Les triangles $ABC$ et $IJK$ ont leurs angles deux à deux de même mesure, donc ils sont semblables.
\end{minipage}

\begin{tcolorbox}[colframe=Green!20, colback=Green!0, boxrule=2pt, arc=6pt, title={\textbf{Définition \arabic{chapter}.\directlua{tex.print(d)}}}, coltitle=black]
	Lorsque deux triangles sont semblables :
\begin{itemize}
	\item les angles superposables sont dits homologues ;
	\item les sommets et les côtés de deux angles homologues sont dits homologues.
\end{itemize}
\end{tcolorbox}
\begin{luacode}
	d=d+1
\end{luacode}

\textbf{\underline{Exemple}}\\
Dans l'exemple précédent, on a démontré que les triangles $ABC$ et $IJK$ étaient semblables. On obtient alors le tableau suivant :
\begin{center}
	\begin{tabular}{|c|c|c|} \hline
		\textbf{Sommets homologues} & \textbf{Côtés homologues} & \textbf{Angles homologues} \tabularnewline \hline
		$A$ et $I$ & $[AB]$ et $[IK]$ & $\widehat{BAC}$ et $\widehat{KIJ}$ \tabularnewline \hline
		$B$ et $K$ & $[BC]$ et $[KJ]$ & $\widehat{ABC}$ et $\widehat{IKJ}$  \tabularnewline \hline
		$C$ et $J$ & $[AC]$ et $[IJ]$ & $\widehat{BCA}$ et $\widehat{KJI}$\tabularnewline \hline
	\end{tabular}
\end{center}

\color{Green}
\section{Triangles semblables et longueurs}
\color{black}
\normalsize{}
\begin{tcolorbox}[colframe=blue!20, colback=blue!0, boxrule=2pt, arc=6pt, title={\textbf{Propriété \arabic{chapter}.\directlua{tex.print(p)}}}, coltitle=black]
	\textbf{SI} deux triangles sont semblables,\\
	\textbf{ALORS} les longueurs de leurs côtés sont deux à deux proportionnelles.
\end{tcolorbox}
\begin{luacode}
	p=p+1
\end{luacode}

\textbf{\underline{Exemple}}\\
\begin{minipage}{0.55\textwidth}
	\begin{center}
		\begin{Geometrie}
		%Triangle ABC
		pair A,B,C,D,E;
		A=u*(1,1);
		B=u*(6,1);
		D=rotation(A,B,-60);
		E=rotation(B,A,40);
		C=demidroite(B,D) intersectionpoint demidroite(A,E);
		label.llft(btex A etex,A);
		label.lrt(btex B etex, B);
		label.top(btex C etex, C);
		trace segment(A,B) withcolor blue;
		trace segment(B,C) withcolor red;
		trace segment(A,C) withcolor Green;
		trace Codeangle(C,B,A,1,btex \ang{} etex);
		trace Codeangle(A,C,B,2,btex \ang{} etex);
		trace Codeangle(B,A,C,0,btex \ang{} etex);
		
		%Triangle IJK
		pair I,J,K,D,E;
		I=u*(7,0.5);
		J=u*(7.5,3);
		D=rotation(I,J,80);
		E=rotation(J,I,-40);
		K=demidroite(J,D) intersectionpoint demidroite(I,E);
		label.llft(btex I etex,I);
		label.ulft(btex J etex, J);
		label.rt(btex K etex, K);
		trace segment(I,K) withcolor blue;
		trace segment(K,J) withcolor red;
		trace segment(I,J) withcolor Green;
		trace Codeangle(J,K,I,1,btex \ang{} etex);
		trace Codeangle(I,J,K,2,btex \ang{} etex);
		trace Codeangle(K,I,J,0,btex \ang{} etex);
		\end{Geometrie}
	\end{center}
\end{minipage} 
\begin{minipage}{0.45\textwidth}
Les triangles $ABC$ et $IJK$ sont semblables. Les longueurs de leurs côtés homologues sont deux à deux proportionnelles.\\
On peut donc écrire que :
\[k=\dfrac{\color{red}BC}{\color{red}KJ}=\dfrac{\color{blue}AB}{\color{blue}IK}=\dfrac{\color{Green}AC}{\color{Green}IJ}\]
Le triangle $ABC$ est un agrandissement du triangle $IJK$ de rapport $k$.
\end{minipage}
~~\\

\begin{tcolorbox}[colframe=blue!20, colback=blue!0, boxrule=2pt, arc=6pt, title={\textbf{Propriété \arabic{chapter}.\directlua{tex.print(p)}}}, coltitle=black]
	\textbf{SI} les longueurs des côtés de deux triangles sont deux à deux proportionnelles,\\
	\textbf{ALORS} ils sont semblables.
\end{tcolorbox}
\begin{luacode}
	p=p+1
\end{luacode}

\textbf{\underline{Exemple}}\\
\begin{minipage}{0.45\textwidth}
	\begin{center}
		\begin{Geometrie}
		%Triangle ABC
		pair A,B,C;
		A=u*(1,1);
		B=u*(5,1);
		C=cercles(A,2u) intersectionpoint cercles(B,3u);
		label.llft(btex A etex,A);
		label.lrt(btex B etex, B);
		label.top(btex C etex, C);
		trace polygone(A,B,C);
		trace appelation(A,B,-3mm,btex $4$ etex);
		trace appelation(A,C, 3mm,btex $2$ etex);
		trace appelation(C,B, 3mm,btex $3$ etex);
		
		%Triangle IJK
		pair H,E,F;
		E=u*(6,0.75);
		F=u*(6.8,0.75);
		H=cercles(F,1.6u) intersectionpoint cercles(E,1.2u);
		label.top(btex H etex,H);
		label.lft(btex E etex, E);
		label.rt(btex F etex, F);
		trace polygone(H,E,F);
		trace appelation(E,F,-3mm,btex \footnotesize{}$0,8$ etex);
		trace appelation(E,H, 3mm,btex \footnotesize{}$1,2$ etex);
		trace appelation(H,F, 3mm,btex \footnotesize{}$1,6$ etex);
		\end{Geometrie}
	\end{center}
\end{minipage} 
\begin{minipage}{0.55\textwidth}
On classe dans l'ordre croissant les longueurs des côtés de chaque triangle dans un tableau :
\begin{center}
	\begin{tabular}{|c|c|c|c|} \hline
		$ABC$ & $AC=2$ & $BC=3$ & $AB=4$ \tabularnewline \hline
		$HEF$ & $EF=0,8$ & $HE=1,2$ & $HF=1,6$ \tabularnewline \hline
	\end{tabular}
\end{center}
Vérifions si ce tableau représente une situation de proportionnalité.
\end{minipage}
~~\\
On calcule :
\[\dfrac{EF}{AC}=\dfrac{0,8}{2}=0,4 \hspace{1cm} ; \hspace{1cm} \dfrac{HE}{BC}=\dfrac{1,2}{3}=0,4 \hspace{1cm} ; \hspace{1cm} \dfrac{HF}{AB}=\dfrac{1,6}{4}=0,4\]
On remarque que $\dfrac{EF}{AC}=\dfrac{HE}{BC}=\dfrac{HF}{AB}=0,4$. Donc les triangles $ABC$ et $HEF$ sont semblables.\\
Le triangle $HEF$ est une réduction du triangle $ABC$ de rapport $k_1=0,4$. Inversement, le triangle $ABC$ est un agrandissement de $HEF$ de rapport $k_2=\dfrac{1}{k_1}=\dfrac{1}{0,4}=2,5$. 

\color{Green}
\section{Bilan}
\color{black}
QCM (une seule réponse possible)\\\footnotesize{}
\textit{Pour la question $1$, il faut utiliser les figures ci-dessous.}\\
\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom= , AlphT, Largeur=3cm]{%
Un triangle semblable au triangle $ABC$ est \ldots  & $EFG$ & $HIJ$ & $LKM$ &3,%
$IJK$ est un triangle isocèle en $I$ tel que $IJ=\Lg{5}$ et $JK=\Lg{4}$. $LMN$ est un triangle semblable à $IJK$ avec $J$ et $K$ homologues respectivement à $M$ et $N$. On sait que $MN=\Lg{14}$. Alors \ldots & $LM=\Lg{14}$ & $LM=\Lg{15}$ & $LM=\Lg{17.5}$ &3,%
$ABC$ est un triangle tel que $AB=\Lg{6}\text{,}\,BC=\Lg{5}$ et $CA=\Lg{9}$. Un triangle semblable à $ABC$ est le triangle \ldots & $JKL$ tel que : $\begin{cases} JK=\Lg{7.2} \\ KL=\Lg{6} \\ LJ=\Lg{9.9} \end{cases}$ & $GHI$ tel que : $\begin{cases} GH=\Lg{12.6} \\ HI=\Lg{8.4} \\ IG=\Lg{7} \end{cases}$ & $DEF$ tel que : $\begin{cases} DE=\Lg{8}\\ EF=\Lg{7}\\ DF=\Lg{11} \end{cases}$ &2,%
$FAR$ et $SUN$ sont deux triangles semblables tel que $\dfrac{FA}{UN}=\dfrac{RA}{US}=\dfrac{RF}{NS}$. Alors \ldots & $\widehat{FAR}=\widehat{SUN}$  & $\widehat{AFR}=\widehat{USN}$  & $\widehat{FRA}=\widehat{SNU}$ &1,%
}

\begin{center}
	\begin{Geometrie}[CoinHD={u*(18,5)}]
		%Réglages
		marque_a:=15;
		
		%Triangle ABC
		pair A,B,C,X,Y;
		A=u*(1,1);
		B=u*(4,1);
		X=rotation(A,B,-100);
		Y=rotation(B,A,30);
		C=demidroite(B,X) intersectionpoint demidroite(A,Y);
		label.llft(btex A etex,A);
		label.lrt(btex B etex, B);
		label.top(btex C etex, C);
		trace polygone(A,B,C);
		trace Codeangle(C,B,A,0,btex \ang{100} etex);
		trace Codeangle(A,C,B,0,btex \ang{50} etex);
		
		%Triangle EFG
		pair F,G,E,X,Y;
		F=u*(5,1.5);
		G=u*(8,2.5);
		X=rotation(F,G,-20);
		Y=rotation(G,F,60);
		E=demidroite(G,X) intersectionpoint demidroite(F,Y);
		label.llft(btex F etex,F);
		label.lrt(btex G etex, G);
		label.top(btex E etex, E);
		trace polygone(F,G,E);
		trace Codeangle(G,F,E,0,btex \ang{60} etex);
		trace Codeangle(E,G,F,0,btex \ang{20} etex);
		
		%Triangle IJH
		pair I,J,H,X,Y;
		J=u*(9,2);
		H=u*(12,0.5);
		X=rotation(J,H,-40);
		Y=rotation(H,J,50);
		I=demidroite(H,X) intersectionpoint demidroite(J,Y);
		label.llft(btex J etex,J);
		label.lrt(btex H etex, H);
		label.top(btex I etex, I);
		trace polygone(I,J,H);
		trace Codeangle(H,J,I,0,btex \ang{50} etex);
		trace Codeangle(I,H,J,0,btex \ang{40} etex);
		
		%Triangle MKL
		pair M,K,L,X,Y;
		K=u*(13.5,1.5);
		L=u*(16,2.5);
		X=rotation(L,K,100);
		Y=rotation(K,L,-30);
		M=demidroite(K,X) intersectionpoint demidroite(L,Y);
		label.bot(btex K etex,K);
		label.rt(btex L etex, L);
		label.ulft(btex M etex, M);
		trace polygone(K,L,M);
		trace Codeangle(L,K,M,0,btex \ang{100} etex);
		trace Codeangle(M,L,K,0,btex \ang{30} etex);
	\end{Geometrie}
\end{center}
\normalsize{}

\color{Green}
\section{Auto-évaluation}
\color{black}

\begin{tabular}{|>{\columncolor{darkgray}\color{black}}m{8cm} |>{\color{black}}m{0.6cm}|m{0.6cm}|m{0.6cm}|m{5cm}|} \hline

\rowcolor{Lavender}\centering{} \color{black} \textbf{Compétences / Savoir-faire} & \centering{}\color{MediumSeaGreen} \faCheck & \centering{}\color{DarkOrange}\faWater & \centering{}\color{Crimson}\faTimes & \centering{}\color{black}Pour réviser \tabularnewline \hline \hline

\cellcolor{Lavender}Je sais reconnaître les angles, sommets et côtés homologues de deux triangles semblables. & & & & \textbf{\underline{Leçon}} : Partie I \smallskip \newline  \textbf{\underline{Exercices}} : 1 - 6 - 7 \tabularnewline \hline\hline

\cellcolor{Lavender}Je suis capable de reconnaître des triangles semblables grâce à leurs mesures d'angles. & & & & \textbf{\underline{Leçon}} : Partie I\smallskip \newline  \textbf{\underline{Exercices}} : 2 - 3 - 4  \tabularnewline \hline \hline

\cellcolor{Lavender}Je suis capable de reconnaître des triangles semblables grâce à la longueur de leurs côtés. & & & & \textbf{\underline{Leçon}} : Partie II \smallskip \newline  \textbf{\underline{Exercices}} : 5 - 6 - 9 \tabularnewline \hline\hline

\cellcolor{Lavender}Je suis capable de trouver le coefficient de proportionnalité de deux triangles semblables. & & & & \textbf{\underline{Leçon}} : Parties I \& II \smallskip \newline  \textbf{\underline{Exercices}} :  \tabularnewline \hline \hline

\cellcolor{Lavender}Je suis capable de calculer les longueurs manquantes de deux triangles semblables grâce au coefficient de proportionnalité. & & & & \textbf{\underline{Leçon}} : Partie II \smallskip \newline  \textbf{\underline{Exercices}} : 7 - 8 \tabularnewline \hline
\end{tabular}


%\begin{comment}
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------- Correction QCM -----------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\newpage
%\setcounter{page}{1}
\footnotesize{}
\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom= , AlphT, Largeur=3cm, Solution, Couleur=LightSkyBlue]{%
.  & . & . & . &1%
}
\\
\\
\\
\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom= , AlphT, Largeur=3cm, Solution, Couleur=LightSkyBlue]{%
.  & . & . & . &2%
}
\normalsize{}
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%---------------------------------------------------------------------- Leçon à trous -----------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\newpage
%\setcounter{page}{1}


%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------- Impression -----------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\newpage

%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%--------------------------------------------------------------- Histoire des mathématiques -------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\newpage
\begin{tcolorbox}[colframe=black, colback=blue!10, boxrule=2pt, arc=6pt]
	\centering{}\LARGE{\textbf{Histoire des mathématiques \faBook}}
\end{tcolorbox}
\begin{tcolorbox}[colframe=black!30, colback=blue!0, boxrule=2pt, arc=6pt, title=?, coltitle=black]

\end{tcolorbox}

\begin{tcolorbox}[colframe=black!10, colback=blue!0, boxrule=1pt, arc=1pt, title=Quelques anecdotes, coltitle=black]

\end{tcolorbox}

\textbf{\underline{Sources}}
\begin{itemize}[label=\textbullet]
	\item Hauchecorne, B. et Suratteau, D. (2008), \textit{Des Mathématiciens de A à Z} ($3^e$ édition), Ellipses
	\item Byrne, O. (2022), \textit{The first six books of the elements of Euclid}, Taschen
	\item Houlou-Garcia, A. (2019), \textit{Mathematikos} (2\ieme\, tirage), Belles lettres
	\item Barbin, E. (2019), \textit{Faire des mathématiques avec l'histoire au lycée}, Ellipses
	\item Dahan-Dalmedico, A. et Peiffer, J. (1986), \textit{Une histoire des mathématiques : Routes et dédales}, Points
	\item Freiberger, M. et Thomas, R. (2018), \textit{Dans le secret des nombres}, Ekho
\end{itemize}

%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%-------------------------------------------------------------------------- Fiche de route ------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\newpage
\begin{tcolorbox}[colframe=black, colback=Khaki!50, boxrule=2pt, arc=6pt]
	\centering{}\LARGE{\textbf{Fiche de route}}
\end{tcolorbox}
\begin{itemize}[label=\textbullet]
	\item \color{Tomato} \textbf{Introduction} \color{black} : Titre, prérequis, objectifs, rappels
	\item \color{RoyalBlue}\underline{Leçon} \color{black} : Partie I : 
	\item \color{ForestGreen}\underline{Exercices}  \color{black} : 
	\item \color{RoyalBlue}\underline{Leçon}  \color{black} : Parties IV et V : Bilan + Auto-évaluation
	\item \color{ForestGreen}\underline{Exercices bilan}  \color{black} : 
	\item \color{Coral} \underline{Histoire des mathématiques}
\end{itemize}

%\setcounter{page}{4}

%\end{comment}
\end{document}