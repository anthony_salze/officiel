---
author: Anthony Salze
title: Leçons et exercices
hide :
    -footer
---
# <span style="color:Tomato"> Leçons et exercices

## <span style="color:LightSalmon">00 : Rappels de 4ème
???+ info "Contenu"
    - Nombres relatifs : additions, soustractions, multiplications et divisions
    - Fractions : additions, soustractions, multiplications, divisions
    - Puissances de 10, exposants positif et négatif
    - Calcul littéral, réduire, développer (simple et double distributivité), factorisation
    - Résolution d'équations
    - Pythagore : théorème, réciproque et contraposée
    - Périmètres, aires, volumes : cercle, disque, polygones usuels, solides usuels

??? tip "Fichiers à télécharger <span style=color:red> :material-file-pdf-box: <span style=color:blue>:material-text-box-edit-outline: "
    === "Fiches"
         - [Fiche 01 : Nombres relatifs](a_telecharger/3_C00/3_C00_1_Nombres_relatifs.pdf){:target="_blank"} 
         - [Fiche 02 : Fractions](a_telecharger/3_C00/3_C00_2_Fractions.pdf){:target="_blank"} 
         - [Fiche 03a : Puissances](a_telecharger/3_C00/3_C00_3_Puissances.pdf){:target="_blank"} 
         - [Fiche 03b : Puissances - Exercices](a_telecharger/3_C00/3_C00_3_Exercices.pdf){:target="_blank"}
         - [Fiche 04 : Calcul littéral](a_telecharger/3_C00/3_C00_4_Calcul_littéral.pdf){:target="_blank"} 
         - [Fiche 05 : Équations](a_telecharger/3_C00/3_C00_5_Equations.pdf){:target="_blank"} 
         - [Fiche 06 : Pythagore](a_telecharger/3_C00/3_C00_6_Pythagore.pdf){:target="_blank"} 
         - [Fiche 07 : Périmètres, aires, volumes](a_telecharger/3_C00/3_C00_7_Aires_périmètres_volumes.pdf){:target="_blank"}
    === "Corrections"
         - [Fiche 01 : Nombres relatifs](a_telecharger/3_C00/3_C00_1_Nombres_relatifs_correction.pdf){:target="_blank"}
         - [Fiche 02 : Fractions](a_telecharger/3_C00/3_C00_2_Fractions_correction.pdf){:target="_blank"} 
         - [Fiche 03a : Puissances](a_telecharger/3_C00/3_C00_3_Puissances_correction.pdf){:target="_blank"}
         - [Fiche 03b : Puissances - Exercices](a_telecharger/3_C00/3_C00_3_Exercices_correction.pdf){:target="_blank"}
         - [Fiche 04 : Calcul littéral](a_telecharger/3_C00/3_C00_4_Calcul_littéral_correction.pdf){:target="_blank"} 
         - [Fiche 05 : Équations](a_telecharger/3_C00/3_C00_5_Equations_correction.pdf){:target="_blank"}
         - [Fiche 06 : Pythagore](a_telecharger/3_C00/3_C00_6_Pythagore.pdf){:target="_blank"}
         - [Fiche 07 : Périmètres, aires et volumes](a_telecharger/3_C00/3_C00_7_Aires_périmètres_volumes_correction.pdf){:target="_blank"}
    === "LaTeX"
         - [Fiche 01 : Nombres relatifs](a_telecharger/3_C00/3_C00_1_Nombres_relatifs.tex) 
         - [Fiche 02 : Fractions](a_telecharger/3_C00/3_C00_2_Fractions.tex) 
         - [Fiche 03a : Puissances](a_telecharger/3_C00/3_C00_3_Puissances.tex) 
         - [Fiche 03b : Puissances - Exercices](a_telecharger/3_C00/3_C00_3_Exercices.tex) 
         - [Fiche 04 : Calcul littéral](a_telecharger/3_C00/3_C00_4_Calcul_littéral.tex) 
         - [Fiche 05 : Équations](a_telecharger/3_C00/3_C00_5_Equations.tex) 
         - [Fiche 06 : Pythagore](a_telecharger/3_C00/3_C00_6_Pythagore.tex) 
         - [Fiche 07 : Périmètres, aires, volumes](a_telecharger/3_C00/3_C00_7_Aires_périmètres_volumes.tex) 
<span style=color:CornflowerBlue>_Dernière mise à jour : le 27/09/24_


## <span style="color:LightSalmon">Chapitre 01 : Calcul littéral
???+ info "Contenu"
    - Simple, double distributivité ;
    - Identités remarquables ;
    - Factorisation ;
    - Montrer que deux expressions littérales sont (ou ne sont pas) égales.

??? tip "Fichiers à télécharger <span style=color:red> :material-file-pdf-box: <span style=color:blue>:material-text-box-edit-outline: "
    === "Leçon"
        [Leçon : 01 - Calcul littéral](a_telecharger/3_C01/3_01_Calcul_Littéral.pdf){:target="_blank"}  
    === "Exercices"
        [Exercices](a_telecharger/3_C01/3_C01_Exercices_énoncé.pdf){:target="_blank"}  
    === "Exercices + correction"
        [Exercices + correction](a_telecharger/3_C01/3_C01_Exercices_correction.pdf){:target="_blank"}
    === "LaTeX"
        - [Leçon](a_telecharger/3_C01/3_01_Calcul_Littéral.tex) 
        - [Leçon Beamer (à venir)](a_telecharger/3_C01/3_C01_Calcul.tex) 
        - [Exercices](a_telecharger/3_C01/3_C01_Exercices.tex) 
    <span style=color:CornflowerBlue>_Dernière mise à jour : le 14/02/25_



## <span style="color:LightSalmon">Chapitre 02 : Homothéties
???+ info "Contenu"
    - Rappels sur les transformations du plan : symétries, translation et rotation ;
    - Nouvelle transformation : homothéties ;
    - Agrandissement - réduction.

??? tip "Fichiers à télécharger <span style=color:red> :material-file-pdf-box: <span style=color:blue>:material-text-box-edit-outline: "
    === "Leçon"
        [Leçon : 02 - Homothéties](a_telecharger/3_C02/3_02_Homothétie.pdf){:target="_blank"}  
    === "Exercices"
        [Exercices](a_telecharger/3_C02/3_C02_Exercices_énoncé.pdf){:target="_blank"}  
    === "Exercices + correction"
        [Exercices + correction](a_telecharger/3_C02/3_C02_Exercices_correction.pdf){:target="_blank"}
    === "LaTeX"
        - [Leçon](a_telecharger/3_C02/3_02_Homothétie.tex) 
        - [Leçon Beamer (à venir)](a_telecharger/3_C01/3_C01_Calcul.tex) 
        - [Exercices](a_telecharger/3_C02/3_C02_Exercices.tex) 
    <span style=color:CornflowerBlue>_Dernière mise à jour : le 14/02/25_



## <span style="color:LightSalmon">Chapitre 03 : Équations
???+ info "Contenu"
    - Rappels sur les équations ;
    - Équations produit nul ;
    - Équations du type : $x^2 =a$.

??? tip "Fichiers à télécharger <span style=color:red> :material-file-pdf-box: <span style=color:blue>:material-text-box-edit-outline: "
    === "Leçon"
        [Leçon : 03 - Équations](a_telecharger/3_C03/3_C03_Équations.pdf){:target="_blank"}  
    === "Exercices"
        [Exercices](a_telecharger/3_C03/3_C03_Exercices.pdf){:target="_blank"}  
    === "Exercices + correction"
        [Exercices + correction](a_telecharger/3_C03/3_C03_Exercices_correction.pdf){:target="_blank"}
    === "LaTeX"
        - [Leçon](a_telecharger/3_C03/3_C03_Équations.tex) 
        - [Leçon Beamer (à venir)](a_telecharger/3_C03) 
        - [Exercices](a_telecharger/3_C03/3_C03_Exercices.tex) 
    <span style=color:CornflowerBlue>_Dernière mise à jour : le 29/11/24_



## <span style="color:LightSalmon">Chapitre 04 : Théorème de Thalès
???+ info "Contenu"
    - Théorème de Thalès ;
    - Configuration triangles emboîtés ;
    - Configuration "Papillon".

??? tip "Fichiers à télécharger <span style=color:red> :material-file-pdf-box: <span style=color:blue>:material-text-box-edit-outline: "
    === "Leçon"
        [Leçon : 04 - Théorème de Thalès](a_telecharger/3_C04/3_C06_Théorème_de_Thalès.pdf){:target="_blank"}  
    === "Exercices"
        [Exercices](a_telecharger/3_C04/3_C04_Exercices.pdf){:target="_blank"}  
    === "Exercices + correction"
        [Exercices + correction](a_telecharger/3_C04/3_C04_Exercices_correction.pdf){:target="_blank"}
    === "LaTeX"
        - [Leçon](a_telecharger/3_C04/3_C04_Exercices.tex) 
        - [Leçon Beamer (à venir)](a_telecharger/3_C01/3_C01_Calcul.tex) 
        - [Exercices](a_telecharger/3_C04/3_C04_Exercices.tex) 
    <span style=color:CornflowerBlue>_Dernière mise à jour : le 04/12/24_


## <span style="color:LightSalmon">Chapitre 05 : Fonctions (introduction)
???+ info "Contenu"
    - Introduction à la notion de fonction ;
    - Vocabulaire : antécédents, images ;
    - Retrouver les antécédents et les images via le calcul, un tableau de valeurs ou graphiquement.

??? tip "Fichiers à télécharger <span style=color:red> :material-file-pdf-box: <span style=color:blue>:material-text-box-edit-outline: "
    === "Leçon"
        [Leçon : 05 - Fonctions (introduction)](a_telecharger/3_C05/3_C05_Fonctions_introduction.pdf){:target="_blank"}  
    === "Exercices"
        [Exercices](a_telecharger/3_C05/3_C05_Exercices_énoncé.pdf){:target="_blank"}  
    === "Exercices + correction"
        [Exercices + correction (à venir)](a_telecharger/3_C05/3_C05_Exercices33.pdf){:target="_blank"}
    === "LaTeX"
        - [Leçon](a_telecharger/3_C05/3_C05_Fonctions_introduction.tex) 
        - [Leçon Beamer (à venir)](a_telecharger/3_C01/3_C01_Calcul.tex) 
        - [Exercices](a_telecharger/3_C05/3_C05_Exercices.tex) 
    <span style=color:CornflowerBlue>_Dernière mise à jour : le 07/01/25_


## <span style="color:LightSalmon">Chapitre 06 : Réciproque et contraposée du théorème de Thalès
???+ info "Contenu"
    - Réciproque du théorème de Thalès ;
    - Contraposée du théorème de Thalès.

??? tip "Fichiers à télécharger <span style=color:red> :material-file-pdf-box: <span style=color:blue>:material-text-box-edit-outline: "
    === "Leçon"
        [Leçon : 06 - Réciproque et contraposée du théorème de Thalès](a_telecharger/3_C06/3_C06_Thalès_2.pdf){:target="_blank"}  
    === "Exercices"
        [Exercices](a_telecharger/3_C06/3_C06_Exercices_énoncé.pdf){:target="_blank"}  
    === "Exercices + correction"
        [Exercices + correction (à venir)](a_telecharger/3_C01/3_C01_Calcul.pdf){:target="_blank"}
    === "LaTeX"
        - [Leçon](a_telecharger/3_C06/3_C06_Thalès_2.tex) 
        - [Leçon Beamer (à venir)](a_telecharger/3_C01/3_C01_Calcul.tex) 
        - [Exercices](a_telecharger/3_C06/3_C06_Exercices.tex) 
    <span style=color:CornflowerBlue>_Dernière mise à jour : le 08/02/25_


## <span style="color:LightSalmon">Chapitre 07 : Fonctions affines et linéaires
???+ info "Contenu"
    - Fonctions affines, linéaires et proportionnalité ;
    - Vocabulaire : coefficient directeur et ordonnée à l'origine ;
    - Lien avec les hausses et les baisses (pourcentages).

??? tip "Fichiers à télécharger <span style=color:red> :material-file-pdf-box: <span style=color:blue>:material-text-box-edit-outline: "
    === "Leçon"
        [Leçon : 07 - Fonctions affines et linéaires](a_telecharger/3_C07/3_C07_Fonctions_2.pdf){:target="_blank"}  
    === "Exercices"
        [Exercices](a_telecharger/3_C07/3_C07_Exercices_énoncé.pdf){:target="_blank"}  
    === "Exercices + correction"
        [Exercices + correction (à venir)](a_telecharger/3_C01/3_C01_Calcul.pdf){:target="_blank"}
    === "LaTeX"
        - [Leçon](a_telecharger/3_C07/3_C07_Fonctions_2.tex) 
        - [Leçon Beamer (à venir)](a_telecharger/3_C01/3_C01_Calcul.tex) 
        - [Exercices](a_telecharger/3_C07/3_C07_Exercices.tex) 
    <span style=color:CornflowerBlue>_Dernière mise à jour : le 08/02/25_


## <span style="color:LightSalmon">Chapitre 08 : Triangles semblables
???+ info "Contenu"
    - Définition de triangles semblables ;
    - Démontrer que deux triangles sont semblables en utilisant leurs angles ou les longueurs de leurs côtés ;
    - Lien avec les homothéties et les configurations de Thalès.

??? tip "Fichiers à télécharger <span style=color:red> :material-file-pdf-box: <span style=color:blue>:material-text-box-edit-outline: "
    === "Leçon"
        [Leçon : 08 - Triangles semblables](a_telecharger/3_C08/3_C08_Triangles_semblables.pdf){:target="_blank"}  
    === "Exercices"
        [Exercices](a_telecharger/3_C08/3_C08_Exercices_énoncé.pdf){:target="_blank"}  
    === "Exercices + correction"
        [Exercices + correction (à venir)](a_telecharger/3_C01/3_C01_Calcul.pdf){:target="_blank"}
    === "LaTeX"
        - [Leçon](a_telecharger/3_C08/3_C08_Triangles_semblables.tex) 
        - [Leçon Beamer (à venir)](a_telecharger/3_C01/3_C01_Calcul.tex) 
        - [Exercices](a_telecharger/3_C08/3_C08_Exercices.tex) 
    <span style=color:CornflowerBlue>_Dernière mise à jour : le 08/02/25_


## <span style="color:LightSalmon">Chapitre 09 : Nombres premiers
???+ info "Contenu"
    - Rappels définition d'un nombre premier ;
    - Décomposition en produit de facteurs premiers ;
    - Problèmes PGCD, PPCM.

??? tip "Fichiers à télécharger <span style=color:red> :material-file-pdf-box: <span style=color:blue>:material-text-box-edit-outline: "
    === "Leçon"
        [Leçon : 09 - Nombres premiers](a_telecharger/3_C09/3_C09_Arithmétique.pdf){:target="_blank"}  
    === "Exercices"
        [Exercices](a_telecharger/3_C09/3_C09_Exercices_énoncé.pdf){:target="_blank"}  
    === "Exercices + correction"
        [Exercices + correction (à venir)](a_telecharger/3_C09/3_C09_Exercices_correction2.pdf){:target="_blank"}
    === "LaTeX"
        - [Leçon](a_telecharger/3_C09/3_C09_Arithmétique.tex) 
        - [Leçon Beamer (à venir)](a_telecharger/3_C09) 
        - [Exercices](a_telecharger/3_C09/3_C09_Exercices.tex)  
    === "Python"
        - [Exercice 17 (à venir)](a_telecharger/3_C09/3_C09_Paques2.py)
    <span style=color:CornflowerBlue>_Dernière mise à jour : le 15/02/25_


## <span style="color:LightSalmon">Chapitre 10 : Sphères et boules
???+ info "Contenu"
    - Rappels sur les solides vus précédemment : cube, pavé droit, prisme droit, cylindre, pyramide et cône ;
    - Sphère et boule ;
    - Calcul de la surface d'une sphère ;
    - Calcul du volume d'une boule.

??? tip "Fichiers à télécharger <span style=color:red> :material-file-pdf-box: <span style=color:blue>:material-text-box-edit-outline: "
    === "Leçon"
        [Leçon : 10 - Sphères et boules (à venir)](a_telecharger/3_C01/3_C01_Calcul.pdf){:target="_blank"}  
    === "Exercices"
        [Exercices (à venir)](a_telecharger/3_C01/3_C01_Calcul.pdf){:target="_blank"}  
    === "Exercices + correction"
        [Exercices + correction (à venir)](a_telecharger/3_C01/3_C01_Calcul.pdf){:target="_blank"}
    === "LaTeX"
        - [Leçon (à venir)](a_telecharger/3_C01/3_C01_Calcul.tex) 
        - [Leçon Beamer (à venir)](a_telecharger/3_C01/3_C01_Calcul.tex) 
        - [Exercices (à venir)](a_telecharger/3_C01/3_C01_Calcul.tex) 
    <span style=color:CornflowerBlue>_Dernière mise à jour : le 27/09/24_



## <span style="color:LightSalmon">Chapitre 11 : Probabilités
???+ info "Contenu"
    - Rappels sur le vocabulaire des probabilités ;

??? tip "Fichiers à télécharger <span style=color:red> :material-file-pdf-box: <span style=color:blue>:material-text-box-edit-outline: "
    === "Leçon"
        [Leçon : 11 - Probabilités (à venir)](a_telecharger/3_C01/3_C01_Calcul.pdf){:target="_blank"}  
    === "Exercices"
        [Exercices (à venir)](a_telecharger/3_C01/3_C01_Calcul.pdf){:target="_blank"}  
    === "Exercices + correction"
        [Exercices + correction (à venir)](a_telecharger/3_C01/3_C01_Calcul.pdf){:target="_blank"}
    === "LaTeX"
        - [Leçon (à venir)](a_telecharger/3_C01/3_C01_Calcul.tex) 
        - [Leçon Beamer (à venir)](a_telecharger/3_C01/3_C01_Calcul.tex) 
        - [Exercices (à venir)](a_telecharger/3_C01/3_C01_Calcul.tex) 
    <span style=color:CornflowerBlue>_Dernière mise à jour : le 27/09/24_


## <span style="color:LightSalmon">Chapitre 12 : Trigonométrie
???+ info "Contenu"
    - Notion de cosinus, sinus, tangente dans un triangle rectangle ;
    - Calculer une longueur manquante dans un triangle rectangle à l'aide des fonctions trigonométriques ;
    - Calculer un angle manquant dans un triangle rectangle à l'aide des fonctions trigonométriques réciproques.

??? tip "Fichiers à télécharger <span style=color:red> :material-file-pdf-box: <span style=color:blue>:material-text-box-edit-outline: "
    === "Leçon"
        [Leçon : 12 - Trigonométrie (à venir)](a_telecharger/3_C01/3_C01_Calcul.pdf){:target="_blank"}  
    === "Exercices"
        [Exercices (à venir)](a_telecharger/3_C01/3_C01_Calcul.pdf){:target="_blank"}  
    === "Exercices + correction"
        [Exercices + correction (à venir)](a_telecharger/3_C01/3_C01_Calcul.pdf){:target="_blank"}
    === "LaTeX"
        - [Leçon (à venir)](a_telecharger/3_C01/3_C01_Calcul.tex) 
        - [Leçon Beamer (à venir)](a_telecharger/3_C01/3_C01_Calcul.tex) 
        - [Exercices (à venir)](a_telecharger/3_C01/3_C01_Calcul.tex) 
    <span style=color:CornflowerBlue>_Dernière mise à jour : le 27/09/24_


## <span style="color:LightSalmon">Chapitre 13 : Statistiques
???+ info "Contenu"
    - Moyenne, moyenne pondérée ;
    - Médiane (cas pair / impair) ;
    - Étendue ;
    - Extension au rangement des données en classe ;
    - Tableur.

??? tip "Fichiers à télécharger <span style=color:red> :material-file-pdf-box: <span style=color:blue>:material-text-box-edit-outline: "
    === "Leçon"
        [Leçon : 13 - Statistiques (à venir)](a_telecharger/3_C01/3_C01_Calcul.pdf){:target="_blank"}  
    === "Exercices"
        [Exercices (à venir)](a_telecharger/3_C01/3_C01_Calcul.pdf){:target="_blank"}  
    === "Exercices + correction"
        [Exercices + correction (à venir)](a_telecharger/3_C01/3_C01_Calcul.pdf){:target="_blank"}
    === "LaTeX"
        - [Leçon (à venir)](a_telecharger/3_C01/3_C01_Calcul.tex) 
        - [Leçon Beamer (à venir)](a_telecharger/3_C01/3_C01_Calcul.tex) 
        - [Exercices (à venir)](a_telecharger/3_C01/3_C01_Calcul.tex) 
    <span style=color:CornflowerBlue>_Dernière mise à jour : le 27/09/24_


## <span style="color:LightSalmon">Chapitre 14 : Sections solides
???+ info "Contenu"
    - Sections solides ;
    - Lien avec les agrandissements réductions pour les pyramides et les cônes.

??? tip "Fichiers à télécharger <span style=color:red> :material-file-pdf-box: <span style=color:blue>:material-text-box-edit-outline: "
    === "Leçon"
        [Leçon : 14 - Sections solides (à venir)](a_telecharger/3_C01/3_C01_Calcul.pdf){:target="_blank"}  
    === "Exercices"
        [Exercices (à venir)](a_telecharger/3_C01/3_C01_Calcul.pdf){:target="_blank"}  
    === "Exercices + correction"
        [Exercices + correction (à venir)](a_telecharger/3_C01/3_C01_Calcul.pdf){:target="_blank"}
    === "LaTeX"
        - [Leçon (à venir)](a_telecharger/3_C01/3_C01_Calcul.tex) 
        - [Leçon Beamer (à venir)](a_telecharger/3_C01/3_C01_Calcul.tex) 
        - [Exercices (à venir)](a_telecharger/3_C01/3_C01_Calcul.tex) 
    <span style=color:CornflowerBlue>_Dernière mise à jour : le 27/09/24_


## <span style="color:LightSalmon">Chapitre 15 : Repérage sur la sphère
???+ info "Contenu"
    - Vocabulaire longitude, latitude ;
    - Se repérer sur une sphère.

??? tip "Fichiers à télécharger <span style=color:red> :material-file-pdf-box: <span style=color:blue>:material-text-box-edit-outline: "
    === "Leçon"
        [Leçon : 15 - Repérage sur la sphère (à venir)](a_telecharger/3_C01/3_C01_Calcul.pdf){:target="_blank"}  
    === "Exercices"
        [Exercices (à venir)](a_telecharger/3_C01/3_C01_Calcul.pdf){:target="_blank"}  
    === "Exercices + correction"
        [Exercices + correction (à venir)](a_telecharger/3_C01/3_C01_Calcul.pdf){:target="_blank"}
    === "LaTeX"
        - [Leçon (à venir)](a_telecharger/3_C01/3_C01_Calcul.tex) 
        - [Leçon Beamer (à venir)](a_telecharger/3_C01/3_C01_Calcul.tex) 
        - [Exercices (à venir)](a_telecharger/3_C01/3_C01_Calcul.tex) 
    <span style=color:CornflowerBlue>_Dernière mise à jour : le 27/09/24_