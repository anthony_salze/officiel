---
author: Anthony Salze
title: Activités
hide :
    -footer
---
# <span style="color:Tomato"> Activités

## <span style="color:LightSalmon">Énergétique du bâtiment
???+ info "Contenu"
    - Fractions, calcul littéral, proportionnalité, tracé un graphique, fonction affine linéaire

??? tip "Fichiers à télécharger <span style=color:red> :material-file-pdf-box: <span style=color:blue>:material-text-box-edit-outline: "
    === "Sujet + correction"
         - [Résistance thermique](a_telecharger/3_Groupe/3_CG1_Énergétique_sujet.pdf){:target="_blank"} 
         - [Correction](a_telecharger/3_Groupe/3_CG1_Énergétique_correction.pdf){:target="_blank"} 
    === "LaTeX"
         - [Sujet](a_telecharger/3_Groupe/3_CG1_Énergétique_sujet.tex) 
         - [Correction](a_telecharger/3_Groupe/3_CG1_Énergétique_correction.tex) 
    <span style=color:CornflowerBlue>_Dernière mise à jour : le 07/01/25_


## <span style="color:LightSalmon">Arbre de Pythagore
???+ info "Contenu"
    - Application du théorème de Pythagore sur un triangle rectangle isocèle, algorithme

??? tip "Fichiers à télécharger <span style=color:red> :material-file-pdf-box: <span style=color:blue>:material-text-box-edit-outline: "
    === "Résultat final"
         - ![](a_telecharger/3_Groupe/3_CG2_Arbre_Pythagore.jpg){ width=50%} 
    <span style=color:CornflowerBlue>_Dernière mise à jour : le 07/01/25_


## <span style="color:LightSalmon">Histoire : Pyramide de Khéops et Thalès
???+ info "Contenu"
    - Théorème de Thalès, propriétés droites perpendiculaires et parallèles, théorème de Pythagore, conversions d'unités, volume pyramide.

??? tip "Fichiers à télécharger <span style=color:red> :material-file-pdf-box: <span style=color:blue>:material-text-box-edit-outline: "
    === "Sujet + correction"
         - [Sujet](a_telecharger/3_Groupe/3_Hist_1_Thalès_Pyramide.pdf){:target="_blank"} 
         - [Correction (à venir)](a_telecharger/3_Groupe){:target="_blank"} 
    === "LaTeX"
         - [Sujet + Correction](a_telecharger/3_Groupe/3_Hist_1_Thalès_Pyramide.tex) 
    <span style=color:CornflowerBlue>_Dernière mise à jour : le 07/01/25_


## <span style="color:LightSalmon">Histoire : Arpentage et Cadastre
???+ info "Contenu"
    - Aires d'un rectangle, d'un triangle rectangle, d'un trapèze droit.

??? tip "Fichiers à télécharger <span style=color:red> :material-file-pdf-box: <span style=color:blue>:material-text-box-edit-outline: "
    === "Sujet + correction"
         - [Sujet](a_telecharger/3_Groupe/3_CG2_Cadastre.pdf){:target="_blank"} 
         - [Correction (à venir)](a_telecharger/3_Groupe){:target="_blank"} 
    === "LaTeX"
         - [Sujet + Correction](a_telecharger/3_Groupe/3_CG2_Cadastre.tex) 
    <span style=color:CornflowerBlue>_Dernière mise à jour : le 11/02/25_


# <span style="color:Tomato"> Approfondissement

## <span style="color:LightSalmon">Exercices "Type brevet"
???+ info "Contenu"
    - Aires, Volumes

??? tip "Fichiers à télécharger <span style=color:red> :material-file-pdf-box: <span style=color:blue>:material-text-box-edit-outline: "
    === "Fiches"
         - [01 : Volumes](a_telecharger/3_Groupe/05_Volumes.pdf){:target="_blank"} 
    === "Corrections"
         - [01 : Volumes (à venir)](a_telecharger/3_Groupe/05_Volumes_correction.pdf){:target="_blank"} 
    === "LaTeX"
         - [01 : Volumes](a_telecharger/3_Groupe/05_Volumes.tex) 
    <span style=color:CornflowerBlue>_Dernière mise à jour : le 07/01/25_


