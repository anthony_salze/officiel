---
author: Anthony Salze
title: Remerciements
hide :
    -navigation
    -toc
    -footer
---

# <span style="color:Tomato">Remerciements

Le site est hébergé par la forge de [l'*Association des Enseignantes et Enseignants d'Informatique de France*](https://aeif.fr/index.php/accueil/){:target="_blank"} .

Le site est construit avec [`mkdocs`](https://www.mkdocs.org/) et en particulier [`mkdocs-material`](https://squidfunk.github.io/mkdocs-material/){:target="_blank"} .

Merci à [Mireille Coilhac](https://forge.aeif.fr/mcoilhac){:target="_blank"}  pour son atelier lors des journées nationales de l'APMEP Rennes 2023.

Le logo :material-calculator-variant: fait partie de MkDocs sous la référence calculator-variant [https://pictogrammers.com/library/mdi/](https://pictogrammers.com/library/mdi/){:target="_blank"} 