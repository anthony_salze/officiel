---
author: Anthony Salze
title: Accueil
hide :
    -navigation
    -footer
    -toc
---
# <span style=color:Tomato>Bienvenue !

Ce site internet est dédié aux élèves du collège Félicien Joly de Fresnes-sur-Escaut, Nord (59) ainsi qu'aux enseignants souhaitant modifier les fichiers via LaTeX.  
  
Voici les différents documents que vous pourrez trouver sur ce site :
    * des leçons, des exercices et des devoirs pour le niveau collège au format .pdf <span style=color:red> :material-file-pdf-box:<span style=color:black> ;  
    * des fichiers GeoGebra illustrant certains exercices ou théorèmes :material-math-compass: ;  
    * quelques documents en lien avec l'histoire des mathématiques ;  
    * les fichiers .tex <span style=color:blue>:material-text-box-edit-outline: <span style=color:black>pour les enseignants utilisant LaTeX.  


Pour accéder au jeu vidéo Mathêma sur l'histoire des mathématiques, [cliquez ici](https://anthony_salze.forge.apps.education.fr/mathema/){:target="_blank"} 

Pour accéder au site du Laboratoire mathématique Grothendieck du collège Félicien Joly de Fresnes-sur-Escaut (59), [cliquez ici](https://college-felicien-joly.forge.apps.education.fr/laboratoire-grothendieck/){:target="_blank"} 


Pour les exercices et les leçons, je m'inspire principalement de manuels scolaires ainsi que d'annales de brevets.  
Pour l'histoire des mathématiques, en fonction de mes lectures sur le sujet, quelques exercices, activités pourront être partagés dans cette section.  
Pour accéder à l'intégralité des documents utilisés,[cliquez ici](Sources/Sources.md){:target="_blank"} .


Si vous trouvez des erreurs sur le site, dans les documents en téléchargement ou que vous rencontrez un quelconque problème, n'hésitez pas à m'en faire part en m'écrivant sur mon adresse académique. Merci !


??? tip "Conditions d'utilisations"
    Sauf mention contraire, les contenus sont réutilisables sous les conditions de la licence [Creative Commons BY-NC-SA](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr){:target="_blank"} . Si j'ai inclus du contenu dont vous étiez l'autrice ou l'auteur sans vous mentionner, veuillez m'en excuser et m'en informer pour que je puisse vous créditer convenablement.

    Ces pages sont conformes au RGPD et respectent votre vie privée. Rien n'est fait pour vous identifier, aucune donnée personnelle n'est collectée.


<span style=color:LightSalmon>_Dernière mise à jour : 18/02/2025_

