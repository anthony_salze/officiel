---
author: Anthony Salze
title: Leçons et exercices
hide :
    -footer
---
# <span style="color:Tomato"> Leçons et exercices

## <span style="color:LightSalmon">01 : Rappels sur les opérations
???+ info "Contenu"
    - Vocabulaires liés aux quatre opérations.
    - Priorités opératoires,
    - Ordre de grandeur.

??? tip "Fichiers à télécharger <span style=color:red> :material-file-pdf-box: <span style=color:blue>:material-text-box-edit-outline: "
    === "Leçon"
        [Leçon : 01 - Opérations](a_telecharger/5_C01/5_C01_Opérations.pdf){:target="_blank"}   
    === "Exercices"
        [Exercices](a_telecharger/5_C01/5_C01_Exercices_énoncé.pdf){:target="_blank"}   
    === "Exercices + correction"
        [Exercices + correction](a_telecharger/5_C01/5_C01_Exercices_correction.pdf){:target="_blank"} 
    === "LaTeX"
        - [Leçon](a_telecharger/5_C01/5_C01_Opérations.tex) 
        - [Leçon Beamer](a_telecharger/5_C01/5_C01_Présentation.tex) 
        - [Exercices](a_telecharger/5_C01/5_C01_Exercices.tex) 
    <span style=color:CornflowerBlue>_Dernière mise à jour : le 12/09/24_


## <span style="color:LightSalmon">02 : Inégalité triangulaire
???+ info "Contenu"
    - Propriété de l'inégalité triangulaire

??? tip "Fichiers à télécharger <span style=color:red> :material-file-pdf-box: <span style=color:blue>:material-text-box-edit-outline: "
    === "Leçon"
        [Leçon : 02 - Inégalité triangulaire](a_telecharger/5_C02/5_C02_Inégalité_triangulaire.pdf){:target="_blank"}   
    === "Exercices"
        [Exercices](a_telecharger/5_C02/5_C02_Exercices_énoncé.pdf){:target="_blank"}   
    === "Exercices + correction"
        [Exercices + correction](a_telecharger/5_C02/5_C02_Exercices_correction.pdf){:target="_blank"} 
    === "LaTeX"
        - [Leçon](a_telecharger/5_C02/5_C02_Inégalité_triangulaire.tex) 
        - [Leçon Beamer](a_telecharger/5_C02/5_C02_Présentation.tex) 
        - [Exercices](a_telecharger/5_C02/5_C02_Exercices.tex) 
    <span style=color:CornflowerBlue>_Dernière mise à jour : le 26/09/24_


## <span style="color:LightSalmon">03 : Proportionnalité
???+ info "Contenu"
    - Reconnaître un tableau de proportionnalité
    - Calculer la quatrième proportionnelle par différentes méthodes (linéarité, retour à l'unité, coefficient de proportionnalité, produit en croix)

??? tip "Fichiers à télécharger <span style=color:red> :material-file-pdf-box: <span style=color:blue>:material-text-box-edit-outline: "
    === "Leçon"
        [Leçon : 03 - Proportionnalité](a_telecharger/5_C03/5_C03_Proportionnalité.pdf){:target="_blank"}   
    === "Exercices"
        [Exercices](a_telecharger/5_C03/5_C03_Exercices.pdf){:target="_blank"}   
    === "Exercices + correction"
        [Exercices + correction (à venir)](a_telecharger/5_C03){:target="_blank"} 
    === "LaTeX"
        - [Leçon](a_telecharger/5_C03/5_C03_Proportionnalité.tex) 
        - [Leçon Beamer (à venir)](a_telecharger/5_C02/5_C02) 
        - [Exercices](a_telecharger/5_C03/5_C03_Exercices.tex) 
    <span style=color:CornflowerBlue>_Dernière mise à jour : le 12/10/24_


## <span style="color:LightSalmon">04 : Hauteurs et médiatrices
???+ info "Contenu"
    - Médiatrices dans un triangle ;
    - Hauteurs dans un triangle ;
    - Centre du cercle circonscrit à un triangle et orthocentre ;
    - Résolution de problème.

??? tip "Fichiers à télécharger <span style=color:red> :material-file-pdf-box: <span style=color:blue>:material-text-box-edit-outline: <span style=color:black> :material-math-compass:"
    === "Leçon"
        [Leçon : 04 - Hauteurs et médiatrices](a_telecharger/5_C04/5_C04_Hauteurs_médiatrices.pdf){:target="_blank"}   
    === "Exercices"
        [Exercices](a_telecharger/5_C04/5_C04_Exercices_énoncé.pdf){:target="_blank"}  
    === "Exercices + correction"
        [Exercices + correction (à venir)](a_telecharger/5_C04/5_C04_Exercices_correction2.pdf){:target="_blank"}  
    === "LaTeX"
        - [Leçon](a_telecharger/5_C04/5_C04_Hauteurs_médiatrices.tex) 
        - [Leçon Beamer (à venir)](a_telecharger/5_C04/5_C04_Présentation.tex) 
        - [Exercices](a_telecharger/5_C04/5_C04_Exercices.tex) 
    <span style=color:CornflowerBlue>_Dernière mise à jour : le 10/11/24_



## <span style="color:LightSalmon">05 : Nombres relatifs (introduction)
???+ info "Contenu"
    - Définition des nombres relatifs ;
    - Comparaison, classement des nombres relatifs ;
    - Placement sur une droite graduée ;
    - Repérage dans un plan.

??? tip "Fichiers à télécharger <span style=color:red> :material-file-pdf-box: <span style=color:blue>:material-text-box-edit-outline: "
    === "Leçon"
        [Leçon : 05 - Nombres relatifs (introduction)](a_telecharger/5_C05/5_C05_Nombres_relatifs_intro.pdf){:target="_blank"}   
    === "Exercices"
        [Exercices](a_telecharger/5_C05/5_C05_Exercices.pdf){:target="_blank"}  
    === "Exercices + correction"
        [Exercices + correction (à venir)](a_telecharger/5_C05){:target="_blank"}  
    === "LaTeX"
        - [Leçon](a_telecharger/5_C05/5_C05_Nombres_relatifs_intro.tex) 
        - [Leçon Beamer (à venir)](a_telecharger/5_C05) 
        - [Exercices](a_telecharger/5_C05/5_C05_Exercices.tex) 
    <span style=color:CornflowerBlue>_Dernière mise à jour : le 04/12/24_


## <span style="color:LightSalmon">06 : Symétrie centrale
???+ info "Contenu"
    - Définition de la symétrie centrale ;
    - Construction de l'image d'une figure par rapport à un point ;
    - Propriétés de la symétrie centrale ;
    - Centre de symétrie d'une figure.

??? tip "Fichiers à télécharger <span style=color:red> :material-file-pdf-box: <span style=color:blue>:material-text-box-edit-outline: "
    === "Leçon"
        [Leçon : 06 - Symétrie centrale](a_telecharger/5_C06/5_C06_Symétrie_centrale.pdf){:target="_blank"}   
    === "Exercices"
        [Exercices](a_telecharger/5_C06/5_C06_Exercices.pdf){:target="_blank"}  
    === "Exercices + correction"
        [Exercices + correction (à venir)](a_telecharger/5_C06){:target="_blank"}  
    === "LaTeX"
        - [Leçon](a_telecharger/5_C06/5_C06_Symétrie_centrale.tex) 
        - [Leçon Beamer (à venir)](a_telecharger/5_C06) 
        - [Exercices](a_telecharger/5_C06/5_C06_Exercices.tex) 
    <span style=color:CornflowerBlue>_Dernière mise à jour : le 04/12/24_



## <span style="color:LightSalmon">07 : Nombres relatifs - additions et soustractions
???+ info "Contenu"
    - Additions de nombres relatifs ;
    - Soustractions de nombres relatifs ;
    - Simplification des écritures.

??? tip "Fichiers à télécharger <span style=color:red> :material-file-pdf-box: <span style=color:blue>:material-text-box-edit-outline: "
    === "Leçon"
        [Leçon : 07 - Nombres relatifs - additions et soustractions](a_telecharger/5_C07/5_C07_Nombres_relatifs_2.pdf){:target="_blank"}   
    === "Exercices"
        [Exercices](a_telecharger/5_C07/5_C07_Exercices_énoncé.pdf){:target="_blank"}  
    === "Exercices + correction"
        [Exercices + correction (à venir)](a_telecharger/5_C07){:target="_blank"}  
    === "LaTeX"
        - [Leçon](a_telecharger/5_C07/5_C07_Nombres_relatifs_2.tex) 
        - [Leçon Beamer (à venir)](a_telecharger/5_C07) 
        - [Exercices](a_telecharger/5_C07/5_C07_Exercices.tex) 
    <span style=color:CornflowerBlue>_Dernière mise à jour : le 07/01/25_



## <span style="color:LightSalmon">08 : Angles et parallélisme
???+ info "Contenu"
    - Vocabulaire : angles oppposés par le sommet, correspondants et alternes-internes ;
    - Lien entre mesure des paires d'angles correspondants / alternes-internes et le parallélisme.

??? tip "Fichiers à télécharger <span style=color:red> :material-file-pdf-box: <span style=color:blue>:material-text-box-edit-outline: "
    === "Leçon"
        [Leçon : 08 - Angles et parallélisme](a_telecharger/5_C08/5_C08_Angles_parallélisme.pdf){:target="_blank"}   
    === "Exercices"
        [Exercices (à venir)](a_telecharger/5_C08){:target="_blank"}  
    === "Exercices + correction"
        [Exercices + correction (à venir)](a_telecharger/5_C08){:target="_blank"}  
    === "LaTeX"
        - [Leçon](a_telecharger/5_C08/5_C08_Angles_parallélisme.tex) 
        - [Leçon Beamer (à venir)](a_telecharger/5_C08) 
        - [Exercices (à venir)](a_telecharger/5_C08) 
    <span style=color:CornflowerBlue>_Dernière mise à jour : le 09/02/25_



## <span style="color:LightSalmon">09 : Arithmétique
???+ info "Contenu"
    - Rappels : diviseurs, multiples, critères de divisibilité par 2, 3, 5 et 9 ;
    - Définition nombre premier ;
    - Méthode du crible d'Ératosthène ;
    - Décomposition des nombres entiers en produit de facteurs premiers ;
    - Fractions irréductibles.

??? tip "Fichiers à télécharger <span style=color:red> :material-file-pdf-box: <span style=color:blue>:material-text-box-edit-outline: "
    === "Leçon"
        [Leçon : 09 - Arithmétique](a_telecharger/5_C09/5_C09_Arithmétique.pdf){:target="_blank"} 
    === "Exercices"
        [Exercices](a_telecharger/5_C09/5_C09_Exercices.pdf){:target="_blank"}  
    === "Exercices + correction"
        [Exercices + correction (à venir)](a_telecharger/5_C09){:target="_blank"}  
    === "LaTeX"
        - [Leçon](a_telecharger/5_C09/5_C09_Arithmétique.tex) 
        - [Leçon Beamer (à venir)](a_telecharger/5_C09) 
        - [Exercices (à venir)](a_telecharger/5_C09) 
    <span style=color:CornflowerBlue>_Dernière mise à jour : le 18/02/25_



## <span style="color:LightSalmon">10 : Angles et triangles
???+ info "Contenu"
    - Somme des angles dans un triangle ;
    - Propriétés des triangles particuliers.

??? tip "Fichiers à télécharger <span style=color:red> :material-file-pdf-box: <span style=color:blue>:material-text-box-edit-outline: "
    === "Leçon"
        [Leçon : 10 - Angles et triangles](a_telecharger/5_C10/5_C10_Angles_triangles.pdf){:target="_blank"}   
    === "Exercices"
        [Exercices](a_telecharger/5_C10/5_C10_Exercices_énoncé.pdf){:target="_blank"}  
    === "Exercices + correction"
        [Exercices + correction (à venir)](a_telecharger/5_C10){:target="_blank"}  
    === "LaTeX"
        - [Leçon](a_telecharger/5_C10/5_C10_Angles_triangles.tex) 
        - [Leçon Beamer (à venir)](a_telecharger/5_C10) 
        - [Exercices](a_telecharger/5_C10/5_C10_Angles_triangles_Exercices.tex) 
    <span style=color:CornflowerBlue>_Dernière mise à jour : le 18/02/25_



## <span style="color:LightSalmon">11 : Calcul littéral (introduction)
???+ info "Contenu"
    - Introduction au calcul littéral ;
    - Simplification des écritures littérales ;
    - Tester si une égalité est vraie pour certaines valeurs ;
    - Résolution de problèmes.

??? tip "Fichiers à télécharger <span style=color:red> :material-file-pdf-box: <span style=color:blue>:material-text-box-edit-outline: "
    === "Leçon"
        [Leçon : 11 - Calcul littéral (introduction)](a_telecharger/5_C11/5_C11_Calcul_littéral.pdf){:target="_blank"}   
    === "Exercices"
        [Exercices](a_telecharger/5_C11/5_C11_Exercices_énoncé.pdf){:target="_blank"}  
    === "Exercices + correction"
        [Exercices + correction (à venir)](a_telecharger/5_C11){:target="_blank"}  
    === "LaTeX"
        - [Leçon](a_telecharger/5_C11/5_C11_Calcul_littéral.tex) 
        - [Leçon Beamer (à venir)](a_telecharger/5_C11) 
        - [Exercices](a_telecharger/5_C11/5_C11_Exercices.tex) 
    <span style=color:CornflowerBlue>_Dernière mise à jour : le 18/02/25_



## <span style="color:LightSalmon">12 : Parallélogrammes
???+ info "Contenu"
    - Connaître la définition d'un parallélogramme ;
    - Savoir construire un parallélogramme ;
    - Démontrer qu'un quadrilatère est un parallélogramme, un losange, un rectangle ou un carré à l'aide de leurs propriétés.

??? tip "Fichiers à télécharger <span style=color:red> :material-file-pdf-box: <span style=color:blue>:material-text-box-edit-outline: "
    === "Leçon"
        [Leçon : 12 - Parallélogrammes](a_telecharger/5_C12/5_C12_Parallélogramme.pdf){:target="_blank"}   
    === "Exercices"
        [Exercices](a_telecharger/5_C12/5_C12_Exercices_énoncé.pdf){:target="_blank"}  
    === "Exercices + correction"
        [Exercices + correction (à venir)](a_telecharger/5_C12){:target="_blank"}  
    === "LaTeX"
        - [Leçon](a_telecharger/5_C12/5_C12_Parallélogramme.tex) 
        - [Leçon Beamer (à venir)](a_telecharger/5_C12) 
        - [Exercices](a_telecharger/5_C12/5_C12_Exercices.tex) 
    <span style=color:CornflowerBlue>_Dernière mise à jour : le 18/02/25_



## <span style="color:LightSalmon">13 : Nombres rationnels (introduction)
???+ info "Contenu"
    - 
    - 

??? tip "Fichiers à télécharger <span style=color:red> :material-file-pdf-box: <span style=color:blue>:material-text-box-edit-outline: "
    === "Leçon"
        [Leçon : 13 - Nombres rationnels (à venir)](a_telecharger/5_C13){:target="_blank"}   
    === "Exercices"
        [Exercices (à venir)](a_telecharger/5_C13){:target="_blank"}  
    === "Exercices + correction"
        [Exercices + correction (à venir)](a_telecharger/5_C13){:target="_blank"}  
    === "LaTeX"
        - [Leçon (à venir)](a_telecharger/5_C13) 
        - [Leçon Beamer (à venir)](a_telecharger/5_C13) 
        - [Exercices (à venir)](a_telecharger/5_C13) 
    <span style=color:CornflowerBlue>_Dernière mise à jour : le 29/09/24_



## <span style="color:LightSalmon">14 : Statistiques
???+ info "Contenu"
    - 
    - 

??? tip "Fichiers à télécharger <span style=color:red> :material-file-pdf-box: <span style=color:blue>:material-text-box-edit-outline: "
    === "Leçon"
        [Leçon : 14 - Statistiques (à venir)](a_telecharger/5_C14){:target="_blank"}   
    === "Exercices"
        [Exercices (à venir)](a_telecharger/5_C14){:target="_blank"}  
    === "Exercices + correction"
        [Exercices + correction (à venir)](a_telecharger/5_C14){:target="_blank"}  
    === "LaTeX"
        - [Leçon (à venir)](a_telecharger/5_C14) 
        - [Leçon Beamer (à venir)](a_telecharger/5_C14) 
        - [Exercices (à venir)](a_telecharger/5_C14) 
    <span style=color:CornflowerBlue>_Dernière mise à jour : le 29/09/24_


## <span style="color:LightSalmon">15 : Additions de fractions
???+ info "Contenu"
    - 
    - 

??? tip "Fichiers à télécharger <span style=color:red> :material-file-pdf-box: <span style=color:blue>:material-text-box-edit-outline: "
    === "Leçon"
        [Leçon : 15 - Additions de fractions (à venir)](a_telecharger/5_C15){:target="_blank"}   
    === "Exercices"
        [Exercices (à venir)](a_telecharger/5_C15){:target="_blank"}  
    === "Exercices + correction"
        [Exercices + correction (à venir)](a_telecharger/5_C15){:target="_blank"}  
    === "LaTeX"
        - [Leçon (à venir)](a_telecharger/5_C15) 
        - [Leçon Beamer (à venir)](a_telecharger/5_C15) 
        - [Exercices (à venir)](a_telecharger/5_C15) 
    <span style=color:CornflowerBlue>_Dernière mise à jour : le 29/09/24_


## <span style="color:LightSalmon">16 : Solides
???+ info "Contenu"
    - 
    - 

??? tip "Fichiers à télécharger <span style=color:red> :material-file-pdf-box: <span style=color:blue>:material-text-box-edit-outline: "
    === "Leçon"
        [Leçon : 16 - Solides (à venir)](a_telecharger/5_C16){:target="_blank"}   
    === "Exercices"
        [Exercices (à venir)](a_telecharger/5_C16){:target="_blank"}  
    === "Exercices + correction"
        [Exercices + correction (à venir)](a_telecharger/5_C16){:target="_blank"}  
    === "LaTeX"
        - [Leçon (à venir)](a_telecharger/5_C16) 
        - [Leçon Beamer (à venir)](a_telecharger/5_C16) 
        - [Exercices (à venir)](a_telecharger/5_C16) 
    <span style=color:CornflowerBlue>_Dernière mise à jour : le 29/09/24_



## <span style="color:LightSalmon">17 : Probabilités
???+ info "Contenu"
    - 
    - 

??? tip "Fichiers à télécharger <span style=color:red> :material-file-pdf-box: <span style=color:blue>:material-text-box-edit-outline: "
    === "Leçon"
        [Leçon : 17 - Probabilités (à venir)](a_telecharger/5_C17){:target="_blank"}   
    === "Exercices"
        [Exercices (à venir)](a_telecharger/5_C17){:target="_blank"}  
    === "Exercices + correction"
        [Exercices + correction (à venir)](a_telecharger/5_C17){:target="_blank"}  
    === "LaTeX"
        - [Leçon (à venir)](a_telecharger/5_C17) 
        - [Leçon Beamer (à venir)](a_telecharger/5_C17) 
        - [Exercices (à venir)](a_telecharger/5_C17) 
    <span style=color:CornflowerBlue>_Dernière mise à jour : le 29/09/24_