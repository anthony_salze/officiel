\documentclass[dvipsnames, a4paper, 12pt]{report}

\usepackage{lmodern} %police standard latin moderne
\usepackage[frenchb]{babel} %langue française
\usepackage[utf8]{inputenc} %pour l'utf-8
\usepackage[T1]{fontenc} %pour les césures et accentuations
\usepackage{geometry} %pour changer les marges d'un document
\usepackage{graphicx}
\usepackage{fancybox}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{fontawesome5}
\usepackage{ProfCollege}
\usepackage{luacode}
\usepackage{xlop}

\usepackage[explicit]{titlesec} %Permet de modifier les différentes parties, chapitres, sous-parties,...

\usepackage{colortbl} %permet de mettre des cellules de tableau en couleur

\usepackage{multicol}

\usepackage{cellspace} %pour définir les espaces en haut et en bas des cellules d'un tableau
\setlength{\cellspacebottomlimit}{5pt}
\setlength{\cellspacetoplimit}{5pt}
\setlength{\parindent}{0pt}

\usepackage{xcolor} %permet de mettre du texte en couleur

\usepackage{array} %gestion des tableaux
\usepackage{tcolorbox} %créer des boîtes de couleurs

\usepackage{hyperref} %permet de gérer le sommaire et la page de garde et les options du PDF

\geometry{top=2cm, bottom=2cm, right=2cm, left=2cm}   
\titlespacing{\chapter}{0pt}{0pt}{5pt}

\setcounter{chapter}{7}

\usepackage{lastpage}
\usepackage{fancyhdr} %définition des en-têtes et des bas de page
\fancyhf{}
\fancypagestyle{main}{
	\renewcommand\headrulewidth{5pt}
	\fancyhead[L]{5\ieme\, - Chapitre \arabic{chapter} - Nombres relatifs : + et -}
	\fancyhead[R]{Collèges Félicien Joly / Marie Curie}
	\fancyfoot[C]{\thepage/\pageref{LastPage}}
	}
\pagestyle{main}

%gestion des entêtes
\makeatletter\@addtoreset{section}{chapter}\makeatother % RAZ des numéros de section après un chapitre
\renewcommand{\thepart}{\Alph{part} -} % Pour mettre des I, II, etc. aux parties
\renewcommand{\thesection}{\color{Green} \Roman{section} -}% Idem pour les sections et avoir le numéro de chapitre
\renewcommand{\thesubsection}{\arabic{subsection} -}

\begin{document}
%format du titre des chapitres placé après Sommaire sinon cela bugue
\titleformat{\chapter}[frame] {\normalfont \bfseries \large}{\textcolor{red} {\chaptertitlename\ \thechapter\,-\,#1}}{0pt}{\Huge}

\setcounter{chapter}{6}

\chapter{Nombres relatifs : additions et soustractions}
\begin{luacode}
	d=1
	p=1
	t=1
	m=1
\end{luacode}

\color{blue}\underline{\textbf{Prérequis}}
\begin{itemize}
	\item Chapitre 05 : Nombres relatifs introduction ;
	\item Additions et soustractions avec les nombres décimaux.
\end{itemize}~~

\underline{\textbf{Objectifs}}
\begin{itemize}
	\item Additionner et soustraire des nombres relatifs ;
	\item Enchaînements d'additions et de soustractions avec les nombres relatifs.
\end{itemize}
\color{black}
~~\\
\textbf{\underline{Rappels}} : QCM (plusieurs réponses possibles)\\\footnotesize{}
\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom= , AlphT, Largeur=3cm]{%
Sur la droite graduée ci-dessus$\text{,}$ l'abscisse du point M est \ldots   & comprise entre $-3$ et $-4$. & $-\num{3.5}$ & $-\num{2.5}$ &1,%2
Parmi ces nombres relatifs$\text{,}$ celui qui a la plus petite distance à zéro est \ldots & $-\num{5.6}$ & $\num{5.7}$ & $-\num{11.6}$ &1,%
L'opposé du nombre relatif $-\num{17.7}$ est \ldots & $-\num{7.17}$ & $\num{17.7}$ & $-\num{17.7}$ &2,%
Le nombre $-\num{19.19}$ est compris entre \ldots & $-\num{19.2}$ et $-\num{19.20}$ & $-\num{19}$ et $-\num{19.2}$ & $-\num{19.1}$ et $-\num{19.2}$ &2,%3
$\num{19.8} - (\num{4.8} - \num{2.1})$ est égal à \ldots & $-\num{26.7}$ & $\num{12.9}$ & $\num{17.1}$ &3%
}
\begin{center}\Reperage[AffichageNom, AffichageGrad, Pasx=2]{-7/M, 3/A}\end{center}

\normalsize{}

\color{Green}
\section{Additionner deux nombres relatifs}
\color{black}
\normalsize{}

\begin{tcolorbox}[colframe=blue!20, colback=blue!0, boxrule=2pt, arc=6pt, title={\textbf{Propriété \arabic{chapter}.\directlua{tex.print(p)}}}, coltitle=black]
	Si deux nombres relatifs ont le même signe, alors leur somme a :
	\begin{itemize}
		\item le même signe que les deux nombres ;
		\item pour partie numérique, la somme des parties numériques de chaque nombre.
	\end{itemize}
\end{tcolorbox}
\begin{luacode}
	p=p+1
\end{luacode}

\underline{\textbf{Exemple}}\\
On veut calculer $(-3) + (-2)$.\\ 
$-3$ et $-2$ sont deux nombres négatifs. Donc :
\begin{itemize}
	\item leur somme est négative ;
	\item on ajoute leur partie numérique : $(-3) + (-2) = -(3+2) = -5$
\end{itemize}
~~
\\
\begin{tcolorbox}[colframe=blue!20, colback=blue!0, boxrule=2pt, arc=6pt, title={\textbf{Propriété \arabic{chapter}.\directlua{tex.print(p)}}}, coltitle=black]
	Si deux nombres relatifs sont de signe contraire, alors leur somme a :
	\begin{itemize}
		\item le même signe que le nombre qui a la plus grande partie numérique ;
		\item pour partie numérique, la différence des parties numériques de chaque nombre.
	\end{itemize}
\end{tcolorbox}
\begin{luacode}
	p=p+1
\end{luacode}

\underline{\textbf{Exemples}}\\
\begin{minipage}{0.45\textwidth}
On veut calculer $(-5 )+(+ 7)$.\\ 
$-5$ et $7$ sont de signe contraire. Donc :
\begin{itemize}
	\item leur somme est positive car le nombre qui a la plus grande partie numérique est $7$ ;
	\item on soustrait leur partie numérique : $(-5) + (+7) =+( 7-5) = +2=2$
\end{itemize}
\end{minipage} \hspace{0.05cm} \vrule \hspace{0.05cm}
\begin{minipage}{0.45\textwidth}
On veut calculer $(-13) +(+ 6)$.\\ 
$-13$ et $6$ sont de signe contraire. Donc :
\begin{itemize}
	\item leur somme est négative car le nombre qui a la plus grande partie numérique est $-13$ ;
	\item on soustrait leur partie numérique : $(-13) +(+ 6) = -(13-6) = -7$
\end{itemize}
\end{minipage}


\color{Green}
\section{Soustraire deux nombres relatifs}
\color{black}
\normalsize{}

\begin{tcolorbox}[colframe=blue!20, colback=blue!0, boxrule=2pt, arc=6pt, title={\textbf{Propriété \arabic{chapter}.\directlua{tex.print(p)}}}, coltitle=black]
	Soustraire un nombre relatif revient à ajouter son opposé.
\end{tcolorbox}
\begin{luacode}
	p=p+1
\end{luacode}

\underline{\textbf{Exemples}}\\
\begin{minipage}{0.45\textwidth}
	On souhaite calculer $ (-7) -(-5)$.\\
	Pour soustraire $-5$, on ajoute son opposé $5$.
	\begin{flalign*}
		(-7) -(-5)&=(-7) + (+5)&\\
		&=2
	\end{flalign*}
\end{minipage} \hspace{0.05cm} \vrule \hspace{0.05cm}
\begin{minipage}{0.45\textwidth}
	On souhaite calculer $(+ 9) - (+10)$.\\
	Pour soustraire $10$, on ajoute son opposé $-10$.
	\begin{flalign*}
		(+9)-(+10)&=(+9) + (-10)&\\
		&=-1
	\end{flalign*}
\end{minipage}


\color{Green}
\section{Enchaînement d'opérations}
\color{black}
\normalsize{}

\begin{tcolorbox}[colframe=blue!20, colback=blue!0, boxrule=2pt, arc=6pt, title={\textbf{Propriété \arabic{chapter}.\directlua{tex.print(p)}}}, coltitle=black]
	Dans une somme de plusieurs nombres relatifs, on peut :
	\begin{itemize}
		\item modifier l'ordre des termes ;
		\item regrouper des termes ensemble.
	\end{itemize}
\end{tcolorbox}
\begin{luacode}
	p=p+1
\end{luacode}

\begin{tcolorbox}[colframe=black!20, colback=black!0, boxrule=2pt, arc=6pt, title={\textbf{Méthode \arabic{chapter}.\directlua{tex.print(m)}}}, coltitle=black]
	Pour calculer une somme algébrique de plusieurs nombres relatifs, il faut :
	\begin{enumerate}[label=\textbf{\arabic*)}]
		\item transformer les soustractions en additions (\textit{propriété \arabic{chapter}.\directlua{tex.print(p-2)}}) ;
		\item additionner tous les nombres positifs ensemble et tous les nombres négatifs ensemble (\textit{propriété \arabic{chapter}.\directlua{tex.print(p-1)}}) ;
		\item terminer l'addition des deux derniers nombres obtenus à l'étape précédente (\textit{propriétés \arabic{chapter}.\directlua{tex.print(p-4)} et \arabic{chapter}.\directlua{tex.print(p-3)}}).
	\end{enumerate}
\end{tcolorbox}
\begin{luacode}
	m=m+1
\end{luacode}

\underline{\textbf{Exemple}}
\begin{flalign*}
A& = (-5) + (+8) -(+4)+(-2)-(-7)&\\
&\text{On transforme les soustractions en additions.}\\
A&=(-5) +(+8)\, \textcolor{SeaGreen}{ +\,(-4)} +(-2)\, \textcolor{ForestGreen}{ +\,(+7)}\\
&\text{On regroupe les nombres positifs entre eux et les nombres négatifs entre eux.}\\
A&=\textcolor{red}{(+8)+(+7)} + \textcolor{RoyalBlue}{(-5)+(-4)+(-2)}\\
A&=\textcolor{red}{(+15)} + \textcolor{RoyalBlue}{(-11)}\\
A&=4
\end{flalign*} 

\color{Green}
\section{Simplification d'écritures}
\color{black}
Certaines parenthèses peuvent être supprimées pour simplifier la lisibilité des calculs.
\begin{itemize}[label=\textbullet]
	\begin{multicols}{2}
		\item $+(+2)=+2$ ;
		\item $-(-2)=+2$ ;
		\item $-(+2)=-2$ ;
		\item $+(-2)=-2$.
	\end{multicols}
\end{itemize}

\underline{\textbf{Exemple}}
\begin{flalign*}
A& = (-5) + (+8) -(+4)+(-2)-(-7)&\\
A&=-5 + 8 -4 -2+7\\
A&=8+7 -4 -2 -5\\
A&=15 -11\\
A&=4
\end{flalign*} 


\color{Green}
\section{Bilan}
\color{black}
QCM (une seule réponse possible)\\\footnotesize{}
\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom= , AlphT, Largeur=3cm]{%
$-(7)+( -5) = (-12)$ peut s'écrire plus simplement \ldots  & $-7+-5=-12$ & $-7 + (-5) = -12$ & $-7-5=-12$ &3,%
$2-7 = \ldots$ & $-27$ & $5$ & $-5$ & 3,%
$-5-4 = \ldots$ & $-9$ & $1$ & $-1$ & 1,%
$6 - (-4) = \ldots$ & $10$ & $2$ & $-2$ &1,%
$-\num{7.4} - (-\num{2.7}) = \ldots$ & $-\num{10.1}$ & $\num{10.1}$ & $-\num{4.7}$ & 3,%
$6-7-3+2 = \ldots$ & $18$ & $-2$ & $-10$ & 2,%
Sur une droite graduée$\text{,}$ les points $A$ et $B$ ont pour abscisses respectives $-5$ et $-8$. La distance $BA$ est égale à \ldots & $-3$ & $3$ & $13$ & 2%
}
\normalsize{}


\color{Green}
\section{Auto-évaluation}
\color{black}
\begin{tabular}{|>{\columncolor{darkgray}\color{black}}m{8cm} |>{\color{black}}m{0.6cm}|m{0.6cm}|m{0.6cm}|m{5cm}|} \hline

\rowcolor{Lavender}\centering{} \color{black} \textbf{Compétences / Savoir-faire} & \centering{}\color{MediumSeaGreen} \faCheck & \centering{}\color{DarkOrange}\faWater & \centering{}\color{Crimson}\faTimes & \centering{}\color{black}Pour réviser \tabularnewline \hline \hline

\cellcolor{Lavender}Je suis capable d'additionner plusieurs nombres relatifs ensemble. & & & & \textbf{\underline{Leçon}} : Parties I \& III  \smallskip \newline  \textbf{\underline{Exercices}} :  \tabularnewline \hline \hline

\cellcolor{Lavender}Je suis capable de soustraire plusieurs nombres relatifs. & & & & \textbf{\underline{Leçon}} : Parties II \& III  \smallskip \newline  \textbf{\underline{Exercices}} :    \tabularnewline \hline \hline

\cellcolor{Lavender}Je suis capable de faire des enchaînements d'opérations en faisant attention aux priorités opératoires. & & & & \textbf{\underline{Leçon}} : Partie III  \smallskip \newline  \textbf{\underline{Exercices}} :    \tabularnewline \hline \hline

\cellcolor{Lavender}Je suis capable de simplifier les expressions pour gagner en lisibilité. & & & & \textbf{\underline{Leçon}} : Partie IV  \smallskip \newline  \textbf{\underline{Exercices}} :    \tabularnewline \hline 
\end{tabular}

%\begin{comment}
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------- Correction QCM -----------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\newpage
\setcounter{page}{1}
\footnotesize{}
\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom= , AlphT, Largeur=3cm, Solution, Couleur=cyan!20]{%
Sur la droite graduée ci-dessus$\text{,}$ l'abscisse du point M est \ldots   & comprise entre $-3$ et $-4$. & \cellcolor{cyan!20} $-\num{3.5}$ & $-\num{2.5}$ &1,%
Parmi ces nombres relatifs$\text{,}$ celui qui a la plus petite distance à zéro est \ldots & $-\num{5.6}$ & $\num{5.7}$ & $-\num{11.6}$ &1,%
L'opposé du nombre relatif $-\num{17.7}$ est \ldots & $-\num{7.17}$ & $\num{17.7}$ & $-\num{17.7}$ &2,%
Le nombre $-\num{19.19}$ est compris entre \ldots & $-\num{19.2}$ et $-\num{19.20}$ & $-\num{19}$ et $-\num{19.2}$ & \cellcolor{cyan!20} $-\num{19.1}$ et $-\num{19.2}$ &2,%
$\num{19.8} - (\num{4.8} - \num{2.1})$ est égal à \ldots & $-\num{26.7}$ & $\num{12.9}$ & $\num{17.1}$ &3%
}
\\
\\
\\
\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom= , AlphT, Largeur=3cm, Solution, Couleur=cyan!20]{%
$-(7)+( -5) = (-12)$ peut s'écrire plus simplement \ldots  & $-7+-5=-12$ & $-7 + (-5) = -12$ & $-7-5=-12$ &3,%
$2-7 = \ldots$ & $-27$ & $5$ & $-5$ & 3,%
$-5-4 = \ldots$ & $-9$ & $1$ & $-1$ & 1,%
$6 - (-4) = \ldots$ & $10$ & $2$ & $-2$ &1,%
$-\num{7.4} - (-\num{2.7}) = \ldots$ & $-\num{10.1}$ & $\num{10.1}$ & $-\num{4.7}$ & 3,%
$6-7-3+2 = \ldots$ & $18$ & $-2$ & $-10$ & 2,%
Sur une droite graduée$\text{,}$ les points $A$ et $B$ ont pour abscisses respectives $-5$ et $-8$. La distance $BA$ est égale à \ldots & $-3$ & $3$ & $13$ & 2%
}
\normalsize{}

%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%---------------------------------------------------------------------- Leçon à trous -----------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\newpage
\setcounter{page}{1}
\titleformat{\chapter}[frame] {\normalfont \bfseries \large}{\textcolor{red} {\chaptertitlename\ \thechapter\,-\,#1}}{0pt}{\Huge}

\setcounter{chapter}{6}
\setcounter{page}{1}
\chapter{\pointilles[14cm]}
\begin{luacode}
	d=1
	p=1
	t=1
	m=1
\end{luacode}

\color{blue}\underline{\textbf{Prérequis}}
\begin{itemize}
	\item \pointilles[16cm];
	\item \pointilles[16cm]\\
	\pointilles[16cm] 
\end{itemize}~~

\underline{\textbf{Objectifs}}
\begin{itemize}
	\item \pointilles[16cm] 
	\item \pointilles[16cm] \\
	\pointilles[16cm] 
\end{itemize}
\color{black}
~~\\
\textbf{\underline{Rappels}} : QCM (plusieurs réponses possibles)\\\footnotesize{}
\begin{center}\Reperage[AffichageNom, AffichageGrad, Pasx=2]{-7/M, 3/A}\end{center}
\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom= , AlphT, Largeur=3cm]{%
Sur la droite graduée ci-dessus$\text{,}$ l'abscisse du point M est \ldots   & comprise entre $-3$ et $-4$. & $-\num{3.5}$ & $-\num{2.5}$ &1,%2
Parmi ces nombres relatifs$\text{,}$ celui qui a la plus petite distance à zéro est \ldots & $-\num{5.6}$ & $\num{5.7}$ & $-\num{11.6}$ &1,%
L'opposé du nombre relatif $-\num{17.7}$ est \ldots & $-\num{7.17}$ & $\num{17.7}$ & $-\num{17.7}$ &2,%
Le nombre $-\num{19.19}$ est compris entre \ldots & $-\num{19.2}$ et $-\num{19.20}$ & $-\num{19}$ et $-\num{19.2}$ & $-\num{19.1}$ et $-\num{19.2}$ &2,%3
$\num{19.8} - (\num{4.8} - \num{2.1})$ est égal à \ldots & $-\num{26.7}$ & $\num{12.9}$ & $\num{17.1}$ &3%
}
\normalsize{}

\color{Green}
\section{\pointilles[10cm]}
\color{black}
\normalsize{}

\begin{tcolorbox}[colframe=blue!20, colback=blue!0, boxrule=2pt, arc=6pt, title={\textbf{Propriété \arabic{chapter}.\directlua{tex.print(p)}}}, coltitle=black] \vspace{0.3cm}
\pointilles[16cm] 
\begin{itemize}
	\item \pointilles[15cm] 
	\item \pointilles[15cm]\\
	\pointilles[15cm] 
	\pointilles[15cm] 
\end{itemize}
\end{tcolorbox}
\begin{luacode}
	p=p+1
\end{luacode}

\underline{\textbf{Exemple}}\\
\pointilles[17cm] \\
\pointilles[17cm] 
\begin{itemize}
	\item \pointilles[16cm] 
	\item \pointilles[16cm] 
\end{itemize}

\begin{tcolorbox}[colframe=blue!20, colback=blue!0, boxrule=2pt, arc=6pt, title={\textbf{Propriété \arabic{chapter}.\directlua{tex.print(p)}}}, coltitle=black] \vspace{0.3cm}
\pointilles[16cm] 
\begin{itemize}
	\item \pointilles[15cm] \\
	\pointilles[15cm] 
	\item \pointilles[15cm]\\
	\pointilles[15cm]\\
	\pointilles[15cm] 
\end{itemize}
\end{tcolorbox}
\begin{luacode}
	p=p+1
\end{luacode}

\underline{\textbf{Exemples}}\\
\begin{minipage}{0.45\textwidth} \vspace{0.5cm}
\pointilles[7.5cm]\\
\pointilles[7.5cm]\\
\pointilles[7.5cm]\\
\pointilles[7.5cm]\\
\pointilles[7.5cm]\\
\pointilles[7.5cm]\\
\pointilles[7.5cm]\\
\pointilles[7.5cm]\\
\pointilles[7.5cm]\\
\pointilles[7.5cm]\\
\pointilles[7.5cm]\\
\pointilles[7.5cm]\\
\pointilles[7.5cm]
\end{minipage} \hspace{0.05cm} \vrule \hspace{0.05cm}
\begin{minipage}{0.45\textwidth}
\vspace{0.5cm}
\pointilles[7.5cm]\\
\pointilles[7.5cm]\\
\pointilles[7.5cm]\\
\pointilles[7.5cm]\\
\pointilles[7.5cm]\\
\pointilles[7.5cm]\\
\pointilles[7.5cm]\\
\pointilles[7.5cm]\\
\pointilles[7.5cm]\\
\pointilles[7.5cm]\\
\pointilles[7.5cm]\\
\pointilles[7.5cm]\\
\pointilles[7.5cm]
\end{minipage}


\color{Green}
\section{\pointilles[10cm]\\}
\color{black}
\normalsize{}

\begin{tcolorbox}[colframe=blue!20, colback=blue!0, boxrule=2pt, arc=6pt, title={\textbf{Propriété \arabic{chapter}.\directlua{tex.print(p)}}}, coltitle=black] \vspace{0.3cm}
	\pointilles[16cm] 
\end{tcolorbox}
\begin{luacode}
	p=p+1
\end{luacode}

\underline{\textbf{Exemples}}\\
\begin{minipage}{0.45\textwidth}
\vspace{0.5cm}
\pointilles[7.5cm]\\
\pointilles[7.5cm]\\
\pointilles[7.5cm]\\
\pointilles[7.5cm]\\
\pointilles[7.5cm]
\end{minipage} \hspace{0.05cm} \vrule \hspace{0.05cm}
\begin{minipage}{0.45\textwidth}\vspace{0.5cm}
\pointilles[7.5cm]\\
\pointilles[7.5cm]\\
\pointilles[7.5cm]\\
\pointilles[7.5cm]\\
\pointilles[7.5cm]
\end{minipage}


\color{Green}
\section{\pointilles[10cm]\\}
\color{black}
\normalsize{}
\begin{tcolorbox}[colframe=blue!20, colback=blue!0, boxrule=2pt, arc=6pt, title={\textbf{Propriété \arabic{chapter}.\directlua{tex.print(p)}}}, coltitle=black]
	\pointilles[16cm] 
	\begin{itemize}
		\item \pointilles[15cm] ;
		\item \pointilles[15cm].
	\end{itemize}
\end{tcolorbox}
\begin{luacode}
	p=p+1
\end{luacode}

\begin{tcolorbox}[colframe=black!20, colback=black!0, boxrule=2pt, arc=6pt, title={\textbf{Méthode \arabic{chapter}.\directlua{tex.print(m)}}}, coltitle=black]
	\pointilles[16cm] 
	\begin{enumerate}[label=\textbf{\arabic*)}]
		\item \pointilles[15cm]\\
		\pointilles[15cm] 
		\item \pointilles[15cm]\\
		 \pointilles[15cm]\\
		 \pointilles[15cm] 
		\item \pointilles[15cm] 
	\end{enumerate}
\end{tcolorbox}
\begin{luacode}
	m=m+1
\end{luacode}

\underline{\textbf{Exemple}}\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]

\color{Green}
\section{\pointilles[10cm]\\}
\color{black}
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\

\underline{\textbf{Exemple}}\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]


\color{Green}
\section{Bilan}
\color{black}
QCM (une seule réponse possible)\\\footnotesize{}
\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom= , AlphT, Largeur=3cm]{%
$-(7)+( -5) = (-12)$ peut s'écrire plus simplement \ldots  & $-7+-5=-12$ & $-7 + (-5) = -12$ & $-7-5=-12$ &3,%
$2-7 = \ldots$ & $-27$ & $5$ & $-5$ & 3,%
$-5-4 = \ldots$ & $-9$ & $1$ & $-1$ & 1,%
$6 - (-4) = \ldots$ & $10$ & $2$ & $-2$ &1,%
$-\num{7.4} - (-\num{2.7}) = \ldots$ & $-\num{10.1}$ & $\num{10.1}$ & $-\num{4.7}$ & 3,%
$6-7-3+2 = \ldots$ & $18$ & $-2$ & $-10$ & 2,%
Sur une droite graduée$\text{,}$ les points $A$ et $B$ ont pour abscisses respectives $-5$ et $-8$. La distance $BA$ est égale à \ldots & $-3$ & $3$ & $13$ & 2%
}
\normalsize{}

\color{Green}
\section{Auto-évaluation}
\color{black}
\begin{tabular}{|>{\columncolor{darkgray}\color{white}}m{8cm}|>{\color{white}}m{0.6cm}|m{0.6cm}|m{0.6cm}|m{5cm}|} \hline

\rowcolor{darkgray}\centering{} Compétences & \centering{}\color{white} Oui & \centering{}\color{white}Bof & \centering{}\color{white}Non & \centering{}\color{white}Pour réviser \tabularnewline \hline \hline

\cellcolor{gray}Je suis capable d'additionner plusieurs nombres relatifs ensemble. & & & & \textbf{\underline{Leçon}} : Parties I \& III  \smallskip \newline  \textbf{\underline{Exercices}} :  \tabularnewline \hline \hline

\cellcolor{gray}Je suis capable de soustraire plusieurs nombres relatifs. & & & & \textbf{\underline{Leçon}} : Parties II \& III  \smallskip \newline  \textbf{\underline{Exercices}} :    \tabularnewline \hline \hline

\cellcolor{gray}Je suis capable de faire des enchaînements d'opérations en faisant attention aux priorités opératoires. & & & & \textbf{\underline{Leçon}} : Partie III  \smallskip \newline  \textbf{\underline{Exercices}} :    \tabularnewline \hline \hline

\cellcolor{gray}Je suis capable de simplifier les expressions pour gagner en lisibilité. & & & & \textbf{\underline{Leçon}} : Partie IV  \smallskip \newline  \textbf{\underline{Exercices}} :    \tabularnewline \hline 
\end{tabular}



%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------- Impression -----------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\newpage
\textbf{\underline{Rappels}} : QCM (plusieurs réponses possibles)\\\footnotesize{}
\begin{center}\Reperage[AffichageNom, AffichageGrad, Pasx=2]{-7/M, 3/A}\end{center}
\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom= , AlphT, Largeur=3cm]{%
Sur la droite graduée ci-dessus$\text{,}$ l'abscisse du point M est \ldots   & comprise entre $-3$ et $-4$. & $-\num{3.5}$ & $-\num{2.5}$ &1,%2
Parmi ces nombres relatifs$\text{,}$ celui qui a la plus petite distance à zéro est \ldots & $-\num{5.6}$ & $\num{5.7}$ & $-\num{11.6}$ &1,%
L'opposé du nombre relatif $-\num{17.7}$ est \ldots & $-\num{7.17}$ & $\num{17.7}$ & $-\num{17.7}$ &2,%
Le nombre $-\num{19.19}$ est compris entre \ldots & $-\num{19.2}$ et $-\num{19.20}$ & $-\num{19}$ et $-\num{19.2}$ & $-\num{19.1}$ et $-\num{19.2}$ &2,%3
$\num{19.8} - (\num{4.8} - \num{2.1})$ est égal à \ldots & $-\num{26.7}$ & $\num{12.9}$ & $\num{17.1}$ &3%
}
\\
\\
\\
\\
\\
QCM (une seule réponse possible)\\\footnotesize{}
\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom= , AlphT, Largeur=3cm]{%
$-(7)+( -5) = (-12)$ peut s'écrire plus simplement \ldots  & $-7+-5=-12$ & $-7 + (-5) = -12$ & $-7-5=-12$ &3,%
$2-7 = \ldots$ & $-27$ & $5$ & $-5$ & 3,%
$-5-4 = \ldots$ & $-9$ & $1$ & $-1$ & 1,%
$6 - (-4) = \ldots$ & $10$ & $2$ & $-2$ &1,%
$-\num{7.4} - (-\num{2.7}) = \ldots$ & $-\num{10.1}$ & $\num{10.1}$ & $-\num{4.7}$ & 3,%
$6-7-3+2 = \ldots$ & $18$ & $-2$ & $-10$ & 2,%
Sur une droite graduée$\text{,}$ les points $A$ et $B$ ont pour abscisses respectives $-5$ et $-8$. La distance $BA$ est égale à \ldots & $-3$ & $3$ & $13$ & 2%
}
\normalsize{}



%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%--------------------------------------------------------------- Histoire des mathématiques -------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\newpage
\begin{tcolorbox}[colframe=black, colback=blue!10, boxrule=2pt, arc=6pt]
	\centering{}\LARGE{\textbf{Histoire des mathématiques \faBook}}
\end{tcolorbox}
\begin{tcolorbox}[colframe=black!30, colback=blue!0, boxrule=2pt, arc=6pt, title=François Viète ($1540\, ;\, 1603$), coltitle=black]

\end{tcolorbox}

\textbf{\underline{Sources}}
\begin{itemize}[label=\textbullet]
	\item Hauchecorne, B. et Suratteau, D. (2008), \textit{Des Mathématiciens de A à Z} ($3^e$ édition), Ellipses
	\item Byrne, O. (2022), \textit{The first six books of the elements of Euclid}, Taschen
	\item Houlou-Garcia, A. (2019), \textit{Mathematikos} (2\ieme\, tirage), Belles lettres
	\item Barbin, E. (2019), \textit{Faire des mathématiques avec l'histoire au lycée}, Ellipses
	\item Dahan-Dalmedico, A. et Peiffer, J. (1986), \textit{Une histoire des mathématiques : Routes et dédales}, Points
	\item Freiberger, M. et Thomas, R. (2018), \textit{Dans le secret des nombres}, Ekho
\end{itemize}

%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%-------------------------------------------------------------------------- Fiche de route ------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

\newpage
\begin{tcolorbox}[colframe=black, colback=Khaki!50, boxrule=2pt, arc=6pt]
	\centering{}\LARGE{\textbf{Fiche de route}}
\end{tcolorbox}
\begin{itemize}[label=\textbullet]
	\item \color{RoyalBlue}\underline{Leçon} \color{black} : Partie :
	\item \color{ForestGreen}\underline{Exercices}  \color{black} :
	\item \color{RoyalBlue}\underline{Leçon}  \color{black} : Parties et : Bilan + Auto-évaluation
	\item \color{ForestGreen}\underline{Exercices bilan}  \color{black} : 
\end{itemize}
%\end{comment}

%\setcounter{page}{4}
\end{document}
