\documentclass[dvipsnames, a4paper, 12pt]{report}

\usepackage{lmodern} %police standard latin moderne
\usepackage[frenchb]{babel} %langue française
\usepackage[utf8]{inputenc} %pour l'utf-8
\usepackage[T1]{fontenc} %pour les césures et accentuations
\usepackage{geometry} %pour changer les marges d'un document
\usepackage{graphicx}
\usepackage{fancybox}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{fontawesome5}
\usepackage{ProfCollege}
\usepackage{luacode}
\usepackage{xlop}

\usepackage[explicit]{titlesec} %Permet de modifier les différentes parties, chapitres, sous-parties,...

\usepackage{colortbl} %permet de mettre des cellules de tableau en couleur

\usepackage{multicol}

\usepackage{cellspace} %pour définir les espaces en haut et en bas des cellules d'un tableau
\setlength{\cellspacebottomlimit}{5pt}
\setlength{\cellspacetoplimit}{5pt}
\setlength{\parindent}{0pt}

\usepackage{xcolor} %permet de mettre du texte en couleur

\usepackage{array} %gestion des tableaux
\usepackage{tcolorbox} %créer des boîtes de couleurs

\usepackage{hyperref} %permet de gérer le sommaire et la page de garde et les options du PDF

\geometry{top=2cm, bottom=2cm, right=2cm, left=2cm}   
\titlespacing{\chapter}{0pt}{0pt}{5pt}

\setcounter{chapter}{10}

\usepackage{lastpage}
\usepackage{fancyhdr} %définition des en-têtes et des bas de page
\fancyhf{}
\fancypagestyle{main}{
	\renewcommand\headrulewidth{5pt}
	\fancyhead[L]{5\ieme\, - Chapitre \arabic{chapter} - Angles et triangles}
	\fancyhead[R]{Collège}
	\fancyfoot[C]{\thepage/\pageref{LastPage}}
	}
\pagestyle{main}

%gestion des entêtes
\makeatletter\@addtoreset{section}{chapter}\makeatother % RAZ des numéros de section après un chapitre
\renewcommand{\thepart}{\Alph{part} -} % Pour mettre des I, II, etc. aux parties
\renewcommand{\thesection}{\color{Green} \Roman{section} -}% Idem pour les sections et avoir le numéro de chapitre
\renewcommand{\thesubsection}{\arabic{subsection} -}

\begin{document}
%format du titre des chapitres placé après Sommaire sinon cela bugue
\titleformat{\chapter}[frame] {\normalfont \bfseries \Large}{\textcolor{red} {\chaptertitlename\ \thechapter\,-\,#1}}{0pt}{\Huge}
%\begin{comment}
\setcounter{chapter}{9}

\chapter{Angles et triangles}
\begin{luacode}
	d=1
	p=1
	t=1
	m=1
\end{luacode}

\color{blue}\underline{\textbf{Prérequis}}
\begin{itemize}
	\item Notions d'angles et vocabulaire (aigu, obtus, droit, plat, nul) ;
	\item Utilisation du rapporteur.
\end{itemize}~~

\underline{\textbf{Objectifs}}
\begin{itemize}
	\item Connaître la propriété sur les mesures des angles dans un triangle ;
	\item Connaître les propriétés sur les triangles particuliers.
\end{itemize}
\color{black}
~~\\
\textbf{\underline{Rappels}} :\\
Un triangle est \pointilles[14cm]\\
Un triangle isocèle possède \pointilles[12cm]\\
Un triangle équilatéral possède \pointilles[11cm]\\
Un triangle rectangle possède \pointilles[11cm]\\
Pour mesurer un angle, on utilise un \pointilles[11cm]\\
Un angle plat mesure \pointilles[2cm]\\
\\
\href{run:./5_C10_Somme_angle_triangle.ggb}{GeoGebra : Propriété sur la somme des angles dans un triangle}

\color{Green}
\section{Cas général}
\color{black}
\normalsize{}

\begin{tcolorbox}[colframe=blue!20, colback=blue!0, boxrule=2pt, arc=6pt, title={\textbf{Propriété \arabic{chapter}.\directlua{tex.print(p)}}}, coltitle=black]
	La somme des mesures des trois angles d'un triangle est toujours égale à $180^\circ$.
\end{tcolorbox}
\begin{luacode}
	p=p+1
\end{luacode}

\underline{\textbf{Exemple}}\\
On considère un triangle MNP tel que $\widehat{MNP}=37^\circ$ et $\widehat{MPN}=96^\circ$.\\
En utilisant la \textit{Propriété \arabic{chapter}.\directlua{tex.print(p-1)}}, on peut en déduire la mesure de l'angle $\widehat{NPM}$.
\begin{flalign*}
	\widehat{NPM}&=180^\circ - (\widehat{MNP} + \widehat{MPN})&\\
	&=180^\circ - (37^\circ + 96^\circ)\\
	&=180^\circ - 133^\circ\\
	&=47^\circ
\end{flalign*}
La mesure de l'angle $\widehat{NPM}$ est de $47^\circ$.

\color{Green}
\section{Cas particuliers}
\color{black}
\normalsize{}

\begin{tcolorbox}[colframe=blue!20, colback=blue!0, boxrule=2pt, arc=6pt, title={\textbf{Propriétés \arabic{chapter}.\directlua{tex.print(p)} - \arabic{chapter}.\directlua{tex.print(p+1)} - \arabic{chapter}.\directlua{tex.print(p+2)} - \arabic{chapter}.\directlua{tex.print(p+3)}}}, coltitle=black]
\begin{itemize}[label=\textbullet]
	\item Dans un triangle équilatéral, les angles ont tous la même mesure : $60^\circ$.
	\item Dans un triangle isocèle, les angles à la base sont de même mesure.
	\item Si un triangle possède deux angles de même mesure alors c'est un triangle isocèle.
	\item Dans un triangle rectangle, la somme des mesures des deux angles aigus est égale à $90^\circ$.
\end{itemize}
\end{tcolorbox}
\begin{luacode}
	p=p+4
\end{luacode}

\underline{\textbf{Exemple 1}}\\
\begin{minipage}{0.6\textwidth}
À l'aide des informations données sur la figure ci-contre, donner (en justifiant) :
	\begin{enumerate}[label=\textbf{\arabic*)}]	
		\item la mesure de l'angle $\widehat{ABC}$ ;
		\item la mesure de l'angle $\widehat{ACB}$.
	\end{enumerate}
\end{minipage}
\begin{minipage}{0.4\textwidth}
	\begin{Geometrie}
		pair A,B,C;
		A=u*(1,4);
		B-A=u*(0,-2);
		C-A=u*(6,-1);
		trace polygone(A,B,C);
		label.lft(btex A etex,A);
		label.lft(btex B etex,B);
		label.rt(btex C etex,C);
		marque_s:=marque_s/3;
		trace Codelongueur(A,C,B,C,2);
		trace Codeangle(B,A,C,0,btex \ang{79} etex);
	\end{Geometrie}
\end{minipage}
~~\\
\begin{enumerate}[label=\textbf{\arabic*)}]
	\item D'après le codage sur la figure, les longueurs $CA$ et $CB$ sont égales. Donc le triangle $ABC$ est un triangle isocèle en $C$.\\
	Dans un triangle isocèle, les angles à la base sont de même mesure donc $\widehat{BAC} = \widehat{ABC} = 79^\circ$.
	\item La somme des mesures des angles dans un triangle est toujours égale à $180^\circ$ donc :
	\begin{flalign*}
		\widehat{ACB}&=180^\circ - (\widehat{BAC} \times 2)&\\
		&=180^\circ-(79^\circ \times 2)\\
		&=180^\circ - 158^\circ\\
		&=22^\circ
	\end{flalign*}
	Ainsi, l'angle $\widehat{ACB}$ mesure $22^\circ$.
\end{enumerate}
~~\\
\underline{\textbf{Exemple 2}}\\
\begin{minipage}{0.6\textwidth}
À l'aide des informations données sur la figure ci-contre, donner la mesure de l'angle $\widehat{DBC}$. Justifier.\\
\\
Le triangle ADB possède trois côtés de même longueur, c'est donc un triangle équilatéral.\\
Donc : $\widehat{DAB} = \widehat{ABD} = \widehat{BDA} = 60^\circ$.\\
\\
\underline{Mesure de l'angle $\widehat{DBC}$}
\begin{flalign*}
	\widehat{DBC}&=\widehat{ABD} + \widehat{ABC}&\\
	&=60^\circ + 90^\circ\\
	&=150^\circ
\end{flalign*}
La mesure de l'angle $\widehat{DBA}$ est égale à $150^\circ$.
\end{minipage}
\begin{minipage}{0.4\textwidth}
	\begin{center}
		\begin{Geometrie}[CoinHD={(6u,10u)}]
			pair A,B,C,D;
			A=u*(1,5);
			B-A=u*(4,0);
			D-A=u*(2,3.47);
			C-B=u*(0,-4);
			trace polygone(A,B,C);
			trace polygone(A,B,D);
			label.lft(btex A etex,A);
			label.lrt(btex B etex,B);
			label.rt(btex C etex,C);
			label.top(btex D etex, D);
			marque_s:=marque_s/3;
			trace Codelongueur(A,B,A,D,D,B,B,C,2);
			trace codeperp(A,B,C,7);
		\end{Geometrie}
	\end{center}
\end{minipage}
~~\\



\color{Green}
\section{Bilan}
\color{black}
QCM (une seule réponse possible)\\\footnotesize{}
\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom= , AlphT, Largeur=3cm]{%
Un triangle possède des angles de mesures $27^\circ$ et $45^\circ$. Son troisième angle mesure \ldots  & $18^\circ$ & $108^\circ$ & $188^\circ$ &2,%
Un triangle possède des angles de mesures $64^\circ$ et $58^\circ$. Sa nature est \ldots & quelconque. & isocèle. & rectangle &2,%
Un triangle possède des angles de mesures $18^\circ$ et $72^\circ$. Sa nature est \ldots & rectangle. & isocèle. & équilatéral &1,%
ABC est un triangle isocèle en $A$ tel que $\widehat{ABC}=60^\circ$. Alors \ldots & $\widehat{ACB}=59^\circ$ & ABC est rectangle.  & ABC est équilatéral. &3,%
Sur la figure ci-dessous$\text{,}$ les points $A\text{, }\, C \text{ et } D$ sont alignés. L'angle $\widehat{CBD}$ mesure \ldots & $27^\circ$ & $63^\circ$ &  $18^\circ$ &3%
}
\normalsize{}
\begin{center}
	\begin{Geometrie}
		%Réglages
		marque_s:=1.5;
		
		%Figure
		pair A,B,C,D;
		A=u*(1,1);
		B=u*(1,3);
		D=u*(3,1);
		C=u*(5.5,1);
		trace polygone(A,B,C);
		trace segment(B,D);
		trace Codelongueur(A,B,A,D,2);
		label.llft(btex A etex, A);
		label.bot(btex D etex, D);
		label.lrt(btex C etex, C);
		label.ulft(btex B etex, B);
		trace codeperp(D,A,B,8);
		trace Codeangle(B,C,D,0, btex \ang{27} etex);
		trace Codeangle(D,B,C,1,btex ? etex);
	\end{Geometrie}
\end{center}

\color{Green}
\section{Auto-évaluation}
\color{black}
\begin{tabular}{|>{\columncolor{darkgray}\color{black}}m{8cm} |>{\color{black}}m{0.6cm}|m{0.6cm}|m{0.6cm}|m{5cm}|} \hline

\rowcolor{Lavender}\centering{} \color{black} \textbf{Compétences / Savoir-faire} & \centering{}\color{MediumSeaGreen} \faCheck & \centering{}\color{DarkOrange}\faWater & \centering{}\color{Crimson}\faTimes & \centering{}\color{black}Pour réviser \tabularnewline \hline \hline

\cellcolor{Lavender}Je connais les propriétés des angles dans un triangle selon sa nature. & & & & \textbf{\underline{Leçon}} :  Parties I \& II \smallskip \newline  \textbf{\underline{Exercices}} :  \tabularnewline \hline \hline

\cellcolor{Lavender}Je sais résoudre des problèmes faisant intervenir les mesures d'angles dans les triangles. & & & & \textbf{\underline{Leçon}} : Partie II \smallskip \newline  \textbf{\underline{Exercices}} :    \tabularnewline \hline
\end{tabular}

%\end{comment}
%\begin{comment}
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------- Correction QCM -----------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\newpage
\textbf{\underline{Rappels}} :\\
Un triangle est un polygone avec 3 côtés.\\
Un triangle isocèle possède deux côtés de même longueur.\\
Un triangle équilatéral possède trois côtés de même longueur.\\
Un triangle rectangle possède un angle droit.\\
Pour mesurer un angle, on utilise un rapporteur.\\
Un angle plat mesure $180^\circ$.\\
\\
\\
\\
\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom= , AlphT, Largeur=3cm, Solution, Couleur=cyan!20]{%
Un triangle possède des angles de mesures $27^\circ$ et $45^\circ$. Son troisième angle mesure \ldots  & $18^\circ$ & $108^\circ$ & $188^\circ$ &2,%
Un triangle possède des angles de mesures $64^\circ$ et $58^\circ$. La nature de ce triangle est \ldots & quelconque. & isocèle. & rectangle &2,%
Un triangle possède des angles de mesures $18^\circ$ et $72^\circ$. La nature de ce triangle est \ldots & rectangle. & isocèle. & équilatéral &1,%
ABC est un triangle isocèle en $A$ tel que $\widehat{ABC}=60^\circ$. Alors \ldots & $\widehat{ACB}=59^\circ$ & ABC est rectangle.  & ABC est équilatéral. &3,%
Les points $A\text{,}\, C et D$ sont alignés. L'angle $\widehat{CBD}$ mesure \ldots \includegraphics[width=5cm]{5_C10_QCM.png} & $27^\circ$ & $63^\circ$ &  $18^\circ$ &3%
}

\normalsize{}
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%---------------------------------------------------------------------- Leçon à trous -----------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\newpage
\setcounter{page}{1}
%format du titre des chapitres placé après Sommaire sinon cela bugue
\titleformat{\chapter}[frame] {\normalfont \bfseries \Large}{\textcolor{red} {\chaptertitlename\ \thechapter\,-\,#1}}{0pt}{\Huge}

\setcounter{chapter}{9}

\chapter{\pointilles[10cm]}
\begin{luacode}
	d=1
	p=1
	t=1
	m=1
\end{luacode}

\color{blue}\underline{\textbf{Prérequis}}
\begin{itemize}
	\item Notions \pointilles[13cm] ;
	\item Utilisation du \pointilles[3cm]
\end{itemize}~~

\underline{\textbf{Objectifs}}
\begin{itemize}
	\item Connaître \pointilles[13cm] ;
	\item Connaître \pointilles[13cm]
\end{itemize}
\color{black}
~~\\
\textbf{\underline{Rappels}} :\\
Un triangle est \pointilles[14cm]\\
Un triangle isocèle possède \pointilles[12cm]\\
Un triangle équilatéral possède \pointilles[11cm]\\
Un triangle rectangle possède \pointilles[11cm]\\
Pour mesurer un angle, on utilise un \pointilles[11cm]\\
Un angle plat mesure \pointilles[2cm]\\


\color{Green}
\section{Cas général}
\color{black}
\normalsize{}

\begin{tcolorbox}[colframe=blue!20, colback=blue!0, boxrule=2pt, arc=6pt, title={\textbf{Propriété \arabic{chapter}.\directlua{tex.print(p)}}}, coltitle=black]
	La \pointilles[15cm]
\end{tcolorbox}
\begin{luacode}
	p=p+1
\end{luacode}

\underline{\textbf{Exemple}}\\
On considère un triangle MNP tel que \pointilles[8cm].\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]

\color{Green}
\section{Cas particuliers}
\color{black}
\normalsize{}

\begin{tcolorbox}[colframe=blue!20, colback=blue!0, boxrule=2pt, arc=6pt, title={\textbf{Propriétés \arabic{chapter}.\directlua{tex.print(p)} - \arabic{chapter}.\directlua{tex.print(p+1)} - \arabic{chapter}.\directlua{tex.print(p+2)} - \arabic{chapter}.\directlua{tex.print(p+3)}}}, coltitle=black]
\begin{itemize}[label=\textbullet]
	\item Dans un triangle \pointilles[12cm]
	\item Dans un triangle \pointilles[12cm]
	\item Si un triangle \pointilles[12.5cm]
	\item Dans un triangle \pointilles[12cm]
\end{itemize}
\end{tcolorbox}
\begin{luacode}
	p=p+4
\end{luacode}

\underline{\textbf{Exemple 1}}\\
\begin{minipage}{0.6\textwidth}
À l'aide des informations données sur la figure ci-contre, donner (en justifiant) :
	\begin{enumerate}[label=\textbf{\arabic*)}]	
		\item la mesure de l'angle $\widehat{ABC}$ ;
		\item la mesure de l'angle $\widehat{ACB}$.
	\end{enumerate}
\end{minipage}
\begin{minipage}{0.4\textwidth}
	\begin{Geometrie}
		pair A,B,C;
		A=u*(1,4);
		B-A=u*(0,-2);
		C-A=u*(6,-1);
		trace polygone(A,B,C);
		label.lft(btex A etex,A);
		label.lft(btex B etex,B);
		label.rt(btex C etex,C);
		marque_s:=marque_s/3;
		trace Codelongueur(A,C,B,C,2);
		trace Codeangle(B,A,C,0,btex \ang{79} etex);
	\end{Geometrie}
\end{minipage}
~~\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\\

\underline{\textbf{Exemple 2}}\\
\begin{minipage}{0.6\textwidth}
À l'aide des informations données sur la figure ci-contre, donner la mesure de l'angle $\widehat{DBC}$. Justifier.\\
\\
\pointilles[11cm]\\
\pointilles[11cm]\\
\pointilles[11cm]\\
\pointilles[11cm]\\
\pointilles[11cm]\\
\pointilles[11cm]\\
\pointilles[11cm]\\
\pointilles[11cm]\\
\pointilles[11cm]\\
\pointilles[11cm]\\
\pointilles[11cm]\\
\pointilles[11cm]\\
\pointilles[11cm]
\end{minipage}
\begin{minipage}{0.4\textwidth}
	\begin{center}
		\begin{Geometrie}[CoinHD={(6u,10u)}]
			pair A,B,C,D;
			A=u*(1,5);
			B-A=u*(4,0);
			D-A=u*(2,3.47);
			C-B=u*(0,-4);
			trace polygone(A,B,C);
			trace polygone(A,B,D);
			label.lft(btex A etex,A);
			label.lrt(btex B etex,B);
			label.rt(btex C etex,C);
			label.top(btex D etex, D);
			marque_s:=marque_s/3;
			trace Codelongueur(A,B,A,D,D,B,B,C,2);
			trace codeperp(A,B,C,7);
		\end{Geometrie}
	\end{center}
\end{minipage}
~~\\


\newpage
\color{Green}
\section{Bilan}
\color{black}
QCM (une seule réponse possible)\\\footnotesize{}
\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom= , AlphT, Largeur=3cm]{%
Un triangle possède des angles de mesures $27^\circ$ et $45^\circ$. Son troisième angle mesure \ldots  & $18^\circ$ & $108^\circ$ & $188^\circ$ &2,%
Un triangle possède des angles de mesures $64^\circ$ et $58^\circ$. Sa nature est \ldots & quelconque. & isocèle. & rectangle &2,%
Un triangle possède des angles de mesures $18^\circ$ et $72^\circ$. Sa nature est \ldots & rectangle. & isocèle. & équilatéral &1,%
ABC est un triangle isocèle en $A$ tel que $\widehat{ABC}=60^\circ$. Alors \ldots & $\widehat{ACB}=59^\circ$ & ABC est rectangle.  & ABC est équilatéral. &3,%
Sur la figure ci-dessous$\text{,}$ les points $A\text{, }\, C \text{ et } D$ sont alignés. L'angle $\widehat{CBD}$ mesure \ldots & $27^\circ$ & $63^\circ$ &  $18^\circ$ &3%
}
\normalsize{}
\begin{center}
	\begin{Geometrie}
		%Réglages
		marque_s:=1.5;
		
		%Figure
		pair A,B,C,D;
		A=u*(1,1);
		B=u*(1,3);
		D=u*(3,1);
		C=u*(5.5,1);
		trace polygone(A,B,C);
		trace segment(B,D);
		trace Codelongueur(A,B,A,D,2);
		label.llft(btex A etex, A);
		label.bot(btex D etex, D);
		label.lrt(btex C etex, C);
		label.ulft(btex B etex, B);
		trace codeperp(D,A,B,8);
		trace Codeangle(B,C,D,0, btex \ang{27} etex);
		trace Codeangle(D,B,C,1,btex ? etex);
	\end{Geometrie}
\end{center}

\color{Green}
\section{Auto-évaluation}
\color{black}
\begin{tabular}{|>{\columncolor{darkgray}\color{black}}m{8cm} |>{\color{black}}m{0.6cm}|m{0.6cm}|m{0.6cm}|m{5cm}|} \hline

\rowcolor{Lavender}\centering{} \color{black} \textbf{Compétences / Savoir-faire} & \centering{}\color{MediumSeaGreen} \faCheck & \centering{}\color{DarkOrange}\faWater & \centering{}\color{Crimson}\faTimes & \centering{}\color{black}Pour réviser \tabularnewline \hline \hline

\cellcolor{Lavender}Je connais les propriétés des angles dans un triangle selon sa nature. & & & & \textbf{\underline{Leçon}} :  Parties I \& II \smallskip \newline  \textbf{\underline{Exercices}} :  \tabularnewline \hline \hline

\cellcolor{Lavender}Je sais résoudre des problèmes faisant intervenir les mesures d'angles dans les triangles. & & & & \textbf{\underline{Leçon}} : Partie II \smallskip \newline  \textbf{\underline{Exercices}} :    \tabularnewline \hline
\end{tabular}

%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------- Impression -----------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\newpage
QCM (une seule réponse possible)\\\footnotesize{}
\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom= , AlphT, Largeur=3cm]{%
Un triangle possède des angles de mesures $27^\circ$ et $45^\circ$. Son troisième angle mesure \ldots  & $18^\circ$ & $108^\circ$ & $188^\circ$ &2,%
Un triangle possède des angles de mesures $64^\circ$ et $58^\circ$. Sa nature est \ldots & quelconque. & isocèle. & rectangle &2,%
Un triangle possède des angles de mesures $18^\circ$ et $72^\circ$. Sa nature est \ldots & rectangle. & isocèle. & équilatéral &1,%
ABC est un triangle isocèle en $A$ tel que $\widehat{ABC}=60^\circ$. Alors \ldots & $\widehat{ACB}=59^\circ$ & ABC est rectangle.  & ABC est équilatéral. &3,%
Sur la figure ci-dessous$\text{,}$ les points $A\text{, }\, C \text{ et } D$ sont alignés. L'angle $\widehat{CBD}$ mesure \ldots & $27^\circ$ & $63^\circ$ &  $18^\circ$ &3%
}
\normalsize{}
\begin{center}
	\begin{Geometrie}
		%Réglages
		marque_s:=1.5;
		
		%Figure
		pair A,B,C,D;
		A=u*(1,1);
		B=u*(1,3);
		D=u*(3,1);
		C=u*(5.5,1);
		trace polygone(A,B,C);
		trace segment(B,D);
		trace Codelongueur(A,B,A,D,2);
		label.llft(btex A etex, A);
		label.bot(btex D etex, D);
		label.lrt(btex C etex, C);
		label.ulft(btex B etex, B);
		trace codeperp(D,A,B,8);
		trace Codeangle(B,C,D,0, btex \ang{27} etex);
		trace Codeangle(D,B,C,1,btex ? etex);
	\end{Geometrie}
\end{center}
~~\\
QCM (une seule réponse possible)\\\footnotesize{}
\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom= , AlphT, Largeur=3cm]{%
Un triangle possède des angles de mesures $27^\circ$ et $45^\circ$. Son troisième angle mesure \ldots  & $18^\circ$ & $108^\circ$ & $188^\circ$ &2,%
Un triangle possède des angles de mesures $64^\circ$ et $58^\circ$. Sa nature est \ldots & quelconque. & isocèle. & rectangle &2,%
Un triangle possède des angles de mesures $18^\circ$ et $72^\circ$. Sa nature est \ldots & rectangle. & isocèle. & équilatéral &1,%
ABC est un triangle isocèle en $A$ tel que $\widehat{ABC}=60^\circ$. Alors \ldots & $\widehat{ACB}=59^\circ$ & ABC est rectangle.  & ABC est équilatéral. &3,%
Sur la figure ci-dessous$\text{,}$ les points $A\text{, }\, C \text{ et } D$ sont alignés. L'angle $\widehat{CBD}$ mesure \ldots & $27^\circ$ & $63^\circ$ &  $18^\circ$ &3%
}
\normalsize{}
\begin{center}
	\begin{Geometrie}
		%Réglages
		marque_s:=1.5;
		
		%Figure
		pair A,B,C,D;
		A=u*(1,1);
		B=u*(1,3);
		D=u*(3,1);
		C=u*(5.5,1);
		trace polygone(A,B,C);
		trace segment(B,D);
		trace Codelongueur(A,B,A,D,2);
		label.llft(btex A etex, A);
		label.bot(btex D etex, D);
		label.lrt(btex C etex, C);
		label.ulft(btex B etex, B);
		trace codeperp(D,A,B,8);
		trace Codeangle(B,C,D,0, btex \ang{27} etex);
		trace Codeangle(D,B,C,1,btex ? etex);
	\end{Geometrie}
\end{center}

\newpage
\begin{tabular}{|>{\columncolor{darkgray}\color{black}}m{8cm} |>{\color{black}}m{0.6cm}|m{0.6cm}|m{0.6cm}|m{5cm}|} \hline

\rowcolor{Lavender}\centering{} \color{black} \textbf{Compétences / Savoir-faire} & \centering{}\color{MediumSeaGreen} \faCheck & \centering{}\color{DarkOrange}\faWater & \centering{}\color{Crimson}\faTimes & \centering{}\color{black}Pour réviser \tabularnewline \hline \hline

\cellcolor{Lavender}Je connais les propriétés des angles dans un triangle selon sa nature. & & & & \textbf{\underline{Leçon}} :  Parties I \& II \smallskip \newline  \textbf{\underline{Exercices}} :  \tabularnewline \hline \hline

\cellcolor{Lavender}Je sais résoudre des problèmes faisant intervenir les mesures d'angles dans les triangles. & & & & \textbf{\underline{Leçon}} : Partie II \smallskip \newline  \textbf{\underline{Exercices}} :    \tabularnewline \hline
\end{tabular}
~~\\\\\\

\begin{tabular}{|>{\columncolor{darkgray}\color{black}}m{8cm} |>{\color{black}}m{0.6cm}|m{0.6cm}|m{0.6cm}|m{5cm}|} \hline

\rowcolor{Lavender}\centering{} \color{black} \textbf{Compétences / Savoir-faire} & \centering{}\color{MediumSeaGreen} \faCheck & \centering{}\color{DarkOrange}\faWater & \centering{}\color{Crimson}\faTimes & \centering{}\color{black}Pour réviser \tabularnewline \hline \hline

\cellcolor{Lavender}Je connais les propriétés des angles dans un triangle selon sa nature. & & & & \textbf{\underline{Leçon}} :  Parties I \& II \smallskip \newline  \textbf{\underline{Exercices}} :  \tabularnewline \hline \hline

\cellcolor{Lavender}Je sais résoudre des problèmes faisant intervenir les mesures d'angles dans les triangles. & & & & \textbf{\underline{Leçon}} : Partie II \smallskip \newline  \textbf{\underline{Exercices}} :    \tabularnewline \hline
\end{tabular}
~~\\\\\\

\begin{tabular}{|>{\columncolor{darkgray}\color{black}}m{8cm} |>{\color{black}}m{0.6cm}|m{0.6cm}|m{0.6cm}|m{5cm}|} \hline

\rowcolor{Lavender}\centering{} \color{black} \textbf{Compétences / Savoir-faire} & \centering{}\color{MediumSeaGreen} \faCheck & \centering{}\color{DarkOrange}\faWater & \centering{}\color{Crimson}\faTimes & \centering{}\color{black}Pour réviser \tabularnewline \hline \hline

\cellcolor{Lavender}Je connais les propriétés des angles dans un triangle selon sa nature. & & & & \textbf{\underline{Leçon}} :  Parties I \& II \smallskip \newline  \textbf{\underline{Exercices}} :  \tabularnewline \hline \hline

\cellcolor{Lavender}Je sais résoudre des problèmes faisant intervenir les mesures d'angles dans les triangles. & & & & \textbf{\underline{Leçon}} : Partie II \smallskip \newline  \textbf{\underline{Exercices}} :    \tabularnewline \hline
\end{tabular}
~~\\\\\\

\begin{tabular}{|>{\columncolor{darkgray}\color{black}}m{8cm} |>{\color{black}}m{0.6cm}|m{0.6cm}|m{0.6cm}|m{5cm}|} \hline

\rowcolor{Lavender}\centering{} \color{black} \textbf{Compétences / Savoir-faire} & \centering{}\color{MediumSeaGreen} \faCheck & \centering{}\color{DarkOrange}\faWater & \centering{}\color{Crimson}\faTimes & \centering{}\color{black}Pour réviser \tabularnewline \hline \hline

\cellcolor{Lavender}Je connais les propriétés des angles dans un triangle selon sa nature. & & & & \textbf{\underline{Leçon}} :  Parties I \& II \smallskip \newline  \textbf{\underline{Exercices}} :  \tabularnewline \hline \hline

\cellcolor{Lavender}Je sais résoudre des problèmes faisant intervenir les mesures d'angles dans les triangles. & & & & \textbf{\underline{Leçon}} : Partie II \smallskip \newline  \textbf{\underline{Exercices}} :    \tabularnewline \hline
\end{tabular}
~~\\\\\\

\begin{tabular}{|>{\columncolor{darkgray}\color{black}}m{8cm} |>{\color{black}}m{0.6cm}|m{0.6cm}|m{0.6cm}|m{5cm}|} \hline

\rowcolor{Lavender}\centering{} \color{black} \textbf{Compétences / Savoir-faire} & \centering{}\color{MediumSeaGreen} \faCheck & \centering{}\color{DarkOrange}\faWater & \centering{}\color{Crimson}\faTimes & \centering{}\color{black}Pour réviser \tabularnewline \hline \hline

\cellcolor{Lavender}Je connais les propriétés des angles dans un triangle selon sa nature. & & & & \textbf{\underline{Leçon}} :  Parties I \& II \smallskip \newline  \textbf{\underline{Exercices}} :  \tabularnewline \hline \hline

\cellcolor{Lavender}Je sais résoudre des problèmes faisant intervenir les mesures d'angles dans les triangles. & & & & \textbf{\underline{Leçon}} : Partie II \smallskip \newline  \textbf{\underline{Exercices}} :    \tabularnewline \hline
\end{tabular}
~~\\\\\\

\begin{tabular}{|>{\columncolor{darkgray}\color{black}}m{8cm} |>{\color{black}}m{0.6cm}|m{0.6cm}|m{0.6cm}|m{5cm}|} \hline

\rowcolor{Lavender}\centering{} \color{black} \textbf{Compétences / Savoir-faire} & \centering{}\color{MediumSeaGreen} \faCheck & \centering{}\color{DarkOrange}\faWater & \centering{}\color{Crimson}\faTimes & \centering{}\color{black}Pour réviser \tabularnewline \hline \hline

\cellcolor{Lavender}Je connais les propriétés des angles dans un triangle selon sa nature. & & & & \textbf{\underline{Leçon}} :  Parties I \& II \smallskip \newline  \textbf{\underline{Exercices}} :  \tabularnewline \hline \hline

\cellcolor{Lavender}Je sais résoudre des problèmes faisant intervenir les mesures d'angles dans les triangles. & & & & \textbf{\underline{Leçon}} : Partie II \smallskip \newline  \textbf{\underline{Exercices}} :    \tabularnewline \hline
\end{tabular}

%\end{comment}
%\begin{comment}
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%--------------------------------------------------------------- Histoire des mathématiques -------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\newpage
\begin{tcolorbox}[colframe=black, colback=blue!10, boxrule=2pt, arc=6pt]
	\centering{}\LARGE{\textbf{Histoire des mathématiques}}\\
	Thalès et Euclide
\end{tcolorbox}
\begin{tcolorbox}[colframe=black!30, colback=blue!0, boxrule=2pt, arc=6pt, title=Thalès ($\approx -625\, ;\, \approx -547$), coltitle=black]
\begin{minipage}{0.75\textwidth}
Thalès de Milet est un mathématicien grec, né à Milet autour de $-625$ et décédé autour de $-547$. C'est avant tout un commerçant. Il s'est enrichi pour consacrer la fin de sa vie aux voyages et aux études scientifiques. Son premier périple le conduit en \'Egypte où il calcule la hauteur des pyramides du plateau de Gizeh grâce à la projection de leur ombre au sol. C'est l'une des premières utilisations du théorème portant son nom (enseigné en $4$\ieme\,- $3$\ieme). De retour à Milet, il acquiert une réputation d'ingénieur, d'homme d'affaires et d'\'Etat, de philosophe, de mathématicien et d'astronome. Il est l'un des premiers à se rendre compte qu'une année dure $365$ jours et il prédit certaines éclipses de Soleil. Thalès est le premier homme au nom duquel sont attachés des découvertes mathématiques.
\end{minipage}
\begin{minipage}{0.2\textwidth}
	\includegraphics[width=4cm]{5_C10_Thalès.jpg}
\end{minipage}
~~\\\\\\
On ne peut être certain de l'authenticité des découvertes lui étant attribuées. Mais ce qui est sûr, c'est que c'est avec lui que débute la spéculation mathématique, c'est-à-dire la recherche pure. Ses principaux travaux traitent des droites, des angles et des triangles. Les résultats qu'il énonce sont des propositions isolées et il semble que certaines d'entres elles soient déjà connues des Babyloniens.
\\
\\
Six résultats importants sont attribués à Thalès, cependant, nous énoncerons ici que ceux apparaissant dans le programme de $5$\ieme.
\begin{itemize}[label=\textbullet]
	\item Le théorème du pont aux ânes. : ce théorème affirme que les angles à la base d'un triangle isocèle sont égaux.
	\item Les angles opposés formés par deux droites sécantes sont de même mesure.
	\item Un diamètre partage un cercle en deux parties égales.
\end{itemize}
\end{tcolorbox}

\begin{tcolorbox}[colframe=black!10, colback=blue!0, boxrule=1pt, arc=1pt, title=Quelques anecdotes, coltitle=black]
Thalès fut à la fois astronome et mathématicien... et peut-être le premier savant distrait de l'humanité. Une nuit, alors qu'il marchait en contemplant un magnifique ciel étoilé, il ne vit pas un puits devant lui, et tomba au fond. Une vielle femme, témoin de la scène, s'exclama : \guillemetleft\,Comment pouvez-vous prétendre savoir ce qui se passe dans le ciel, alors que vous êtes incapable de voir ce qui est à vos pieds ? \guillemetright.\\
\\
Un jour, Thalès avait chargé ses mulets pour transporter du sel dont il faisait commerce. Au cours du voyage, l'un des animaux s'allongea dans un ruisseau pour se reposer, et s'émerveilla de sentir son fardeau plus léger ; le sel avait fondu. À chaque ruisseau, l'animal recommença. Pour l'en décourager, Thalès le chargea au voyage suivant d'éponges et de chiffons. Dès qu'il s'allongeait, elles se gonflaient d'eau, alourdissant le fardeau, ce qui fit perdre au mulet son comportement paresseux.
\end{tcolorbox}

\begin{tcolorbox}[colframe=black!30, colback=blue!0, boxrule=2pt, arc=6pt, title=Euclide ($\approx -330\, ;\, \approx -275$), coltitle=black]
\begin{minipage}{0.75\textwidth}
Euclide est un mathématicien grec. On ne sait pratiquement rien de son existence. Il est l'auteur d'un ouvrage mathématique monumental, intitulé \textit{Les \'Eléments}. Ce manuel regroupe toutes les connaissances mathématiques de l'époque en géométrie et en théorie des nombres. Il est composé de 13 volumes dans lesquels Euclide classe les propositions dans un ordre logique.\\
\\
Dans les quatre premiers livres, Euclide parle de Géométrie. Il introduit les notions fondamentales que sont les points, les droites, les segments, le parallélisme, le cercle,... 
\end{minipage}
\begin{minipage}{0.2\textwidth}
	\includegraphics[width=4cm]{5_C10_Euclide3.jpg}
\end{minipage}
~~\\\\
C'est également dans cette partie de son manuel qu'il déduit des résultats classiques sur les triangles (isocèles et équilatéraux) et leurs angles. Il énonce également le théorème de Pythagore et en donne une preuve. Les autres livres seront décrits plus tard dans votre scolarité.\\
\\
Ci-dessous, deux extraits du livre \textit{Les \'Eléments} portant sur les propriétés des triangles isocèles.\\
\includegraphics[width=7cm]{5_C10_Euclide1.jpg}
\includegraphics[width=8.3cm]{5_C10_Euclide2.jpg}
\end{tcolorbox}

\begin{tcolorbox}[colframe=black!10, colback=blue!0, boxrule=1pt, arc=1pt, title=Quelques anecdotes, coltitle=black]
Euclide insiste toujours auprès de ses élèves pour dire que l'apprentissage des connaissances ne se fait pas dans un but intéressé, mais pour l'amour du Savoir. Un jour, un garçon qui assiste depuis peu au cours d'Euclide, s'interroge : \guillemetleft\,Que puis-je gagner à écouter tout ceci ? \guillemetright. Euclide prend quelques pièces de monnaie et dit à son esclave : \guillemetleft\,Donne-les lui, puisqu'il tient à faire du gain de ce qu'il apprend ! \guillemetright
\end{tcolorbox}

Sources\\
Hauchecorne, B. et Suratteau, D. (2008), \textit{Des Mathématiciens de A à Z} ($3^e$ édition), Ellipses\\
Byrne, O. (2022), \textit{The first six books of the elements of Euclid}, Taschen

%\end{comment}
\end{document}
