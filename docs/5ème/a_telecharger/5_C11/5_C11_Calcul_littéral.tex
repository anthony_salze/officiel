\documentclass[dvipsnames, a4paper, 12pt]{report}

\usepackage{lmodern} %police standard latin moderne
\usepackage[frenchb]{babel} %langue française
\usepackage[utf8]{inputenc} %pour l'utf-8
\usepackage[T1]{fontenc} %pour les césures et accentuations
\usepackage{geometry} %pour changer les marges d'un document
\usepackage{graphicx}
\usepackage{fancybox}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{fontawesome5}
\usepackage{ProfCollege}
\usepackage{luacode}
\usepackage{xlop}

\usepackage[explicit]{titlesec} %Permet de modifier les différentes parties, chapitres, sous-parties,...

\usepackage{colortbl} %permet de mettre des cellules de tableau en couleur

\usepackage{multicol}

\usepackage{cellspace} %pour définir les espaces en haut et en bas des cellules d'un tableau
\setlength{\cellspacebottomlimit}{5pt}
\setlength{\cellspacetoplimit}{5pt}
\setlength{\parindent}{0pt}

\usepackage{xcolor} %permet de mettre du texte en couleur

\usepackage{array} %gestion des tableaux
\usepackage{tcolorbox} %créer des boîtes de couleurs

\usepackage{hyperref} %permet de gérer le sommaire et la page de garde et les options du PDF

\geometry{top=2cm, bottom=2cm, right=2cm, left=2cm}   
\titlespacing{\chapter}{0pt}{0pt}{5pt}

\setcounter{chapter}{11}

\usepackage{lastpage}
\usepackage{fancyhdr} %définition des en-têtes et des bas de page
\fancyhf{}
\fancypagestyle{main}{
	\renewcommand\headrulewidth{5pt}
	\fancyhead[L]{5\ieme\, - Chapitre \arabic{chapter} - Calcul littéral}
	\fancyhead[R]{Collège}
	\fancyfoot[C]{\thepage/\pageref{LastPage}}
	\fancyfoot[R]{\footnotesize{Anthony SALZE}}
	}
\pagestyle{main}

%gestion des entêtes
\makeatletter\@addtoreset{section}{chapter}\makeatother % RAZ des numéros de section après un chapitre
\renewcommand{\thepart}{\Alph{part} -} % Pour mettre des I, II, etc. aux parties
\renewcommand{\thesection}{\color{Green} \Roman{section} -}% Idem pour les sections et avoir le numéro de chapitre
\renewcommand{\thesubsection}{\arabic{subsection} -}

\begin{document}
%format du titre des chapitres placé après Sommaire sinon cela bugue
\titleformat{\chapter}[frame] {\normalfont \bfseries \Large}{\textcolor{red} {\chaptertitlename\ \thechapter\,-\,#1}}{0pt}{\Huge}

\setcounter{chapter}{10}

\chapter{Introduction au calcul littéral}
\begin{luacode}
	d=1
	p=1
	t=1
	m=1
\end{luacode}

\color{blue}\underline{\textbf{Prérequis}}
\begin{itemize}
	\item Priorités opératoires ;
	\item Calculs avec les nombres relatifs.
\end{itemize}~~

\underline{\textbf{Objectifs}}
\begin{itemize}
	\item Utiliser des lettres pour représenter des nombres.
	\item Simplifier des expressions littérales.
	\item Tester si deux expressions littérales sont égales.
	\item Résoudre des problèmes.
\end{itemize}
\color{black}
~~\\
\textbf{\underline{Rappels}} : QCM (plusieurs réponses possibles)\\\footnotesize{}
\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom= , AlphT, Largeur=3cm]{%
$5 \times (4+3) =$ \ldots  & $23$ & $35$ & $(4+3) \times 5$ &2,% 3
$8+2 \times 3 = $ \ldots  & $14$ & $30$ & $2 \times 3 +8$ &1,% 3
$5 \times 12 =$ \ldots & $5 \times 6 \times 2$ & $5 \times 10 + 5 \times 2$ & $60$ &1,% 2,3
Soit $ABC$ un triangle rectangle en $A$ tel que $AB = \Lg{3}$ ; $BC = \Lg{5}$ et $AC = \Lg{4} \text{. }$ On peut affirmer que \ldots  & le périmètre de $ABC$ est $\Lg{12}$. & l'aire du triangle $ABC$ est de $\Aire{12}$. & l'aire du triangle $ABC$ est de $\Aire{6}$. &1,%
Soit un rectangle dont la longueur est $L$ et la largeur $l \text{. }$ On peut affirmer que \ldots  & le périmètre est $2 \times L + 2 \times L$. & le périmètre est $(L+l) \times 2$. & l'aire est $L \times l$. &1% 2,3
}
\normalsize{}

\color{Green}
\section{Définition et simplification d'écritures}
\color{black}
\normalsize{}

\begin{tcolorbox}[colframe=Green!20, colback=Green!0, boxrule=2pt, arc=6pt, title={\textbf{Définition \arabic{chapter}.\directlua{tex.print(d)}}}, coltitle=black]
	Une expression littérale est une expression mathématique dans laquelle un ou plusieurs nombres sont représentés par des lettres.
\end{tcolorbox}
\begin{luacode}
	d=d+1
\end{luacode}

\textbf{\underline{Exemples}}
\begin{itemize}[label=\textbullet]
	\item $4x+7y-48$ et $-4c^2-25$ sont des expressions littérales.
	\item L'aire d'un rectangle s'exprime par la formule : $L \times l$. C'est une expression littérale dans laquelle $L$ désigne la longueur du rectangle et $l$ sa largeur.
\end{itemize}
\medskip
\begin{tcolorbox}[colframe=black!20, colback=blue!0, boxrule=2pt, arc=6pt, title={\textbf{Convention \arabic{chapter}.\directlua{tex.print(m)}}}, coltitle=black]
	Le signe \guillemetleft\, $ \times $ \guillemetright\, de la multiplication peut ne pas être écrit lorsqu'il est situé avant une lettre ou une parenthèse.
\end{tcolorbox}
\begin{luacode}
	m=m+1
\end{luacode}

\underline{\textbf{Exemples}}\\
Dans les exemples ci-dessous, $x, y, a, b$ représentent des nombres. 
\begin{itemize}[label=\textbullet] \vspace{-0.4cm}
	\begin{multicols}{2} 
		\item $5 \times x = 5x$
		\item $a\times b= ab$
		\item $3 \times 2 \times y + x = 6 \times y + x =6y+x$
		\item $7 \times (8-a)=7(8-a)$
	\end{multicols}
\end{itemize}
\medskip
\begin{tcolorbox}[colframe=black!20, colback=blue!0, boxrule=2pt, arc=6pt, title={\textbf{Convention \arabic{chapter}.\directlua{tex.print(m)}}}, coltitle=black]
$x$ désigne un nombre quelconque.
\begin{itemize}[label=\textbullet] \vspace{-0.4cm}
	\begin{multicols}{2}
		\item $1 \times x = x$
		\item $0 \times x = 0$
		\item $x \times x = x^2$ \faArrowRight\, On lit \guillemetleft\,$x$ au carré \guillemetright.
		\item $x \times x \times x = x^3$ \faArrowRight\, On lit \guillemetleft\,$x$ au cube \guillemetright.
		\end{multicols}
	\end{itemize}	
\end{tcolorbox}
\begin{luacode}
	m=m+1
\end{luacode}

\underline{\textbf{Exemples}}
\begin{itemize}[label=\textbullet] \vspace{-0.3cm}
	\begin{multicols}{2}
		\item $5 \times x \times x = 5x^2$
		\item $2a \times 6a \times a = 2 \times 6 \times a \times a \times a =12a^3$
	\end{multicols}
\end{itemize}

\color{Green}
\section{Tester une égalité}
\color{black}
\begin{tcolorbox}[colframe=Green!20, colback=Green!0, boxrule=2pt, arc=6pt, title={\textbf{Définition \arabic{chapter}.\directlua{tex.print(d)}}}, coltitle=black]
	Une égalité est constituée de deux expressions, appelées membres, séparées par le symbole \guillemetleft\, $=$ \guillemetright.
\end{tcolorbox}
\begin{luacode}
	d=d+1
\end{luacode}

\underline{\textbf{Exemples}}
\begin{itemize}[label=\textbullet] \vspace{-0.3cm}
	\begin{multicols}{2}
		\item $3 \times 14 = 42$
		\item $3x = 7 + 5$
	\end{multicols}
\end{itemize}
\medskip
\begin{tcolorbox}[colframe=blue!20, colback=blue!0, boxrule=2pt, arc=6pt, title={\textbf{Propriété \arabic{chapter}.\directlua{tex.print(p)}}}, coltitle=black]
	Une égalité dont au moins l'un des membres est une expression littérale peut être vraie pour certaines valeurs et fausses pour d'autres valeurs.
\end{tcolorbox}
\begin{luacode}
	p=p+1
\end{luacode}

\underline{\textbf{Exemple}}\\
On considère l'égalité $ 2 + x = 9$.
\begin{itemize}[label=\textbullet]
	\item Cette égalité est vraie pour $x=7$ car $2 + 7 =9$.
	\item Cette égalité est fausse pour $x=1$ car $ 2 +1 =3 \neq 9$.
\end{itemize}
\medskip
\begin{tcolorbox}[colframe=black!20, colback=blue!0, boxrule=2pt, arc=6pt, title={\textbf{Convention \arabic{chapter}.\directlua{tex.print(m)}}}, coltitle=black]
	Pour tester si une égalité est vraie :
	\begin{enumerate}[label=\textbf{\arabic*)}]
		\item Dans le membre de gauche, on remplace chaque lettre par le nombre donné et on calcule.
		\item On fait la même chose dans le membre de droite.
		\item On compare les deux résultats obtenus :
			\begin{itemize}[label=\textbullet]
				\item si les deux résultats sont identiques, l'égalité est vraie pour le nombre donné.
				\item si les deux résultats sont différents, l'égalité est fausse pour le nombre donné.
			\end{itemize}
	\end{enumerate}	
\end{tcolorbox}
\begin{luacode}
	m=m+1
\end{luacode}

\underline{\textbf{Exemple}}\\
On considère l'égalité suivante : $3x + 5 = 5x -9$.
\begin{enumerate}[label=\textbf{\arabic*)}]
	\item Cette égalité est-elle vraie pour $x=2$ ?
	\item Cette égalité est-elle vraie pour $x=7$ ?
\end{enumerate}
~~\\
\begin{enumerate}[label=\textbf{\arabic*)}]
	\item \begin{minipage}{0.45\textwidth}
		\underline{Membre de gauche}
		\begin{flalign*}
			3 \times 2 + 5 & =6+5&\\
			&=11
		\end{flalign*}
	\end{minipage} \hspace{0.05cm} \vrule \hspace{0.05cm}
	\begin{minipage}{0.45\textwidth}
		\underline{Membre de droite}
		\begin{flalign*}
			5 \times 2 - 9 &= 10-9&\\
			&=1
		\end{flalign*}
	\end{minipage}
	~~\\
	On a $11 \neq 1$ donc cette égalité est fausse pour $x=2$.\\
	\item \begin{minipage}{0.45\textwidth}
		\underline{Membre de gauche}
		\begin{flalign*}
			3 \times 7 + 5 & =21+5&\\
			&=26
		\end{flalign*}
	\end{minipage} \hspace{0.05cm} \vrule \hspace{0.05cm}
	\begin{minipage}{0.45\textwidth}
		\underline{Membre de droite}
		\begin{flalign*}
			5 \times 7 - 9 &= 35-9&\\
			&=26
		\end{flalign*}
	\end{minipage}
	~~\\
	Les deux résultats sont identiques donc cette égalité est vraie pour $x=7$.
\end{enumerate}

\color{Green}
\section{Résolution de problèmes}
\color{black}

\underline{\textbf{Exemple 1}}\\
\begin{minipage}{0.45\textwidth}
On considère un carré dont la longueur du côté est $c+5$ cm.
\begin{enumerate}[label=\textbf{\arabic*)}]
	\item Donner l'expression du périmètre de ce carré en fonction de $c$.
	\item Calculer ce périmètre pour $c=13$.
\end{enumerate}
\end{minipage} \hspace{0.05cm} \vrule \hspace{0.05cm}
\begin{minipage}{0.45\textwidth}
\begin{enumerate}[label=\textbf{\arabic*)}]
	\item \underline{Périmètre du carré}\\
	$c+5+c+5+c+5+c+5=4c+20$\\
	Le périmètre de ce carré vaut $4c+20$ cm.
	\item On remplace $c$ par $13$.\\
	$4\times 13 + 20= 52+20=72$\\
	Si $c$ vaut $\Lg{13}$, le périmètre de ce carré vaut $\Lg{72}$.
\end{enumerate}
\end{minipage}
~~\\
\underline{\textbf{Exemple 2}}\\
\begin{minipage}{0.45\textwidth}
À la boulangerie, le prix d'un pain au chocolat est $1,10$ \texteuro\, et celui d'une baguette est $0,90$ \texteuro. François Viète achète $n$ pains au chocolat et trois baguettes.
\begin{enumerate}[label=\textbf{\arabic*)}]
	\item Donner l'expression littérale qui permet de calculer le prix dépensé par François Viète.
	\item  François Viète achète $8$ pains au chocolat, combien a-t-il dépensé ?
\end{enumerate}
\end{minipage} \hspace{0.05cm} \vrule \hspace{0.05cm}
\begin{minipage}{0.45\textwidth}
\begin{enumerate}[label=\textbf{\arabic*)}]
	\item Soit $P$ le prix payé par François Viète. \vspace{-0.5cm}
		\begin{flalign*}
			P &= n \times 1,10 + 3 \times 0,90&\\
			P&= n \times 1,10 + 2,70
		\end{flalign*}
	\item Dans cette question, on fixe $n=8$. Donc :\vspace{-0.5cm}
		\begin{flalign*}
			P &= 8 \times 1,10 + 2,70&\\
			P&=8,80 + 2,70\\
			P&=11,50
		\end{flalign*}
		François Viète a payé $11,50$ \texteuro.
\end{enumerate}
\end{minipage}

\color{Green}
\section{Bilan}
\color{black}
QCM (une seule réponse possible)\\\footnotesize{}
\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom= , AlphT, Largeur=3cm]{%
$A=5x-2$. La valeur de $A$ pour $x=3$ est \ldots  & $6$ & $9$ & $13$ &3,%
$B=2y^2$. La valeur de $B$ pour $y=5$ est \ldots  & $10$ & $50$ & $100$ &2,%
René Descartes achète un livre à $6$ \texteuro\, et $n$ stylos à $2$ \texteuro. L'expression littérale qui permet de calculer le montant $M$ de sa dépense en \texteuro est \ldots & $M=6 \times 2 \times n$ & $M=6n+2$ & $M=6+2n$ &3,%
Voici un programme de calcul :
\begin{itemize}[label=\textbullet]
	\item Choisir un nombre $a$.
	\item Ajouter $3$ au résultat précédent.
	\item Multiplier par $2$ le résultat précédent.
\end{itemize}
L'expression littérale correspondant à ce programme de calcul est \ldots  & $2a +3$ & $a+ 3 \times 2$ & $2 (a+3) $&3,%
L'égalité $5x+7=2x+16$ est vraie pour \ldots  & $x=2$ & $x=3$ & $x=4$ &2,%
L'égalité qui est vraie pour $x=4$ est \ldots  & $x^2-9=x+3$ & $2x = x+5$ & $8x-4=0$ &1%
}
\normalsize{}

\color{Green}
\section{Auto-évaluation}
\color{black}
\begin{tabular}{|>{\columncolor{darkgray}\color{black}}m{8cm} |>{\color{black}}m{0.6cm}|m{0.6cm}|m{0.6cm}|m{5cm}|} \hline

\rowcolor{Lavender}\centering{} \color{black} \textbf{Compétences / Savoir-faire} & \centering{}\color{MediumSeaGreen} \faCheck & \centering{}\color{DarkOrange}\faWater & \centering{}\color{Crimson}\faTimes & \centering{}\color{black}Pour réviser \tabularnewline \hline \hline

\cellcolor{Lavender}Je suis capable de simplifier les expressions littérales lorsque cela est possible. & & & & \textbf{\underline{Leçon}} : Partie I  \smallskip \newline  \textbf{\underline{Exercices}} :  \tabularnewline \hline \hline

\cellcolor{Lavender}Je suis capable de montrer qu'une égalité est vraie ou fausse pour une valeur donnée. & & & & \textbf{\underline{Leçon}} : Partie II \smallskip \newline  \textbf{\underline{Exercices}} :    \tabularnewline \hline \hline

\cellcolor{Lavender}Je suis capable de résoudre des problèmes en utilisant le calcul littéral. & & & & \textbf{\underline{Leçon}} : Partie III \smallskip \newline  \textbf{\underline{Exercices}} :  \tabularnewline \hline 
\end{tabular}

%\begin{comment}
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------- Correction QCM -----------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\newpage
%\setcounter{page}{1}
\footnotesize{}\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom= , AlphT, Largeur=3cm, Solution, Couleur=cyan!20]{%
$5 \times (4+3) =$ \ldots  & $23$ & $35$ & \cellcolor{cyan!20} $(4+3) \times 5$ &2,%
$8+2 \times 3 = $ \ldots  & $14$ & $30$ & \cellcolor{cyan!20} $2 \times 3 +8$ &1,%
$5 \times 12 =$ \ldots & $5 \times 6 \times 2$ & \cellcolor{cyan!20} $5 \times 10 + 5 \times 2$ & \cellcolor{cyan!20} $60$ &1,%
Soit $ABC$ un triangle rectangle en $A$ tel que $AB = \Lg{3}$ ; $BC = \Lg{5}$ et $AC = \Lg{4} \text{. }$ On peut affirmer que \ldots  & le périmètre de $ABC$ est $\Lg{12}$. & l'aire du triangle $ABC$ est de $\Aire{12}$. & l'aire du triangle $ABC$ est de $\Aire{6}$. &1,%
Soit un rectangle dont la longueur est $L$ et la largeur $l \text{. }$ On peut affirmer que \ldots  & le périmètre est $2 \times L + 2 \times L$. & \cellcolor{cyan!20}le périmètre est $(L+l) \times 2$. & \cellcolor{cyan!20}l'aire est $L \times l$. &1%
}
\\
\\
\\
\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom= , AlphT, Largeur=3cm, Solution, Couleur=cyan!20]{%
$A=5x-2$. La valeur de $A$ pour $x=3$ est \ldots  & $6$ & $9$ & $13$ &3,%
$B=2y^2$. La valeur de $B$ pour $y=5$ est \ldots  & $10$ & $50$ & $100$ &2,%
René Descartes achète un livre à $6$ \texteuro et $n$ stylos à $2$ \texteuro. L'expression littérale qui permet de calculer le montant $M$ de sa dépense en \texteuro\, est \ldots & $M=6 \times 2 \times n$ & $M=6n+2$ & $M=6+2n$ &3,%
Voici un programme de calcul :
\begin{itemize}[label=\textbullet]
	\item Choisir un nombre $a$.
	\item Ajouter 3 au résultat précédent.
	\item Multiplier par 2 le résultat précédent.
\end{itemize}
L'expression littérale correspondant à ce programme de calcul est \ldots  & $2a +3$ & $a+ 3 \times 2$ & $2 (a+3) $&3,%
L'égalité $5x+7=2x+16$ est vraie pour \ldots  & $x=2$ & $x=3$ & $x=4$ &2,%
L'égalité qui est vraie pour $x=4$ est \ldots  & $x^2-9=x+3$ & $2x = x+5$ & $8x-4=0$ &1%
}
\normalsize{}

%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%---------------------------------------------------------------------- Leçon à trous -----------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\newpage
%\setcounter{page}{1}
%format du titre des chapitres placé après Sommaire sinon cela bugue
\titleformat{\chapter}[frame] {\normalfont \bfseries \Large}{\textcolor{red} {\chaptertitlename\ \thechapter\,-\,#1}}{0pt}{\Huge}

\setcounter{chapter}{10}

\chapter{\pointilles[13cm]}
\begin{luacode}
	d=1
	p=1
	t=1
	m=1
\end{luacode}

\color{blue}\underline{\textbf{Prérequis}}
\begin{itemize}
	\item \pointilles[10cm] ;
	\item Calculs \pointilles[10cm].
\end{itemize}~~

\underline{\textbf{Objectifs}}
\begin{itemize}
	\item \pointilles[10cm].
	\item \pointilles[10cm].
	\item \pointilles[10cm].
	\item \pointilles[10cm].
\end{itemize}
\color{black}
~~\\
\textbf{\underline{Rappels}} : QCM (plusieurs réponses possibles)\\\footnotesize{}
\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom= , AlphT, Largeur=3cm]{%
$5 \times (4+3) =$ \ldots  & $23$ & $35$ & $(4+3) \times 5$ &2,% 3
$8+2 \times 3 = $ \ldots  & $14$ & $30$ & $2 \times 3 +8$ &1,% 3
$5 \times 12 =$ \ldots & $5 \times 6 \times 2$ & $5 \times 10 + 5 \times 2$ & $60$ &1,% 2,3
Soit $ABC$ un triangle rectangle en $A$ tel que $AB = \Lg{3}$ ; $BC = \Lg{5}$ et $AC = \Lg{4} \text{. }$ On peut affirmer que \ldots  & le périmètre de $ABC$ est $\Lg{12}$. & l'aire du triangle $ABC$ est de $\Aire{12}$. & l'aire du triangle $ABC$ est de $\Aire{6}$. &1,%
Soit un rectangle dont la longueur est $L$ et la largeur $l \text{. }$ On peut affirmer que \ldots  & le périmètre est $2 \times L + 2 \times L$. & le périmètre est $(L+l) \times 2$. & l'aire est $L \times l$. &1% 2,3
}
\normalsize{}

\color{Green}
\section{Définition et \pointilles[7cm].}
\color{black}
\normalsize{}

\begin{tcolorbox}[colframe=Green!20, colback=Green!0, boxrule=2pt, arc=6pt, title={\textbf{Définition \arabic{chapter}.\directlua{tex.print(d)}}}, coltitle=black]
	Une \pointilles[15cm]\\
	\pointilles[16cm]
\end{tcolorbox}
\begin{luacode}
	d=d+1
\end{luacode}

\textbf{\underline{Exemples}}
\begin{itemize}[label=\textbullet]
	\item \pointilles[16cm].
	\item L'aire d'un \pointilles[10cm]. C'est une expression \pointilles[16cm].
\end{itemize}
\bigskip
\begin{tcolorbox}[colframe=black!20, colback=blue!0, boxrule=2pt, arc=6pt, title={\textbf{Convention \arabic{chapter}.\directlua{tex.print(m)}}}, coltitle=black]
	Le \pointilles[15cm]\\
	\pointilles[16cm]
\end{tcolorbox}
\begin{luacode}
	m=m+1
\end{luacode}

\underline{\textbf{Exemples}}\\
Dans les exemples ci-dessous, $x, y, a, b$ représentent des nombres. 
\begin{itemize}[label=\textbullet] \vspace{-0.4cm}
	\begin{multicols}{2} 
		\item \pointilles[7cm]
		\item \pointilles[7cm]
		\item \pointilles[7cm]
		\item \pointilles[7cm]
	\end{multicols}
\end{itemize}
\bigskip
\begin{tcolorbox}[colframe=black!20, colback=blue!0, boxrule=2pt, arc=6pt, title={\textbf{Convention \arabic{chapter}.\directlua{tex.print(m)}}}, coltitle=black]
$x$ désigne un nombre quelconque.
\begin{itemize}[label=\textbullet] \vspace{-0.4cm}
	\begin{multicols}{2}
		\item \pointilles[7cm]
		\item \pointilles[7cm]
		\item \pointilles[7cm]
		\item \pointilles[7cm]
		\end{multicols}
	\end{itemize}	
\end{tcolorbox}
\begin{luacode}
	m=m+1
\end{luacode}

\underline{\textbf{Exemples}}
\begin{itemize}[label=\textbullet] \vspace{-0.4cm}
	\begin{multicols}{2}
		\item \pointilles[7cm]
		\item \pointilles[7cm]
	\end{multicols}
\end{itemize}

\color{Green}
\section{\pointilles[7cm]}
\color{black}
\begin{tcolorbox}[colframe=Green!20, colback=Green!0, boxrule=2pt, arc=6pt, title={\textbf{Définition \arabic{chapter}.\directlua{tex.print(d)}}}, coltitle=black]
	Une \pointilles[15cm]\\
	\pointilles[16cm]
\end{tcolorbox}
\begin{luacode}
	d=d+1
\end{luacode}

\underline{\textbf{Exemples}}
\begin{itemize}[label=\textbullet] \vspace{-0.4cm}
	\begin{multicols}{2}
		\item \pointilles[7cm]
		\item \pointilles[7cm]
	\end{multicols}
\end{itemize}
\bigskip
\begin{tcolorbox}[colframe=blue!20, colback=blue!0, boxrule=2pt, arc=6pt, title={\textbf{Propriété \arabic{chapter}.\directlua{tex.print(p)}}}, coltitle=black]
	Une \pointilles[15cm]\\
	\pointilles[16cm]
\end{tcolorbox}
\begin{luacode}
	p=p+1
\end{luacode}

\underline{\textbf{Exemple}}\\
On considère l'égalité $ 2 + x = 9$.
\begin{itemize}[label=\textbullet]
	\item Cette \pointilles[12cm]
	\item Cette \pointilles[12cm]
\end{itemize}
\bigskip
\begin{tcolorbox}[colframe=black!20, colback=blue!0, boxrule=2pt, arc=6pt, title={\textbf{Convention \arabic{chapter}.\directlua{tex.print(m)}}}, coltitle=black]
	Pour tester si une égalité est vraie :
	\begin{enumerate}[label=\textbf{\arabic*)}]
		\item Dans \pointilles[14cm] 
		\item On \pointilles[14cm]
		\item On \pointilles[14cm]
			\begin{itemize}[label=\textbullet]
				\item si \pointilles[13cm]
				\item si \pointilles[13cm]
			\end{itemize}
	\end{enumerate}	
\end{tcolorbox}
\begin{luacode}
	m=m+1
\end{luacode}

\underline{\textbf{Exemple}}\\
On considère l'égalité suivante : $3x + 5 = 5x -9$.
\begin{enumerate}[label=\textbf{\arabic*)}]
	\item Cette égalité est-elle vraie pour $x=2$ ?
	\item Cette égalité est-elle vraie pour $x=7$ ?
\end{enumerate}
~~\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]

\color{Green}
\section{\pointilles[7cm]\\}
\color{black}

\underline{\textbf{Exemple 1}}\\
On considère un carré dont la longueur du côté est $c+5$ cm.
\begin{enumerate}[label=\textbf{\arabic*)}]
	\item Donner l'expression du périmètre de ce carré en fonction de $c$.
	\item Calculer ce périmètre pour $c=13$.
\end{enumerate}
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]
\\\\
\underline{\textbf{Exemple 2}}\\
À la boulangerie, le prix d'un pain au chocolat est $1,10$ \texteuro\, et celui d'une baguette est $0,90$ \texteuro. François Viète achète $n$ pains au chocolat et trois baguettes.
\begin{enumerate}[label=\textbf{\arabic*)}]
	\item Donner l'expression littérale qui permet de calculer le prix dépensé par François Viète.
	\item  François Viète achète $8$ pains au chocolat, combien a-t-il dépensé ?
\end{enumerate}
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]

\color{Green}
\section{Bilan}
\color{black}
QCM (une seule réponse possible)\\\footnotesize{}
\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom= , AlphT, Largeur=3cm]{%
$A=5x-2$. La valeur de $A$ pour $x=3$ est \ldots  & $6$ & $9$ & $13$ &3,%
$B=2y^2$. La valeur de $B$ pour $y=5$ est \ldots  & $10$ & $50$ & $100$ &2,%
René Descartes achète un livre à $6$ \texteuro\, et $n$ stylos à $2$ \texteuro. L'expression littérale qui permet de calculer le montant $M$ de sa dépense en \texteuro\, est \ldots & $M=6 \times 2 \times n$ & $M=6n+2$ & $M=6+2n$ &3,%
Voici un programme de calcul :
\begin{itemize}[label=\textbullet]
	\item Choisir un nombre $a$.
	\item Ajouter 3 au résultat précédent.
	\item Multiplier par 2 le résultat précédent.
\end{itemize}
L'expression littérale correspondant à ce programme de calcul est \ldots  & $2a +3$ & $a+ 3 \times 2$ & $2 (a+3) $&3,%
L'égalité $5x+7=2x+16$ est vraie pour \ldots  & $x=2$ & $x=3$ & $x=4$ &2,%
L'égalité qui est vraie pour $x=4$ est \ldots  & $x^2-9=x+3$ & $2x = x+5$ & $8x-4=0$ &1%
}
\normalsize{}

\color{Green}
\section{Auto-évaluation}
\color{black}
\begin{tabular}{|>{\columncolor{darkgray}\color{black}}m{8cm} |>{\color{black}}m{0.6cm}|m{0.6cm}|m{0.6cm}|m{5cm}|} \hline

\rowcolor{Lavender}\centering{} \color{black} \textbf{Compétences / Savoir-faire} & \centering{}\color{MediumSeaGreen} \faCheck & \centering{}\color{DarkOrange}\faWater & \centering{}\color{Crimson}\faTimes & \centering{}\color{black}Pour réviser \tabularnewline \hline \hline

\cellcolor{Lavender}Je suis capable de simplifier les expressions littérales lorsque cela est possible. & & & & \textbf{\underline{Leçon}} : Partie I  \smallskip \newline  \textbf{\underline{Exercices}} :  \tabularnewline \hline \hline

\cellcolor{Lavender}Je suis capable de montrer qu'une égalité est vraie ou fausse pour une valeur donnée. & & & & \textbf{\underline{Leçon}} : Partie II \smallskip \newline  \textbf{\underline{Exercices}} :    \tabularnewline \hline \hline

\cellcolor{Lavender}Je suis capable de résoudre des problèmes en utilisant le calcul littéral. & & & & \textbf{\underline{Leçon}} : Partie III \smallskip \newline  \textbf{\underline{Exercices}} :  \tabularnewline \hline 
\end{tabular}


%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------- Impression -----------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\newpage
\textbf{\underline{Rappels}} : QCM (plusieurs réponses possibles)\\\footnotesize{}
\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom= , AlphT, Largeur=3cm]{%
$5 \times (4+3) =$ \ldots  & $23$ & $35$ & $(4+3) \times 5$ &2,% 3
$8+2 \times 3 = $ \ldots  & $14$ & $30$ & $2 \times 3 +8$ &1,% 3
$5 \times 12 =$ \ldots & $5 \times 6 \times 2$ & $5 \times 10 + 5 \times 2$ & $60$ &1,% 2,3
Soit $ABC$ un triangle rectangle en $A$ tel que $AB = \Lg{3}$ ; $BC = \Lg{5}$ et $AC = \Lg{4} \text{. }$ On peut affirmer que \ldots  & le périmètre de $ABC$ est $\Lg{12}$. & l'aire du triangle $ABC$ est de $\Aire{12}$. & l'aire du triangle $ABC$ est de $\Aire{6}$. &1,%
Soit un rectangle dont la longueur est $L$ et la largeur $l \text{. }$ On peut affirmer que \ldots  & le périmètre est $2 \times L + 2 \times L$. & le périmètre est $(L+l) \times 2$. & l'aire est $L \times l$. &1% 2,3
}
\normalsize{}
\\
\\
\\
QCM (une seule réponse possible)\\\footnotesize{}
\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom= , AlphT, Largeur=3cm]{%
$A=5x-2$. La valeur de $A$ pour $x=3$ est \ldots  & $6$ & $9$ & $13$ &3,%
$B=2y^2$. La valeur de $B$ pour $y=5$ est \ldots  & $10$ & $50$ & $100$ &2,%
René Descartes achète un livre à $6$ \texteuro\, et $n$ stylos à $2$ \texteuro. L'expression littérale qui permet de calculer le montant $M$ de sa dépense en \texteuro\, est \ldots & $M=6 \times 2 \times n$ & $M=6n+2$ & $M=6+2n$ &3,%
Voici un programme de calcul :
\begin{itemize}[label=\textbullet]
	\item Choisir un nombre $a$.
	\item Ajouter 3 au résultat précédent.
	\item Multiplier par 2 le résultat précédent.
\end{itemize}
L'expression littérale correspondant à ce programme de calcul est \ldots  & $2a +3$ & $a+ 3 \times 2$ & $2 (a+3) $&3,%
L'égalité $5x+7=2x+16$ est vraie pour \ldots  & $x=2$ & $x=3$ & $x=4$ &2,%
L'égalité qui est vraie pour $x=4$ est \ldots  & $x^2-9=x+3$ & $2x = x+5$ & $8x-4=0$ &1%
}
\normalsize{}

%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%--------------------------------------------------------------- Histoire des mathématiques -------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\newpage
\begin{tcolorbox}[colframe=black, colback=blue!10, boxrule=2pt, arc=6pt]
	\centering{}\LARGE{\textbf{Histoire des mathématiques \faBook}}\\
	Viète, Descartes
\end{tcolorbox}
\begin{tcolorbox}[colframe=black!30, colback=blue!0, boxrule=2pt, arc=6pt, title=François Viète ($1540\, ;\, 1603$), coltitle=black]
	\begin{minipage}{0.3\textwidth}
		\includegraphics[width=5cm]{Francois_Viete.jpeg}
	\end{minipage} \hspace{0.05\textwidth}
	\begin{minipage}{0.65\textwidth}
	François Viète est un mathématicien et astronome français du XVI\ieme\,siècle. Son travail a permis de populariser les notations littérales en mathématiques. Il introduit les voyelles pour noter les inconnues et les consonnes pour les quantités connues. Il simplifie les notations utilisées jusqu'alors mais en introduit de nouvelles un peu lourdes.\\
	\\
	Par exemple, $A^3$ se note \textit{A cubus}, le signe $=$ se note \textit{\ae qualis} et le signe de la multiplication $\times$ s'écrit \textit{in} (vient du latin). Voici un exemple de calcul écrit par François Viète : \textit{B in A quadratum plus D plano in A \ae quari C solido}, qui se traduit par $BA^2+DA=C$, que nous noterions maintenant $bx^2+dx=c$.\\
	\end{minipage}
	~~\\
	Il est l'un des premiers à introduire la barre de fraction pour la division et les signes $+$ et $-$. Il milite également pour abandonner le système sexagésimal (base $60$) pour un système en base décimal (base $10$).
\end{tcolorbox}

\begin{tcolorbox}[colframe=black!10, colback=blue!0, boxrule=1pt, arc=1pt, title=Quelques anecdotes, coltitle=black]
	Au début du règne d'Henri IV, alors que celui-ci luttait contre des alliés de l'Espagne, François Viète traduisit facilement les messages secrets espagnols. Philippe II, roi d'Espagne, pensant qu'ils étaient indéchiffrables, se plaignit auprès du Pape, que François Viète utilisait des pratiques magiques contraires à la foi chrétienne.
\end{tcolorbox}


\textbf{\underline{Sources}}
\begin{itemize}[label=\textbullet]
	\item Hauchecorne, B. et Suratteau, D. (2008), \textit{Des Mathématiciens de A à Z} ($3^e$ édition), Ellipses
	\item Barbin, E. (2019), \textit{Faire des mathématiques avec l'histoire au lycée}, Ellipses
\end{itemize}

%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%-------------------------------------------------------------------------- Fiche de route ------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

\newpage
\begin{tcolorbox}[colframe=black, colback=Khaki!50, boxrule=2pt, arc=6pt]
	\centering{}\LARGE{\textbf{Fiche de route}}
\end{tcolorbox}
\begin{itemize}[label=\textbullet]
	\item \color{RoyalBlue}\underline{Leçon} \color{black} : Partie :
	\item \color{ForestGreen}\underline{Exercices}  \color{black} :
	\item \color{RoyalBlue}\underline{Leçon}  \color{black} : Parties et : Bilan + Auto-évaluation
	\item \color{ForestGreen}\underline{Exercices bilan}  \color{black} : 
\end{itemize}


%\setcounter{page}{4}
%\end{comment}
\end{document}
