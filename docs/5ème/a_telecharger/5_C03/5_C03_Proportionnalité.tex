\documentclass[dvipsnames, a4paper, 12pt]{report}

\usepackage{lmodern} %police standard latin moderne
\usepackage[frenchb]{babel} %langue française
\usepackage[utf8]{inputenc} %pour l'utf-8
\usepackage[T1]{fontenc} %pour les césures et accentuations
\usepackage{geometry} %pour changer les marges d'un document
\usepackage{graphicx}
\usepackage{fancybox}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{xlop}
\usepackage{fontawesome5}
\usepackage{ProfCollege}
\usepackage{luacode}
\usepackage[explicit]{titlesec} %Permet de modifier les différentes parties, chapitres, sous-parties,...

\usepackage{colortbl} %permet de mettre des cellules de tableau en couleur

\usepackage{multicol}

\usepackage{cellspace} %pour définir les espaces en haut et en bas des cellules d'un tableau
\setlength{\cellspacebottomlimit}{5pt}
\setlength{\cellspacetoplimit}{5pt}
\setlength{\parindent}{0pt}

\usepackage{xcolor} %permet de mettre du texte en couleur

\usepackage{array} %gestion des tableaux
\usepackage{tcolorbox} %créer des boîtes de couleurs

\usepackage{hyperref} %permet de gérer le sommaire et la page de garde et les options du PDF

\geometry{top=2cm, bottom=2cm, right=2cm, left=2cm}   
\titlespacing{\chapter}{0pt}{0pt}{5pt}

\usepackage{lastpage}
\usepackage{fancyhdr} %définition des en-têtes et des bas de page
\fancyhf{}
\fancypagestyle{main}{
	\renewcommand\headrulewidth{5pt}
	\fancyhead[L]{5\ieme\, - Chapitre 03 - Proportionnalité}
	\fancyhead[R]{Collèges Marie Curie / Félicien Joly}
	\fancyfoot[C]{\thepage/\pageref{LastPage}}
	}
\pagestyle{main}

%gestion des entêtes
\makeatletter\@addtoreset{section}{chapter}\makeatother % RAZ des numéros de section après un chapitre
\renewcommand{\thepart}{\Alph{part} -} % Pour mettre des I, II, etc. aux parties
\renewcommand{\thesection}{\color{Green} \Roman{section} -}% Idem pour les sections et avoir le numéro de chapitre
\renewcommand{\thesubsection}{\arabic{subsection} -}

\newcommand\ex{\textbf{\underline{Exemple}}}
\newcommand\exs{\textbf{\underline{Exemples}}}
\newcommand\G{\color{Green}}
\newcommand\R{\color{red}}
\newcommand\B{\color{black}}
\newcommand\bl{\color{blue}}

\begin{document}
%format du titre des chapitres placé après Sommaire sinon cela bugue
\titleformat{\chapter}[frame] {\normalfont \bfseries \Large}{\textcolor{red} {\chaptertitlename\ \thechapter\,-\,#1}}{0pt}{\Huge}

\setcounter{chapter}{2}

\chapter{Proportionnalité}
\begin{luacode}
	d=1
	p=1
	t=1
	m=1
\end{luacode}

\color{blue}\underline{\textbf{Prérequis}}
\begin{itemize}
	\item Fractions
	\item Opérations de base avec les nombres décimaux.
\end{itemize}~~

\underline{\textbf{Objectifs}}
\begin{itemize}
	\item Être capable de savoir si un tableau représente une situation de proportionnalité.
	\item Calculer la quatrième proportionnelle.
\end{itemize}
\color{black}


\color{Green}
\section{Tableau de proportionnalité}
\color{black}
\normalsize{}
\begin{tcolorbox}[colframe=blue!20, colback=blue!0, boxrule=2pt, arc=6pt, title={\textbf{Propriété \arabic{chapter}.\directlua{tex.print(p)}}}, coltitle=black]
	Si le tableau ci-dessous est un tableau de proportionnalité, l'égalité des produits en croix permet d'écrire $a \times d = c \times b$.
	\begin{center}
		\begin{tabular}{|c|c|c|} \hline
			\textbf{Grandeur A} & a & b \tabularnewline \hline
			\textbf{Grandeur B} &c & d \tabularnewline \hline
		\end{tabular}
	\end{center}
\end{tcolorbox}
\begin{luacode}
	p=p+1
\end{luacode}

\begin{tcolorbox}[colframe=Green!20, colback=Green!0, boxrule=2pt, arc=6pt, title={\textbf{Définition \arabic{chapter}.\directlua{tex.print(d)}}}, coltitle=black]
Pour savoir si un tableau représente une situation de proportionnalité, on peut utiliser la méthode du produit en croix.
\begin{itemize}[label=\textbullet]
	\item Si l'égalité est respectée, le tableau représente une situation de proportionnalité.
	\item Sinon, le tableau ne représente pas une situation de proportionnalité.
\end{itemize}
\end{tcolorbox}
\begin{luacode}
	d=d+1
\end{luacode}

\textbf{\underline{Exemple}}\\
\begin{minipage}{0.5\textwidth}
Les quantités de pommes de terre et de carottes pour réaliser une purée sont synthétisées dans le tableau ci-dessous :
\begin{center}
	\Propor[GrandeurA=\begin{tabular}{c}Masse carottes \\ (en g) \end{tabular}, GrandeurB=\begin{tabular}{c}Masse PDT \\ (en g) \end{tabular}]{50/75,100/150}%
\end{center}
\end{minipage}
\hspace{0.05cm} \vrule \hspace{0.05cm}
\begin{minipage}{0.45\textwidth}
On réalise le produit en croix :\\
$\begin{cases}
 50 \times 150 = \num{7500}\\
 75 \times 100 = \num{7500}
 \end{cases}$\\\\
Comme $50 \times 150 = 75 \times 100$, le tableau représente bien une situation de proportionnalité.
\end{minipage}



\color{Green}
\section{Quatrième proportionnelle : produit en croix}
\color{black}

\begin{tcolorbox}[colframe=Green!20, colback=Green!0, boxrule=2pt, arc=6pt, title={\textbf{Définition \arabic{chapter}.\directlua{tex.print(d)} : quatrième proportionnelle}}, coltitle=black]
Dans un tableau de proportionnalité, si on connaît trois valeurs sur quatre, il est possible de calculer la quatrième valeur.
Cette valeur est appelée la quatrième proportionnelle.
\end{tcolorbox}
\begin{luacode}
	d=d+1
\end{luacode}


\textbf{\underline{\'Enoncé}}\\
On sait que 64 cartes Pokémon pèsent 108,8 g. Quelle est la masse de 723 cartes ?
\\\\
\textbf{\underline{Solution}}\\
On a le tableau de proportionnalité suivant : \vspace{-0.5cm}
\begin{center}
	\Propor[Math, GrandeurA=Nombre de cartes, GrandeurB=Masse (en g)]{64/\num{108.8}, 723/\dots}
\end{center}
\textbf{Masse de 723 cartes Pokémon}\\
Soit $M$ la masse recherchée, le produit en croix permet d'écrire $64 \times N = 108,8 \times 723$.\\\\
Donc : $N = \displaystyle \frac{723 \times 108,8}{64} = \num{1229.1}$\\\\
La masse de 723 cartes Pokémon est de 1 229,1 g.

\color{Green}
\section{Notion de pourcentage}
\color{black}

\begin{tcolorbox}[colframe=blue!20, colback=blue!0, boxrule=2pt, arc=6pt, title={\textbf{Propriété \arabic{chapter}.\directlua{tex.print(p)}}}, coltitle=black]
	Un pourcentage de $t$ \% traduit une situation de proportionnalité de coefficient $\dfrac{t}{100}$.\\
	Donc appliquer un taux de $t$ \% revient à multiplier par $\dfrac{t}{100}$.
\end{tcolorbox}
\begin{luacode}
	p=p+1
\end{luacode}

\textbf{\underline{Exemple}}\\
Dans une classe de $30$ élèves, $60$ \% des élèves pratiquent un sport.\\\\
$30 \times \dfrac{60}{100} = 18$. Il y a donc 18 élèves sportifs dans cette classe.
\\


\begin{tcolorbox}[colframe=Green!20, colback=Green!0, boxrule=2pt, arc=6pt, title={\textbf{Définition \arabic{chapter}.\directlua{tex.print(d)} : quatrième proportionnelle}}, coltitle=black]
Déterminer un pourcentage, c'est déterminer une proportion écrite sous forme d'une écriture fractionnaire dont le dénominateur est $100$.
\end{tcolorbox}
\begin{luacode}
	d=d+1
\end{luacode}

\textbf{\underline{Exemple}}\\
Sur 550 élèves, 231 sont externes. On a le tableau de proportionnalité suivant :
\begin{center}
	\begin{tabular}{|c|c|c|} \hline
		Nombre d'externes & 231 & $N$ \tabularnewline \hline
		Nombre d'élèves & 550 & 100 \tabularnewline \hline
	\end{tabular}
\end{center}
D'après l'égalité des produits en croix, on a : $231 \times 100 = 550 \times N$.\\\\
Ainsi, $N = \dfrac{231 \times 100}{550}=42$. Il y a donc $42$ \% d'externes dans ce collège.

\color{Green}
\section{Bilan}
\color{black}
\textbf{Plusieurs réponses possibles}.\\\\

\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom= , AlphT]{%
12 lettres identiques pèsent \Masse{500}. Alors \ldots & 24 de ces lettres pèsent \Masse[kg]{1}. & 3 de ces lettres pèsent \Masse{125}. &27 de ces lettres pèsent \Masse[kg]{1.125}.&3, %
Une voiture de course consomme \Capa{12} pour parcourir \Lg[km]{100}. On suppose que la consommation est proportionnelle à la distance parcourue. Lors d'un parcours de \Lg[km]{225}$\text{,}$ la consommation est de \ldots & \Capa{5.3}. & \Capa{27}. & \Capa{1 875}.&3, %
Dans une boulangerie$\text{,}$ 8 croissants identiques coûtent $\num{10,40}$ \texteuro. Le prix d'un seul croissant est \ldots & \num{0.77} \texteuro. & \num{1.04} \texteuro. & \num{1.3} \texteuro. &3,%
La distance qui sépare un observateur d'un éclair est proportionnelle à la durée entre l'instant où il le voit et l'instant où il entend le tonnerre. On sait que s'il y a \Temps{;;;;;9} entre ces deux instants$\text{,}$ l'orage est à \Lg[km]{3} de l'observateur et s'il y a \Temps{;;;;;12}$\text{,}$ il se trouve à \Lg[km]{4}. Si la durée entre les deux instants est de \Temps{;;;;;21}$\text{,}$ la distance qui sépare l'orage de l'observateur est \ldots & \Lg[km]{7}. & \Lg[km]{12}. & \Lg[km]{21}. &1%

}


\color{Green}
\section{Auto-évaluation}
\color{black}
\begin{tabular}{|>{\columncolor{darkgray}\color{black}}m{8cm} |>{\color{black}}m{0.6cm}|m{0.6cm}|m{0.6cm}|m{5cm}|} \hline

\rowcolor{Lavender}\centering{} \color{black} Compétences & \centering{}\color{MediumSeaGreen} \faCheck & \centering{}\color{DarkOrange}\faWater & \centering{}\color{Crimson}\faTimes & \centering{}\color{black}Pour réviser \tabularnewline \hline \hline

\cellcolor{Lavender}Je suis capable de reconnaître une situation de proportionnalité. & & & & \textbf{\underline{Leçon}} : Partie I \smallskip \newline  \textbf{\underline{Exercices}} :  1 - 2  \tabularnewline \hline \hline

\cellcolor{Lavender}Je sais calculer une quatrième proportionnelle. & & & & \textbf{\underline{Leçon}} : Partie II \smallskip \newline  \textbf{\underline{Exercices}} : 3 à 5  \tabularnewline \hline \hline

\cellcolor{Lavender}Je sais utiliser la notion de pourcentage. & & & & \textbf{\underline{Leçon}} : Partie III \smallskip \newline  \textbf{\underline{Exercices}} : 6 à 9 \tabularnewline \hline
\end{tabular}



%\begin{comment}
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------- Correction QCM -----------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\newpage
%\setcounter{page}{1}
\QCM[Alterne, Solution, Couleur=cyan!20,  Stretch=2,Reponses=3, Titre, Nom= , AlphT]{%
12 lettres identiques pèsent \Masse{500}. Alors \ldots & 24 de ces lettres pèsent \Masse[kg]{1}. &  \cellcolor{cyan!20}3 de ces lettres pèsent \Masse{125}. & \cellcolor{cyan!20}27 de ces lettres pèsent \Masse[kg]{1.125}.&1, %
Une voiture de course consomme \Capa{12} pour parcourir \Lg[km]{100}. On suppose que la consommation est proportionnelle à la distance parcourue. Lors d'un parcours de \Lg[km]{225}$\text{,}$ la consommation est de \ldots & \Capa{5.3}. & \Capa{27}. & \Capa{1 875}.&2, %
Dans une boulangerie$\text{,}$ 8 croissants identiques coûtent $\num{10,40}$ \texteuro. Le prix d'un seul croissant est \ldots & \num{0.77} \texteuro. & \num{1.04} \texteuro. & \num{1.3} \texteuro. &3,%
La distance qui sépare un observateur d'un éclair est proportionnelle à la durée entre l'instant où il le voit et l'instant où il entend le tonnerre. On sait que s'il y a \Temps{;;;;;9} entre ces deux instants$\text{,}$ l'orage est à \Lg[km]{3} de l'observateur et s'il y a \Temps{;;;;;12}$\text{,}$ il se trouve à \Lg[km]{4}. Si la durée entre les deux instants est de \Temps{;;;;;21}$\text{,}$ la distance qui sépare l'orage de l'observateur est \ldots & \Lg[km]{7}. & \Lg[km]{12}. & \Lg[km]{21}. &1,%
}
\normalsize{}
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%---------------------------------------------------------------------- Leçon à trous -----------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\newpage
%\setcounter{page}{1}
\titleformat{\chapter}[frame] {\normalfont \bfseries \Large}{\textcolor{red} {\chaptertitlename\ \thechapter\,-\,#1}}{0pt}{\Huge}

\setcounter{chapter}{2}

\chapter{\pointilles[10cm]}
\begin{luacode}
	d=1
	p=1
	t=1
	m=1
\end{luacode}

\color{blue}\underline{\textbf{Prérequis}}
\begin{itemize}
	\item \pointilles[10cm]
	\item Opérations de base avec les \pointilles[10cm].
\end{itemize}~~

\underline{\textbf{Objectifs}}
\begin{itemize}
	\item Être capable de savoir si un tableau représente une \pointilles[7cm].
	\item Calculer la \pointilles[10cm].
	\item Notion de \pointilles[7cm].
\end{itemize}
\color{black}


\color{Green}
\section{Tableau de proportionnalité}
\color{black}
\normalsize{}
\begin{tcolorbox}[colframe=blue!20, colback=blue!0, boxrule=2pt, arc=6pt, title={\textbf{Propriété \arabic{chapter}.\directlua{tex.print(p)}}}, coltitle=black]
	Si le tableau ci-dessous est un \pointilles[7cm], l'\pointilles[5cm] des \pointilles[7cm] permet d'écrire \pointilles[5cm].
	\begin{center}
		\begin{tabular}{|c|c|c|} \hline
			\textbf{Grandeur A} & a & b \tabularnewline \hline
			\textbf{Grandeur B} & c & d \tabularnewline \hline
		\end{tabular}
	\end{center}
\end{tcolorbox}
\begin{luacode}
	p=p+1
\end{luacode}

\begin{tcolorbox}[colframe=Green!20, colback=Green!0, boxrule=2pt, arc=6pt, title={\textbf{Définition \arabic{chapter}.\directlua{tex.print(d)}}}, coltitle=black]
Pour savoir si un tableau représente une situation de proportionnalité, on peut utiliser la méthode du \pointilles[7cm].
\begin{itemize}[label=\textbullet]
	\item Si l'égalité est \pointilles[3cm], le tableau représente une \pointilles[7cm].
	\item \pointilles[3cm], le tableau ne représente pas une \pointilles[7cm].
\end{itemize}
\end{tcolorbox}
\begin{luacode}
	d=d+1
\end{luacode}

\textbf{\underline{Exemple}}\\
\begin{minipage}{0.5\textwidth}
Les quantités de pommes de terre et de carottes pour réaliser une purée sont synthétisées dans le tableau ci-dessous :
\begin{center}
	\Propor[GrandeurA=\begin{tabular}{c}Masse carottes \\ (en g) \end{tabular}, GrandeurB=\begin{tabular}{c}Masse PDT \\ (en g) \end{tabular}]{50/75,100/150}%
\end{center}
\end{minipage}
\hspace{0.05cm} \vrule \hspace{0.05cm}
\begin{minipage}{0.45\textwidth}
\pointilles[8cm]\\
\pointilles[8cm]\\
\pointilles[8cm]\\
\pointilles[8cm] \\
\pointilles[8cm]\\
\pointilles[8cm]
\end{minipage}



\color{Green}
\section{Quatrième proportionnelle : produit en croix}
\color{black}

\begin{tcolorbox}[colframe=Green!20, colback=Green!0, boxrule=2pt, arc=6pt, title={\textbf{Définition \arabic{chapter}.\directlua{tex.print(d)} : quatrième proportionnelle}}, coltitle=black]
Dans un tableau de proportionnalité, si on connaît \pointilles[10cm], il est possible de calculer la \pointilles[7cm]. Cette valeur est appelée la \pointilles[7cm].
\end{tcolorbox}
\begin{luacode}
	d=d+1
\end{luacode}


\textbf{\underline{\'Enoncé}}\\
On sait que 64 cartes Pokémon pèsent 108,8 g. Quelle est la masse de 723 cartes ?
\\\\
\textbf{\underline{Solution}}\\
On a le tableau de proportionnalité suivant : \vspace{-0.5cm}
\begin{center}
	\Propor[Math, GrandeurA=Nombre de cartes, GrandeurB=Masse (en g)]{\dots/\dots, \dots/\dots}
\end{center}\vspace{-0.5cm}
\textbf{Masse de 723 cartes Pokémon}\\
Soit $M$ la masse recherchée, le produit en croix permet d'écrire \pointilles[5cm].\\\\
\pointilles[17cm]\\\\
\pointilles[10cm]

\color{Green}
\section{Notion de pourcentage}
\color{black}

\begin{tcolorbox}[colframe=blue!20, colback=blue!0, boxrule=2pt, arc=6pt, title={\textbf{Propriété \arabic{chapter}.\directlua{tex.print(p)}}}, coltitle=black]
	Un pourcentage de \pointilles[2cm] traduit une \pointilles[7cm] de coefficient \pointilles[2cm].\\
	Donc appliquer un taux de \pointilles[2cm] revient à multiplier par\pointilles[2cm].
\end{tcolorbox}
\begin{luacode}
	p=p+1
\end{luacode}

\textbf{\underline{Exemple}}\\
Dans une classe de $30$ élèves, $60$ \% des élèves pratiquent un sport.\\\\
\pointilles[17cm]
\\


\begin{tcolorbox}[colframe=Green!20, colback=Green!0, boxrule=2pt, arc=6pt, title={\textbf{Définition \arabic{chapter}.\directlua{tex.print(d)} : quatrième proportionnelle}}, coltitle=black]
Déterminer un \pointilles[5cm], c'est déterminer une \pointilles[5cm] écrite sous forme d'une \pointilles[7cm] dont le \pointilles[7cm].
\end{tcolorbox}
\begin{luacode}
	d=d+1
\end{luacode}

\textbf{\underline{Exemple}}\\
Sur 550 élèves, 231 sont externes. On a le tableau de proportionnalité suivant :
\begin{center}
	\begin{tabular}{|c|c|c|} \hline
		\cellcolor{black!10}Nombre d'externes & \ldots & \ldots \tabularnewline \hline
		\cellcolor{black!10}Nombre d'élèves & \ldots &  \ldots \tabularnewline \hline
	\end{tabular}
\end{center}
D'après l'égalité des produits en croix, on a :\pointilles[5cm].\\\\
\pointilles[17cm]

\color{Green}
\section{Bilan}
\color{black}
\textbf{Plusieurs réponses possibles}.\\\\

\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom= , AlphT]{%
12 lettres identiques pèsent \Masse{500}. Alors \ldots & 24 de ces lettres pèsent \Masse[kg]{1}. & 3 de ces lettres pèsent \Masse{125}. &27 de ces lettres pèsent \Masse[kg]{1.125}.&3, %
Une voiture de course consomme \Capa{12} pour parcourir \Lg[km]{100}. On suppose que la consommation est proportionnelle à la distance parcourue. Lors d'un parcours de \Lg[km]{225}$\text{,}$ la consommation est de \ldots & \Capa{5.3}. & \Capa{27}. & \Capa{1 875}.&3, %
Dans une boulangerie$\text{,}$ 8 croissants identiques coûtent $\num{10,40}$ \texteuro. Le prix d'un seul croissant est \ldots & \num{0.77} \texteuro. & \num{1.04} \texteuro. & \num{1.3} \texteuro. &3,%
La distance qui sépare un observateur d'un éclair est proportionnelle à la durée entre l'instant où il le voit et l'instant où il entend le tonnerre. On sait que s'il y a \Temps{;;;;;9} entre ces deux instants$\text{,}$ l'orage est à \Lg[km]{3} de l'observateur et s'il y a \Temps{;;;;;12}$\text{,}$ il se trouve à \Lg[km]{4}. Si la durée entre les deux instants est de \Temps{;;;;;21}$\text{,}$ la distance qui sépare l'orage de l'observateur est \ldots & \Lg[km]{7}. & \Lg[km]{12}. & \Lg[km]{21}. &1%

}


\color{Green}
\section{Auto-évaluation}
\color{black}
\begin{tabular}{|>{\columncolor{darkgray}\color{black}}m{8cm} |>{\color{black}}m{0.6cm}|m{0.6cm}|m{0.6cm}|m{5cm}|} \hline

\rowcolor{Lavender}\centering{} \color{black} Compétences & \centering{}\color{MediumSeaGreen} \faCheck & \centering{}\color{DarkOrange}\faWater & \centering{}\color{Crimson}\faTimes & \centering{}\color{black}Pour réviser \tabularnewline \hline \hline

\cellcolor{Lavender}Je suis capable de reconnaître une situation de proportionnalité. & & & & \textbf{\underline{Leçon}} : Partie I \smallskip \newline  \textbf{\underline{Exercices}} :  1 - 2  \tabularnewline \hline \hline

\cellcolor{Lavender}Je sais calculer une quatrième proportionnelle. & & & & \textbf{\underline{Leçon}} : Partie II \smallskip \newline  \textbf{\underline{Exercices}} : 3 à 5  \tabularnewline \hline \hline

\cellcolor{Lavender}Je sais utiliser la notion de pourcentage. & & & & \textbf{\underline{Leçon}} : Partie III \smallskip \newline  \textbf{\underline{Exercices}} : 6 à 9 \tabularnewline \hline
\end{tabular}

%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------- Impression -----------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\newpage
\begin{tabular}{|>{\columncolor{darkgray}\color{black}}m{8cm} |>{\color{black}}m{0.6cm}|m{0.6cm}|m{0.6cm}|m{5cm}|} \hline

\rowcolor{Lavender}\centering{} \color{black} Compétences & \centering{}\color{MediumSeaGreen} \faCheck & \centering{}\color{DarkOrange}\faWater & \centering{}\color{Crimson}\faTimes & \centering{}\color{black}Pour réviser \tabularnewline \hline \hline

\cellcolor{Lavender}Je suis capable de reconnaître une situation de proportionnalité. & & & & \textbf{\underline{Leçon}} : Partie I \smallskip \newline  \textbf{\underline{Exercices}} :  1 - 2  \tabularnewline \hline \hline

\cellcolor{Lavender}Je sais calculer une quatrième proportionnelle. & & & & \textbf{\underline{Leçon}} : Partie II \smallskip \newline  \textbf{\underline{Exercices}} : 3 à 5  \tabularnewline \hline \hline

\cellcolor{Lavender}Je sais utiliser la notion de pourcentage. & & & & \textbf{\underline{Leçon}} : Partie III \smallskip \newline  \textbf{\underline{Exercices}} : 6 à 9 \tabularnewline \hline
\end{tabular}
~~\\\\\\


\textbf{Plusieurs réponses possibles}.\\\\

\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom= , AlphT]{%
12 lettres identiques pèsent \Masse{500}. Alors \ldots & 24 de ces lettres pèsent \Masse[kg]{1}. & 3 de ces lettres pèsent \Masse{125}. &27 de ces lettres pèsent \Masse[kg]{1.125}.&3, %
Une voiture de course consomme \Capa{12} pour parcourir \Lg[km]{100}. On suppose que la consommation est proportionnelle à la distance parcourue. Lors d'un parcours de \Lg[km]{225}$\text{,}$ la consommation est de \ldots & \Capa{5.3}. & \Capa{27}. & \Capa{1 875}.&3, %
Dans une boulangerie$\text{,}$ 8 croissants identiques coûtent $\num{10,40}$ \texteuro. Le prix d'un seul croissant est \ldots & \num{0.77} \texteuro. & \num{1.04} \texteuro. & \num{1.3} \texteuro. &3,%
La distance qui sépare un observateur d'un éclair est proportionnelle à la durée entre l'instant où il le voit et l'instant où il entend le tonnerre. On sait que s'il y a \Temps{;;;;;9} entre ces deux instants$\text{,}$ l'orage est à \Lg[km]{3} de l'observateur et s'il y a \Temps{;;;;;12}$\text{,}$ il se trouve à \Lg[km]{4}. Si la durée entre les deux instants est de \Temps{;;;;;21}$\text{,}$ la distance qui sépare l'orage de l'observateur est \ldots & \Lg[km]{7}. & \Lg[km]{12}. & \Lg[km]{21}. &1%

}

%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%--------------------------------------------------------------- Histoire des mathématiques -------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\newpage
\begin{tcolorbox}[colframe=black, colback=blue!10, boxrule=2pt, arc=6pt]
	\centering{}\LARGE{\textbf{Histoire des mathématiques \faBook}}
\end{tcolorbox}
\begin{tcolorbox}[colframe=black!30, colback=blue!0, boxrule=2pt, arc=6pt, title=?, coltitle=black]

\end{tcolorbox}

\begin{tcolorbox}[colframe=black!10, colback=blue!0, boxrule=1pt, arc=1pt, title=Quelques anecdotes, coltitle=black]

\end{tcolorbox}

\textbf{\underline{Sources}}
\begin{itemize}[label=\textbullet]
	\item Hauchecorne, B. et Suratteau, D. (2008), \textit{Des Mathématiciens de A à Z} ($3^e$ édition), Ellipses
	\item Byrne, O. (2022), \textit{The first six books of the elements of Euclid}, Taschen
	\item Houlou-Garcia, A. (2019), \textit{Mathematikos} (2\ieme\, tirage), Belles lettres
	\item Barbin, E. (2019), \textit{Faire des mathématiques avec l'histoire au lycée}, Ellipses
	\item Dahan-Dalmedico, A. et Peiffer, J. (1986), \textit{Une histoire des mathématiques : Routes et dédales}, Points
	\item Freiberger, M. et Thomas, R. (2018), \textit{Dans le secret des nombres}, Ekho
\end{itemize}

%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%-------------------------------------------------------------------------- Fiche de route ------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\newpage
\begin{tcolorbox}[colframe=black, colback=Khaki!50, boxrule=2pt, arc=6pt]
	\centering{}\LARGE{\textbf{Fiche de route}}
\end{tcolorbox}
\begin{itemize}[label=\textbullet]
	\item \color{Tomato} \textbf{Introduction} \color{black} : Titre, prérequis, objectifs, rappels
	\item \color{RoyalBlue}\underline{Leçon} \color{black} : Partie I : 
	\item \color{ForestGreen}\underline{Exercices}  \color{black} : 
	\item \color{RoyalBlue}\underline{Leçon}  \color{black} : Parties IV et V : Bilan + Auto-évaluation
	\item \color{ForestGreen}\underline{Exercices bilan}  \color{black} : 
	\item \color{Coral} \underline{Histoire des mathématiques}
\end{itemize}

%\setcounter{page}{4}

%\end{comment}
\end{document}
