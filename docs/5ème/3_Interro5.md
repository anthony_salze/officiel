---
author: Anthony Salze
title: Interrogations écrites
hide :
    -footer
---
# <span style="color:Tomato"> Évaluations

## <span style="color:LightSalmon">IE 01 - Priorités opératoires, calculs, problèmes
??? tip "Interrogations écrites <span style=color:red> :material-file-pdf-box: <span style=color:blue>:material-text-box-edit-outline: "
    === "IE 01"
        [PDF (à venir)](a_telecharger/3_IE/3_IE_00_Révisions_numérique&.pdf){:target="_blank"}   
        [LaTeX (à venir)](a_telecharger/3_IE/3_IE_00_Révisions_numérique&.tex)  
        [LaTeX_main (à venir)](a_telecharger/3_IE/3_IE_00_Révisions_num&.tex) 

        
    <span style=color:CornflowerBlue>_Dernière mise à jour : le 29/09/24_