---
author: Anthony Salze
title: Activités
hide :
    -footer
---
# <span style="color:Tomato"> Activités

## <span style="color:LightSalmon"> Histoire des mathématiques
??? tip "Activités <span style=color:red> :material-file-pdf-box: <span style=color:blue>:material-text-box-edit-outline: "
    === "Act_01 - Découverte de l'écriture cunéiforme"
        Activité grandement inspirée par un article paru dans la Gazette de la Société Mathématique de France (Numéro spécial juillet 2024 - Ateliers clefs en main, S. FIORELLI, E. RAPHAEL, Une mystérieuse tablette, pp. 18-19).  
        Les documents originaux sont disponibles à l'adresse suivante : [cliquez ici](https://kits.math.cnrs.fr/activites/tablettes-babyloniennes){:target="_blank"}  

        | [Sujet PDF](a_telecharger/5_Act/5_Hist_1/5_Hist_1_Cuneiforme_decouverte.pdf){:target="_blank"} | [Document 1 : Table de 5](a_telecharger/5_Act/5_Hist_1/Table_de_5.pdf){:target="_blank"} | [Document 2 : Table de 7](a_telecharger/5_Act/5_Hist_1/Table_de_7.pdf){:target="_blank"} | [Sujet LaTeX](a_telecharger/5_Act/5_Hist_1/5_Hist_1_Cuneiforme_decouverte.tex) |  

        <span style=color:CornflowerBlue> _Dernière mise à jour : le 12/10/24_


## <span style="color:LightSalmon"> Arts
??? tip "Activités <span style=color:red> :material-file-pdf-box: <span style=color:blue>:material-text-box-edit-outline: "
    === "Act_01 - Alhambra"
        Source : Broug, E. (2019). *Islamic Geometric Patterns* (2ème édition). Thames & Hudson.  
        Texte descriptif de l'Alhambra généré par IA.  

        | [Sujet PDF](a_telecharger/5_Act/5_Arts_1_Alhambra/5_Art_2_Alhambra.pdf){:target="_blank"} | [Sujet LaTeX](a_telecharger/5_Act/5_Arts_1_Alhambra/5_Art_2_Alhambra.tex) |  

        **Travaux des élèves (Séance 1)**  
        ![](a_telecharger/5_Act/5_Arts_1_Alhambra/Alhambra_01.png){ width=30%} 
        ![](a_telecharger/5_Act/5_Arts_1_Alhambra/Alhambra_02.png){ width=30%} 
        ![](a_telecharger/5_Act/5_Arts_1_Alhambra/Alhambra_03.png){ width=30%} 
        ![](a_telecharger/5_Act/5_Arts_1_Alhambra/Alhambra_04.png){ width=30%} 
        ![](a_telecharger/5_Act/5_Arts_1_Alhambra/Alhambra_05.png){ width=30%} 
        ![](a_telecharger/5_Act/5_Arts_1_Alhambra/Alhambra_06.png){ width=30%} 
        ![](a_telecharger/5_Act/5_Arts_1_Alhambra/Alhambra_07.png){ width=30%} 
        ![](a_telecharger/5_Act/5_Arts_1_Alhambra/Alhambra_08.png){ width=30%} 
        ![](a_telecharger/5_Act/5_Arts_1_Alhambra/Alhambra_09.png){ width=30%}  
        ![](a_telecharger/5_Act/5_Arts_1_Alhambra/Alhambra_10.png){ width=30%} 
        ![](a_telecharger/5_Act/5_Arts_1_Alhambra/Alhambra_11.png){ width=30%} 
        ![](a_telecharger/5_Act/5_Arts_1_Alhambra/Alhambra_12.png){ width=30%} 
        ![](a_telecharger/5_Act/5_Arts_1_Alhambra/Alhambra_13.png){ width=30%}  

        **Pavage Alhambra (Séance 2)** 
        (à venir)

        <span style=color:CornflowerBlue> _Dernière mise à jour : le 10/11/24_

    === "Act_02 - Mosquée Ibn Tulun"
        Source : Broug, E. (2019). *Islamic Geometric Patterns* (2ème édition). Thames & Hudson.  
        Texte descriptif de la mosquée d'Ibn Tulun généré par IA.  

        | [Sujet PDF](a_telecharger/5_Act/5_Arts_2_Ibn_Tulun/5_Art_1_Mosquee_Ibn_Tulun.pdf){:target="_blank"} | [Sujet LaTeX](a_telecharger/5_Act/5_Arts_2_Ibn_Tulun/5_Art_1_Mosquee_Ibn_Tulun.tex) |  
        
        **Travaux des élèves (Séance 1)**  
        ![](a_telecharger/5_Act/5_Arts_2_Ibn_Tulun/5_Art_2_Ibn_Tulun_final.jpg){ width=50%} 

        <span style=color:CornflowerBlue> _Dernière mise à jour : le 07/01/25_

    === "Act_03 - Mosquée Kairouan"
        Source : Broug, E. (2019). *Islamic Geometric Patterns* (2ème édition). Thames & Hudson.  
        Texte descriptif de la mosquée de Kairouan généré par IA.  

        | [Sujet PDF](a_telecharger/5_Act/5_Arts_3_Kairouan/5_Art_3_Mosquee_Kairouan.pdf){:target="_blank"} | [Sujet LaTeX](a_telecharger/5_Act/5_Arts_3_Kairouan/5_Art_3_Mosquee_Kairouan.tex) |  
        
        **Travaux des élèves (Séances 1 & 2)**  
        (à venir)

        <span style=color:CornflowerBlue> _Dernière mise à jour : le 07/01/25_


## <span style="color:LightSalmon"> Cartographie
??? tip "Activités <span style=color:red> :material-file-pdf-box: <span style=color:blue>:material-text-box-edit-outline: "
    === "Act_01 - Médiatrice, hauteur, points remarquables dans un triangle"
        Activité fortement inspirée d'un travail proposé par M. VERDIER (IA-IPR de l'académie de Lille).

        | [Sujet PDF](a_telecharger/5_Act/5_Cart_1/5_Cartographie.pdf){:target="_blank"} | [Sujet LaTeX](a_telecharger/5_Act/5_Cart_1/5_Cartographie.tex) |  

        <span style=color:CornflowerBlue> _Dernière mise à jour : le 07/01/25_