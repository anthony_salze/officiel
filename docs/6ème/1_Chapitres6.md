---
author: Anthony Salze
title: Leçons et exercices
hide :
    -footer
---
# <span style="color:Tomato">Leçons et exercices

## <span style="color:LightSalmon">01 : Les nombres entiers
???+ info "Contenu"
    - Ecriture en lettre des nombres
    - Additions, soustractions et multiplications
    - Placement d'un nombre entier dans un tableau
    - Vocabulaire : unités, dizaines, centaines, milliers, millions, milliards

??? tip "Fichiers à télécharger <span style=color:red> :material-file-pdf-box: <span style=color:blue>:material-text-box-edit-outline: <span style=color:black> :material-math-compass:"
    === "Leçon"
        [Leçon : 01 - Les nombres entiers](a_telecharger/6_C01/6_C01_Nombres_entiers.pdf){:target="_blank"}   
    === "Exercices"
        [Exercices](a_telecharger/6_C01/6_C01_Exercices_énoncé.pdf){:target="_blank"}   
    === "Exercices + correction"
        [Exercices + correction](a_telecharger/6_C01/6_C01_Exercices_correction.pdf){:target="_blank"} 
    === "LaTeX"
        - [Leçon](a_telecharger/6_C01/6_C01_Nombres_entiers.tex) 
        - [Leçon Beamer](a_telecharger/6_C01/6_C01_Présentation.tex) 
        - [Exercices](a_telecharger/6_C01/6_C01_Exercices.tex)
    === "GeoGebra"
        - [Glisse-Nombre](a_telecharger/6_C01/6_C01_Glisse_nombres_dyn.ggb)
    <span style=color:CornflowerBlue>_Dernière mise à jour : le 12/09/24_


## <span style="color:LightSalmon">02 : Cercles et segments
???+ info "Contenu"
    - Tracer des segments et des cercles
    - Utilisation du compas
    - Vocabulaire : extrémités, milieu, rayon, diamètre, corde, centre
    - Petit programme de construction

??? tip "Fichiers à télécharger <span style=color:red> :material-file-pdf-box: <span style=color:blue>:material-text-box-edit-outline: <span style=color:black> :material-math-compass:"
    === "Leçon"
        [Leçon : 02 - Segments et cercles](a_telecharger/6_C02/6_C02_Cercles_Segments.pdf){:target="_blank"}   
    === "Exercices"
        [Exercices](a_telecharger/6_C02/6_C02_Exercices_énoncé.pdf){:target="_blank"}   
    === "Exercices + correction"
        [Exercices + correction](a_telecharger/6_C02/6_C02_Exercices_correction.pdf){:target="_blank"} 
    === "LaTeX"
        - [Leçon](a_telecharger/6_C02/6_C02_Cercles_Segments.tex) 
        - [Leçon Beamer](a_telecharger/6_C02/6_C02_Présentation.tex) 
        - [Exercices](a_telecharger/6_C02/6_C02_Exercices.tex) 
    === "GeoGebra"
        - [Illustration du théorème des milieux](a_telecharger/6_C02/6_C02_Théorème_des_milieux.ggb)
    <span style=color:CornflowerBlue>_Dernière mise à jour : le 10/11/24_


## <span style="color:LightSalmon">03 : Les nombres décimaux (partie 1)
???+ info "Contenu"
    - Vocabulaire : dixièmes, centièmes, millièmes, partie entière, partie décimale
    - Fractions décimales
    - Décomposition des nombres décimaux

??? tip "Fichiers à télécharger <span style=color:red> :material-file-pdf-box: <span style=color:blue>:material-text-box-edit-outline: "
    === "Leçon"
        [Leçon : 03 - Les nombres décimaux 1](a_telecharger/6_C03/6_C03_Les_Nombres_Décimaux_1.pdf){:target="_blank"}  
    === "Exercices"
        [Exercices](a_telecharger/6_C03/6_C03_Exercices_énoncé.pdf){:target="_blank"}   
    === "Exercices + correction"
        [Exercices + correction](a_telecharger/6_C03/6_C03_Exercices_correction.pdf){:target="_blank"} 
    === "LaTeX"
        - [Leçon](a_telecharger/6_C03/6_C03_Nombres_décimaux1.tex) 
        - [Leçon Beamer](a_telecharger/6_C03/6_C03_Présentation.tex) 
        - [Exercices](a_telecharger/6_C03/6_C03_Exercices.tex) 
    <span style=color:CornflowerBlue>_Dernière mise à jour : le 10/11/24_


## <span style="color:LightSalmon">04 : Droites et demi-droites
???+ info "Contenu"
    - Tracer des droites et demi-droites
    - Vocabulaire : origine d'une demi-droite
    - Programme de construction

??? tip "Fichiers à télécharger <span style=color:red> :material-file-pdf-box: <span style=color:blue>:material-text-box-edit-outline: <span style=color:black> :material-math-compass:"
    === "Leçon"
        [Leçon : 04 - Droites et demi-droites](a_telecharger/6_C04/6_C04_Droites_et_demi-droites.pdf){:target="_blank"}   
    === "Exercices"
        [Exercices](a_telecharger/6_C04/6_C04_Exercices_énoncé.pdf){:target="_blank"}   
    === "Exercices + correction"
        [Exercices + correction](a_telecharger/6_C04/6_C04_Exercices_correction.pdf){:target="_blank"} 
    === "LaTeX"
        - [Leçon](a_telecharger/6_C04/6_C04_Droites_et_demi-droites.tex) 
        - [Leçon Beamer](a_telecharger/6_C04/6_C04_Présentation.tex) 
        - [Exercices](a_telecharger/6_C04/6_C04_Exercices.tex) 
    === "GeoGebra"
        - [Droite d'Euler](a_telecharger/6_C04/6_C04_Droite_Euler.ggb)
        - [Droite de Pascal](a_telecharger/6_C04/6_C04_Droite_Pascal.ggb)
        - [Droite de Simson](a_telecharger/6_C04/6_C04_Droite_Simson.ggb)
    <span style=color:CornflowerBlue>_Dernière mise à jour : le 10/11/24_


## <span style="color:LightSalmon">05 : Nombres décimaux 2
???+ info "Contenu"
    - Placer un nombre décimal sur une demi-droite graduée
    - Comparaison de nombres
    - Encadrer, intercaler, arrondir
    - Résolution de petits problèmes

??? tip "Fichiers à télécharger <span style=color:red> :material-file-pdf-box: <span style=color:blue>:material-text-box-edit-outline: "
    === "Leçon"
        [Leçon : 05 - Les nombres décimaux 2](a_telecharger/6_C05/6_C05_Les_nombres_décimaux_2.pdf){:target="_blank"}  
    === "Exercices"
        [Exercices](a_telecharger/6_C05/6_C05_Exercices_énoncé.pdf){:target="_blank"}  
    === "Exercices + correction"
        [Exercices + correction](a_telecharger/6_C05/6_C05_Exercices_correction.pdf){:target="_blank"}  
    === "LaTeX"
        - [Leçon](a_telecharger/6_C05/6_C05_Les_nombres_décimaux_2.tex) 
        - [Exercices](a_telecharger/6_C05/6_C05_Exercices.tex) 
    <span style=color:CornflowerBlue>_Dernière mise à jour : le 29/11/24_


## <span style="color:LightSalmon">06 : Droites : propriétés
???+ info "Contenu"
    - Tracer des droites perpendiculaires et parallèles
    - Propriétés reliant ces droites
    - Méthode de rédaction d'un raisonnment en géométrie

??? tip "Fichiers à télécharger <span style=color:red> :material-file-pdf-box: <span style=color:blue>:material-text-box-edit-outline: <span style=color:black> :material-math-compass:"
    === "Leçon"
        [Leçon : 06 - Droites : propriétés](a_telecharger/6_C06/6_C06_Droites_propriétés.pdf){:target="_blank"}  
    === "Exercices"
        [Exercices](a_telecharger/6_C06/6_C06_Exercices_énoncé.pdf){:target="_blank"}  
    === "Exercices + correction"
        [Exercices + correction (à venir)](a_telecharger/6_C06/6_C06_Exercices_énoncé2.pdf){:target="_blank"}  
    === "LaTeX"
        - [Leçon](a_telecharger/6_C06/6_C06_Droites_propriétés.tex) 
        - [Exercices](a_telecharger/6_C06/6_C06_Exercices.tex) 
    === "GeoGebra"
        - [Méthode de construction : perpendiculaire](a_telecharger/6_C06/6_C06_Perpendiculaire.ggb) 
        - [Méthode de construction : parallèle](a_telecharger/6_C06/6_C06_Parallèle.ggb) 
    <span style=color:CornflowerBlue>_Dernière mise à jour : le 29/11/24_


## <span style="color:LightSalmon">07 : Nombres décimaux 3 : adidtions et soustractions
???+ info "Contenu"
    - Additions de nombres décimaux
    - Soustractions de nombres décimaux
    - Résolution de problèmes

??? tip "Fichiers à télécharger <span style=color:red> :material-file-pdf-box: <span style=color:blue>:material-text-box-edit-outline: <span style=color:black> :material-math-compass:"
    === "Leçon"
        [Leçon : 07 - Nombres décimaux 3 (additions et soustractions)](a_telecharger/6_C07/6_C07_Nombres_décimaux_3.pdf){:target="_blank"}   
    === "Exercices"
        [Exercices](a_telecharger/6_C07/6_C07_Exercices_énoncé.pdf){:target="_blank"}   
    === "Exercices + correction"
        [Exercices + correction](a_telecharger/6_C07/6_C07_Exercices_correction.pdf){:target="_blank"}   
    === "LaTeX"
        - [Leçon](a_telecharger/6_C07/6_C07_Nombres_décimaux_3.tex) 
        - [Exercices](a_telecharger/6_C07/6_C07_Exercices.tex) 
    <span style=color:CornflowerBlue>_Dernière mise à jour : le 07/01/25_

## <span style="color:LightSalmon">08 : Les angles
???+ info "Contenu"
    - Définition et nature des angles.
    - Calculs des mesures d'un angle, notion d'angles complémentaires et supplémentaires.
    - Utilisation du rapporteur.
    - Compléments : bissectrice.

??? tip "Fichiers à télécharger <span style=color:red> :material-file-pdf-box: <span style=color:blue>:material-text-box-edit-outline: <span style=color:black> :material-math-compass:"
    === "Leçon"
        [Leçon : 08 - Les angles](a_telecharger/6_C08/6_C08_Angles.pdf){:target="_blank"}   
    === "Exercices"
        [Exercices](a_telecharger/6_C08/6_C08_Exercices_énoncé.pdf){:target="_blank"}   
    === "Exercices + correction"
        Non disponible
    === "LaTeX"
        - [Leçon](a_telecharger/6_C08/6_C08_Angles.tex) 
        - [Exercices](a_telecharger/6_C08/6_C08_Exercices.tex) 
    === "GeoGebra"
        - [Rapporteur](a_telecharger/6_C08/6_C08_Rapporteur.ggb)
        - [Rapporteur exerciseur](a_telecharger/6_C08/6_C08_Rapporteur_exerciseur.ggb)
        Exerciseur réalisé par Raphaël PETIT et Ludovic SOCQUET-MEILLERET.
    <span style=color:CornflowerBlue>_Dernière mise à jour : le 16/03/24_


## <span style="color:LightSalmon">09 : Multiplication de nombres décimaux
???+ info "Contenu"
    - Méthode pour multiplier deux nombres décimaux.
    - Multiplication par 10, 100, 1000 etc et par 0,1 ; 0,01 ; 0,001 etc.
    - Conversions d'unités.
    - Résolution de problèmes.

??? tip "Fichiers à télécharger <span style=color:red> :material-file-pdf-box: <span style=color:blue>:material-text-box-edit-outline: <span style=color:black> :material-math-compass:"
    === "Leçon"
        [Leçon : 09 - Multiplication avec les nombres décimaux](a_telecharger/6_C09/6_C09_Multiplication.pdf){:target="_blank"}   
    === "Exercices"
        [Exercices](a_telecharger/6_C09/6_C09_Exercices_énoncé.pdf){:target="_blank"}  
    === "Exercices + correction"
        Non disponible
    === "LaTeX"
        - [Leçon](a_telecharger/6_C09/6_C09_Multiplication.tex) 
        - [Exercices](a_telecharger/6_C09/6_C09_Exercices.tex) 
    <span style=color:CornflowerBlue>_Dernière mise à jour : le 17/02/25_


## <span style="color:LightSalmon">10 : Triangles
???+ info "Contenu"
    - Nature des triangles : isocèle, équilatéral, rectangle.
    - Propriétés (angles) pour les triangles isocèles et équilatéraux.
    - Construction de triangles avec les outils de géométrie.
    - Résolution de problèmes.

??? tip "Fichiers à télécharger <span style=color:red> :material-file-pdf-box: <span style=color:blue>:material-text-box-edit-outline: <span style=color:black> :material-math-compass:"
    === "Leçon"
        [Leçon : 10 - Triangles](a_telecharger/6_C10/6_C10_Triangles.pdf){:target="_blank"}   
    === "Exercices"
        [Exercices](a_telecharger/6_C10/6_C10_Exercices_énoncé.pdf){:target="_blank"}   
    === "Exercices + correction"
        Non disponible
    === "LaTeX"
        - [Leçon](a_telecharger/6_C10/6_C10_Triangles.tex) 
        - [Exercices](a_telecharger/6_C10/6_C10_Exercices.tex) 
    <span style=color:CornflowerBlue>_Dernière mise à jour : le 17/02/25_


## <span style="color:LightSalmon">11 : Divisions euclidienne et décimale
???+ info "Contenu"
    - Rappels sur les divisions euclidiennes (vocabulaire : dividende, diviseur, quotient, reste).
    - Notions de multiples, diviseurs, divisibilité.
    - Critères de divisibilité par 2, 5, 3 et 9.
    - Divisions décimales.
    - Priorités opératoires.
    - Résolution de problèmes.

??? tip "Fichiers à télécharger <span style=color:red> :material-file-pdf-box: <span style=color:blue>:material-text-box-edit-outline: <span style=color:black> :material-math-compass:"
    === "Leçon"
        [Leçon : 11 - Divisions](a_telecharger/6_C11/6_C11_Division_Décimale.pdf){:target="_blank"}   
    === "Exercices"
        [Exercices](a_telecharger/6_C11/6_C11_Exercices_énoncé.pdf){:target="_blank"}   
    === "Exercices + correction"
        [Exercices + correction (à venir)](a_telecharger/6_C11/6_C11_Exercices_correction2.pdf){:target="_blank"}   
    === "LaTeX"
        - [Leçon](a_telecharger/6_C11/6_C11_Divisions.tex) 
        - [Exercices](a_telecharger/6_C11/6_C11_Exercices.tex) 
    <span style=color:CornflowerBlue>_Dernière mise à jour : le 17/02/25_



## <span style="color:LightSalmon">12 : Quadrilatères
???+ info "Contenu"
    - Définitions de parallélogramme, losange, rectangle et carré.
    - Construction de ces figures.

??? tip "Fichiers à télécharger <span style=color:red> :material-file-pdf-box: <span style=color:blue>:material-text-box-edit-outline: <span style=color:black> :material-math-compass:"
    === "Leçon"
        [Leçon : 12 - Quadrilatères](a_telecharger/6_C12/6_C12_Quadrilatères.pdf){:target="_blank"}   
    === "Exercices"
        [Exercices](a_telecharger/6_C12/6_C12_Exercices_énoncé.pdf){:target="_blank"}   
    === "Exercices + correction"
        [Exercices + correction (à venir)](a_telecharger/6_C12/6_C12_Exercices_correction2.pdf){:target="_blank"}   
    === "LaTeX"
        - [Leçon](a_telecharger/6_C12/6_C12_Quadrilatères.tex) 
        - [Exercices](a_telecharger/6_C12/6_C12_Exercices.tex) 
    <span style=color:CornflowerBlue>_Dernière mise à jour : le 17/02/25_



## <span style="color:LightSalmon">13 : Fractions
???+ info "Contenu"
    - Fraction partage.
    - Fraction quotient.
    - Placer une fraction sur une demi-droite graduée.
    - Comparaison et encadrement d'une fraction.
    - Calculer une fraction d'une quantité.
    - Notion de pourcentage.

??? tip "Fichiers à télécharger <span style=color:red> :material-file-pdf-box: <span style=color:blue>:material-text-box-edit-outline: <span style=color:black> :material-math-compass:"
    === "Leçon"
        [Leçon : 13 - Fractions](a_telecharger/6_C13/6_C13_Fractions.pdf){:target="_blank"}   
    === "Exercices"
        [Exercices](a_telecharger/6_C13/6_C13_Exercices_énoncé.pdf){:target="_blank"}   
    === "Exercices + correction"
        Non disponible
    === "LaTeX"
        - [Leçon](a_telecharger/6_C13/6_C13_Fractions.tex) 
        - [Exercices](a_telecharger/6_C13/6_C13_Fractions.tex) 
    <span style=color:CornflowerBlue>_Dernière mise à jour : le 17/02/25_



## <span style="color:LightSalmon">14 : Proportionnalité
???+ info "Contenu"
    - Savoir si un tableau représente une situation de proportionnalité.
    - Calculer une quatrième proportionnelle à l'aide de différentes méthodes (coefficient de proportionnalité, linéarité, retour à l'unité et produit en croix).
    - Utiliser l'échelle d'un plan, d'une carte, ...

??? tip "Fichiers à télécharger <span style=color:red> :material-file-pdf-box: <span style=color:blue>:material-text-box-edit-outline: <span style=color:black> :material-math-compass:"
    === "Leçon"
        [Leçon : 14 - Proportionnalité](a_telecharger/6_C14/6_C14_Proportionnalité.pdf){:target="_blank"}   
    === "Exercices"
        [Non disponible](a_telecharger/6_C14/6_C14_Exercices_énoncé.pdf){:target="_blank"}   
    === "Exercices + correction"
        à venir
    === "LaTeX"
        - [Non disponible](a_telecharger/6_C14/6_C14_Proportionnalité.tex) 
        - [Non disponible](a_telecharger/6_C14/6_C14_Exercices.tex) 
    <span style=color:CornflowerBlue>_Dernière mise à jour : le 17/04/24_