---
author: Anthony Salze
title: Interrogations écrites
hide :
    -footer
---
# <span style="color:Tomato"> Évaluations

## <span style="color:LightSalmon">Interrogations écrites
??? tip "Interrogations écrites <span style=color:red> :material-file-pdf-box: <span style=color:blue>:material-text-box-edit-outline: "
    === "IE 01 - Nombres entiers"
        [PDF](a_telecharger/6_C00b/6_IE_01_Nombres_entiers.pdf){:target="_blank"}   
        [LaTeX](a_telecharger/6_C00b/6_IE_01_Nombres_entiers.tex)  
        [LaTeX_main](a_telecharger/6_C00b/6_IE_01_Nombres_entiers_main.tex) 
    === "IE 02 - Distance et cercle"
        [PDF (à venir)](a_telecharger/6_C00b/6_IE_02_Distance_et_cercle1.pdf){:target="_blank"}   
        [LaTeX (à venir)](a_telecharger/6_C00b/6_IE_02_Distance_et_cercle1.tex)  
        [LaTeX_main (à venir)](a_telecharger/6_C00b/6_IE_01_Nombres_entiers_main1.tex) 
    === "IE 03 - Segments et droites"
        [PDF (à venir)](a_telecharger/6_C00b/6_IE_03_Segments_Droites1.pdf){:target="_blank"}   
        [LaTeX (à venir)](a_telecharger/6_C00b/6_IE_03_Segments_Droites1.tex)  
        [LaTeX_main (à venir)](a_telecharger/6_C00b/6_IE_01_Nombres_entiers_main1.tex) 
    <span style=color:CornflowerBlue>_Dernière mise à jour : le 29/09/24_
        



