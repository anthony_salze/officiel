\documentclass[dvipsnames, a4paper, 12pt]{report}

\usepackage{lmodern} %police standard latin moderne
\usepackage[frenchb]{babel} %langue française
\usepackage[utf8]{inputenc} %pour l'utf-8
\usepackage[T1]{fontenc} %pour les césures et accentuations
\usepackage{geometry} %pour changer les marges d'un document
\usepackage{graphicx}
\usepackage{fancybox}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{fontawesome5}
\usepackage{ProfCollege}
\usepackage{luacode}
\usepackage{xlop}

\usepackage[explicit]{titlesec} %Permet de modifier les différentes parties, chapitres, sous-parties,...

\usepackage{colortbl} %permet de mettre des cellules de tableau en couleur

\usepackage{multicol}

\usepackage{cellspace} %pour définir les espaces en haut et en bas des cellules d'un tableau
\setlength{\cellspacebottomlimit}{5pt}
\setlength{\cellspacetoplimit}{5pt}
\setlength{\parindent}{0pt}

\usepackage{xcolor} %permet de mettre du texte en couleur

\usepackage{array} %gestion des tableaux
\usepackage{tcolorbox} %créer des boîtes de couleurs

\usepackage{hyperref} %permet de gérer le sommaire et la page de garde et les options du PDF

\geometry{top=2cm, bottom=2cm, right=2cm, left=2cm}   
\titlespacing{\chapter}{0pt}{0pt}{5pt}

\setcounter{chapter}{9}

\usepackage{lastpage}
\usepackage{fancyhdr} %définition des en-têtes et des bas de page
\fancyhf{}
\fancypagestyle{main}{
	\renewcommand\headrulewidth{5pt}
	\fancyhead[L]{6\ieme\, - Chapitre \arabic{chapter} - Nombres décimaux 4 - Multiplications}
	\fancyhead[R]{Collège}
	\fancyfoot[C]{\thepage/\pageref{LastPage}}
	\fancyfoot[R]{\tiny{Anthony Salze}}
	}
\pagestyle{main}

%gestion des entêtes
\makeatletter\@addtoreset{section}{chapter}\makeatother % RAZ des numéros de section après un chapitre
\renewcommand{\thepart}{\Alph{part} -} % Pour mettre des I, II, etc. aux parties
\renewcommand{\thesection}{\color{Green} \Roman{section} -}% Idem pour les sections et avoir le numéro de chapitre
\renewcommand{\thesubsection}{\arabic{subsection} -}

\begin{document}
%format du titre des chapitres placé après Sommaire sinon cela bugue
\titleformat{\chapter}[frame] {\normalfont \bfseries \large}{\textcolor{red} {\chaptertitlename\ \thechapter\,-\,#1}}{0pt}{\Huge}

\setcounter{chapter}{8}

\chapter{Nombres décimaux : multiplications}
\begin{luacode}
	d=1
	p=1
	t=1
	m=1
\end{luacode}

{\color{blue}\underline{\textbf{Prérequis}}
\begin{itemize}
	\item Les nombres entiers et les nombres décimaux (chapitres $1$, $3$, $5$ et $7$).
\end{itemize}~~

\underline{\textbf{Objectifs}}
\begin{itemize}
	\item Réaliser des multiplications avec les nombres décimaux ;
	\item Savoir multiplier de tête par $10$ ; $100$ ; ... et par $0,1$ ; $0,01$ ; ... ;
	\item Réaliser des conversions d'unités.
\end{itemize}


\color{Green}
\section{Méthode générale}
\color{black}
\normalsize{}
\begin{tcolorbox}[colframe=black!20, colback=black!0, boxrule=2pt, arc=6pt, title={\textbf{Méthode \arabic{chapter}.\directlua{tex.print(m)}}}, coltitle=black]
	\begin{enumerate}[label=\textbf{\arabic*)}]
		\item Réaliser la multiplication de la même façon qu'avec des nombres entiers, sans tenir compte des virgules.
		\item Compter le nombre de chiffre derrière la virgule des facteurs.
		\item Le nombre trouvé à l'étape $2$ indique le nombre de chiffre derrière la virgule dans le résultat final.
	\end{enumerate}
\end{tcolorbox}
\begin{luacode}
	m=m+1
\end{luacode}

\textbf{\underline{Exemple}}\\
On souhaite calculer le produit suivant : $78,897 \times 3,78$.
\begin{center}
	\begin{minipage}{0.2\textwidth}
		\opmul[voperator=bottom, decimalsepsymbol={,}, shiftintermediarysymbol={0}, displayshiftintermediary=all, operandstyle.1.-1=\color{red}, operandstyle.1.-2=\color{red}, operandstyle.1.-3=\color{red}, operandstyle.2.-1=\color{red}, operandstyle.2.-2=\color{red}, resultstyle.-1=\color{blue}, resultstyle.-2=\color{blue}, resultstyle.-3=\color{blue}, resultstyle.-4=\color{blue}, resultstyle.-5=\color{blue}]{78,897}{3,78}
	\end{minipage}
	\begin{minipage}{0.7\textwidth}
		\begin{enumerate}[label=\textbf{\arabic*)}]
			\item On réalise la multiplication comme si on faisait le calcul suivant : $\num{78897} \times 378$.
			\item On compte le nombre de chiffres après la virgule dans les facteurs. Ici, il y a $5$ chiffres après la virgule.
			\item On en déduit qu'il y aura $5$ chiffres derrière la virgule dans notre résultat.
		\end{enumerate}
	\end{minipage}
\end{center}
Donc : $78,897 \times 3,78 = \num{298,23066}$

\color{Green}
\section{Multiplier par 10 ; 100 ;... et par 0,1 ; 0,01 ;...}
\color{black}
\begin{tcolorbox}[colframe=black!20, colback=black!0, boxrule=2pt, arc=6pt, title={\textbf{Méthode \arabic{chapter}.\directlua{tex.print(m)}}}, coltitle=black]
\begin{itemize}[label=\textbullet]
    \item Multiplier un nombre par $10$ ; $100$ ou $\num{1000}$ revient à décaler la virgule de ce nombre d'un, de deux ou de trois rangs vers la droite.
    \item Multiplier un nombre par $0,1$ ; $0,01$ ou $0,001$ est équivalent à diviser ce nombre par $10$ ; $100$ ou $1000$. Cela revient à décaler la virgule de ce nombre d'un, de deux ou de trois rangs vers la gauche.
\end{itemize}
\end{tcolorbox}
\begin{luacode}
	m=m+1
\end{luacode}

\textbf{\underline{Exemples}}
\begin{multicols}{2}
    $3\color{red},\color{black}789 \times 1\color{red}0\color{black} = 37\color{red},\color{black}89$\\
    $25\color{red},\color{black}468 \times 1\color{red}00\color{black} = 2\,546\color{red},\color{black}8$\\
    $489\color{red},\color{black}3 \times 1\color{red}000\color{black} = 489\,3\color{red}00\color{black}$\\
    $47\color{red},\color{black}54 \times \color{red}0\color{black},1 = 4\color{red},\color{black}754$\\
    $57 \times \color{red}0,0\color{black}1 = \color{red}0,\color{black}57$\\
    $1\,489\color{red},\color{black}789 \times \color{red}0,00\color{black}1 = 1\color{red},\color{black}489\,789$
\end{multicols}

\color{Green}
\section{Conversions d'unités}
\color{black}
La méthode \arabic{chapter}.\directlua{tex.print(m-1)} permet de réaliser des conversions d'unités.\bigskip\\

\Tableau[Gramme,NbLignes=5,CodeAfter={%
\tikz\draw[-stealth,out=30,in=150] (2-|1.5) to node[above,midway]{\tiny$\times10$}(2-|2.5);
\tikz\draw[-stealth,out=30,in=150] (2-|2.5) to node[above,midway]{\tiny$\times10$}(2-|3.5);
\tikz\draw[-stealth,out=30,in=150] (2-|3.5) to node[above,midway]{\tiny$\times10$} (2-|4.5);
\tikz\draw[-stealth,out=30,in=150] (2-|4.5) to node[above,midway]{\tiny$\times10$}(2-|5.5);
\tikz\draw[-stealth,out=30,in=150] (2-|5.5) to node[above,midway]{\tiny$\times10$}(2-|6.5);
\tikz\draw[-stealth,out=30,in=150] (2-|6.5) to node[above,midway]{\tiny$\times10$}(2-|7.5);
\tikz\draw[-stealth,out=70,in=110] (2-|1.5) to node[above,midway]{\tiny$\times100$}(2-|3.5);
\tikz\draw[-stealth,out=70,in=110] (2-|4.5) to node[above,midway]{\tiny$\times1000$}(2-|7.5);
\tikz\draw[-stealth,out=-150,in=-30] (8.5-|7.5) to node[above,midway]{\tiny$: 10$}(8.5-|6.5);
\tikz\draw[-stealth,out=-150,in=-30] (8.5-|6.5) to node[above,midway]{\tiny$: 10$}(8.5-|5.5);
\tikz\draw[-stealth,out=-150,in=-30] (8.5-|5.5) to node[above,midway]{\tiny$: 10$}(8.5-|4.5);
\tikz\draw[-stealth,out=-150,in=-30] (8.5-|4.5) to node[above,midway]{\tiny$: 10$}(8.5-|3.5);
\tikz\draw[-stealth,out=-150,in=-30] (8.5-|3.5) to node[above,midway]{\tiny$: 10$}(8.5-|2.5);
\tikz\draw[-stealth,out=-150,in=-30] (8.5-|2.5) to node[above,midway]{\tiny$: 10$}(8.5-|1.5);
\tikz\draw[-stealth,out=-110,in=-70] (8.5-|7.5) to node[above,midway]{\tiny$: 100$}(8.5-|5.5);
\tikz\draw[-stealth,out=-110,in=-70] (8.5-|4.5) to node[above,midway]{\tiny$: 1000$}(8.5-|1.5);
}]{}
\bigskip\\

\textbf{\underline{Exemples}} \vspace{-0.3cm}
\begin{multicols}{2}
$\Masse[kg]{5,8} = 5,8 \times \Masse{1000} = \Masse{5800}$\\
$\Masse[cg]{78} = 78 : \Masse{100} = \Masse{0,78}$\\
$\Lg[dm]{155,4} = 155,4 : \Lg[dam]{100} = \Lg[dam]{1,554}$\\
$\Capa[hL]{0.0458} = 0,0458 \times  \Capa[mL]{100000} = \Capa[mL]{4580}$
\end{multicols}

\textbf{\underline{Remarques}}
\begin{itemize}[label=\textbullet]
	\item Une tonne correspond à \Masse[kg]{1000}.
	\item Un quintal correspond à \Masse[kg]{100}. \faExclamationTriangle\, Cette unité n'est pas une unité légale française.
\end{itemize}

\color{Green}
\section{Bilan}
\color{black}
\textbf{\underline{QCM Bilan}} (plusieurs réponses possibles).\\

\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom=, AlphT, Largeur=3cm]{%
$\num{7.63} \times \num{4.6} =$ \ldots & \num{35.098} & \num{35.980}  & \num{350.980}  &1,%
$\num{78.157} \times 1000 = $ \ldots & \num{781.57} & \num{7 815.7}  & \num{78 157}  &3,%
$\num{8.8} \times \num{0.01} = $ \ldots &\num{0.088} & \num{0.88}  & 88  &1,%
\Masse[kg]{1} de crabe coûte \num{11.7} \texteuro. \Masse[kg]{5.6} de crabe coûte \ldots & \num{17.3} \texteuro & \num{6.1} \texteuro & \num{65.52} \texteuro & 3,%
\Masse{200} de reblochon coûte \num{3.62} \texteuro. Un kilogramme de ce reblochon coûte \ldots & \num{18.10} \texteuro & \num{3620} \texteuro  & 5 \texteuro & 1,%
\Lg{4536} =  \ldots  &\Lg[m]{45.36} & \Lg[dm]{45.36}  & \Lg[mm]{45 360} &1,%3
\Masse[kg]{7.689} = \ldots & \Masse[mg]{7 689 000} & \Masse{7 689}  & \Masse[hg]{76.89} & 1%2,3
}


\color{Green}
\section{Auto-évaluation}
\color{black}
\begin{tabular}{|>{\columncolor{darkgray}\color{white}}m{8cm}|>{\color{white}}m{0.6cm}|m{0.6cm}|m{0.6cm}|m{5cm}|} \hline

\rowcolor{darkgray}\centering{} Compétences & \centering{}\color{white} Oui & \centering{}\color{white}\scriptsize{}\rotatebox{45}{Moyen} & \centering{}\color{white}Non & \centering{}\color{white}Pour réviser \tabularnewline \hline \hline

\cellcolor{gray}Je suis capable de réaliser une mutliplication de deux nombres décimaux. & & & & \textbf{\underline{Leçon}} : Partie I \smallskip \newline  \textbf{\underline{Exercices}} : $1$ à $3$ \tabularnewline \hline \hline

\cellcolor{gray}Je suis capable de multiplier rapidement par $10$ ; $100$ ; $\num{1000}$ ; $0,1$ ; $0,01$ ; $0,001$. & & & & \textbf{\underline{Leçon}} : Partie II \smallskip \newline  \textbf{\underline{Exercices}} : $7$ - $8$ \tabularnewline \hline \hline

\cellcolor{gray}Je suis capable de réaliser des conversions d'unités. & & & & \textbf{\underline{Leçon}} : Partie III \smallskip \newline  \textbf{\underline{Exercices}} : $9$ \tabularnewline \hline \hline

\cellcolor{gray}Je suis capable de résoudre des problèmes faisant intervenir les nombres décimaux. & & & & \textbf{\underline{Exercices}} : $4$ à $6$ - $10$ - $11$ \tabularnewline \hline
\end{tabular}



%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------- Annexes -----------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\newpage
\Tableau[Gramme,NbLignes=5,CodeAfter={%
\tikz\draw[-stealth,out=30,in=150] (2-|1.5) to node[above,midway]{\tiny$\times10$}(2-|2.5);
\tikz\draw[-stealth,out=30,in=150] (2-|2.5) to node[above,midway]{\tiny$\times10$}(2-|3.5);
\tikz\draw[-stealth,out=30,in=150] (2-|3.5) to node[above,midway]{\tiny$\times10$} (2-|4.5);
\tikz\draw[-stealth,out=30,in=150] (2-|4.5) to node[above,midway]{\tiny$\times10$}(2-|5.5);
\tikz\draw[-stealth,out=30,in=150] (2-|5.5) to node[above,midway]{\tiny$\times10$}(2-|6.5);
\tikz\draw[-stealth,out=30,in=150] (2-|6.5) to node[above,midway]{\tiny$\times10$}(2-|7.5);
\tikz\draw[-stealth,out=70,in=110] (2-|1.5) to node[above,midway]{\tiny$\times100$}(2-|3.5);
\tikz\draw[-stealth,out=70,in=110] (2-|4.5) to node[above,midway]{\tiny$\times1000$}(2-|7.5);
\tikz\draw[-stealth,out=-150,in=-30] (8.5-|7.5) to node[above,midway]{\tiny$: 10$}(8.5-|6.5);
\tikz\draw[-stealth,out=-150,in=-30] (8.5-|6.5) to node[above,midway]{\tiny$: 10$}(8.5-|5.5);
\tikz\draw[-stealth,out=-150,in=-30] (8.5-|5.5) to node[above,midway]{\tiny$: 10$}(8.5-|4.5);
\tikz\draw[-stealth,out=-150,in=-30] (8.5-|4.5) to node[above,midway]{\tiny$: 10$}(8.5-|3.5);
\tikz\draw[-stealth,out=-150,in=-30] (8.5-|3.5) to node[above,midway]{\tiny$: 10$}(8.5-|2.5);
\tikz\draw[-stealth,out=-150,in=-30] (8.5-|2.5) to node[above,midway]{\tiny$: 10$}(8.5-|1.5);
\tikz\draw[-stealth,out=-110,in=-70] (8.5-|7.5) to node[above,midway]{\tiny$: 100$}(8.5-|5.5);
\tikz\draw[-stealth,out=-110,in=-70] (8.5-|4.5) to node[above,midway]{\tiny$: 1000$}(8.5-|1.5);
}]{{5,}8/1,5800/1,78/5,{0,}78/4}
\bigskip\\

\Tableau[Escalier,Gramme]{}

%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------- Correction QCM -----------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\newpage
\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom=, AlphT, Largeur=3cm, Solution, Couleur=cyan!20]{%
$\num{7.63} \times \num{4.6} =$ \ldots & \num{35.098} & \num{35.980}  & \num{350.980}  &1,%
$\num{78.157} \times 1000 = $ \ldots & \num{781.57} & \num{7 815.7}  & \num{78 157}  &3,%
$\num{8.8} \times \num{0.01} = $ \ldots &\num{0.088} & \num{0.88}  & 88  &1,%
\Masse[kg]{1} de crabe coûte \num{11.7} \texteuro. \Masse[kg]{5.6} de crabe coûte \ldots & \num{17.3} \texteuro & \num{6.1} \texteuro & \num{65.52} \texteuro & 3,%
\Masse{200} de reblochon coûte \num{3.62} \texteuro. Un kilogramme de ce reblochon coûte \ldots & \num{18.10} \texteuro & \num{3620} \texteuro  & 5 \texteuro & 1,%
\Lg{4536} =  \ldots  &\Lg[m]{45.36} & \Lg[dm]{45.36}  & \cellcolor{cyan!20} \Lg[mm]{45 360} &1,%3
\Masse[kg]{7.689} = \ldots & \Masse[mg]{7 689 000} & \cellcolor{cyan!20} \Masse{7 689}  & \cellcolor{cyan!20} \Masse[hg]{76.89} & 1%2,3
}

\normalsize{}
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%---------------------------------------------------------------------- Leçon à trous -----------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\titleformat{\chapter}[frame] {\normalfont \bfseries \Large}{\textcolor{red} {\chaptertitlename\ \thechapter\,-\,#1}}{0pt}{\Huge}

\setcounter{chapter}{8}

\chapter{\pointilles[10cm]}
\begin{luacode}
	d=1
	p=1
	t=1
	m=1
\end{luacode}

{\color{blue}\underline{\textbf{Prérequis}}
\begin{itemize}
	\item \pointilles[16cm]
\end{itemize}~~

\underline{\textbf{Objectifs}}
\begin{itemize}
	\item \pointilles[16cm]
	\item \pointilles[16cm]
	\item \pointilles[16cm]
\end{itemize}


\color{Green}
\section{\pointilles[12cm]}
\color{black}
\normalsize{}
\begin{tcolorbox}[colframe=black!20, colback=black!0, boxrule=2pt, arc=6pt, title={\textbf{Méthode \arabic{chapter}.\directlua{tex.print(m)}}}, coltitle=black]
\begin{enumerate}[label=\textbf{\arabic*)}]
	\item \pointilles[15cm] 
	\item \pointilles[15cm] 
	\item \pointilles[15cm] 
\end{enumerate}
\end{tcolorbox}
\begin{luacode}
	m=m+1
\end{luacode}

\textbf{\underline{Exemple}}\\
On souhaite calculer le produit suivant : $78,897 \times 3,78$.\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]

\color{Green}
\section{\pointilles[12cm]}
\color{black}
\normalsize{}
\begin{tcolorbox}[colframe=black!20, colback=black!0, boxrule=2pt, arc=6pt, title={\textbf{Méthode \arabic{chapter}.\directlua{tex.print(m)}}}, coltitle=black]
\begin{itemize}[label=\textbullet]
    \item \pointilles[15cm]\\
    \pointilles[15cm]\\
     \pointilles[15cm]\\
    \pointilles[15cm]
    \item \pointilles[15cm]\\
    \pointilles[15cm]\\
     \pointilles[15cm]\\
    \pointilles[15cm]
\end{itemize}
\end{tcolorbox}
\begin{luacode}
	m=m+1
\end{luacode}
\newpage
\textbf{\underline{Exemples}}
\begin{multicols}{2}
    $3\color{red},\color{black}789 \times 1\color{red}0\color{black} = \pointilles[2cm]$\\
    $25\color{red},\color{black}468 \times 1\color{red}00\color{black} = \pointilles[2cm]$\\
    $489\color{red},\color{black}3 \times 1\color{red}000\color{black} = \pointilles[2cm]$\\
    $47\color{red},\color{black}54 \times \color{red}0\color{black},1 =\pointilles[2cm]$\\
    $57 \times \color{red}0,0\color{black}1 = \pointilles[2cm]$\\
    $1\,489\color{red},\color{black}789 \times \color{red}0,00\color{black}1 =\pointilles[2cm]$
\end{multicols}

\color{Green}
\section{\pointilles[12cm]}
\color{black}
\normalsize{}
\pointilles[17cm]\bigskip\\

\Tableau[Gramme,NbLignes=5,CodeAfter={%
\tikz\draw[-stealth,out=30,in=150] (2-|1.5) to node[above,midway]{\tiny$\times10$}(2-|2.5);
\tikz\draw[-stealth,out=30,in=150] (2-|2.5) to node[above,midway]{\tiny$\times10$}(2-|3.5);
\tikz\draw[-stealth,out=30,in=150] (2-|3.5) to node[above,midway]{\tiny$\times10$} (2-|4.5);
\tikz\draw[-stealth,out=30,in=150] (2-|4.5) to node[above,midway]{\tiny$\times10$}(2-|5.5);
\tikz\draw[-stealth,out=30,in=150] (2-|5.5) to node[above,midway]{\tiny$\times10$}(2-|6.5);
\tikz\draw[-stealth,out=30,in=150] (2-|6.5) to node[above,midway]{\tiny$\times10$}(2-|7.5);
\tikz\draw[-stealth,out=70,in=110] (2-|1.5) to node[above,midway]{\tiny$\times100$}(2-|3.5);
\tikz\draw[-stealth,out=70,in=110] (2-|4.5) to node[above,midway]{\tiny$\times1000$}(2-|7.5);
\tikz\draw[-stealth,out=-150,in=-30] (8.5-|7.5) to node[above,midway]{\tiny$: 10$}(8.5-|6.5);
\tikz\draw[-stealth,out=-150,in=-30] (8.5-|6.5) to node[above,midway]{\tiny$: 10$}(8.5-|5.5);
\tikz\draw[-stealth,out=-150,in=-30] (8.5-|5.5) to node[above,midway]{\tiny$: 10$}(8.5-|4.5);
\tikz\draw[-stealth,out=-150,in=-30] (8.5-|4.5) to node[above,midway]{\tiny$: 10$}(8.5-|3.5);
\tikz\draw[-stealth,out=-150,in=-30] (8.5-|3.5) to node[above,midway]{\tiny$: 10$}(8.5-|2.5);
\tikz\draw[-stealth,out=-150,in=-30] (8.5-|2.5) to node[above,midway]{\tiny$: 10$}(8.5-|1.5);
\tikz\draw[-stealth,out=-110,in=-70] (8.5-|7.5) to node[above,midway]{\tiny$: 100$}(8.5-|5.5);
\tikz\draw[-stealth,out=-110,in=-70] (8.5-|4.5) to node[above,midway]{\tiny$: 1000$}(8.5-|1.5);
}]{}
\bigskip\\


\textbf{\underline{Exemples}} \vspace{-0.3cm}
\begin{multicols}{2}
$\Masse[kg]{58} = \pointilles[6cm]$\\
$\Masse[cg]{78} = \pointilles[6cm]$\\
$\Lg[dm]{155,4} = \pointilles[6cm]$\\
$\Capa[hL]{0.0458} = \pointilles[6cm]$
\end{multicols}

\textbf{\underline{Remarques}}
\begin{itemize}[label=\textbullet]
	\item \pointilles[16cm]
	\item \pointilles[16cm]
\end{itemize}


\color{Green}
\section{Bilan}
\color{black}
\textbf{\underline{QCM Bilan}} (plusieurs réponses possibles).\\

\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom=, AlphT, Largeur=3cm]{%
$\num{7.63} \times \num{4.6} =$ \ldots & \num{35.098} & \num{35.980}  & \num{350.980}  &1,%
$\num{78.157} \times 1000 = $ \ldots & \num{781.57} & \num{7 815.7}  & \num{78 157}  &3,%
$\num{8.8} \times \num{0.01} = $ \ldots &\num{0.088} & \num{0.88}  & 88  &1,%
\Masse[kg]{1} de crabe coûte \num{11.7} \texteuro. \Masse[kg]{5.6} de crabe coûte \ldots & \num{17.3} \texteuro & \num{6.1} \texteuro & \num{65.52} \texteuro & 3,%
\Masse{200} de reblochon coûte \num{3.62} \texteuro. Un kilogramme de ce reblochon coûte \ldots & \num{18.10} \texteuro & \num{3620} \texteuro  & 5 \texteuro & 1,%
\Lg{4536} =  \ldots  &\Lg[m]{45.36} & \Lg[dm]{45.36}  & \Lg[mm]{45 360} &1,%3
\Masse[kg]{7.689} = \ldots & \Masse[mg]{7 689 000} & \Masse{7 689}  & \Masse[hg]{76.89} & 1%2,3
}


\color{Green}
\section{Auto-évaluation}
\color{black}
\begin{tabular}{|>{\columncolor{darkgray}\color{white}}m{8cm}|>{\color{white}}m{0.6cm}|m{0.6cm}|m{0.6cm}|m{5cm}|} \hline

\rowcolor{darkgray}\centering{} Compétences & \centering{}\color{white} Oui & \centering{}\color{white}\scriptsize{}\rotatebox{45}{Moyen} & \centering{}\color{white}Non & \centering{}\color{white}Pour réviser \tabularnewline \hline \hline

\cellcolor{gray}Je suis capable de réaliser une mutliplication de deux nombres décimaux. & & & & \textbf{\underline{Leçon}} : Partie I \smallskip \newline  \textbf{\underline{Exercices}} : $1$ à $3$ \tabularnewline \hline \hline

\cellcolor{gray}Je suis capable de multiplier rapidement par $10$ ; $100$ ; $\num{1000}$ ; $0,1$ ; $0,01$ ; $0,001$. & & & & \textbf{\underline{Leçon}} : Partie II \smallskip \newline  \textbf{\underline{Exercices}} : $7$ - $8$ \tabularnewline \hline \hline

\cellcolor{gray}Je suis capable de réaliser des conversions d'unités. & & & & \textbf{\underline{Leçon}} : Partie III \smallskip \newline  \textbf{\underline{Exercices}} : $9$ \tabularnewline \hline \hline

\cellcolor{gray}Je suis capable de résoudre des problèmes faisant intervenir les nombres décimaux. & & & & \textbf{\underline{Exercices}} : $4$ à $6$ - $10$ - $11$ \tabularnewline \hline
\end{tabular}
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------- Impression -----------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\newpage
\textbf{\underline{QCM Bilan}} (plusieurs réponses possibles).\\

\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom=, AlphT, Largeur=3cm]{%
$\num{7.63} \times \num{4.6} =$ \ldots & \num{35.098} & \num{35.980}  & \num{350.980}  &1,%
$\num{78.157} \times 1000 = $ \ldots & \num{781.57} & \num{7 815.7}  & \num{78 157}  &3,%
$\num{8.8} \times \num{0.01} = $ \ldots &\num{0.088} & \num{0.88}  & 88  &1,%
\Masse[kg]{1} de crabe coûte \num{11.7} \texteuro. \Masse[kg]{5.6} de crabe coûte \ldots & \num{17.3} \texteuro & \num{6.1} \texteuro & \num{65.52} \texteuro & 3,%
\Masse{200} de reblochon coûte \num{3.62} \texteuro. Un kilogramme de ce reblochon coûte \ldots & \num{18.10} \texteuro & \num{3620} \texteuro  & 5 \texteuro & 1,%
\Lg{4536} =  \ldots  &\Lg[m]{45.36} & \Lg[dm]{45.36}  & \Lg[mm]{45 360} &1,%3
\Masse[kg]{7.689} = \ldots & \Masse[mg]{7 689 000} & \Masse{7 689}  & \Masse[hg]{76.89} & 1%2,3
}
~~\\\\

\begin{tabular}{|>{\columncolor{darkgray}\color{white}}m{8cm}|>{\color{white}}m{0.6cm}|m{0.6cm}|m{0.6cm}|m{5cm}|} \hline

\rowcolor{darkgray}\centering{} Compétences & \centering{}\color{white} Oui & \centering{}\color{white}\scriptsize{}\rotatebox{45}{Moyen} & \centering{}\color{white}Non & \centering{}\color{white}Pour réviser \tabularnewline \hline \hline

\cellcolor{gray}Je suis capable de réaliser une mutliplication de deux nombres décimaux. & & & & \textbf{\underline{Leçon}} : Partie I \smallskip \newline  \textbf{\underline{Exercices}} : $1$ à $3$ \tabularnewline \hline \hline

\cellcolor{gray}Je suis capable de multiplier rapidement par $10$ ; $100$ ; $\num{1000}$ ; $0,1$ ; $0,01$ ; $0,001$. & & & & \textbf{\underline{Leçon}} : Partie II \smallskip \newline  \textbf{\underline{Exercices}} : $7$ - $8$ \tabularnewline \hline \hline

\cellcolor{gray}Je suis capable de réaliser des conversions d'unités. & & & & \textbf{\underline{Leçon}} : Partie III \smallskip \newline  \textbf{\underline{Exercices}} : $9$ \tabularnewline \hline \hline

\cellcolor{gray}Je suis capable de résoudre des problèmes faisant intervenir les nombres décimaux. & & & & \textbf{\underline{Exercices}} : $4$ à $6$ - $10$ - $11$ \tabularnewline \hline
\end{tabular}
~~\\\\\\\\\\
\Tableau[Gramme,NbLignes=5,CodeAfter={%
\tikz\draw[-stealth,out=30,in=150] (2-|1.5) to node[above,midway]{\tiny$\times10$}(2-|2.5);
\tikz\draw[-stealth,out=30,in=150] (2-|2.5) to node[above,midway]{\tiny$\times10$}(2-|3.5);
\tikz\draw[-stealth,out=30,in=150] (2-|3.5) to node[above,midway]{\tiny$\times10$} (2-|4.5);
\tikz\draw[-stealth,out=30,in=150] (2-|4.5) to node[above,midway]{\tiny$\times10$}(2-|5.5);
\tikz\draw[-stealth,out=30,in=150] (2-|5.5) to node[above,midway]{\tiny$\times10$}(2-|6.5);
\tikz\draw[-stealth,out=30,in=150] (2-|6.5) to node[above,midway]{\tiny$\times10$}(2-|7.5);
\tikz\draw[-stealth,out=70,in=110] (2-|1.5) to node[above,midway]{\tiny$\times100$}(2-|3.5);
\tikz\draw[-stealth,out=70,in=110] (2-|4.5) to node[above,midway]{\tiny$\times1000$}(2-|7.5);
\tikz\draw[-stealth,out=-150,in=-30] (8.5-|7.5) to node[above,midway]{\tiny$: 10$}(8.5-|6.5);
\tikz\draw[-stealth,out=-150,in=-30] (8.5-|6.5) to node[above,midway]{\tiny$: 10$}(8.5-|5.5);
\tikz\draw[-stealth,out=-150,in=-30] (8.5-|5.5) to node[above,midway]{\tiny$: 10$}(8.5-|4.5);
\tikz\draw[-stealth,out=-150,in=-30] (8.5-|4.5) to node[above,midway]{\tiny$: 10$}(8.5-|3.5);
\tikz\draw[-stealth,out=-150,in=-30] (8.5-|3.5) to node[above,midway]{\tiny$: 10$}(8.5-|2.5);
\tikz\draw[-stealth,out=-150,in=-30] (8.5-|2.5) to node[above,midway]{\tiny$: 10$}(8.5-|1.5);
\tikz\draw[-stealth,out=-110,in=-70] (8.5-|7.5) to node[above,midway]{\tiny$: 100$}(8.5-|5.5);
\tikz\draw[-stealth,out=-110,in=-70] (8.5-|4.5) to node[above,midway]{\tiny$: 1000$}(8.5-|1.5);
}]{{5,}8/1,5800/1,78/5,{0,}78/4}
\bigskip\\

%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%--------------------------------------------------------------- Histoire des mathématiques -------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\newpage
\begin{tcolorbox}[colframe=black, colback=blue!10, boxrule=2pt, arc=6pt]
	\centering{}\LARGE{\textbf{Histoire des mathématiques \faBook}}
\end{tcolorbox}
\begin{tcolorbox}[colframe=black!30, colback=blue!0, boxrule=2pt, arc=6pt, title=?, coltitle=black]

\end{tcolorbox}

\begin{tcolorbox}[colframe=black!10, colback=blue!0, boxrule=1pt, arc=1pt, title=Quelques anecdotes, coltitle=black]

\end{tcolorbox}

\textbf{\underline{Sources}}
\begin{itemize}[label=\textbullet]
	\item Hauchecorne, B. et Suratteau, D. (2008), \textit{Des Mathématiciens de A à Z} ($3^e$ édition), Ellipses
	\item Byrne, O. (2022), \textit{The first six books of the elements of Euclid}, Taschen
	\item Houlou-Garcia, A. (2019), \textit{Mathematikos} (2\ieme\, tirage), Belles lettres
	\item Barbin, E. (2019), \textit{Faire des mathématiques avec l'histoire au lycée}, Ellipses
	\item Dahan-Dalmedico, A. et Peiffer, J. (1986), \textit{Une histoire des mathématiques : Routes et dédales}, Points
	\item Freiberger, M. et Thomas, R. (2018), \textit{Dans le secret des nombres}, Ekho
\end{itemize}

%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%-------------------------------------------------------------------------- Fiche de route ------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\newpage
\begin{tcolorbox}[colframe=black, colback=Khaki!50, boxrule=2pt, arc=6pt]
	\centering{}\LARGE{\textbf{Fiche de route}}
\end{tcolorbox}
\begin{itemize}[label=\textbullet]
	\item \color{Tomato} \textbf{Introduction} \color{black} : Titre, prérequis, objectifs, rappels
	\item \color{RoyalBlue}\underline{Leçon} \color{black} : Partie I : 
	\item \color{ForestGreen}\underline{Exercices}  \color{black} : 
	\item \color{RoyalBlue}\underline{Leçon}  \color{black} : Parties IV et V : Bilan + Auto-évaluation
	\item \color{ForestGreen}\underline{Exercices bilan}  \color{black} : 
	\item \color{Coral} \underline{Histoire des mathématiques}
\end{itemize}

\end{document}
