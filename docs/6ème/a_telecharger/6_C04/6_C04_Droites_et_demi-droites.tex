\documentclass[a4paper, 12pt, dvipsnames]{report}

\usepackage{lmodern} %police standard latin moderne
\usepackage[frenchb]{babel} %langue française
\usepackage[utf8]{inputenc} %pour l'utf-8
\usepackage[T1]{fontenc} %pour les césures et accentuations
\usepackage{geometry} %pour changer les marges d'un document
\usepackage{graphicx}
\usepackage{fancybox}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{ProfCollege}
\usepackage{luacode}


\usepackage[explicit]{titlesec} %Permet de modifier les différentes parties, chapitres, sous-parties,...

\usepackage{colortbl} %permet de mettre des cellules de tableau en couleur

\usepackage{multicol}

\usepackage{cellspace} %pour définir les espaces en haut et en bas des cellules d'un tableau
\setlength{\cellspacebottomlimit}{5pt}
\setlength{\cellspacetoplimit}{5pt}
\setlength{\parindent}{0pt}

\usepackage{xcolor} %permet de mettre du texte en couleur

\usepackage{array} %gestion des tableaux
\usepackage{tcolorbox} %créer des boîtes de couleurs

\usepackage{hyperref} %permet de gérer le sommaire et la page de garde et les options du PDF

\geometry{top=2cm, bottom=2cm, right=2cm, left=2cm}   
\titlespacing{\chapter}{0pt}{0pt}{5pt}

\usepackage{lastpage}
\usepackage{fancyhdr} %définition des en-têtes et des bas de page
\fancyhf{}
\fancypagestyle{main}{
	\renewcommand\headrulewidth{5pt}
	\fancyhead[L]{6\ieme - Chapitre 04 - Droites et demi-droites}
	\fancyhead[R]{Collège}
	\fancyfoot[C]{\thepage/\pageref{LastPage}}
	\fancyfoot[R]{\tiny{Anthony Salze}}
	}
\pagestyle{main}

%gestion des entêtes
\makeatletter\@addtoreset{section}{chapter}\makeatother % RAZ des numéros de section après un chapitre
\renewcommand{\thepart}{\Alph{part} -} % Pour mettre des I, II, etc. aux parties
\renewcommand{\thesection}{\color{Green} \Roman{section} -}% Idem pour les sections et avoir le numéro de chapitre
\renewcommand{\thesubsection}{\arabic{subsection} -}

\begin{document}
%format du titre des chapitres placé après Sommaire sinon cela bugue
\titleformat{\chapter}[frame] {\normalfont \bfseries \Huge}{\textcolor{red} {\chaptertitlename\ \thechapter\,-\,#1}}{0pt}{\Huge}

\setcounter{chapter}{3}
\begin{luacode}
	d=1
	p=1
	t=1
	m=1
\end{luacode}

\chapter{Droites et demi-droites}

{\color{blue}\underline{\textbf{Prérequis}}
\begin{itemize}
	\item Chapitre $2$.
	\item Notions de point d'intersection, d'appartenance.
\end{itemize}~~

\underline{\textbf{Objectifs}}
\begin{itemize}
	\item Tracer les éléments de géométrie de base (point, demi-droite, droite, segment) à l'aide d'une règle.
	\item Identifier les différents éléments de géométrie sur une figure.
	\item Connaître les notations des éléments de géométrie de base dans un texte.
	\item Être capable de suivre les étapes d'un programme de construction.
\end{itemize}}

\color{Green}
\section{\'Eléments de géométrie de base}
\color{blue}
	\subsection{Les points}
\color{black}
\normalsize{}
\begin{tcolorbox}[colframe=Green!20, colback=Green!0, boxrule=2pt, arc=6pt, title={\textbf{Définition \arabic{chapter}.\directlua{tex.print(d)}}}, coltitle=black]
Sur une figure, un point est toujours représenté par une croix. On le nomme avec une lettre majuscule.
\end{tcolorbox}
\begin{luacode}
	d=d+1
\end{luacode}

\textbf{\underline{Exemples}}\\
\begin{minipage}{0.2\textwidth}
	\begin{center}
		\begin{Geometrie}[CoinHD={u*(3,3)}]
			pair A;
			A=u*(1,1);
			marque_p:="croix";
			pointe(A);
			label.top(btex A etex, A);
		\end{Geometrie}
	\end{center}
\end{minipage}
\hspace{0.1cm} \vrule \hspace{0.1cm} \vrule \hspace{0.1cm}
\begin{minipage}{0.33\textwidth}
	\begin{center}
		\begin{Geometrie}[CoinHD={u*(3,3)}]
			pair C,D;
			C=u*(1,1);
			D=u*(2,2);
			marque_p:="croix";
			pointe(C,D);
			trace droite(C,D);
			label.top(btex C etex, C);
			label.top(btex D etex, D);
		\end{Geometrie}
	\end{center}
\end{minipage}
\hspace{0.1cm} \vrule \hspace{0.1cm} \vrule \hspace{0.1cm}
\begin{minipage}{0.33\textwidth}
	\begin{center}
		\begin{Geometrie}[CoinHD={u*(3,3)}]
			pair C,D,E;
			C=u*(1,1);
			D=u*(1.5,1.5);
			E=u*(2,1.5);
			trace droite(C,D);
			trace droite(E,D);
			label.top(btex F etex, D);
		\end{Geometrie}
	\end{center}
\end{minipage}
\\
\\
\color{blue}
	\subsection{Droites, demi-droites et segments}
\color{black}
\begin{tabular}{|c|c|c|m{4.7cm}|} \hline
Nom & Figure & Notations & Description \tabularnewline \hline
Une droite & \begin{Geometrie}[CoinHD={u*(3.5,3.5)}]
			pair A,B;
			A=u*(1,1);
			B=u*(3,1.5);
			marque_p:="croix";
			pointe(A,B);
			trace droite(A,B);
			label.top(btex A etex, A);
			label.top(btex B etex, B);
		\end{Geometrie}
& $(AB)$ ou $(BA)$ & Une droite est illimitée.  Par conséquent, elle ne peut pas être mesurée et ne possède pas de longueur. \tabularnewline \hline
Un segment & \begin{Geometrie}[CoinHD={u*(3.5,3.5)}]
			pair C,D;
			C=u*(0.25,0.75);
			D=u*(2.75,0.25);
			trace segment(C,D);
			marque_s:=marque_s/2;
			trace marquesegment(C,D);
			label.top(btex C etex, C);
			label.top(btex D etex, D);
		\end{Geometrie}
 & $[CD]$ ou $[DC]$ & Un segment est une portion de droite délimité par deux points appelés extrémités. \tabularnewline \hline
\end{tabular}
~~\\
\begin{tabular}{|c|c|c|m{4.7cm}|} \hline
Nom & Figure & Notations & Description \tabularnewline \hline

Une demi-droite & \begin{Geometrie}[CoinHD={u*(3.5,3.5)}]
			pair E,F;
			E=u*(0.25,0.75);
			F=u*(2.75,1.75);
			trace demidroite(E,F);
			marque_s:=marque_s/2;
			marque_p:="croix";
			pointe(F);
			trace marquedemidroite(E,F);
			label.top(btex E etex, E);
			label.top(btex F etex, F);
		\end{Geometrie} 
		& $[EF)$ & Une demi-droite est une portion de droite limitée d'un seul côté par un point appelé origine. \tabularnewline \hline
\end{tabular}

\color{Green}
\section{Alignement de points}
\color{black}
\begin{tcolorbox}[colframe=Green!20, colback=Green!0, boxrule=2pt, arc=6pt, title={\textbf{Définition \arabic{chapter}.\directlua{tex.print(d)}}}, coltitle=black]
	Trois points sont alignés s'ils appartiennent tous à la même droite.
\end{tcolorbox}
\begin{luacode}
	d=d+1
\end{luacode}

\textbf{\underline{Exemple}}\\
\begin{minipage}{0.4\textwidth}
	\begin{Geometrie}[CoinHD={u*(6,6)}]
			pair A,B,C,D;
			A=u*(1,1.5);
			B=u*(2,2);
			C=u*(4,1.75);
			D=u*(5,3.5);
			trace droite(A,B);
			marque_s:=marque_s/2;
			marque_p:="croix";
			pointe(A,B,C,D);
			label.top(btex A etex, A);
			label.top(btex B etex, B);
			label.top(btex C etex, C);
			label.top(btex D etex, D);
			trace appelation(B,D,3mm,btex $(d_1)$ etex);
		\end{Geometrie} 
\end{minipage}
\hspace{0.5cm}
\begin{minipage}{0.55\textwidth}
	Les points $A$, $B$ et $D$ appartiennent tous à la même droite $(d_1)$. Ils sont donc alignés.\\
	Par exemple, on note $A \in (BD)$, ce qui veut dire \guillemotleft\,$A$ appartient à la droite $(BD)$ \guillemotright.\\
	On peut aussi noter que $B \in [AD]$ et que $C \notin (d_1)$.
\end{minipage}

\vspace{-0.5cm}

\color{Green}
\section{Programme de construction}
\color{black}
\begin{tcolorbox}[colframe=Green!20, colback=Green!0, boxrule=2pt, arc=6pt, title={\textbf{Définition \arabic{chapter}.\directlua{tex.print(d)}}}, coltitle=black]
	Un programme de construction est une liste d'instructions permettant de construire une figure.
\end{tcolorbox}
\begin{luacode}
	d=d+1
\end{luacode}

\textbf{\underline{Exemple}}\\
\begin{minipage}{0.6\textwidth}
	Voici un programme de construction :
	\begin{itemize}[label=\textbullet]
		\item Placer trois points $A$, $B$ et $C$ non alignés.
		\item Tracer la droite $(AB)$.
		\item Placer un point $D$ tel que $D \in (AB)$ et tel que $D \notin [AB]$.
		\item Tracer le segment $[DC]$ et la demi-droite $[CA)$.
	\end{itemize}
\end{minipage}
\begin{minipage}{0.3\textwidth}
	\begin{Geometrie}[CoinHD={u*(6,6)}]
			pair A,B,C,D;
			A=u*(1,3);
			B=u*(2,2.5);
			D=u*(4,1.5);
			C=u*(3,4);
			trace droite(A,B);
			trace demidroite(C,A);
			marque_s:=marque_s/2;
			marque_p:="croix";
			pointe(A,B,D);
			trace segment(D,C);
			trace marquedemidroite(C,A);
			label.top(btex A etex, A);
			label.top(btex B etex, B);
			label.top(btex C etex, C);
			label.urt(btex D etex, D);
		\end{Geometrie} 
\end{minipage}

\color{Green}
\section{Auto-évaluation}
\color{black}
\begin{tabular}{|>{\columncolor{darkgray}\color{black}}m{8cm} |>{\color{black}}m{0.6cm}|m{0.6cm}|m{0.6cm}|m{5cm}|} \hline

\rowcolor{Lavender}\centering{} \color{black} Compétences & \centering{}\color{MediumSeaGreen} \faCheck & \centering{}\color{DarkOrange}\faWater & \centering{}\color{Crimson}\faTimes & \centering{}\color{black}Pour réviser \tabularnewline \hline \hline


\cellcolor{Lavender}Je sais reconnaître les éléments géométriques de base sur une figure donnée ainsi que leur notation mathématique dans un texte. & & & & \textbf{\underline{Leçon}} : Partie I \smallskip \newline  \textbf{\underline{Exercices}} : $1$ à $11 - 14 - 15$ \tabularnewline \hline \hline

\cellcolor{Lavender}Je connais la définition de l'alignement de plusieurs points. & & & & \textbf{\underline{Leçon}} : Partie II\smallskip \newline  \textbf{\underline{Exercices}} : $13 - 16 - 17$ \tabularnewline \hline \hline

\cellcolor{Lavender}Je suis capable de suivre une liste d'instructions d'un programme pour construire une figure. & & & & \textbf{\underline{Leçon}} : Partie III \smallskip \newline  \textbf{\underline{Exercices}} : $12$ + exercice supplémentaire  \tabularnewline \hline
\end{tabular}


%\begin{comment}
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%---------------------------------------------------------------------- Leçon à trous -----------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\newpage
%\setcounter{page}{1}
\titleformat{\chapter}[frame] {\normalfont \bfseries \huge}{\textcolor{red} {\chaptertitlename\ \thechapter\,-\,#1}}{0pt}{\huge}

\setcounter{chapter}{3}
\begin{luacode}
	d=1
	p=1
	t=1
	m=1
\end{luacode}

\chapter{\pointilles[10cm]}

{\color{blue}\underline{\textbf{Prérequis}}
\begin{itemize}
	\item \pointilles[16cm]
\end{itemize}~~

\underline{\textbf{Objectifs}}
\begin{itemize}
	\item Tracer les éléments de géométrie de base (\pointilles[7cm]) à l'aide d'une règle.
	\item Identifier les différents \pointilles[7cm] sur une figure.
	\item Connaître les \pointilles[3cm] des éléments de géométrie de base dans un texte.
	\item Être capable de suivre les \pointilles[10cm].
\end{itemize}}

\color{Green}
\section{\pointilles[8cm]}
\color{blue}
	\subsection{\pointilles[4cm]}
\color{black}
\normalsize{}
\begin{tcolorbox}[colframe=Green!20, colback=Green!0, boxrule=2pt, arc=6pt, title={\textbf{Définition \arabic{chapter}.\directlua{tex.print(d)}}}, coltitle=black]
\pointilles[16cm]\\
\pointilles[16cm]\\
\pointilles[16cm]
\end{tcolorbox}
\begin{luacode}
	d=d+1
\end{luacode}

\textbf{\underline{Exemple}}\\
\\
\\
\\
\\
\\
\\
\\
\\
\\
\\
\\
\color{blue}
	\subsection{\pointilles[10cm]}
\color{black}
\begin{tabular}{|c|c|c|m{4.7cm}|} \hline
Nom & Figure & Notations & Description \tabularnewline \hline
\pointilles[3cm] & \hspace{4cm}  & \pointilles[3cm] & Une \pointilles[3cm] est \pointilles[3cm].  Par conséquent, elle ne peut pas être \pointilles[3cm] et ne possède pas de \pointilles[3cm]. \tabularnewline \hline
\end{tabular}
~~\\
\begin{tabular}{|c|c|c|m{4.7cm}|} \hline
Nom & Figure & Notations & Description \tabularnewline \hline
\pointilles[3cm] & \hspace{4cm}  & \pointilles[3cm] &  Un \pointilles[3cm] est une \pointilles[3cm] de \pointilles[3cm] délimité par \pointilles[3cm] appelés \pointilles[3cm]. \tabularnewline \hline
\pointilles[3cm] & \hspace{4cm}  & \pointilles[3cm] &  Une \pointilles[3cm] est une \pointilles[3cm] de \pointilles[3cm] d'un \pointilles[3cm] par un point appelé \pointilles[3cm]. \tabularnewline \hline
\end{tabular}

\color{Green}
\section{\pointilles[7cm]}
\color{black}
\begin{tcolorbox}[colframe=Green!20, colback=Green!0, boxrule=2pt, arc=6pt, title={\textbf{Définition \arabic{chapter}.\directlua{tex.print(d)}}}, coltitle=black]
	\pointilles[16cm]\\
	\pointilles[16cm]
\end{tcolorbox}
\begin{luacode}
	d=d+1
\end{luacode}

\textbf{\underline{Exemple}}\\
\begin{minipage}{0.3\textwidth}
	\hspace{5cm}
\end{minipage}
\hspace{0.5cm}
\begin{minipage}{0.65\textwidth}
	\pointilles[11cm]\\
	\pointilles[11cm]\\
	\pointilles[11cm]\\
	\pointilles[11cm]\\
	\pointilles[11cm]\\
	\pointilles[11cm]\\
	\pointilles[11cm]
\end{minipage}

\color{Green}
\section{\pointilles[8cm]}
\color{black}
\begin{tcolorbox}[colframe=Green!20, colback=Green!0, boxrule=2pt, arc=6pt, title={\textbf{Définition \arabic{chapter}.\directlua{tex.print(d)}}}, coltitle=black]
	\pointilles[16cm]\\
	\pointilles[16cm]
\end{tcolorbox}
\begin{luacode}
	d=d+1
\end{luacode}

\textbf{\underline{Exemple}}\\
\begin{minipage}{0.6\textwidth}
	Voici un programme de construction :
	\begin{itemize}[label=\textbullet]
		\item Placer trois points $A$, $B$ et $C$ non alignés.
		\item Tracer la droite $(AB)$.
		\item Placer un point $D$ tel que $D \in (AB)$ et tel que $D \notin [AB]$.
		\item Tracer le segment $[DC]$ et la demi-droite $[CA)$.
	\end{itemize}
\end{minipage}
\begin{minipage}{0.3\textwidth}
	\hspace{6cm}
\end{minipage}

\color{Green}
\section{Auto-évaluation}
\color{black}
\begin{tabular}{|>{\columncolor{darkgray}\color{black}}m{8cm} |>{\color{black}}m{0.6cm}|m{0.6cm}|m{0.6cm}|m{5cm}|} \hline

\rowcolor{Lavender}\centering{} \color{black} Compétences & \centering{}\color{MediumSeaGreen} \faCheck & \centering{}\color{DarkOrange}\faWater & \centering{}\color{Crimson}\faTimes & \centering{}\color{black}Pour réviser \tabularnewline \hline \hline


\cellcolor{Lavender}Je sais reconnaître les éléments géométriques de base sur une figure donnée ainsi que leur notation mathématique. & & & & \textbf{\underline{Leçon}} : Partie I \smallskip \newline  \textbf{\underline{Exercices}} : $1$ à $11 - 14 - 15$ \tabularnewline \hline \hline

\cellcolor{Lavender}Je connais la définition de l'alignement de plusieurs points. & & & & \textbf{\underline{Leçon}} : Partie II\smallskip \newline  \textbf{\underline{Exercices}} : $13 - 16 - 17$ \tabularnewline \hline \hline

\cellcolor{Lavender}Je suis capable de suivre une liste d'instructions d'un programme pour construire une figure. & & & & \textbf{\underline{Leçon}} : Partie III \smallskip \newline  \textbf{\underline{Exercices}} : $12$ + exercice supplémentaire  \tabularnewline \hline
\end{tabular}



%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------- Impression -----------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\newpage
\color{Green}
\section{Auto-évaluation}
\color{black}
\begin{tabular}{|>{\columncolor{darkgray}\color{black}}m{8cm} |>{\color{black}}m{0.6cm}|m{0.6cm}|m{0.6cm}|m{5cm}|} \hline

\rowcolor{Lavender}\centering{} \color{black} Compétences & \centering{}\color{MediumSeaGreen} \faCheck & \centering{}\color{DarkOrange}\faWater & \centering{}\color{Crimson}\faTimes & \centering{}\color{black}Pour réviser \tabularnewline \hline \hline


\cellcolor{Lavender}Je sais reconnaître les éléments géométriques de base sur une figure donnée ainsi que leur notation mathématique. & & & & \textbf{\underline{Leçon}} : Partie I \smallskip \newline  \textbf{\underline{Exercices}} : $1$ à $11 - 14 - 15$ \tabularnewline \hline \hline

\cellcolor{Lavender}Je connais la définition de l'alignement de plusieurs points. & & & & \textbf{\underline{Leçon}} : Partie II\smallskip \newline  \textbf{\underline{Exercices}} : $13 - 16 - 17$ \tabularnewline \hline \hline

\cellcolor{Lavender}Je suis capable de suivre une liste d'instructions d'un programme pour construire une figure. & & & & \textbf{\underline{Leçon}} : Partie III \smallskip \newline  \textbf{\underline{Exercices}} : $12$ + exercice supplémentaire  \tabularnewline \hline
\end{tabular}
~~\\\\\\

\color{Green}
\section{Auto-évaluation}
\color{black}
\begin{tabular}{|>{\columncolor{darkgray}\color{black}}m{8cm} |>{\color{black}}m{0.6cm}|m{0.6cm}|m{0.6cm}|m{5cm}|} \hline

\rowcolor{Lavender}\centering{} \color{black} Compétences & \centering{}\color{MediumSeaGreen} \faCheck & \centering{}\color{DarkOrange}\faWater & \centering{}\color{Crimson}\faTimes & \centering{}\color{black}Pour réviser \tabularnewline \hline \hline


\cellcolor{Lavender}Je sais reconnaître les éléments géométriques de base sur une figure donnée ainsi que leur notation mathématique. & & & & \textbf{\underline{Leçon}} : Partie I \smallskip \newline  \textbf{\underline{Exercices}} : $1$ à $11 - 14 - 15$ \tabularnewline \hline \hline

\cellcolor{Lavender}Je connais la définition de l'alignement de plusieurs points. & & & & \textbf{\underline{Leçon}} : Partie II\smallskip \newline  \textbf{\underline{Exercices}} : $13 - 16 - 17$ \tabularnewline \hline \hline

\cellcolor{Lavender}Je suis capable de suivre une liste d'instructions d'un programme pour construire une figure. & & & & \textbf{\underline{Leçon}} : Partie III \smallskip \newline  \textbf{\underline{Exercices}} : $12$ + exercice supplémentaire  \tabularnewline \hline
\end{tabular}
~~\\\\\\


\color{Green}
\section{Auto-évaluation}
\color{black}
\begin{tabular}{|>{\columncolor{darkgray}\color{black}}m{8cm} |>{\color{black}}m{0.6cm}|m{0.6cm}|m{0.6cm}|m{5cm}|} \hline

\rowcolor{Lavender}\centering{} \color{black} Compétences & \centering{}\color{MediumSeaGreen} \faCheck & \centering{}\color{DarkOrange}\faWater & \centering{}\color{Crimson}\faTimes & \centering{}\color{black}Pour réviser \tabularnewline \hline \hline


\cellcolor{Lavender}Je sais reconnaître les éléments géométriques de base sur une figure donnée ainsi que leur notation mathématique. & & & & \textbf{\underline{Leçon}} : Partie I \smallskip \newline  \textbf{\underline{Exercices}} : $1$ à $11 - 14 - 15$ \tabularnewline \hline \hline

\cellcolor{Lavender}Je connais la définition de l'alignement de plusieurs points. & & & & \textbf{\underline{Leçon}} : Partie II\smallskip \newline  \textbf{\underline{Exercices}} : $13 - 16 - 17$ \tabularnewline \hline \hline

\cellcolor{Lavender}Je suis capable de suivre une liste d'instructions d'un programme pour construire une figure. & & & & \textbf{\underline{Leçon}} : Partie III \smallskip \newline  \textbf{\underline{Exercices}} : $12$ + exercice supplémentaire  \tabularnewline \hline
\end{tabular}

%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%--------------------------------------------------------------- Histoire des mathématiques -------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\newpage
\begin{tcolorbox}[colframe=black, colback=blue!10, boxrule=2pt, arc=6pt]
	\centering{}\LARGE{\textbf{Histoire des mathématiques \faBook}}
\end{tcolorbox}
\begin{tcolorbox}[colframe=black!30, colback=blue!0, boxrule=2pt, arc=6pt, title=\textbf{Les droites}, coltitle=black]
\subsection{Introduction}
Revenons au temps d'Euclide (vers $-300$). Dans le chapitre $2$, nous avions vu comment Euclide avait défini le segment (postulat $1$) et le cercle (postulat $3$). Voici l'intitulé du \textbf{postulat $2$} : \guillemetleft Un segment de droite peut être prolongé indéfiniment en une ligne droite. \guillemetright.\\
Ce postulat définit tout simplement les caractéristiques d'une droite (illimitée, sans milieu ni longueur).

\subsection{Droites remarquables d'un triangle}
Un triangle possède quatre droites particulières : les hauteurs, les médiatrices, les médianes et les bissectrices. Vous définirez les caractéristiques de chacune de ces droites au cours de l'année de 5\ieme.
\begin{center}
\begin{Geometrie}[CoinHD={u*(6.5,6.5)}]
		pair A,B,C,H,I;
		A=u*(1,3);
		B=u*(4,2);
		C=u*(6,5);
		H=Orthocentre(A,B,C);
		I=projection(B,A,C);
		fill A--B--C--cycle withcolor 0.8 white;
		trace segment(A,B);
		trace segment(B,C);
		trace segment(A,C);
		trace perpendiculaire(A,B,C) dashed evenly;
		trace perpendiculaire(B,C,A) dashed evenly;
		trace perpendiculaire(A,C,B) dashed evenly;
		trace codeperp(B,I,C,5);
		label.top(btex A etex, A);
		label.lrt(btex B etex, B);
		label.ulft(btex C etex, C);
		label.rt(btex H etex, H);
	\end{Geometrie} 
	\begin{Geometrie}[CoinHD={u*(7.5,7.5)}]
		pair A,B,C,H,I;
		A=u*(1,3);
		B=u*(4,2);
		C=u*(6,5);
		H=CentreCercleC(A,B,C);
		I=milieu(A,B);
		trace cercles(H,A);
		fill A--B--C--cycle withcolor 0.8 white;
		marque_s:=marque_s/3;
		trace segment(A,B);
		trace segment(B,C);
		trace segment(A,C);
		trace mediatrice(A,B) dashed evenly;
		trace mediatrice(A,C) dashed evenly;
		trace mediatrice(B,C) dashed evenly;
		trace codeperp(B,I,H,5);
		trace Codelongueur(A,I,I,B,2);
		label.lft(btex A etex, A);
		label.lrt(btex B etex, B);
		label.urt(btex C etex, C);
		label.rt(btex $O_1$ etex, H);
	\end{Geometrie} \\
	\begin{Geometrie}[CoinHD={u*(7.5,7.5)}]
		pair A,B,C,H,I;
		A=u*(1,3);
		B=u*(4,2);
		C=u*(6,5);
		H=CentreCercleI(A,B,C);
		I =projection(H,A,B);
		fill A--B--C--cycle withcolor 0.8 white;
		marque_s:=marque_s/3;
		trace segment(A,B);
		trace segment(B,C);
		trace segment(A,C);
		trace bissectrice(A,B,C) dashed evenly;
		trace bissectrice(B,C,A) dashed evenly;
		trace bissectrice(C,A,B) dashed evenly;
		trace cercles(H,I);
		trace Codeangle(H,A,C,2,btex \color{black!20}. etex);
		trace Codeangle(B,A,H,2,btex \color{black!20}. etex);
		label.lft(btex A etex, A);
		label.lrt(btex B etex, B);
		label.urt(btex C etex, C);
		label.top(btex $O_2$ etex, H);
	\end{Geometrie} 
	\begin{Geometrie}[CoinHD={u*(7.5,7.5)}]
		pair A,B,C,G,I,J,K;
		A=u*(1,3);
		B=u*(4,2);
		C=u*(6,5);
		G=iso(A,B,C);
		I =milieu(A,B);
		J=milieu(A,C);
		K=milieu(B,C);
		fill A--B--C--cycle withcolor 0.8 white;
		marque_s:=marque_s/3;
		trace segment(A,B);
		trace segment(B,C);
		trace segment(A,C);
		trace droite(A,K) dashed evenly;
		trace droite(B,J) dashed evenly;
		trace droite(C,I) dashed evenly;
		label.ulft(btex A etex, A);
		label.rt(btex B etex, B);
		label.top(btex C etex, C);
		label.ulft(btex G etex, G);
	\end{Geometrie} 
\end{center}
\end{tcolorbox}
\newpage

\color{black}
\begin{tcolorbox}[colframe=black!30, colback=blue!0, boxrule=2pt, arc=6pt, title=\textbf{Les droites}, coltitle=black]
\subsection{La droite d'Euler}
La droite d'Euler est une droite remarquable d'un triangle quelconque non équilatéral car elle passe par le centre du cercle circonscrit $O_1$, l'orthocentre $H$ et le centre de gravité $G$.
\begin{center}
	\begin{Geometrie}[CoinHD={u*(7.5,7.5)}]
		pair A,B,C,H,G,O,P;
		A=u*(1,3);
		B=u*(4,2);
		C=u*(6,5);
		G=iso(A,B,C);
		O=CentreCercleI(A,B,C);
		P=CentreCercleC(A,B,C);
		H=Orthocentre(A,B,C);
		fill A--B--C--cycle withcolor 0.8 white;
		marque_s:=marque_s/3;
		trace segment(A,B);
		trace segment(B,C);
		trace segment(A,C);
		trace droite(G,H) dashed evenly;
		marque_p:="croix";
		pointe(G,H,O,P);
		label.ulft(btex A etex, A);
		label.llft(btex B etex, B);
		label.top(btex C etex, C);
		label.urt(btex G etex, G);
		label.urt(btex H etex, H);
		label.urt(btex $O_1$ etex, P);
		label.lft(btex $O_2$ etex, O);
	\end{Geometrie} 
\end{center}

\subsection{Autres droites}
Pour ceux que cela intéresse, vous pouvez également vous documenter sur les droites de Pascal ou de Simson.
\end{tcolorbox}

\textbf{\underline{Sources}}
\begin{itemize}[label=\textbullet]
	\item Hauchecorne, B. et Suratteau, D. (2008), \textit{Des Mathématiciens de A à Z} ($3^e$ édition), Ellipses
	\item Byrne, O. (2022), \textit{The first six books of the elements of Euclid}, Taschen
	\item Houlou-Garcia, A. (2019), \textit{Mathematikos} (2\ieme\, tirage), Belles lettres
	\item Barbin, E. (2019), \textit{Faire des mathématiques avec l'histoire au lycée}, Ellipses
	\item Dahan-Dalmedico, A. et Peiffer, J. (1986), \textit{Une histoire des mathématiques : Routes et dédales}, Points
	\item Freiberger, M. et Thomas, R. (2018), \textit{Dans le secret des nombres}, Ekho
\end{itemize}



%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%-------------------------------------------------------------------------- Fiche de route ------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\newpage
\begin{tcolorbox}[colframe=black, colback=Khaki!50, boxrule=2pt, arc=6pt]
	\centering{}\LARGE{\textbf{Fiche de route}}
\end{tcolorbox}
\begin{itemize}[label=\textbullet]
	\item \color{Tomato} \textbf{Introduction} \color{black} : Titre, prérequis, objectifs, rappels
	\item \color{RoyalBlue}\underline{Leçon} \color{black} : Partie I : 
	\item \color{ForestGreen}\underline{Exercices}  \color{black} : 
	\item \color{RoyalBlue}\underline{Leçon}  \color{black} : Parties IV et V : Bilan + Auto-évaluation
	\item \color{ForestGreen}\underline{Exercices bilan}  \color{black} : 
	\item \color{Coral} \underline{Histoire des mathématiques}
\end{itemize}

%\end{comment}
%\setcounter{page}{4}
\end{document}
