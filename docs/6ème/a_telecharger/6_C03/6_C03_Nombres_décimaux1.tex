\documentclass[dvipsnames, a4paper, 12pt]{report}

\usepackage{lmodern} %police standard latin moderne
\usepackage[frenchb]{babel} %langue française
\usepackage[utf8]{inputenc} %pour l'utf-8
\usepackage[T1]{fontenc} %pour les césures et accentuations
\usepackage{geometry} %pour changer les marges d'un document
\usepackage{graphicx}
\usepackage{fancybox}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{fontawesome5}
\usepackage{ProfCollege}
\usepackage{luacode}
\usepackage{xlop}
\usepackage{cancel}
\usepackage{pdflscape}

\usepackage[explicit]{titlesec} %Permet de modifier les différentes parties, chapitres, sous-parties,...

\usepackage{colortbl} %permet de mettre des cellules de tableau en couleur

\usepackage{multicol}

\usepackage{cellspace} %pour définir les espaces en haut et en bas des cellules d'un tableau
\setlength{\cellspacebottomlimit}{5pt}
\setlength{\cellspacetoplimit}{5pt}
\setlength{\parindent}{0pt}

\usepackage{xcolor} %permet de mettre du texte en couleur

\usepackage{array} %gestion des tableaux
\usepackage{tcolorbox} %créer des boîtes de couleurs

\usepackage{hyperref} %permet de gérer le sommaire et la page de garde et les options du PDF

\geometry{top=2cm, bottom=2cm, right=2cm, left=2cm}   
\titlespacing{\chapter}{0pt}{0pt}{5pt}

\setcounter{chapter}{3}

\usepackage{lastpage}
\usepackage{fancyhdr} %définition des en-têtes et des bas de page
\fancyhf{}
\fancypagestyle{main}{
	\renewcommand\headrulewidth{5pt}
	\fancyhead[L]{6\ieme\, - Chapitre \arabic{chapter} - Nombres décimaux (1)}
	\fancyhead[R]{Collège}
	\fancyfoot[C]{\thepage/\pageref{LastPage}}
	\fancyfoot[R]{\tiny{Anthony Salze}}
	}
\pagestyle{main}

%gestion des entêtes
\makeatletter\@addtoreset{section}{chapter}\makeatother % RAZ des numéros de section après un chapitre
\renewcommand{\thepart}{\Alph{part} -} % Pour mettre des I, II, etc. aux parties
\renewcommand{\thesection}{\color{Green} \Roman{section} -}% Idem pour les sections et avoir le numéro de chapitre
\renewcommand{\thesubsection}{\arabic{subsection} -}

\begin{document}
%format du titre des chapitres placé après Sommaire sinon cela bugue
\titleformat{\chapter}[frame] {\normalfont \bfseries \Huge}{\textcolor{red} {\chaptertitlename\ \thechapter\,-\,#1}}{0pt}{\Huge}

\setcounter{chapter}{2}

\chapter{Les nombres décimaux 1}
\begin{luacode}
	d=1
	p=1
	t=1
	m=1
\end{luacode}

{\color{blue}\underline{\textbf{Prérequis}}
\begin{itemize}
	\item Connaître l'écriture en lettres des différents nombres.
	\item Connaître la définition d'un nombre entier.
\end{itemize}
~~\\
\underline{\textbf{Objectifs}}
\begin{itemize}
	\item Découvrir les nombres décimaux.
	\item \'Ecrire un nombre décimal sous différentes formes.
	\item Connaître le vocabulaire associé aux nombres entiers et décimaux.
\end{itemize}}~~
\\

\textbf{\underline{Rappels}} : QCM (plusieurs réponses possibles)\\\footnotesize{}
\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom= , AlphT, Largeur=3cm]{%
Le nombre \num{4000300} se lit \ldots & quatre-mille-trois cents. & quatre-millions-trois-cents. & quatre-milliards-trois-cents. & 2,%
Une autre écriture du nombre \num{7.62} est \dots & $\dfrac{762}{100}$ & $7 + \dfrac{62}{100}$ & $ 7 + \dfrac{6}{10} + \dfrac{2}{100}$ &1,%2,3
Le nombre \num{2.81} peut se lire \ldots & 2 unités et 81 dixièmes. & 2 unités et 81 centièmes. & 2 unités$\text{,}$ 8 dixièmes et 1 centième. &2,%3
La partie décimale du nombre \num{15.24} est \ldots & \num{15} & \num{24} & \num{0.24} &3,%
Il est exact que \ldots & $\num{5.8} > \num{5.23}$ & $\num{4.6} = \num{4.60}$ & $\num{9.9} < \num{9.85}$ &1%2
}
\\
\\
\normalsize{}

\color{Green}
\section{Définition}
\color{black}
\normalsize{}
\begin{tcolorbox}[colframe=Green!20, colback=Green!0, boxrule=2pt, arc=6pt, title={\textbf{Définition \arabic{chapter}.\directlua{tex.print(d)}}}, coltitle=black]
Un nombre décimal est un nombre dont la partie décimale est finie.\\
Pour distinguer la partie entière d'un nombre du reste, on place une virgule entre le chiffre des unités et celui des dixièmes.
\end{tcolorbox}
\begin{luacode}
	d=d+1
\end{luacode}

\textbf{\underline{Exemples}}
\begin{itemize}[label=\textbullet]
	\item \num{34.67} est un nombre décimal. Sa partie entière est 34 et sa partie décimale est \num{0.67}.
	\item \num{1.3333}\ldots n'est pas un nombre décimal car sa partie décimale n'est pas finie.
\end{itemize}
~~\\
\textbf{\underline{Remarques}}
\begin{itemize}[label=\textbullet]
	\item Un nombre entier est aussi un nombre décimal. Sa partie décimale est bien finie mais égale à 0. Par exemple, le nombre 4 peut aussi s'écrire \num{4.0}.
	\item Certains $0$ sont inutiles car ils ne changent pas la valeur du nombre.\\
	Par exemple, dans le nombre $001\,407,50$ : on peut rayer les deux premiers zéros et le dernier mais pas celui entre les chiffres $4$ et $7$ \faLongArrowAltRight\,  $\cancel{0}\cancel{0}1\,407,5\cancel{0} = \num{1407,5}$.
\end{itemize}

\color{Green}
\section{Fractions décimales}
\color{black}
\begin{tcolorbox}[colframe=Green!20, colback=Green!0, boxrule=2pt, arc=6pt, title={\textbf{Définition \arabic{chapter}.\directlua{tex.print(d)}}}, coltitle=black]
$a$ et $b$ sont deux nombres entiers et $b \ne 0$.\\
Une fraction est une façon d'écrire un nombre.\\
Elle s'écrit sous cette forme : $\dfrac{a}{b} = \dfrac{\text{\faCloud\,\,\,\,\,\, (nuage)}}{\text{\includegraphics[width=0.7cm]{Figure1c.png} (démon)}} =  \dfrac{\color{black}{\text{numérateur}}}{\color{black}{\text{dénominateur}}}$\\
\faExclamationTriangle\, Le dénominateur ne peut jamais être égal à $0$ !!
\end{tcolorbox}
\begin{luacode}
	d=d+1
\end{luacode}

\begin{tcolorbox}[colframe=Green!20, colback=Green!0, boxrule=2pt, arc=6pt, title={\textbf{Définition \arabic{chapter}.\directlua{tex.print(d)}}}, coltitle=black]
Une fraction décimale est une fraction dont le dénominateur est $10$, $100$, $\num{1000}$, \ldots
\end{tcolorbox}
\begin{luacode}
	d=d+1
\end{luacode}

\textbf{\underline{Exemples}}
\begin{itemize}[label=\textbullet]
	\item $\dfrac{38}{10}$ est la fraction décimale du nombre $\num{3.8}$.
	\item $\dfrac{41}{100}$ est la fraction décimale du nombre $\num{0.41}$.
	\item Le nombre $45,67$ peut donc s'écrire $\dfrac{\num{4567}}{100}$.
\end{itemize}
~~
\begin{tcolorbox}[colframe=blue!20, colback=blue!0, boxrule=2pt, arc=6pt, title={\textbf{Propriété \arabic{chapter}.\directlua{tex.print(p)}}}, coltitle=black]
Un nombre décimal peut toujours s'écrire sous la forme d'une fraction décimale.
\end{tcolorbox}
\begin{luacode}
	p=p+1
\end{luacode}

\textbf{\underline{Exemples}}
\begin{multicols}{5}
	$\dfrac{1}{1\color{red}{0}}=0,\color{red}1$\\
	$\dfrac{48}{1\color{red}{00}} = 0,\color{red}{48}$\\
	$34 = \dfrac{34}{1} = \dfrac{340}{10}$\\
	$5,\color{red}{24}\color{black} = \dfrac{524}{1\color{red}{00}}$\\
	$38,\color{red}{756}\color{black} = \dfrac{38\,756}{1\,\color{red}{000}}$	
\end{multicols}


\color{Green}
\section{Rang d'un chiffre}
\color{black}
\Tableau[Incline,CouleurG=blue!15,CouleurM=green!15,Couleurm=red!15,Couleuru=LightPink,Couleurd=LightGoldenrod]{}

Nous pouvons écrire un nombre avec plusieurs écritures différentes. Prenons, \num{7452.813} :
\begin{itemize}[label=\textbullet]
	\item $\num{7452.813} = 7 \times 1\,000 + 4 \times 100 + 5 \times 10 + 2 \times 1 + 8 \times 0,1 + 1 \times 0,01 + 3 \times 0,001$
	\item $\num{7452.813} =\num{7000} + 400 + 50 + 2 + 0,8 + 0,01 + 0,003$
	\item $\num{7452.813} = \num{7452} + 0,813$
	\item $\num{7452.813} = \num{7452} + \dfrac{813}{1\,000}$
	\item $\num{7452.813} = \num{7452} + \dfrac{8}{10} + \dfrac{1}{100} + \dfrac{3}{\num{1000}}$
	\item $\num{7452.813} = 7 \times 1\,000 + 4 \times 100 + 5 \times 10 + 2 \times 1 + \dfrac{8}{10} + \dfrac{1}{100} + \dfrac{3}{\num{1000}}$
\end{itemize}


\begin{tcolorbox}[colframe=Gray!20, colback=Gray!0, boxrule=2pt, arc=6pt, title={\textbf{Méthode \arabic{chapter}.\directlua{tex.print(m)}}}, coltitle=black]
Pour trouver le nombre de centièmes dans le nombre \textbf{$\num{148,7658}$}, il faut :
\begin{itemize}[label=\textbullet]
	\item repérer le chiffre des centièmes : ici $6$ ;
	\item puis constituer un nombre en prenant tous les chiffres avant celui-ci : $\num{14876}$ ;
	\item \underline{Conclusion} : Dans le nombre $\num{148,7658}$, il y a $\num{14876}$ centièmes.
\end{itemize}
\end{tcolorbox}
\begin{luacode}
	m=m+1
\end{luacode}

\textbf{\underline{Exemples}}\\
Donner le nombre de dizaines et le nombre de dixièmes dans le nombre $\num{147895,2548899}$.\\
Dans $\num{147895,2548899}$, il y a $\num{14789}$ dizaines et $\num{1478952}$ dixièmes.


\color{Green}
\section{Bilan}
\color{black}
QCM (plusieurs réponses possibles)\\\footnotesize{}
\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom= , AlphT, Largeur=3cm]{%
$\dfrac{900}{\num{1000}}$ est égal à \ldots & $\dfrac{90}{100}$ & $\dfrac{90}{10}$ & $\dfrac{9}{100}$ &1,%
$ 8 + \dfrac{7}{10} + \dfrac{5}{\num{1000}}$ est égal à \ldots & $8 + \dfrac{75}{\num{1000}}$ & $\dfrac{\num{8705}}{\num{1000}}$ & $ 8 + \frac{705}{\num{1000}}$ &2,%3
Le nombre $\dfrac{504}{10}$ peut s'écrire \ldots & $\num{504.10}$ & $50 + \dfrac{4}{10}$ & $\num{50.4}$ &2,%3
La partie décimale du nombre \num{14.632} est \ldots & $\dfrac{632}{\num{1000}}$ & $632$ & $\num{0.632}$ &1,%3
Le chiffre des centièmes dans le nombre \num{1254.789} est \ldots & $2$ & $7$ & $8$ &3,%
Le nombre de centièmes dans le nombre \num{1254.789} est \ldots & $8$ & \num{1254789} & \num{125478}& 3
}
\normalsize{}


\color{Green}
\section{Auto-évaluation}
\color{black}

\begin{tabular}{|>{\columncolor{darkgray}\color{black}}m{8cm} |>{\color{black}}m{0.6cm}|m{0.6cm}|m{0.6cm}|m{5cm}|} \hline

\rowcolor{Lavender}\centering{} \color{black} Compétences & \centering{}\color{MediumSeaGreen} \faCheck & \centering{}\color{DarkOrange}\faWater & \centering{}\color{Crimson}\faTimes & \centering{}\color{black}Pour réviser \tabularnewline \hline \hline

\cellcolor{Lavender}Je sais différencier la partie entière et la partie décimale d'un nombre décimal. & & & & \textbf{\underline{Leçon}} : Partie I \smallskip \newline  \textbf{\underline{Exercice}} : 1 \tabularnewline \hline \hline

\cellcolor{Lavender}Je suis capable d'identifier les zéros inutiles d'un nombre décimal. & & & & \textbf{\underline{Leçon}} : Partie I \smallskip \newline  \textbf{\underline{Exercice}} : 2 \tabularnewline \hline \hline

\cellcolor{Lavender}Je suis capable d'écrire un nombre décimal en utilisant les fractions décimales. & & & & \textbf{\underline{Leçon}} : Partie II \smallskip \newline  \textbf{\underline{Exercices}} : 3 à 9 \tabularnewline \hline \hline

\cellcolor{Lavender}Je suis capable d'identifier et de nommer le rang d'un chiffre dans un nombre décimal. & & & & \textbf{\underline{Leçon}} : Partie III \smallskip \newline  \textbf{\underline{Exercices}} : 10 à 15 \tabularnewline \hline 
\end{tabular}


%\begin{comment}
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------- Correction QCM -----------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\newpage
\setcounter{page}{1}
\footnotesize{}
\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom= , AlphT, Largeur=3cm, Solution, Couleur=cyan!20]{%
Le nombre \num{4000300} se lit \ldots & quatre-mille-trois cents. & quatre-millions-trois-cents. & quatre-milliards-trois-cents. & 2,%
Une autre écriture du nombre \num{7.62} est \dots & $\dfrac{762}{100}$ & \cellcolor{cyan!20} $7 + \dfrac{62}{100}$ & \cellcolor{cyan!20} $ 7 + \dfrac{6}{10} + \dfrac{2}{100}$ &1,%
Le nombre \num{2.81} peut se lire \ldots & 2 unités et 81 dixièmes. & 2 unités et 81 centièmes. & \cellcolor{cyan!20} 2 unités$\text{,}$ 8 dixièmes et 1 centième. &2,%3
La partie décimale du nombre \num{15.24} est \ldots & \num{15} & \num{24} & \num{0.24} &3,%
Il est exact que \ldots & $\num{5.8} > \num{5.23}$ & \cellcolor{cyan!20}$\num{4.6} = \num{4.60}$ & $\num{9.9} < \num{9.85}$ &1%2
}
\\
\\
\\
\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom= , AlphT, Largeur=3cm, Solution, Couleur=cyan!20]{%
$\dfrac{900}{\num{1000}}$ est égal à \ldots & $\dfrac{90}{100}$ & $\dfrac{90}{10}$ & $\dfrac{9}{100}$ &1,%
$ 8 + \dfrac{7}{10} + \dfrac{5}{\num{1000}}$ est égal à \ldots & $8 + \dfrac{75}{\num{1000}}$ & $\dfrac{\num{8705}}{\num{1000}}$ & \cellcolor{cyan!20} $ 8 + \frac{705}{\num{1000}}$ &2,%3
Le nombre $\dfrac{504}{10}$ peut s'écrire \ldots & $\num{504.10}$ & $50 + \dfrac{4}{10}$ & \cellcolor{cyan!20} $\num{50.4}$ &2,%3
La partie décimale du nombre \num{14.632} est \ldots & $\dfrac{632}{\num{1000}}$ & $632$ & \cellcolor{cyan!20}$\num{0.632}$ &1,%3
Le chiffre des centièmes dans le nombre \num{1254.789} est \ldots & $2$ & $7$ & $8$ &3,%
Le nombre de centièmes dans le nombre \num{1254.789} est \ldots & $8$ & \num{1254789} & \num{125478}& 3
}


%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%---------------------------------------------------------------------- Leçon à trous -----------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\newpage
%\setcounter{page}{1}
\titleformat{\chapter}[frame] {\normalfont \bfseries \Huge}{\textcolor{red} {\chaptertitlename\ \thechapter\,-\,#1}}{0pt}{\Huge}

\setcounter{chapter}{2}

\chapter{\pointilles[10cm]}
\begin{luacode}
	d=1
	p=1
	t=1
	m=1
\end{luacode}

{\color{blue}\underline{\textbf{Prérequis}}
\begin{itemize}
	\item Connaître \pointilles[7cm] des différents nombres.
	\item Connaître la définition d'un\pointilles[7cm]
\end{itemize}
~~\\
\underline{\textbf{Objectifs}}
\begin{itemize}
	\item Découvrir les \pointilles[7cm].
	\item \'Ecrire un \pointilles[7cm] sous différentes formes.
	\item Connaître le vocabulaire associé aux \pointilles[10cm].
\end{itemize}}~~
\\

\textbf{\underline{Rappels}} : QCM (plusieurs réponses possibles)\\\footnotesize{}
\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom= , AlphT, Largeur=3cm]{%
Le nombre \num{4000300} se lit \ldots & quatre-mille-trois cents. & quatre-millions-trois-cents. & quatre-milliards-trois-cents. & 2,%
Une autre écriture du nombre \num{7.62} est \dots & $\dfrac{762}{100}$ & $7 + \dfrac{62}{100}$ & $ 7 + \dfrac{6}{10} + \dfrac{2}{100}$ &1,%2,3
Le nombre \num{2.81} peut se lire \ldots & 2 unités et 81 dixièmes. & 2 unités et 81 centièmes. & 2 unités$\text{,}$ 8 dixièmes et 1 centième. &2,%3
La partie décimale du nombre \num{15.24} est \ldots & \num{15} & \num{24} & \num{0.24} &3,%
Il est exact que \ldots & $\num{5.8} > \num{5.23}$ & $\num{4.6} = \num{4.60}$ & $\num{9.9} < \num{9.85}$ &1%2
}
\\
\\
\normalsize{}

\color{Green}
\section{Définition}
\color{black}
\normalsize{}
\begin{tcolorbox}[colframe=Green!20, colback=Green!0, boxrule=2pt, arc=6pt, title={\textbf{Définition \arabic{chapter}.\directlua{tex.print(d)}}}, coltitle=black]
Un \pointilles[7cm] est un nombre dont la partie \pointilles[10cm].\\
Pour distinguer la \pointilles[7cm] d'un nombre du reste, on place une \pointilles[5cm] entre le chiffre des \pointilles[10cm].
\end{tcolorbox}
\begin{luacode}
	d=d+1
\end{luacode}

\textbf{\underline{Exemples}}
\begin{itemize}[label=\textbullet]
	\item \num{34.67} est un nombre \pointilles[5cm]. Sa \pointilles[6cm] est 34 et sa partie décimale est \pointilles[2cm].
	\item \num{1.3333}\ldots n'est pas un \pointilles[10cm] car sa partie décimale \pointilles[7cm].
\end{itemize}
~~\\
\textbf{\underline{Remarques}}
\begin{itemize}[label=\textbullet]
	\item Un \pointilles[7cm] est aussi un \pointilles[6cm]. Sa partie décimale est bien \pointilles[3cm] mais égale à \pointilles[1cm]. Par exemple, le nombre 4 peut aussi s'écrire \pointilles[2cm].
	\item Certains \pointilles[1cm] sont \pointilles[3cm] car ils ne changent pas la \pointilles[5cm].\\
	Par exemple, dans le nombre $001\,407,50$ : on peut rayer les \pointilles[6cm] et le \pointilles[3cm] mais pas celui entre les \pointilles[7cm]\\ \faLongArrowAltRight\, \pointilles[7cm].
\end{itemize}

\color{Green}
\section{\pointilles[10cm]}
\color{black}
\begin{tcolorbox}[colframe=Green!20, colback=Green!0, boxrule=2pt, arc=6pt, title={\textbf{Définition \arabic{chapter}.\directlua{tex.print(d)}}}, coltitle=black]
$a$ et $b$ sont \pointilles[7cm] et \pointilles[2cm].\\
Une \pointilles[3cm] est une façon d'écrire un nombre.\\
Elle s'écrit sous cette forme : $\pointilles[2cm] = \dfrac{\text{\faCloud\,\,\,\,\,\, (nuage)}}{\text{\includegraphics[width=0.7cm]{Figure1c.png} (démon)}} =  \dfrac{\color{black}{\text{\pointilles[3cm]}}}{\color{black}{\text{\pointilles[3cm]}}}$\\
\faExclamationTriangle\, Le \pointilles[3cm] ne peut jamais être égal à \pointilles[2cm] !!
\end{tcolorbox}
\begin{luacode}
	d=d+1
\end{luacode}

\begin{tcolorbox}[colframe=Green!20, colback=Green!0, boxrule=2pt, arc=6pt, title={\textbf{Définition \arabic{chapter}.\directlua{tex.print(d)}}}, coltitle=black]
Une \pointilles[7cm] est une fraction dont le \pointilles[10cm].
\end{tcolorbox}
\begin{luacode}
	d=d+1
\end{luacode}

\textbf{\underline{Exemples}}
\begin{itemize}[label=\textbullet]
	\item $\dfrac{38}{10}$ est la fraction décimale du nombre \pointilles[3cm].
	\item \pointilles[2cm] est la fraction décimale du nombre $\num{0.41}$.
	\item Le nombre $45,67$ peut donc s'écrire \pointilles[3cm].
\end{itemize}
~~
\begin{tcolorbox}[colframe=blue!20, colback=blue!0, boxrule=2pt, arc=6pt, title={\textbf{Propriété \arabic{chapter}.\directlua{tex.print(p)}}}, coltitle=black]
Un \pointilles[7cm] peut \pointilles[5cm] s'écrire sous la forme d'une \pointilles[7cm].
\end{tcolorbox}
\begin{luacode}
	p=p+1
\end{luacode}

\textbf{\underline{Exemples}}\\
\begin{minipage}{0.33\textwidth}
	$\dfrac{1}{1\color{red}{0}}=\pointilles[1cm]$
\end{minipage}
\begin{minipage}{0.33\textwidth}
	$\pointilles[2cm] = 0,\color{red}{48}$
\end{minipage}
\begin{minipage}{0.33\textwidth}
	$34 = \dfrac{34}{1} = \dfrac{340}{10}$
\end{minipage}
~~\\
\begin{minipage}{0.33\textwidth}
	$5,\color{red}{24}\color{black} = \pointilles[2cm]$
\end{minipage}
\begin{minipage}{0.33\textwidth}
	$\pointilles[2cm] = \dfrac{38\,756}{1\,\color{red}{000}}$	
\end{minipage}


\color{Green}
\section{Rang d'un chiffre}
\color{black}
\Tableau[Incline,CouleurG=blue!15,CouleurM=green!15,Couleurm=red!15,Couleuru=LightPink,Couleurd=LightGoldenrod]{}

Nous pouvons écrire un nombre avec plusieurs écritures différentes. Prenons, \num{7452.813} :
\begin{itemize}[label=\textbullet]
	\item $\num{7452.813} =\pointilles[14cm]$
	\item $\num{7452.813} =\pointilles[14cm]$
	\item $\num{7452.813} = \pointilles[14cm]$\\
	\item $\num{7452.813} = \pointilles[14cm]$\\
	\item $\num{7452.813} = \pointilles[14cm]$\\
	\item $\num{7452.813} = \pointilles[14cm]$
\end{itemize}
~~
\begin{tcolorbox}[colframe=Gray!20, colback=Gray!0, boxrule=2pt, arc=6pt, title={\textbf{Méthode \arabic{chapter}.\directlua{tex.print(m)}}}, coltitle=black]
Pour trouver \pointilles[7cm] dans le nombre \textbf{$\num{148,7658}$}, il faut :
\begin{itemize}[label=\textbullet]
	\item \pointilles[15cm] ;
	\item \pointilles[15cm] ;
	\item \pointilles[15cm].
\end{itemize}
\end{tcolorbox}
\begin{luacode}
	m=m+1
\end{luacode}

\textbf{\underline{Exemple}}\\
Donner le nombre de dizaines et le nombre de dixièmes dans le nombre $\num{147895,2548899}$.\\
\pointilles[17cm]\\
\pointilles[17cm]

\color{Green}
\section{Bilan}
\color{black}
QCM (plusieurs réponses possibles)\\\footnotesize{}
\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom= , AlphT, Largeur=3cm]{%
$\dfrac{900}{\num{1000}}$ est égal à \ldots & $\dfrac{90}{100}$ & $\dfrac{90}{10}$ & $\dfrac{9}{100}$ &1,%
$ 8 + \dfrac{7}{10} + \dfrac{5}{\num{1000}}$ est égal à \ldots & $8 + \dfrac{75}{\num{1000}}$ & $\dfrac{\num{8705}}{\num{1000}}$ & $ 8 + \frac{705}{\num{1000}}$ &2,%3
Le nombre $\dfrac{504}{10}$ peut s'écrire \ldots & $\num{504.10}$ & $50 + \dfrac{4}{10}$ & $\num{50.4}$ &2,%3
La partie décimale du nombre \num{14.632} est \ldots & $\dfrac{632}{\num{1000}}$ & $632$ & $\num{0.632}$ &1,%3
Le chiffre des centièmes dans le nombre \num{1254.789} est \ldots & $2$ & $7$ & $8$ &3,%
Le nombre de centièmes dans le nombre \num{1254.789} est \ldots & $8$ & \num{1254789} & \num{125478}& 3
}
\normalsize{}


\color{Green}
\section{Auto-évaluation}
\color{black}


\begin{tabular}{|>{\columncolor{darkgray}\color{black}}m{8cm} |>{\color{black}}m{0.6cm}|m{0.6cm}|m{0.6cm}|m{5cm}|} \hline

\rowcolor{Lavender}\centering{} \color{black} Compétences & \centering{}\color{MediumSeaGreen} \faCheck & \centering{}\color{DarkOrange}\faWater & \centering{}\color{Crimson}\faTimes & \centering{}\color{black}Pour réviser \tabularnewline \hline \hline

\cellcolor{Lavender}Je sais différencier la partie entière et la partie décimale d'un nombre décimal. & & & & \textbf{\underline{Leçon}} : Partie I \smallskip \newline  \textbf{\underline{Exercice}} : 1 \tabularnewline \hline \hline

\cellcolor{Lavender}Je suis capable d'identifier les zéros inutiles d'un nombre décimal. & & & & \textbf{\underline{Leçon}} : Partie I \smallskip \newline  \textbf{\underline{Exercice}} : 2 \tabularnewline \hline \hline

\cellcolor{Lavender}Je suis capable d'écrire un nombre décimal en utilisant les fractions décimales. & & & & \textbf{\underline{Leçon}} : Partie II \smallskip \newline  \textbf{\underline{Exercices}} : 3 à 9 \tabularnewline \hline \hline

\cellcolor{Lavender}Je suis capable d'identifier et de nommer le rang d'un chiffre dans un nombre décimal. & & & & \textbf{\underline{Leçon}} : Partie III \smallskip \newline  \textbf{\underline{Exercices}} : 10 à 15 \tabularnewline \hline 
\end{tabular}


%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------- Impression -----------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\newpage
\textbf{\underline{Rappels}} : QCM (plusieurs réponses possibles)\\\footnotesize{}
\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom= , AlphT, Largeur=3cm]{%
Le nombre \num{4000300} se lit \ldots & quatre-mille-trois cents. & quatre-millions-trois-cents. & quatre-milliards-trois-cents. & 2,%
Une autre écriture du nombre \num{7.62} est \dots & $\dfrac{762}{100}$ & $7 + \dfrac{62}{100}$ & $ 7 + \dfrac{6}{10} + \dfrac{2}{100}$ &1,%2,3
Le nombre \num{2.81} peut se lire \ldots & 2 unités et 81 dixièmes. & 2 unités et 81 centièmes. & 2 unités$\text{,}$ 8 dixièmes et 1 centième. &2,%3
La partie décimale du nombre \num{15.24} est \ldots & \num{15} & \num{24} & \num{0.24} &3,%
Il est exact que \ldots & $\num{5.8} > \num{5.23}$ & $\num{4.6} = \num{4.60}$ & $\num{9.9} < \num{9.85}$ &1%2
}
\\
\\
\normalsize{}

QCM (plusieurs réponses possibles)\\\footnotesize{}
\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom= , AlphT, Largeur=3cm]{%
$\dfrac{900}{\num{1000}}$ est égal à \ldots & $\dfrac{90}{100}$ & $\dfrac{90}{10}$ & $\dfrac{9}{100}$ &1,%
$ 8 + \dfrac{7}{10} + \dfrac{5}{\num{1000}}$ est égal à \ldots & $8 + \dfrac{75}{\num{1000}}$ & $\dfrac{\num{8705}}{\num{1000}}$ & $ 8 + \frac{705}{\num{1000}}$ &2,%3
Le nombre $\dfrac{504}{10}$ peut s'écrire \ldots & $\num{504.10}$ & $50 + \dfrac{4}{10}$ & $\num{50.4}$ &2,%3
La partie décimale du nombre \num{14.632} est \ldots & $\dfrac{632}{\num{1000}}$ & $632$ & $\num{0.632}$ &1,%3
Le chiffre des centièmes dans le nombre \num{1254.789} est \ldots & $2$ & $7$ & $8$ &3,%
Le nombre de centièmes dans le nombre \num{1254.789} est \ldots & $8$ & \num{1254789} & \num{125478}& 3
}
\normalsize{}
\\
\\
\\

\begin{tabular}{|>{\columncolor{darkgray}\color{black}}m{8cm} |>{\color{black}}m{0.6cm}|m{0.6cm}|m{0.6cm}|m{5cm}|} \hline

\rowcolor{Lavender}\centering{} \color{black} Compétences & \centering{}\color{MediumSeaGreen} \faCheck & \centering{}\color{DarkOrange}\faWater & \centering{}\color{Crimson}\faTimes & \centering{}\color{black}Pour réviser \tabularnewline \hline \hline

\cellcolor{Lavender}Je sais différencier la partie entière et la partie décimale d'un nombre décimal. & & & & \textbf{\underline{Leçon}} : Partie I \smallskip \newline  \textbf{\underline{Exercice}} : 1 \tabularnewline \hline \hline

\cellcolor{Lavender}Je suis capable d'identifier les zéros inutiles d'un nombre décimal. & & & & \textbf{\underline{Leçon}} : Partie I \smallskip \newline  \textbf{\underline{Exercice}} : 2 \tabularnewline \hline \hline

\cellcolor{Lavender}Je suis capable d'écrire un nombre décimal en utilisant les fractions décimales. & & & & \textbf{\underline{Leçon}} : Partie II \smallskip \newline  \textbf{\underline{Exercices}} : 3 à 9 \tabularnewline \hline \hline

\cellcolor{Lavender}Je suis capable d'identifier et de nommer le rang d'un chiffre dans un nombre décimal. & & & & \textbf{\underline{Leçon}} : Partie III \smallskip \newline  \textbf{\underline{Exercices}} : 10 à 15 \tabularnewline \hline 
\end{tabular}
~~\\
\\
\\
\Tableau[Incline,CouleurG=blue!15,CouleurM=green!15,Couleurm=red!15,Couleuru=LightPink,Couleurd=LightGoldenrod, NbLignes=18]{}
\Tableau[Incline,CouleurG=blue!15,CouleurM=green!15,Couleurm=red!15,Couleuru=LightPink,Couleurd=LightGoldenrod, NbLignes=18]{}


%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%--------------------------------------------------------------- Histoire des mathématiques -------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\newpage
\begin{tcolorbox}[colframe=black, colback=blue!10, boxrule=2pt, arc=6pt]
	\centering{}\LARGE{\textbf{Histoire des mathématiques \faBook}}
\end{tcolorbox}
\begin{tcolorbox}[colframe=black!30, colback=blue!0, boxrule=2pt, arc=6pt, title=\textbf{Histoire des nombres décimaux}, coltitle=black]
Les nombres décimaux sont utilisés depuis l'époque des égyptiens et des babyloniens (environ  $\textbf{-2500}$ ans) mais pas sous la forme que nous les connaissons actuellement. Ces peuples utilisaient directement des fractions pour les représenter. Nous reviendrons plus tard sur cette notion lorsque nous aborderons ce sujet en classe.\\
\\
Plus tard, autour des années  $\textbf{900}$ à  $\textbf{1450}$, les savants arabes (Ibrahim al Uqlidisi, Muhammad al Karkhi, Omar Khayyâm et Jemshid al Kashi) ont contribué à la diffusion de l'écriture des nombres sous forme de fractions décimales.\\
\\
\begin{minipage}{0.25\textwidth}
	\begin{center}
		\includegraphics[width=3.5cm]{Figure2.png}\\
		\textbf{Simon Stevin}
	\end{center}
\end{minipage}
\begin{minipage}{0.75\textwidth}
	Plus tard, les nombres décimaux commencent à apparaître dans les écrits. Le mathématicien et statisticien belge Simon Stevin ( $\textbf{1548 - 1620}$), adopte une nouvelle écriture pour les nombres décimaux dans son livre \textit{La Disme}. Par exemple le nombre $73,489$ sera noté : $73 \textcircled{\tiny{0}}\,4 \textcircled{\tiny{1}}\,8 \textcircled{\tiny{2}}\,9 \textcircled{\tiny{3}}$. La notion de rang pour les chiffres apparaît clairement. Le nombre d'unités est associé à \textcircled{\tiny{0}}, le chiffre des dixièmes est associé à \textcircled{\tiny{1}}, celui des centièmes à \textcircled{\tiny{2}} et celui des millièmes à \textcircled{\tiny{3}}.
\end{minipage}
~~\\

\begin{minipage}{0.65\textwidth}
Les calculs sont plus simples à réaliser. La somme des termes $32,554$ et $21,782$ avec cette notation s'écrit :
\end{minipage}
\begin{minipage}{0.3\textwidth}
\begin{center}
	\begin{tabular}{cccc} 
$32^{\textcircled{\tiny{0}}}$ & $5^{\textcircled{\tiny{1}}}$ & $5^{\textcircled{\tiny{2}}}$ & $4^{\textcircled{\tiny{3}}}$ \tabularnewline
$21^{\textcircled{\tiny{0}}}$ & $7^{\textcircled{\tiny{1}}}$ & $8^{\textcircled{\tiny{2}}}$ & $2^{\textcircled{\tiny{3}}}$ \tabularnewline \hline
& & & \tabularnewline
 $54^{\textcircled{\tiny{0}}}$ & $3^{\textcircled{\tiny{1}}}$ & $3^{\textcircled{\tiny{2}}}$ & $6^{\textcircled{\tiny{3}}}$ \tabularnewline
	\end{tabular}
\end{center}
\end{minipage}
~~\\

\begin{minipage}{0.75\textwidth}
En $\textbf{1592}$, l'italien Giovanni Antonio Magini ($\textbf{1555 - 1617}$) propose une écriture des nombres décimaux encore utilisée de nos jours dans les pays anglo-saxons : $73.489$\\
\\
En $\textbf{1595}$, le suisse Jost Bürgi ($\textbf{1552 - 1632}$) ajoute un petit rond au-dessus du chiffre des unités d'un nombre : $7\overset{\tiny{\textcircled{\color{white}0}}}3489$\\
\\
Quelques années plus tard, les mathématiciens néerlandais Willebrord van Roijen Snell ($\textbf{1580 - 1626}$) et écossais John Napier ($\textbf{1550 - 1617}$) commencent à utiliser la virgule pour séparer la partie entière et la partie décimale d'un nombre.
\end{minipage}
\begin{minipage}{0.2\textwidth}
	\begin{center}
		\includegraphics[width=3.5cm]{Figure3.png}\\
		\textbf{John Napier}
	\end{center}
\end{minipage}

\end{tcolorbox}


\textbf{\underline{Sources}}
\begin{itemize}[label=\textbullet]
	\item Hauchecorne, B. et Suratteau, D. (2008), \textit{Des Mathématiciens de A à Z} ($3^e$ édition), Ellipses
	\item Dahan-Dalmedico, A. et Peiffer, J. (1986), \textit{Une histoire des mathématiques : Routes et dédales}, Points
\end{itemize}


%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%-------------------------------------------------------------------------- Fiche de route ------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\newpage
\begin{tcolorbox}[colframe=black, colback=Khaki!50, boxrule=2pt, arc=6pt]
	\centering{}\LARGE{\textbf{Fiche de route}}
\end{tcolorbox}
\begin{itemize}[label=\textbullet]
	\item \color{Tomato} \textbf{Introduction} \color{black} : Titre, prérequis, objectifs, rappels
	\item \color{RoyalBlue}\underline{Leçon} \color{black} : Partie I : définitions (nombre décimal, partie entière, partie décimale,...)
	\item \color{ForestGreen}\underline{Exercices}  \color{black} : 1, 2, 3
	\item \color{RoyalBlue}\underline{Leçon} \color{black} : Partie II : fractions décimales (définitions + exemples)
	\item \color{ForestGreen}\underline{Exercices}  \color{black} : 4, 5, 6
	\item \color{RoyalBlue}\underline{Leçon} \color{black} : Partie III : tableau
	\item \color{ForestGreen}\underline{Exercices}  \color{black} : 7, 8, 9
	\item \color{RoyalBlue}\underline{Leçon} \color{black} : Partie III : rang des chiffres (chiffre des ..., nombre des ...)
	\item \color{ForestGreen}\underline{Exercices}  \color{black} : 10, 11, 12, 13
	\item \color{RoyalBlue}\underline{Leçon}  \color{black} : Parties IV et V : Bilan + Auto-évaluation
	\item \color{ForestGreen}\underline{Exercices bilan}  \color{black} : 14, 15
	\item \color{Coral} \underline{Histoire des mathématiques}
\end{itemize}

\setcounter{page}{4}
%\end{comment}

\end{document}
