\documentclass[dvipsnames, a4paper, 12pt]{report}

\usepackage{lmodern} %police standard latin moderne
\usepackage[frenchb]{babel} %langue française
\usepackage[utf8]{inputenc} %pour l'utf-8
\usepackage[T1]{fontenc} %pour les césures et accentuations
\usepackage{geometry} %pour changer les marges d'un document
\usepackage{graphicx}
\usepackage{fancybox}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{fontawesome5}
\usepackage{ProfCollege}
\usepackage{luacode}
\usepackage{xlop}

\usepackage[explicit]{titlesec} %Permet de modifier les différentes parties, chapitres, sous-parties,...

\usepackage{colortbl} %permet de mettre des cellules de tableau en couleur

\usepackage{multicol}

\usepackage{cellspace} %pour définir les espaces en haut et en bas des cellules d'un tableau
\setlength{\cellspacebottomlimit}{5pt}
\setlength{\cellspacetoplimit}{5pt}
\setlength{\parindent}{0pt}

\usepackage{xcolor} %permet de mettre du texte en couleur

\usepackage{array} %gestion des tableaux
\usepackage{tcolorbox} %créer des boîtes de couleurs

\usepackage{hyperref} %permet de gérer le sommaire et la page de garde et les options du PDF

\geometry{top=2cm, bottom=2cm, right=2cm, left=2cm}   
\titlespacing{\chapter}{0pt}{0pt}{5pt}

\setcounter{chapter}{13}

\usepackage{lastpage}
\usepackage{fancyhdr} %définition des en-têtes et des bas de page
\fancyhf{}
\fancypagestyle{main}{
	\renewcommand\headrulewidth{5pt}
	\fancyhead[L]{6\ieme\, - Chapitre \arabic{chapter} - Introduction aux fractions}
	\fancyhead[R]{Collège}
	\fancyfoot[C]{\thepage/\pageref{LastPage}}
	\fancyfoot[R]{\footnotesize{Anthony Salze}}
	}
\pagestyle{main}

%gestion des entêtes
\makeatletter\@addtoreset{section}{chapter}\makeatother % RAZ des numéros de section après un chapitre
\renewcommand{\thepart}{\Alph{part} -} % Pour mettre des I, II, etc. aux parties
\renewcommand{\thesection}{\color{Green} \Roman{section} -}% Idem pour les sections et avoir le numéro de chapitre
\renewcommand{\thesubsection}{\arabic{subsection} -}

\begin{document}
%format du titre des chapitres placé après Sommaire sinon cela bugue
\titleformat{\chapter}[frame] {\normalfont \bfseries \Large}{\textcolor{red} {\chaptertitlename\ \thechapter\,-\,#1}}{0pt}{\Huge}

\setcounter{chapter}{12}

\chapter{Introduction aux fractions}
\begin{luacode}
	d=1
	p=1
	t=1
	m=1
\end{luacode}

\color{blue}\underline{\textbf{Prérequis}}
\begin{itemize}
	\item Connaître la notion de numérateur et dénominateur.
	\item Divisions décimales et euclidiennes.
	\item Placement d'un point sur une demi-droite graduée.
	\item Comparaison de nombres décimaux.
\end{itemize}~~

\underline{\textbf{Objectifs}}
\begin{itemize}
	\item Connaître la notion de fraction partage.
	\item Connaître la notion de fraction quotient.
	\item Placer une fraction sur une demi-droite graduée.
	\item Comparer deux fractions.
\end{itemize}
\color{black}
~~\\
\textbf{\underline{Rappels}} : QCM (une seule réponse possible)\\\footnotesize{}
\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom= , AlphT, Largeur=3cm]{%
Dans cette figure$\text{,}$ la proportion de carreaux colorés en blanc est de \ldots \begin{center} \includegraphics[width=4cm]{6_C11_Space_invaders.png} \end{center}  &  $\dfrac{2}{21}$ & $\dfrac{21}{35}$ & $\dfrac{21}{23}$ &3,%
Le numérateur de la fraction $\dfrac{5}{4}$ est \ldots & $5$  & $4$ & $\num{1.25}$ &1,%
L'écriture décimale de $\dfrac{567}{100}$ est \ldots & $567$ & $\num{56.7}$ & $\num{5.67}$ & 3,%
Dans la fraction $\dfrac{12}{13}\text{,}$ $13$ est le \ldots & numérateur.  & nominateur. & dénominateur.  &3,%
Marie a mangé le quart d'un gâteau qui pèse $\Masse{200}$. La masse de gâteau qu'elle a mangée est égale à \ldots & $\Masse{200}$.  & $\Masse{100}$. & $\Masse{50}$. &3,%
}
\normalsize{}


\color{Green}
\section{Fraction partage}
\color{black}
\normalsize{}

\begin{tcolorbox}[colframe=Green!20, colback=Green!0, boxrule=2pt, arc=6pt, title={\textbf{Définition \arabic{chapter}.\directlua{tex.print(d)}}}, coltitle=black]
	Lorsqu'on coupe une unité en un nombre entier de parts égales et qu'on prend un nombre entier de ces parts, on obtient une fraction.
\end{tcolorbox}
\begin{luacode}
	d=d+1
\end{luacode}

\underline{\textbf{Exemples}}\\
\begin{minipage}{0.45\textwidth}
L'unité est le disque qu'on partage en $5$ parts égales.\\
Chaque part représente $\dfrac{1}{5}$ de l'unité.
    \begin{center}
    	\Fraction[Reponse, Rayon=1.5cm, Couleur=LightSkyBlue]{4/5}
    \end{center}
$4$ parts sur les $5$ sont coloriées. La partie colorée correspond alors à $\dfrac{4}{5}$ du disque unité.
\end{minipage}
\hspace{0.05cm} \vrule \hspace{0.05cm}
\begin{minipage}{0.45\textwidth}
L'unité est l'heptagone qu'on partage en $7$ parts égales.\\
Chaque part représente $\dfrac{1}{7}$ de l'unité.
    \begin{center}
        \Fraction[Regulier, Cotes=7, Reponse, Rayon=1.5cm, Couleur=LightSkyBlue]{7/7} \Fraction[Regulier, Cotes=7, Reponse, Rayon=1.5cm, Couleur=LightSkyBlue]{2/7}
    \end{center}
Dans ce cas, la partie colorée correspond à $9$ septièmes de l'unité. La fraction correspondante à ce partage est égale à $\dfrac{9}{7}$.
\end{minipage}
~~\\

\begin{tcolorbox}[colframe=blue!20, colback=blue!0, boxrule=2pt, arc=6pt, title={\textbf{Propriété \arabic{chapter}.\directlua{tex.print(p)}}}, coltitle=black]
Pour additionner deux fractions qui ont le même dénominateur, on ajoute les numérateurs, sans changer le dénominateur.
\end{tcolorbox}
\begin{luacode}
	p=p+1
\end{luacode}

\underline{\textbf{Exemple}}\\
\begin{minipage}{0.80\textwidth}
    La partie foncée représente $\dfrac{1}{4}$ du disque et la partie plus claire représente $\dfrac{2}{4}$ du disque.\\\\
    Donc, 1 quart et 2 quarts font 3 quarts. On écrira sous forme mathématique : $\dfrac{1}{4} + \dfrac{2}{4} = \dfrac{3}{4}$.
\end{minipage} \hspace{0.05cm}
\begin{minipage}{0.15\textwidth}
    \Stat[Graphique, Angle, Rayon=1.5cm, Legende=false, ListeCouleurs={SteelBlue, Plum, Plum, White}]{1/90, 2/90, 3/90, 4/90}
\end{minipage}


\color{Green}
\section{Fraction quotient}
\color{black}
\normalsize{}

\begin{tcolorbox}[colframe=Green!20, colback=Green!0, boxrule=2pt, arc=6pt, title={\textbf{Définition \arabic{chapter}.\directlua{tex.print(d)}}}, coltitle=black]
$a$ et $b$ désignent deux nombres ($b \neq 0$).\\
Le quotient de $a$ par $b$ est le nombre, qui multiplié par $b$, donne $a$. On le note $a : b$ ou $\dfrac{a}{b}$.\\
La notation $\dfrac{a}{b}$ est appelée écriture fractionnaire.
\end{tcolorbox}
\begin{luacode}
	d=d+1
\end{luacode}

\underline{\textbf{Exemple}}\\
$\dfrac{12}{7}$ est le quotient de $12$ par $7$. Si on multiplie ce nombre par $7$, on obtient $12$. On écrit alors : $ 7 \times \dfrac{12}{7}=12$.
\\
\begin{tcolorbox}[colframe=Green!20, colback=Green!0, boxrule=2pt, arc=6pt, title={\textbf{Définition \arabic{chapter}.\directlua{tex.print(d)}}}, coltitle=black]
Si $a$ et $b$ sont des nombres entiers ($b \neq 0$), on dit que le nombre $\displaystyle \frac{a}{b}$ est une fraction.
\end{tcolorbox}
\begin{luacode}
	d=d+1
\end{luacode}

\underline{\textbf{Exemples}}
\begin{itemize}[label=\textbullet]
    \item $\dfrac{12}{5}$ est une fraction dont l'écriture décimale est $2,4$.
    \item \faExclamationTriangle\,Tous les nombres écrits sous forme de fraction ne sont pas toujours des nombres décimaux. Par exemple, $\dfrac{2}{3}$ ne possède pas d'écriture décimale, ce n'est donc pas un nombre décimal.
\end{itemize}
~~

\begin{tcolorbox}[colframe=Green!20, colback=Green!0, boxrule=2pt, arc=6pt, title={\textbf{Définition \arabic{chapter}.\directlua{tex.print(d)}}}, coltitle=black]
On peut placer un point sur une demi-droite graduée à l'aide d'une fraction appelée \textbf{abscisse} du point.
\end{tcolorbox}
\begin{luacode}
	d=d+1
\end{luacode}

\underline{\textbf{Exemple}}
\begin{center}
	\Reperage[DemiDroite, AffichageAbs=2, AffichageNom, AffichageGrad, Pasx=6, Unitex=7]{5/A,13/B}
\end{center}
L'unité est partagée en $6$ parts égales. L'abscisse du point $A$ est $\dfrac{5}{6}$ et celle du point $B$ est $\dfrac{13}{6}$.\\

\begin{tcolorbox}[colframe=black!20, colback=black!0, boxrule=2pt, arc=6pt, title={\textbf{Méthode \arabic{chapter}.\directlua{tex.print(m)} : Placer une fraction sur une demi-droite graduée}}, coltitle=black]
Pour placer la fraction $\dfrac{\color{blue}{a}}{\color{red}{b}}$ sur une demi-droite graduée, on partage l'unité en $\color{red}{b}$ segments de même longueur, puis on reporte $\color{blue}{a}$ fois cette longueur à partir de zéro.
\end{tcolorbox}
\begin{luacode}
	m=m+1
\end{luacode}

\underline{\textbf{Exemple}}\\
On veut repérer la fraction $\dfrac{\color{blue}{10}}{\color{red}{7}}$.\\
On partage donc l'unité en $\color{red}{7}\color{black}$ segments de même \textbf{longueur}.\\
Puis, on reporte $\color{blue}{10}\color{black}$ fois cette \textbf{longueur} à partir de zéro.
\begin{center}
    \includegraphics[width=15cm]{6_C11_Droite_graduée2.png}
\end{center}

\color{Green}
\section{Comparaisons de fractions}
\color{black}
\begin{tcolorbox}[colframe=blue!20, colback=blue!0, boxrule=2pt, arc=6pt, title={\textbf{Propriété \arabic{chapter}.\directlua{tex.print(p)} : \'Egalité de deux fractions}}, coltitle=black]
	Une fraction admet une infinité d'écritures fractionnaires.\\
	Un quotient ne change pas si on multiplie (ou divise) son numérateur et son dénominateur par un même nombre ($\neq 0$).
\end{tcolorbox}
\begin{luacode}
	p=p+1
\end{luacode}


\underline{\textbf{Exemples}}\\
\begin{minipage}{0.45\textwidth}
    \begin{flalign*}
        \dfrac{8}{5}&=\dfrac{8 \times 2}{5 \times 2}&\\
        &=\dfrac{16}{10}
    \end{flalign*}
    Donc : $\dfrac{8}{5} = \dfrac{16}{10}$
\end{minipage} \hspace{0.05cm} \vrule \hspace{0.05cm}
\begin{minipage}{0.45\textwidth}
    \begin{flalign*}
        \dfrac{5}{10}&=\dfrac{5 : 5}{10 : 5}&\\
        &=\dfrac{1}{2}
    \end{flalign*}
    Donc : $\dfrac{5}{10} = \dfrac{1}{2}$
\end{minipage}
~~\\

\begin{tcolorbox}[colframe=blue!20, colback=blue!0, boxrule=2pt, arc=6pt, title={\textbf{Propriété \arabic{chapter}.\directlua{tex.print(p)} : Comparaisons de fractions}}, coltitle=black]
    Si deux fractions ont le même dénominateur, la plus grande est celle qui a le plus grand numérateur.
\end{tcolorbox}
\begin{luacode}
	p=p+1
\end{luacode}

\underline{\textbf{Exemple}}\\
On souhaite comparer les fractions $\dfrac{7}{29}$ et $\dfrac{15}{29}$.\\
Elles ont le même dénominateur. On compare leur numérateur : $15 > 7$. Donc : $\dfrac{7}{29} < \dfrac{15}{29}$.
\\

\begin{tcolorbox}[colframe=blue!20, colback=blue!0, boxrule=2pt, arc=6pt, title={\textbf{Propriété \arabic{chapter}.\directlua{tex.print(p)} : Encadrement d'une fraction par deux nombres entiers}}, coltitle=black]
	Toute fraction peut être encadrée par deux nombres entiers consécutifs (qui se suivent).
\end{tcolorbox}
\begin{luacode}
	p=p+1
\end{luacode}

\underline{\textbf{Exemple}}\\
Dans l'exemple de la \textit{Méthode \arabic{chapter}.\directlua{tex.print(m-1)}}, on a placé la fraction $\dfrac{10}{7}$ sur une demi-droite graduée.\\
On peut voir que cette fraction est comprise entre les deux entiers $1$ et $2$. On peut donc écrire : $ 1 < \dfrac{10}{7} < 2$.

\color{Green}
\section{Résoudre des problèmes avec des fractions}
\color{black}
\begin{tcolorbox}[colframe=blue!20, colback=blue!0, boxrule=2pt, arc=6pt, title={\textbf{Propriété \arabic{chapter}.\directlua{tex.print(p)}}}, coltitle=black]
	Prendre une fraction d'une certaine quantité revient à multiplier cette fraction par cette quantité.
\end{tcolorbox}
\begin{luacode}
	p=p+1
\end{luacode}

\underline{\textbf{Exemple}}\\
Blaise Pasacal a dépensé les $\dfrac{2}{5}$ de ses économies. Il avait $120$ \texteuro. Combien a-t-il dépensé ?\\
\\
$\dfrac{2}{5} \times 120 = 48$\\
Il a dépensé $48$ \texteuro.\\

\begin{tcolorbox}[colframe=Green!20, colback=Green!0, boxrule=2pt, arc=6pt, title={\textbf{Définition \arabic{chapter}.\directlua{tex.print(d)} : Pourcentage}}, coltitle=black]
Un pourcentage de $p\,\%$ correspond à l'écriture fractionnaire $\dfrac{p}{100}$.
\end{tcolorbox}
\begin{luacode}
	d=d+1
\end{luacode}
\newpage
\underline{\textbf{Exemples}}\\
\begin{minipage}{0.55\textwidth}
$37\,\%$ correspond à $\dfrac{37}{100}$ en écriture fractionnaire.
\end{minipage} \hspace{0.05cm} \vrule \hspace{0.05cm}
\begin{minipage}{0.45\textwidth}
\begin{center}
On veut calculer $25\,\%$ de $28$ \texteuro.
    \[\dfrac{25}{100} \times 28 = 7\]
    $25\,\%$ de $28$ \texteuro\,correspond à $7$ \texteuro.
\end{center}
\end{minipage}


\color{Green}
\section{Bilan}
\color{black}
QCM (une seule réponse possible)\\\footnotesize{}
\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom= , AlphT, Largeur=3cm]{%
Sur la figure $1\text{,}$ la partie colorée représente \ldots & $\dfrac{2}{3}$ de la figure.  & $\dfrac{3}{4}$ de la figure. & $\dfrac{4}{5}$ de la figure. &2,%
L'écriture décimale de $\dfrac{5}{4}$ est \ldots & $\num{5.4}$  & $\num{1.25}$ & $\dfrac{10}{8}$ &2,%
Le nombre que l'on doit multiplier par $3$ pour obtenir $10$ est \ldots & $\num{3.33}$  & $\dfrac{3}{10}$ & $\dfrac{10}{3}$  &3,%
Sur la figure $2\text{,}$ l'abscisse du point $A$ est \ldots & $\dfrac{3}{5}$  & $\num{1.3}$ & $\dfrac{8}{5}$ &3,%
Une autre écriture de $\dfrac{12}{18}$ est \ldots & $\dfrac{2}{8}$  & $\dfrac{2}{3}$ & $1+ \dfrac{6}{18}$  &2,%
$24 \times \dfrac{5}{6}$ est égal à \ldots & $(24 \times 6) : 5$  & $(24 : 6) \times 5$ & $\num{19.2}$ &2,%
Laura a dépensé les $\dfrac{5}{6}$ de ses $60$ \texteuro. Il lui reste \ldots & $50$ \texteuro.  & $10$ \texteuro. & $\dfrac{5}{6} \times 60$ \texteuro.  &2,%
Il y a $28$ élèves dans une classe de $6$\ieme. $25\,\%$ d'entre eux sont externes. On peut affirmer qu' \ldots & il y a $25$ externes.  & il y a $\dfrac{28}{25} \times 100$ externes. & il y a $7$ externes. &3%
}
\normalsize{}

\begin{minipage}{0.45\textwidth}
\begin{center}
	\Fraction[Triangle,Longueur=4cm,Parts=4,Reponse, Couleur=Gold,Epaisseur=1.5]{12/16}\\
	\textbf{Figure 1}
\end{center}
\end{minipage}
\begin{minipage}{0.45\textwidth}
\begin{center}
	\Reperage[DemiDroite, AffichageAbs=3, AffichageNom, AffichageGrad, Pasx=5, Unitex=4]{8/A}
	\textbf{Figure 2}
\end{center}
\end{minipage}

%\begin{comment}
\color{Green}
\section{Auto-évaluation}
\color{black}
\begin{tabular}{|>{\columncolor{darkgray}\color{white}}m{8cm}|>{\color{white}}m{0.6cm}|m{0.6cm}|m{0.6cm}|m{5cm}|} \hline

\rowcolor{darkgray}\centering{} Compétences & \centering{}\color{white} Oui & \centering{}\color{white}\scriptsize{}\rotatebox{45}{Moyen} & \centering{}\color{white}Non & \centering{}\color{white}Pour réviser \tabularnewline \hline \hline

\cellcolor{gray}. & & & & \textbf{\underline{Leçon}} :  \smallskip \newline  \textbf{\underline{Exercices}} :  \tabularnewline \hline \hline

\cellcolor{gray}. & & & & \textbf{\underline{Leçon}} :  \smallskip \newline  \textbf{\underline{Exercices}} :    \tabularnewline \hline
\end{tabular}

%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------- Correction QCM -----------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\newpage
%\setcounter{page}{1}
\footnotesize{}
\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom= , AlphT, Largeur=3cm, Solution, Couleur=cyan!20]{%
Dans cette figure$\text{,}$ la proportion de carreaux colorés en blanc est de \ldots \begin{center} \includegraphics[width=4cm]{6_C11_Space_invaders.png} \end{center}  &  $\dfrac{2}{21}$ & $\dfrac{21}{35}$ & $\dfrac{21}{23}$ &3,%
Le numérateur de la fraction $\dfrac{5}{4}$ est \ldots & $5$  & $4$ & $\num{1.25}$ &1,%
L'écriture décimale de $\dfrac{567}{100}$ est \ldots & $567$ & $\num{56.7}$ & $\num{5.67}$ & 3,%
Dans la fraction $\dfrac{12}{13}\text{,}$ $13$ est le \ldots & numérateur.  & nominateur. & dénominateur.  &3,%
Marie a mangé le quart d'un gâteau qui pèse $\Masse{200}$. La masse de gâteau qu'elle a mangée est égale à \ldots & $\Masse{200}$.  & $\Masse{100}$. & $\Masse{50}$. &3,%
}
\\
\\
\\
\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom= , AlphT, Largeur=3cm, Solution, Couleur=cyan!20]{%
Sur la figure $1\text{,}$ la partie colorée représente \ldots & $\dfrac{2}{3}$ de la figure.  & $\dfrac{3}{4}$ de la figure. & $\dfrac{4}{5}$ de la figure. &2,%
L'écriture décimale de $\dfrac{5}{4}$ est \ldots & $\num{5.4}$  & $\num{1.25}$ & $\dfrac{10}{8}$ &2,%
Le nombre que l'on doit multiplier par $3$ pour obtenir $10$ est \ldots & $\num{3.33}$  & $\dfrac{3}{10}$ & $\dfrac{10}{3}$  &3,%
Sur la figure $2\text{,}$ l'abscisse du point $A$ est \ldots & $\dfrac{3}{5}$  & $\num{1.3}$ & $\dfrac{8}{5}$ &3,%
Une autre écriture de $\dfrac{12}{18}$ est \ldots & $\dfrac{2}{8}$  & $\dfrac{2}{3}$ & $1+ \dfrac{6}{18}$  &2,%
$24 \times \dfrac{5}{6}$ est égal à \ldots & $(24 \times 6) : 5$  & $(24 : 6) \times 5$ & $\num{19.2}$ &2,%
Laura a dépensé les $\dfrac{5}{6}$ de ses $60$ \texteuro. Il lui reste \ldots & $50$ \texteuro.  & $10$ \texteuro. & $\dfrac{5}{6} \times 60$ \texteuro.  &2,%
Il y a $28$ élèves dans une classe de $6$\ieme. $25\,\%$ d'entre eux sont externes. On peut affirmer qu' \ldots & il y a $25$ externes.  & il y a $\dfrac{28}{25} \times 100$ externes. & il y a $7$ externes. &3%
}
\normalsize{}

\begin{minipage}{0.45\textwidth}
\begin{center}
	\Fraction[Triangle,Longueur=4cm,Parts=4,Reponse, Couleur=Gold,Epaisseur=1.5]{12/16}\\
	\textbf{Figure 1}
\end{center}
\end{minipage}
\begin{minipage}{0.45\textwidth}
\begin{center}
	\Reperage[DemiDroite, AffichageAbs=3, AffichageNom, AffichageGrad, Pasx=5, Unitex=4]{8/A}
	\textbf{Figure 2}
\end{center}
\end{minipage}
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%---------------------------------------------------------------------- Leçon à trous -----------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\newpage
%\setcounter{page}{1}
\titleformat{\chapter}[frame] {\normalfont \bfseries \Large}{\textcolor{red} {\chaptertitlename\ \thechapter\,-\,#1}}{0pt}{\Huge}

\setcounter{chapter}{12}

\chapter{\pointilles[12cm]}
\begin{luacode}
	d=1
	p=1
	t=1
	m=1
\end{luacode}

\color{blue}\underline{\textbf{Prérequis}}
\begin{itemize}
	\item Connaître \pointilles[14cm]
	\item \pointilles[16cm].
	\item Placement \pointilles[14cm].
	\item Comparaison \pointilles[13cm].
\end{itemize}~~

\underline{\textbf{Objectifs}}
\begin{itemize}
	\item Connaître \pointilles[14cm].
	\item Connaître \pointilles[14cm].
	\item Placer \pointilles[15cm].
	\item Comparer \pointilles[14cm].
\end{itemize}
\color{black}
~~\\
\textbf{\underline{Rappels}} : QCM (une seule réponse possible)\\\footnotesize{}
\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom= , AlphT, Largeur=3cm]{%
Dans cette figure$\text{,}$ la proportion de carreaux colorés en blanc est de \ldots \begin{center} \includegraphics[width=4cm]{6_C11_Space_invaders.png} \end{center}  &  $\dfrac{2}{21}$ & $\dfrac{21}{35}$ & $\dfrac{21}{23}$ &3,%
Le numérateur de la fraction $\dfrac{5}{4}$ est \ldots & $5$  & $4$ & $\num{1.25}$ &1,%
L'écriture décimale de $\dfrac{567}{100}$ est \ldots & $567$ & $\num{56.7}$ & $\num{5.67}$ & 3,%
Dans la fraction $\dfrac{12}{13}\text{,}$ $13$ est le \ldots & numérateur.  & nominateur. & dénominateur.  &3,%
Marie a mangé le quart d'un gâteau qui pèse $\Masse{200}$. La masse de gâteau qu'elle a mangée est égale à \ldots & $\Masse{200}$.  & $\Masse{100}$. & $\Masse{50}$. &3,%
}
\normalsize{}


\color{Green}
\section{\pointilles[10cm]}
\color{black}
\normalsize{}

\begin{tcolorbox}[colframe=Green!20, colback=Green!0, boxrule=2pt, arc=6pt, title={\textbf{Définition \arabic{chapter}.\directlua{tex.print(d)}}}, coltitle=black]
	Lorsqu'on \pointilles[14cm]\\
	\pointilles[16cm]
\end{tcolorbox}
\begin{luacode}
	d=d+1
\end{luacode}

\underline{\textbf{Exemples}}\\
\begin{minipage}{0.45\textwidth}
L'unité \pointilles[6cm]\\
\pointilles[7.5cm]\\
Chaque part \pointilles[5cm]
    \begin{center}
    	\Fraction[Reponse, Rayon=1.5cm, Couleur=LightSkyBlue]{4/5}
    \end{center}
\pointilles[7.5cm]\\
\pointilles[7.5cm]\\
\pointilles[7.5cm]
\end{minipage}
\hspace{0.05cm} \vrule \hspace{0.05cm}
\begin{minipage}{0.45\textwidth}
L'unité \pointilles[6cm]\\
\pointilles[7.5cm]\\
Chaque part \pointilles[5cm]
    \begin{center}
    	\Fraction[Regulier, Cotes=7, Reponse, Rayon=1.5cm, Couleur=LightSkyBlue]{7/7} \Fraction[Regulier, Cotes=7, Reponse, Rayon=1.5cm, Couleur=LightSkyBlue]{2/7}
    \end{center}
\pointilles[7.5cm]\\
\pointilles[7.5cm]\\
\pointilles[7.5cm]\\
\pointilles[7.5cm]
\end{minipage}
~~\\

\begin{tcolorbox}[colframe=blue!20, colback=blue!0, boxrule=2pt, arc=6pt, title={\textbf{Propriété \arabic{chapter}.\directlua{tex.print(p)}}}, coltitle=black]
Pour \pointilles[15cm]\\
\pointilles[16cm]
\end{tcolorbox}
\begin{luacode}
	p=p+1
\end{luacode}

\underline{\textbf{Exemple}}\\
\begin{minipage}{0.80\textwidth}
    La partie \pointilles[12cm]\\
    \pointilles[13.5cm]\\\\
    \pointilles[13.5cm]\\
    \pointilles[13.5cm]
\end{minipage} \hspace{0.05cm}
\begin{minipage}{0.15\textwidth}
    \Stat[Graphique, Angle, Rayon=1.5cm, Legende=false, ListeCouleurs={SteelBlue, Plum, Plum, White}]{1/90, 2/90, 3/90, 4/90}
\end{minipage}


\color{Green}
\section{\pointilles[8cm]}
\color{black}
\normalsize{}

\begin{tcolorbox}[colframe=Green!20, colback=Green!0, boxrule=2pt, arc=6pt, title={\textbf{Définition \arabic{chapter}.\directlua{tex.print(d)}}}, coltitle=black]
$a$ et $b$ désignent deux nombres ($b \neq 0$).\\
\pointilles[16cm]\\
\pointilles[16cm]\\
\pointilles[16cm]
\end{tcolorbox}
\begin{luacode}
	d=d+1
\end{luacode}

\underline{\textbf{Exemple}}\\
\pointilles[17cm]\\
\pointilles[17cm]
\\
\begin{tcolorbox}[colframe=Green!20, colback=Green!0, boxrule=2pt, arc=6pt, title={\textbf{Définition \arabic{chapter}.\directlua{tex.print(d)}}}, coltitle=black]
\pointilles[16cm]\\
\pointilles[16cm]
\end{tcolorbox}
\begin{luacode}
	d=d+1
\end{luacode}

\underline{\textbf{Exemples}}
\begin{itemize}[label=\textbullet]
    \item $\dfrac{12}{5}$ \pointilles[10cm]
    \item \faExclamationTriangle\,\pointilles[15.5cm]\\
    \pointilles[16cm]\\
    \pointilles[16cm]\\
\end{itemize}
~~

\begin{tcolorbox}[colframe=Green!20, colback=Green!0, boxrule=2pt, arc=6pt, title={\textbf{Définition \arabic{chapter}.\directlua{tex.print(d)}}}, coltitle=black]
\pointilles[16cm]\\
\pointilles[16cm]
\end{tcolorbox}
\begin{luacode}
	d=d+1
\end{luacode}

\underline{\textbf{Exemple}}
\begin{center}
	\Reperage[DemiDroite, AffichageAbs=2, AffichageNom, AffichageGrad, Pasx=6, Unitex=7]{5/A,13/B}
\end{center}
\pointilles[17cm]\\
\pointilles[17cm]\\

\begin{tcolorbox}[colframe=black!20, colback=black!0, boxrule=2pt, arc=6pt, title={\textbf{Méthode \arabic{chapter}.\directlua{tex.print(m)} : Placer une fraction sur une demi-droite graduée}}, coltitle=black]
\pointilles[16cm]\\
\pointilles[16cm]\\
\pointilles[16cm]
\end{tcolorbox}
\begin{luacode}
	m=m+1
\end{luacode}

\underline{\textbf{Exemple}}\\
\pointilles[17cm]\\
\pointilles[17cm]
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip


\color{Green}
\section{\pointilles[10cm]}
\color{black}
\begin{tcolorbox}[colframe=blue!20, colback=blue!0, boxrule=2pt, arc=6pt, title={\textbf{Propriété \arabic{chapter}.\directlua{tex.print(p)} : \'Egalité de deux fractions}}, coltitle=black]
	\pointilles[16cm]\\
	\pointilles[16cm]\\
	\pointilles[16cm]\\
	\pointilles[16cm]
\end{tcolorbox}
\begin{luacode}
	p=p+1
\end{luacode}


\underline{\textbf{Exemples}}\\
\begin{minipage}{0.45\textwidth}
   \color{white}test\color{black} \bigskip
    \bigskip
    \bigskip
    \bigskip
    \bigskip
    \bigskip
    \bigskip
    \bigskip
    \bigskip
\end{minipage} \hspace{0.05cm} \vrule \hspace{0.05cm}
\begin{minipage}{0.45\textwidth}
    \bigskip
    \bigskip
    \bigskip
    \bigskip
    \bigskip
    \bigskip
    \bigskip
    \bigskip
    \bigskip
\end{minipage}
~~\\

\begin{tcolorbox}[colframe=blue!20, colback=blue!0, boxrule=2pt, arc=6pt, title={\textbf{Propriété \arabic{chapter}.\directlua{tex.print(p)} : Comparaisons de fractions}}, coltitle=black]
    \pointilles[16cm]\\
    \pointilles[16cm]
\end{tcolorbox}
\begin{luacode}
	p=p+1
\end{luacode}

\underline{\textbf{Exemple}}\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]

\begin{tcolorbox}[colframe=blue!20, colback=blue!0, boxrule=2pt, arc=6pt, title={\textbf{Propriété \arabic{chapter}.\directlua{tex.print(p)} : Encadrement d'une fraction par deux nombres entiers}}, coltitle=black]
	\pointilles[16cm]\\
	\pointilles[16cm]
\end{tcolorbox}
\begin{luacode}
	p=p+1
\end{luacode}

\underline{\textbf{Exemple}}\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]

\color{Green}
\section{\pointilles[10cm]\\}
\color{black}
\begin{tcolorbox}[colframe=blue!20, colback=blue!0, boxrule=2pt, arc=6pt, title={\textbf{Propriété \arabic{chapter}.\directlua{tex.print(p)}}}, coltitle=black]
	\pointilles[16cm]\\
	\pointilles[16cm]
\end{tcolorbox}
\begin{luacode}
	p=p+1
\end{luacode}

\underline{\textbf{Exemple}}\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]\\
\pointilles[17cm]

\begin{tcolorbox}[colframe=Green!20, colback=Green!0, boxrule=2pt, arc=6pt, title={\textbf{Définition \arabic{chapter}.\directlua{tex.print(d)} : Pourcentage}}, coltitle=black]
\pointilles[16cm]\\
\end{tcolorbox}
\begin{luacode}
	d=d+1
\end{luacode}
\newpage
\underline{\textbf{Exemples}}\\
\begin{minipage}{0.55\textwidth}
\pointilles[9cm]\\
\pointilles[9cm]\\
\end{minipage} \hspace{0.05cm} \vrule \hspace{0.05cm}
\begin{minipage}{0.45\textwidth}
\pointilles[7cm]\\
\pointilles[7cm]\\
\pointilles[7cm]\\
\pointilles[7cm]
\end{minipage}


\color{Green}
\section{Bilan}
\color{black}
QCM (une seule réponse possible)\\\footnotesize{}
\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom= , AlphT, Largeur=3cm]{%
Sur la figure $1\text{,}$ la partie colorée représente \ldots & $\dfrac{2}{3}$ de la figure.  & $\dfrac{3}{4}$ de la figure. & $\dfrac{4}{5}$ de la figure. &2,%
L'écriture décimale de $\dfrac{5}{4}$ est \ldots & $\num{5.4}$  & $\num{1.25}$ & $\dfrac{10}{8}$ &2,%
Le nombre que l'on doit multiplier par $3$ pour obtenir $10$ est \ldots & $\num{3.33}$  & $\dfrac{3}{10}$ & $\dfrac{10}{3}$  &3,%
Sur la figure $2\text{,}$ l'abscisse du point $A$ est \ldots & $\dfrac{3}{5}$  & $\num{1.3}$ & $\dfrac{8}{5}$ &3,%
Une autre écriture de $\dfrac{12}{18}$ est \ldots & $\dfrac{2}{8}$  & $\dfrac{2}{3}$ & $1+ \dfrac{6}{18}$  &2,%
$24 \times \dfrac{5}{6}$ est égal à \ldots & $(24 \times 6) : 5$  & $(24 : 6) \times 5$ & $\num{19.2}$ &2,%
Laura a dépensé les $\dfrac{5}{6}$ de ses $60$ \texteuro. Il lui reste \ldots & $50$ \texteuro.  & $10$ \texteuro. & $\dfrac{5}{6} \times 60$ \texteuro.  &2,%
Il y a $28$ élèves dans une classe de $6$\ieme. $25\,\%$ d'entre eux sont externes. On peut affirmer qu' \ldots & il y a $25$ externes.  & il y a $\dfrac{28}{25} \times 100$ externes. & il y a $7$ externes. &3%
}
\normalsize{}

\begin{minipage}{0.45\textwidth}
\begin{center}
	\Fraction[Triangle,Longueur=4cm,Parts=4,Reponse, Couleur=Gold,Epaisseur=1.5]{12/16}\\
	\textbf{Figure 1}
\end{center}
\end{minipage}
\begin{minipage}{0.45\textwidth}
\begin{center}
	\Reperage[DemiDroite, AffichageAbs=3, AffichageNom, AffichageGrad, Pasx=5, Unitex=4]{8/A}
	\textbf{Figure 2}
\end{center}
\end{minipage}


\color{Green}
\section{Auto-évaluation}
\color{black}
\begin{tabular}{|>{\columncolor{darkgray}\color{white}}m{8cm}|>{\color{white}}m{0.6cm}|m{0.6cm}|m{0.6cm}|m{5cm}|} \hline

\rowcolor{darkgray}\centering{} Compétences & \centering{}\color{white} Oui & \centering{}\color{white}\scriptsize{}\rotatebox{45}{Moyen} & \centering{}\color{white}Non & \centering{}\color{white}Pour réviser \tabularnewline \hline \hline

\cellcolor{gray}. & & & & \textbf{\underline{Leçon}} :  \smallskip \newline  \textbf{\underline{Exercices}} :  \tabularnewline \hline \hline

\cellcolor{gray}. & & & & \textbf{\underline{Leçon}} :  \smallskip \newline  \textbf{\underline{Exercices}} :    \tabularnewline \hline
\end{tabular}

%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------- Impression -----------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\newpage
\textbf{\underline{Rappels}} : QCM (une seule réponse possible)\\\footnotesize{}
\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom= , AlphT, Largeur=3cm]{%
Dans cette figure$\text{,}$ la proportion de carreaux colorés en blanc est de \ldots \begin{center} \includegraphics[width=4cm]{6_C11_Space_invaders.png} \end{center}  &  $\dfrac{2}{21}$ & $\dfrac{21}{35}$ & $\dfrac{21}{23}$ &3,%
Le numérateur de la fraction $\dfrac{5}{4}$ est \ldots & $5$  & $4$ & $\num{1.25}$ &1,%
L'écriture décimale de $\dfrac{567}{100}$ est \ldots & $567$ & $\num{56.7}$ & $\num{5.67}$ & 3,%
Dans la fraction $\dfrac{12}{13}\text{,}$ $13$ est le \ldots & numérateur.  & nominateur. & dénominateur.  &3,%
Marie a mangé le quart d'un gâteau qui pèse $\Masse{200}$. La masse de gâteau qu'elle a mangée est égale à \ldots & $\Masse{200}$.  & $\Masse{100}$. & $\Masse{50}$. &3,%
}
\normalsize{}
\\
\\
\\
QCM (une seule réponse possible)\\\footnotesize{}
\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom= , AlphT, Largeur=3cm]{%
Sur la figure $1\text{,}$ la partie colorée représente \ldots & $\dfrac{2}{3}$ de la figure.  & $\dfrac{3}{4}$ de la figure. & $\dfrac{4}{5}$ de la figure. &2,%
L'écriture décimale de $\dfrac{5}{4}$ est \ldots & $\num{5.4}$  & $\num{1.25}$ & $\dfrac{10}{8}$ &2,%
Le nombre que l'on doit multiplier par $3$ pour obtenir $10$ est \ldots & $\num{3.33}$  & $\dfrac{3}{10}$ & $\dfrac{10}{3}$  &3,%
Sur la figure $2\text{,}$ l'abscisse du point $A$ est \ldots & $\dfrac{3}{5}$  & $\num{1.3}$ & $\dfrac{8}{5}$ &3,%
Une autre écriture de $\dfrac{12}{18}$ est \ldots & $\dfrac{2}{8}$  & $\dfrac{2}{3}$ & $1+ \dfrac{6}{18}$  &2,%
$24 \times \dfrac{5}{6}$ est égal à \ldots & $(24 \times 6) : 5$  & $(24 : 6) \times 5$ & $\num{19.2}$ &2,%
Laura a dépensé les $\dfrac{5}{6}$ de ses $60$ \texteuro. Il lui reste \ldots & $50$ \texteuro.  & $10$ \texteuro. & $\dfrac{5}{6} \times 60$ \texteuro.  &2,%
Il y a $28$ élèves dans une classe de $6$\ieme. $25\,\%$ d'entre eux sont externes. On peut affirmer qu' \ldots & il y a $25$ externes.  & il y a $\dfrac{28}{25} \times 100$ externes. & il y a $7$ externes. &3%
}
\normalsize{}
\newpage
\begin{minipage}{0.45\textwidth}
\begin{center}
	\Fraction[Triangle,Longueur=4cm,Parts=4,Reponse, Couleur=Gold,Epaisseur=1.5]{12/16}\\
	\textbf{Figure 1}
\end{center}
\end{minipage}
\begin{minipage}{0.45\textwidth}
\begin{center}
	\Reperage[DemiDroite, AffichageAbs=3, AffichageNom, AffichageGrad, Pasx=5, Unitex=4]{8/A}
	\textbf{Figure 2}
\end{center}
\end{minipage}
\vspace{0.2cm}

\Fraction[Regulier, Cotes=7, Reponse, Rayon=1.5cm, Couleur=LightSkyBlue]{7/7} \Fraction[Regulier, Cotes=7, Reponse, Rayon=1.5cm, Couleur=LightSkyBlue]{2/7} \hspace{1cm}
\Fraction[Reponse, Rayon=1.5cm, Couleur=LightSkyBlue]{4/5} \hspace{1cm}
\Stat[Graphique, Angle, Rayon=1.5cm, Legende=false, ListeCouleurs={SteelBlue, Plum, Plum, White}]{1/90, 2/90, 3/90, 4/90}\\
\Reperage[DemiDroite, AffichageAbs=2, AffichageNom, AffichageGrad, Pasx=6, Unitex=7]{5/A,13/B}\\
\includegraphics[width=15cm]{6_C11_Droite_graduée2.png}
\\ \vspace{-0.5cm}

\begin{minipage}{0.45\textwidth}
\begin{center}
	\Fraction[Triangle,Longueur=4cm,Parts=4,Reponse, Couleur=Gold,Epaisseur=1.5]{12/16}\\
	\textbf{Figure 1}
\end{center}
\end{minipage}
\begin{minipage}{0.45\textwidth}
\begin{center}
	\Reperage[DemiDroite, AffichageAbs=3, AffichageNom, AffichageGrad, Pasx=5, Unitex=4]{8/A}
	\textbf{Figure 2}
\end{center}
\end{minipage}
\bigskip

\Fraction[Regulier, Cotes=7, Reponse, Rayon=1.5cm, Couleur=LightSkyBlue]{7/7} \Fraction[Regulier, Cotes=7, Reponse, Rayon=1.5cm, Couleur=LightSkyBlue]{2/7} \hspace{1cm}
\Fraction[Reponse, Rayon=1.5cm, Couleur=LightSkyBlue]{4/5} \hspace{1cm}
\Stat[Graphique, Angle, Rayon=1.5cm, Legende=false, ListeCouleurs={SteelBlue, Plum, Plum, White}]{1/90, 2/90, 3/90, 4/90}\\
\Reperage[DemiDroite, AffichageAbs=2, AffichageNom, AffichageGrad, Pasx=6, Unitex=7]{5/A,13/B}\\
\includegraphics[width=15cm]{6_C11_Droite_graduée2.png}
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%--------------------------------------------------------------- Histoire des mathématiques -------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\newpage
\begin{tcolorbox}[colframe=black, colback=blue!10, boxrule=2pt, arc=6pt]
	\centering{}\LARGE{\textbf{Histoire des mathématiques \faBook}}
\end{tcolorbox}
\begin{tcolorbox}[colframe=black!30, colback=blue!0, boxrule=2pt, arc=6pt, title=?, coltitle=black]
Différentes unités de mesure. Approximation de $\pi$ avec le périmètre des cercles d'un polygone.
\end{tcolorbox}

\textbf{\underline{Sources}}
\begin{itemize}[label=\textbullet]
	\item Hauchecorne, B. et Suratteau, D. (2008), \textit{Des Mathématiciens de A à Z} ($3^e$ édition), Ellipses
	\item Byrne, O. (2022), \textit{The first six books of the elements of Euclid}, Taschen
	\item Houlou-Garcia, A. (2019), \textit{Mathematikos} (2\ieme\, tirage), Belles lettres
	\item Barbin, E. (2019), \textit{Faire des mathématiques avec l'histoire au lycée}, Ellipses
	\item Dahan-Dalmedico, A. et Peiffer, J. (1986), \textit{Une histoire des mathématiques : Routes et dédales}, Points
	\item Freiberger, M. et Thomas, R. (2018), \textit{Dans le secret des nombres}, Ekho
\end{itemize}

%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%-------------------------------------------------------------------------- Fiche de route ------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\newpage
\begin{tcolorbox}[colframe=black, colback=Khaki!50, boxrule=2pt, arc=6pt]
	\centering{}\LARGE{\textbf{Fiche de route}}
\end{tcolorbox}
\begin{itemize}[label=\textbullet]
	\item \color{Tomato} \textbf{Introduction} \color{black} : Titre, prérequis, objectifs, rappels
	\item \color{RoyalBlue}\underline{Leçon} \color{black} : Partie I : 
	\item \color{ForestGreen}\underline{Exercices}  \color{black} : 
	\item \color{RoyalBlue}\underline{Leçon}  \color{black} : Parties IV et V : Bilan + Auto-évaluation
	\item \color{ForestGreen}\underline{Exercices bilan}  \color{black} : 
	\item \color{Coral} \underline{Histoire des mathématiques}
\end{itemize}

%\setcounter{page}{4}
%\end{comment}
\end{document}
