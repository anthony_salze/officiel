\documentclass[dvipsnames, a4paper, 12pt]{report}

\usepackage{lmodern} %police standard latin moderne
\usepackage[frenchb]{babel} %langue française
\usepackage[utf8]{inputenc} %pour l'utf-8
\usepackage[T1]{fontenc} %pour les césures et accentuations
\usepackage{geometry} %pour changer les marges d'un document
\usepackage{graphicx}
\usepackage{fancybox}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{fontawesome5}
\usepackage{ProfCollege}
\usepackage{luacode}
\usepackage{xlop}

\usepackage[explicit]{titlesec} %Permet de modifier les différentes parties, chapitres, sous-parties,...

\usepackage{colortbl} %permet de mettre des cellules de tableau en couleur

\usepackage{multicol}

\usepackage{cellspace} %pour définir les espaces en haut et en bas des cellules d'un tableau
\setlength{\cellspacebottomlimit}{5pt}
\setlength{\cellspacetoplimit}{5pt}
\setlength{\parindent}{0pt}

\usepackage{xcolor} %permet de mettre du texte en couleur

\usepackage{array} %gestion des tableaux
\usepackage{tcolorbox} %créer des boîtes de couleurs

\usepackage{hyperref} %permet de gérer le sommaire et la page de garde et les options du PDF

\geometry{top=2cm, bottom=2cm, right=2cm, left=2cm}   
\titlespacing{\chapter}{0pt}{0pt}{5pt}

\setcounter{chapter}{8}

\usepackage{lastpage}
\usepackage{fancyhdr} %définition des en-têtes et des bas de page
\fancyhf{}
\fancypagestyle{main}{
	\renewcommand\headrulewidth{5pt}
	\fancyhead[L]{6\ieme\, - Chapitre \arabic{chapter} - Les angles}
	\fancyhead[R]{Collège}
	\fancyfoot[C]{\thepage/\pageref{LastPage}}
	\fancyfoot[R]{\tiny{Anthony Salze}}
	}
\pagestyle{main}

%gestion des entêtes
\makeatletter\@addtoreset{section}{chapter}\makeatother % RAZ des numéros de section après un chapitre
\renewcommand{\thepart}{\Alph{part} -} % Pour mettre des I, II, etc. aux parties
\renewcommand{\thesection}{\color{Green} \Roman{section} -}% Idem pour les sections et avoir le numéro de chapitre
\renewcommand{\thesubsection}{\arabic{subsection} -}

\begin{document}
%format du titre des chapitres placé après Sommaire sinon cela bugue
\titleformat{\chapter}[frame] {\normalfont \bfseries \Large}{\textcolor{red} {\chaptertitlename\ \thechapter\,-\,#1}}{0pt}{\Huge}

\setcounter{chapter}{7}

\chapter{Les angles}
\begin{luacode}
	d=1
	p=1
	t=1
	m=1
\end{luacode}
\color{blue}\underline{\textbf{Prérequis}}
\begin{itemize}
	\item Tracer des droites, demi-droites, segments.
\end{itemize}~~

\underline{\textbf{Objectifs}}
\begin{itemize}
	\item Connaître la définition d'un angle et le vocabulaire associé (aigu, obtus, droit, plat, nul)
	\item Savoir nommer un angle.
	\item Connaître l'utilisation du rapporteur.
\end{itemize}
\color{black}


\color{Green}
\section{Définitions et notations}
\color{black}
\normalsize{}

\begin{tcolorbox}[colframe=Green!20, colback=Green!0, boxrule=2pt, arc=6pt, title={\textbf{Définitions \arabic{chapter}.\directlua{tex.print(d)} - \arabic{chapter}.\directlua{tex.print(d+1)} - \arabic{chapter}.\directlua{tex.print(d+2)}}}, coltitle=black]
\begin{itemize}[label=\textbullet]
    	\item Un angle est une portion du plan délimité par deux demi-droites de même origine.
   	\item Les demi-droites sont les côtés de l'angle.
   	\item L'origine commune de ces deux demi-droites s'appelle le sommet de l'angle.
\end{itemize}
\end{tcolorbox}
\begin{luacode}
	d=d+3
\end{luacode}

\textbf{\underline{Remarques}}
\begin{itemize}[label=\textbullet]
    \item Dans un texte, on note un angle à l'aide de 3 lettres. La lettre centrale est toujours le sommet de l'angle. On surmonte ces 3 lettres d'un chapeau.
    \item Sur une figure, un angle est codé par un arc de cercle dont le centre est le sommet de l'angle.
\end{itemize}
~~\\
\textbf{\underline{Exemple}}\\
\begin{minipage}{0.6\textwidth}
On note l'angle ci-contre $\widehat{B \textcolor{red} A C} \text{ ou } \widehat{C \textcolor{red} A B}$.\\
Les demi-droites [\textcolor{red}A B) et [\textcolor{red} A C) sont les côtés de l'angle.\\
Le point \textcolor{red}A est son sommet.\\
\\
\\
On aurait pu aussi nommer cet angle $\widehat{x \textcolor{red} A y} \text{ ou } \widehat{y \textcolor{red} A x}$.\\
\end{minipage}
\begin{minipage}{0.4\textwidth}
    \begin{center}
    	\begin{Geometrie}[CoinHD={(5u,4u)}]
		pair A,B,C,D,E;
		A=u*(1,1);
		B=u*(3,0.625);
		C=u*(2.5,2.5);
		D=u*(3.5,3.5);
		E=u*(4,0.5);
		marque_s:=marque_s/3;
		trace demidroite(A,B);
		trace demidroite(A,C);
		trace marquedemidroite(B,A);
		trace marquedemidroite(C,A);
		trace Codeangle(B,A,C,0,btex \textbf{\hspace{0.7cm}Angle} etex);
		label.llft(btex \color{red}A etex,A);
		label.lrt(btex B etex,B);
		label.ulft(btex C etex,C);
		label.ulft(btex $x$ etex,D);
		label.bot(btex $y$ etex, E);
	\end{Geometrie}
    \end{center}
\end{minipage}


\color{Green}
\section{Mesure d'un angle}
\color{black}
\begin{tcolorbox}[colframe=Green!20, colback=Green!0, boxrule=2pt, arc=6pt, title={\textbf{Définition \arabic{chapter}.\directlua{tex.print(d)}}}, coltitle=black]
L'unité de mesure des angles est le degré, notée $^\circ$.
\end{tcolorbox}
\begin{luacode}
	d=d+1
\end{luacode}

\begin{tcolorbox}[colframe=Green!20, colback=Green!0, boxrule=2pt, arc=6pt, title={\textbf{Définition \arabic{chapter}.\directlua{tex.print(d)}}}, coltitle=black]
\begin{center}\begin{tabular}{|c|c|c|} \hline
\textbf{Figure} & \textbf{Nom de l'angle $\widehat{BAC}$} & \textbf{Mesure} \tabularnewline \hline
& & \tabularnewline
\begin{Geometrie}[CoinHD={(5u,3u)}]
	pair A,C,B;
	A=u*(1,1);
	B=u*(2,1);
	C=u*(4,1);
	marque_s:=marque_s/3;
	trace demidroite(A,B);
	trace marquedemidroite(A,B);
	trace marquedemidroite(B,C);
	trace marquedemidroite(C,A);
	label.bot(btex A etex, A);
	label.bot(btex B etex, B);
	label.bot(btex C etex, C);
\end{Geometrie} & nul & $0^\circ$ \tabularnewline \hline
\begin{Geometrie}[CoinHD={(4u,3.5u)}]
	pair A,C,B;
	A=u*(1,1);
	B=u*(3,1);
	C=u*(2,3);
	marque_s:=marque_s/3;
	trace demidroite(B,A);
	trace demidroite(C,A);
	trace marquedemidroite(A,B);
	trace Codeangle(B,A,C,0,btex \hspace{0.5cm} etex);
	label.bot(btex A etex, A);
	label.bot(btex B etex, B);
	label.lft(btex C etex, C);
\end{Geometrie} & aigu & $0^\circ < \widehat{BAC} < 90^\circ$ \tabularnewline \hline
\begin{Geometrie}[CoinHD={(4u,3.5u)}]
	pair A,C,B;
	A=u*(1,1);
	B=u*(3,1);
	C=u*(1,3);
	marque_s:=marque_s/3;
	trace demidroite(A,B);
	trace demidroite(A,C);
	trace codeperp(C,A,B,10);
	label.bot(btex A etex, A);
	label.bot(btex B etex, B);
	label.llft(btex C etex, C);
\end{Geometrie} & droit & $90^\circ$ \tabularnewline \hline
\begin{Geometrie}[CoinHD={(4u,3.5u)}]
	pair A,C,B;
	A=u*(2,1);
	B=u*(3,1);
	C=u*(1,3);
	marque_s:=marque_s/3;
	trace demidroite(A,B);
	trace demidroite(A,C);
	trace marquedemidroite(A,B);
	trace Codeangle(B,A,C,0,btex \hspace{0.5cm} etex);
	label.bot(btex A etex, A);
	label.bot(btex B etex, B);
	label.lft(btex C etex, C);
\end{Geometrie} & obtus & $90^\circ < \widehat{BAC} < 180^\circ$  \tabularnewline \hline
& & \tabularnewline
\begin{Geometrie}[CoinHD={(5u,3u)}]
	pair A,C,B;
	C=u*(1,1);
	A=u*(2,1);
	B=u*(4,1);
	trace droite(A,B);
	marque_s:=marque_s/3;
	trace marquedemidroite(A,B);
	trace marquedemidroite(B,C);
	trace marquedemidroite(C,A);
	label.bot(btex A etex, A);
	label.bot(btex B etex, B);
	label.bot(btex C etex, C);
	\end{Geometrie} & plat & $180^\circ$\tabularnewline \hline
\end{tabular}
\end{center}
\end{tcolorbox}
\begin{luacode}
	d=d+1
\end{luacode}

\underline{\textbf{Remarques}}
\begin{itemize}[label=\textbullet]
    \item Deux angles de mêmes mesures auront le même codage.
    \item Il existe d'autres unités pour la mesure d'un angle comme les radians et les grades.
\end{itemize}
~~

\textbf{\underline{Exemple}}\\
\begin{minipage}{0.65\textwidth}
    Les angles $\widehat{OMT},\,\widehat{TOS} \text{ et } \widehat{TSO}$ sont aigus. L'angle $\widehat{OTS}$ est obtus.\\
    \\
    Les codages indiquent que $\widehat{TOS} = \widehat{TSO}$. Ainsi, le triangle $OTS$ est isocèle en $T$.\\
    L'angle $\widehat{ROM}$ est droit. Sa mesure vaut $90^\circ$. Le triangle $MOS$ est rectangle en $O$.\\
    \\
    Les points $M$, $T$ et $S$ sont alignés donc l'angle $\widehat{MTS}$ est plat. Sa mesure vaut $180^\circ$.
\end{minipage}
\begin{minipage}{0.4\textwidth}
\begin{center}
	\begin{Geometrie}[CoinHD={(5.5u,4u)}]
		pair M,T,S,O,R;
		O=u*(1.5,1);
		R=u*(0.5,1);
		S=u*(4.5,1);
		M=u*(1.5,3);
		T=u*(3,2);
		marque_s:=marque_s/3;
		trace droite(R,S);
		trace droite(M,S);
		trace droite(M,O);
		trace segment(O,T);
		trace marquedemidroite(R,S);
		trace codeperp(M,O,R,5) withcolor(red);
		trace Codeangle(S,O,T,2,btex \hspace{0.5cm} etex) withcolor(blue);
		trace Codeangle(T,S,O,2,btex \hspace{0.5cm} etex) withcolor(blue);
		marque_a:=marque_a/2;
		trace Codeangle(O,T,S,0,btex \hspace{0.5cm} etex) withcolor(DeepPink);
		trace Codeangle(O,M,S,0,btex \hspace{0.5cm} etex) withcolor(MediumSlateBlue);
		label.llft(btex O etex, O);
		label.bot(btex R etex, R);
		label.bot(btex S etex, S);
		label.llft(btex M etex, M);
		label.top(btex T etex, T);
	\end{Geometrie}
\end{center}		
\end{minipage}
~~

\begin{tcolorbox}[colframe=Green!20, colback=Green!0, boxrule=2pt, arc=6pt, title={\textbf{Définitions \arabic{chapter}.\directlua{tex.print(d)} - \arabic{chapter}.\directlua{tex.print(d+1)}}}, coltitle=black]
\begin{itemize}[label=\textbullet]
	\item Deux angles sont dits complémentaires si la somme de leur mesure est égale à $90^\circ$.
	\item Deux angles sont dits supplémentaires si la somme de leur mesure est égale à $180^\circ$.
\end{itemize}
\end{tcolorbox}
\begin{luacode}
	d=d+2
\end{luacode}


\color{Green}
\section{Utilisation du rapporteur}
\color{black}
\begin{center}
    \includegraphics[width=15cm]{6_C12_Rapporteur2.png}
\end{center}
\begin{tcolorbox}[colframe=black!20, colback=black!0, boxrule=2pt, arc=6pt, title={\textbf{Méthode \arabic{chapter}.\directlua{tex.print(m)}}}, coltitle=black]
Pour mesurer un angle, on utilise un rapporteur.\\
Si on veut mesurer l'angle $\widehat{EOF}$. Il faut :
\begin{itemize}[label=\textbullet]
    \item placer le centre du rapporteur sur le sommet de l'angle (ici $O$) ;
    \item superposer un côté de l'angle avec la graduation $0^\circ$ du rapporteur (ici la demi-droite $[OE)$);
    \item réfléchir si l'angle mesuré est aigu ou obtus pour savoir si on utilise les graduations intérieures ou extérieures du rapporteur (ici, c'est un angle aigu, il faudra utiliser les graduations intérieures car la mesure d'un angle aigu est comprise entre $0^\circ$ et $90^\circ$) ;
    \item lire la mesure qui correspond à l'ouverture de l'angle sur la graduation choisie du rapporteur (ici, on lit $63^\circ$).
\end{itemize}
\end{tcolorbox}
\begin{luacode}
	m=m+1
\end{luacode}

\color{Green}
\section{Bilan}
\color{black}

QCM (plusieurs réponses possibles)\\\footnotesize{}
\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom=, AlphT, Largeur=3cm]{%
L'angle sur la figure $1$ s'appelle \ldots & $\widehat{ABC}$. & $\widehat{BAC}$. & $\widehat{ACB}$. &1,%
Le sommet de l'angle $\widehat{TRS}$ est le point \ldots & $T$. & $R$. & $S$. &2,%
Un angle droit mesure \ldots & $360^\circ$ & $90^\circ$ & $180^\circ$ &2,%
Un angle dont la mesure est comprise entre $90^\circ$ et $180^\circ$ est un angle \ldots & aigu. & droit. & obtus. &3,%
$\widehat{RST}=180^\circ$. On peut affirmer que \ldots & $R$ est un point de la droite $(ST)$. & les points $R\text{,}$ $S$ et $T$ sont alignés. & l'angle $\widehat{RST}$ est plat. &1%2,3
}
\normalsize{}
\begin{center}
	\begin{Geometrie}[CoinHD={(4u,3u)}]
		pair A,B,C;
		B=u*(1,1);
		C=u*(3,0.5);
		A=rotation(C,B,37);
		trace demidroite(B,C);
		trace demidroite(B,A);
		marque_p:="croix";
		pointe(A,C);
		trace Codeangle(C,B,A,0,btex \textcolor{white}{\ang{37}} etex); 
		label.lft(btex B etex, B);
		label.bot(btex C etex, C);
		label.top(btex A etex, A);
	\end{Geometrie}\\\color{black}
	\textbf{\underline{Figure 1}}
\end{center}

\color{Green}
\section{Auto-évaluation}
\color{black}

\begin{tabular}{|>{\columncolor{darkgray}\color{white}}m{8cm}|>{\color{white}}m{0.6cm}|m{0.6cm}|m{0.6cm}|m{5cm}|} \hline

\rowcolor{darkgray}\centering{} Compétences & \centering{}\color{white} Oui & \centering{}\color{white}\scriptsize{}\rotatebox{45}{Moyen} & \centering{}\color{white}Non & \centering{}\color{white}Pour réviser \tabularnewline \hline \hline

\cellcolor{gray}Je connais le vocabulaire lié aux angles. & & & & \textbf{\underline{Leçon}} : Partie I \smallskip \newline  \textbf{\underline{Exercices}} : 1 à 3 - \tabularnewline \hline \hline

\cellcolor{gray}Je suis capable de résoudre des problèmes sur les angles. & & & & \textbf{\underline{Leçon}} : Partie II \smallskip \newline  \textbf{\underline{Exercices}} : 4 à 7 - 9 \tabularnewline \hline \hline

\cellcolor{gray}Je sais lire la mesure d'un angle avec mon rapporteur. & & & & \textbf{\underline{Leçon}} : Partie III \smallskip \newline  \textbf{\underline{Exercices}} : 8 - 9 - 15  \tabularnewline \hline \hline

\cellcolor{gray}Je suis capable de construire un angle de mesure donnée à l'aide de mon rapporteur. & & & & \textbf{\underline{Leçon}} : Partie III \smallskip \newline  \textbf{\underline{Exercices}} :  10 à 14 - 16 - 17 - 18 \tabularnewline \hline 
\end{tabular}


\color{Green}
\section{Complément : bissectrice d'un angle}
\color{black}
\begin{tcolorbox}[colframe=Green!20, colback=Green!0, boxrule=2pt, arc=6pt, title={\textbf{Définition \arabic{chapter}.\directlua{tex.print(d)}}}, coltitle=black]
La bissectrice d’un angle est la droite passant par le sommet de cet angle et qui le partage en deux angles de même mesure.
\end{tcolorbox}
\begin{luacode}
	d=d+1
\end{luacode}

\textbf{\underline{Exemple}}
\begin{minipage}{0.7\textwidth}
    D’après les codages de la figure, les angles $\widehat{BAO}$ et $\widehat{CAO}$ sont de même mesure donc $\widehat{BAO} = \widehat{CAO}$.\\
    On peut donc dire que la droite $(AO)$ partage l’angle $\widehat{BAC}$ en deux angles de même mesure. C'est donc la bissectrice de l’angle $\widehat{BAC}$.\\
    \\
    De plus, il existe quelques relations entre les mesures de ces angles :\\
    $\widehat{BAC} = 2 \times \widehat{CAO} = 2 \times \widehat{BAO} = 2 \times 47^\circ= 94^\circ$\\
    $\widehat{BAO} = \widehat{CAO} = \dfrac{\widehat{BAC}}{2}=\frac{94^\circ}{2}=47^\circ$
\end{minipage}
\begin{minipage}{0.3\textwidth}
    \begin{center}
        \includegraphics[width=5.5cm]{6_C12_Bissectrice.PNG}
    \end{center}
\end{minipage}

%\begin{comment}
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------- Correction QCM -----------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\newpage
\footnotesize{}
\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom=, AlphT, Largeur=3cm, Solution, Couleur=cyan!20]{%
L'angle sur la figure $1$ s'appelle \ldots & $\widehat{ABC}$. & $\widehat{BAC}$. & $\widehat{ACB}$. &1,%
Le sommet de l'angle $\widehat{TRS}$ est le point \ldots & $T$. & $R$. & $S$. &2,%
Un angle droit mesure \ldots & $360^\circ$ & $90^\circ$ & $180^\circ$ &2,%
Un angle dont la mesure est comprise entre $90^\circ$ et $180^\circ$ est un angle \ldots & aigu. & droit. & obtus. &3,%
$\widehat{RST}=180^\circ$. On peut affirmer que \ldots & $R$ est un point de la droite $(ST)$. & \cellcolor{cyan!20} les points $R\text{,}$ $S$ et $T$ sont alignés. & \cellcolor{cyan!20} l'angle $\widehat{RST}$ est plat. &1%2,3
}
\normalsize{}
\begin{center}
	\begin{Geometrie}[CoinHD={(4u,3u)}]
		pair A,B,C;
		B=u*(1,1);
		C=u*(3,0.5);
		A=rotation(C,B,37);
		trace demidroite(B,C);
		trace demidroite(B,A);
		marque_p:="croix";
		pointe(A,C);
		trace Codeangle(C,B,A,0,btex \textcolor{white}{\ang{37}} etex); 
		label.lft(btex B etex, B);
		label.bot(btex C etex, C);
		label.top(btex A etex, A);
	\end{Geometrie}\\\color{black}
	\textbf{\underline{Figure 1}}
\end{center}

%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%---------------------------------------------------------------------- Leçon à trous -----------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\newpage
%format du titre des chapitres placé après Sommaire sinon cela bugue
\titleformat{\chapter}[frame] {\normalfont \bfseries \Large}{\textcolor{red} {\chaptertitlename\ \thechapter\,-\,#1}}{0pt}{\Huge}

\setcounter{chapter}{8}

\chapter{\pointilles[7cm]}
\begin{luacode}
	d=1
	p=1
	t=1
	m=1
\end{luacode}
\color{blue}\underline{\textbf{Prérequis}}
\begin{itemize}
	\item \pointilles[16cm]
\end{itemize}~~

\underline{\textbf{Objectifs}}
\begin{itemize}
	\item \pointilles[16cm]\\
	\pointilles[16cm]
	\item \pointilles[16cm]
	\item \pointilles[16cm]
\end{itemize}
\color{black}


\color{Green}
\section{\pointilles[12cm]}
\color{black}
\normalsize{}

\begin{tcolorbox}[colframe=Green!20, colback=Green!0, boxrule=2pt, arc=6pt, title={\textbf{Définitions \arabic{chapter}.\directlua{tex.print(d)} - \arabic{chapter}.\directlua{tex.print(d+1)} - \arabic{chapter}.\directlua{tex.print(d+2)}}}, coltitle=black]
\begin{itemize}[label=\textbullet]
    	\item \pointilles[15cm]
   	\item \pointilles[15cm]
   	\item \pointilles[15cm]
\end{itemize}
\end{tcolorbox}
\begin{luacode}
	d=d+3
\end{luacode}

\textbf{\underline{Remarques}}
\begin{itemize}[label=\textbullet]
    \item \pointilles[16cm]\\
    \pointilles[16cm]
    \item \pointilles[16cm]\\
    \pointilles[16cm]
\end{itemize}
~~\\
\textbf{\underline{Exemple}}\\\\
\begin{minipage}{0.6\textwidth}
\pointilles[10cm]\\
\pointilles[10cm]\\
\pointilles[10cm]\\
\pointilles[10cm]\\
\pointilles[10cm]\\
\pointilles[10cm]\\
\pointilles[10cm]
\end{minipage}
\begin{minipage}{0.4\textwidth}
    \begin{center}
    	\begin{Geometrie}[CoinHD={(5u,4u)}]
		pair A,B,C,D,E;
		A=u*(1,1);
		B=u*(3,0.625);
		C=u*(2.5,2.5);
		D=u*(3.5,3.5);
		E=u*(4,0.5);
		marque_s:=marque_s/3;
		trace demidroite(A,B);
		trace demidroite(A,C);
		trace marquedemidroite(B,A);
		trace marquedemidroite(C,A);
		trace Codeangle(B,A,C,0,btex \textbf{\hspace{0.7cm}Angle} etex);
		label.llft(btex \color{red}A etex,A);
		label.lrt(btex B etex,B);
		label.ulft(btex C etex,C);
		label.ulft(btex $x$ etex,D);
		label.bot(btex $y$ etex, E);
	\end{Geometrie}
    \end{center}
\end{minipage}


\color{Green}
\section{\pointilles[10cm]}
\color{black}
\begin{tcolorbox}[colframe=Green!20, colback=Green!0, boxrule=2pt, arc=6pt, title={\textbf{Définition \arabic{chapter}.\directlua{tex.print(d)}}}, coltitle=black]
\pointilles[16cm]
\end{tcolorbox}
\begin{luacode}
	d=d+1
\end{luacode}

\begin{tcolorbox}[colframe=Green!20, colback=Green!0, boxrule=2pt, arc=6pt, title={\textbf{Définition \arabic{chapter}.\directlua{tex.print(d)}}}, coltitle=black]
\begin{center}\begin{tabular}{|c|c|c|} \hline
\textbf{Figure} & \textbf{Nom de l'angle $\widehat{BAC}$} & \textbf{Mesure} \tabularnewline \hline
& & \tabularnewline
\begin{Geometrie}[CoinHD={(5u,3u)}]
	pair A,C,B;
	A=u*(1,1);
	B=u*(2,1);
	C=u*(4,1);
	marque_s:=marque_s/3;
	trace demidroite(A,B);
	trace marquedemidroite(A,B);
	trace marquedemidroite(B,C);
	trace marquedemidroite(C,A);
	label.bot(btex A etex, A);
	label.bot(btex B etex, B);
	label.bot(btex C etex, C);
\end{Geometrie} & \pointilles[4cm] &\pointilles[4cm] \tabularnewline \hline
\begin{Geometrie}[CoinHD={(4u,3.5u)}]
	pair A,C,B;
	A=u*(1,1);
	B=u*(3,1);
	C=u*(2,3);
	marque_s:=marque_s/3;
	trace demidroite(B,A);
	trace demidroite(C,A);
	trace marquedemidroite(A,B);
	trace Codeangle(B,A,C,0,btex \hspace{0.5cm} etex);
	label.bot(btex A etex, A);
	label.bot(btex B etex, B);
	label.lft(btex C etex, C);
\end{Geometrie} & \pointilles[4cm] & \pointilles[4cm] \tabularnewline \hline
\begin{Geometrie}[CoinHD={(4u,3.5u)}]
	pair A,C,B;
	A=u*(1,1);
	B=u*(3,1);
	C=u*(1,3);
	marque_s:=marque_s/3;
	trace demidroite(A,B);
	trace demidroite(A,C);
	trace codeperp(C,A,B,10);
	label.bot(btex A etex, A);
	label.bot(btex B etex, B);
	label.llft(btex C etex, C);
\end{Geometrie} & \pointilles[4cm] & \pointilles[4cm] \tabularnewline \hline
\begin{Geometrie}[CoinHD={(4u,3.5u)}]
	pair A,C,B;
	A=u*(2,1);
	B=u*(3,1);
	C=u*(1,3);
	marque_s:=marque_s/3;
	trace demidroite(A,B);
	trace demidroite(A,C);
	trace marquedemidroite(A,B);
	trace Codeangle(B,A,C,0,btex \hspace{0.5cm} etex);
	label.bot(btex A etex, A);
	label.bot(btex B etex, B);
	label.lft(btex C etex, C);
\end{Geometrie} & \pointilles[4cm] & \pointilles[4cm]  \tabularnewline \hline
& & \tabularnewline
\begin{Geometrie}[CoinHD={(5u,3u)}]
	pair A,C,B;
	C=u*(1,1);
	A=u*(2,1);
	B=u*(4,1);
	trace droite(A,B);
	marque_s:=marque_s/3;
	trace marquedemidroite(A,B);
	trace marquedemidroite(B,C);
	trace marquedemidroite(C,A);
	label.bot(btex A etex, A);
	label.bot(btex B etex, B);
	label.bot(btex C etex, C);
	\end{Geometrie} & \pointilles[4cm] & \pointilles[4cm] \tabularnewline \hline
\end{tabular}
\end{center}
\end{tcolorbox}
\begin{luacode}
	d=d+1
\end{luacode}

\underline{\textbf{Remarques}}
\begin{itemize}[label=\textbullet]
    \item \pointilles[16cm]
    \item \pointilles[16cm]
\end{itemize}
~~

\textbf{\underline{Exemple}}\\\\
\begin{minipage}{0.65\textwidth}
\pointilles[11cm]\\
\pointilles[11cm]\\
\pointilles[11cm]\\
\pointilles[11cm]\\
\pointilles[11cm]\\
\pointilles[11cm]\\
\pointilles[11cm]\\
\pointilles[11cm]\\
\pointilles[11cm]\\
\pointilles[11cm]
\end{minipage}
\begin{minipage}{0.4\textwidth}
\begin{center}
	\begin{Geometrie}[CoinHD={(5.5u,4u)}]
		pair M,T,S,O,R;
		O=u*(1.5,1);
		R=u*(0.5,1);
		S=u*(4.5,1);
		M=u*(1.5,3);
		T=u*(3,2);
		marque_s:=marque_s/3;
		trace droite(R,S);
		trace droite(M,S);
		trace droite(M,O);
		trace segment(O,T);
		trace marquedemidroite(R,S);
		trace codeperp(M,O,R,5) withcolor(red);
		trace Codeangle(S,O,T,2,btex \hspace{0.5cm} etex) withcolor(blue);
		trace Codeangle(T,S,O,2,btex \hspace{0.5cm} etex) withcolor(blue);
		marque_a:=marque_a/2;
		trace Codeangle(O,T,S,0,btex \hspace{0.5cm} etex) withcolor(DeepPink);
		trace Codeangle(O,M,S,0,btex \hspace{0.5cm} etex) withcolor(MediumSlateBlue);
		label.llft(btex O etex, O);
		label.bot(btex R etex, R);
		label.bot(btex S etex, S);
		label.llft(btex M etex, M);
		label.top(btex T etex, T);
	\end{Geometrie}
\end{center}		
\end{minipage}

\begin{tcolorbox}[colframe=Green!20, colback=Green!0, boxrule=2pt, arc=6pt, title={\textbf{Définitions \arabic{chapter}.\directlua{tex.print(d)} - \arabic{chapter}.\directlua{tex.print(d+1)}}}, coltitle=black]
\begin{itemize}[label=\textbullet]
	\item \pointilles[15cm]\\
	\pointilles[15cm]
	\item \pointilles[15cm]\\
	\pointilles[15cm]
\end{itemize}
\end{tcolorbox}


\begin{luacode}
	d=d+2
\end{luacode}
\color{Green}
\section{\pointilles[10cm]}
\color{black}
\begin{center}
    \includegraphics[width=15cm]{6_C12_Rapporteur2.png}
\end{center}
\begin{tcolorbox}[colframe=black!20, colback=black!0, boxrule=2pt, arc=6pt, title={\textbf{Méthode \arabic{chapter}.\directlua{tex.print(m)}}}, coltitle=black]
Pour mesurer un angle, on utilise un rapporteur.\\
Si on veut mesurer l'angle $\widehat{EOF}$. Il faut :
\begin{itemize}[label=\textbullet]
    \item \pointilles[15cm]
    \item \pointilles[15cm]\\
    \pointilles[15cm]
    \item \pointilles[15cm]\\
    \pointilles[15cm]\\
    \pointilles[15cm]\\
    \pointilles[15cm]
    \item \pointilles[15cm]\\
    \pointilles[15cm]
\end{itemize}
\end{tcolorbox}
\begin{luacode}
	m=m+1
\end{luacode}



\color{Green}
\section{Bilan}
\color{black}

QCM (plusieurs réponses possibles)\\\footnotesize{}
\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom=, AlphT, Largeur=3cm]{%
L'angle sur la figure $1$ s'appelle \ldots & $\widehat{ABC}$. & $\widehat{BAC}$. & $\widehat{ACB}$. &1,%
Le sommet de l'angle $\widehat{TRS}$ est le point \ldots & $T$. & $R$. & $S$. &2,%
Un angle droit mesure \ldots & $360^\circ$ & $90^\circ$ & $180^\circ$ &2,%
Un angle dont la mesure est comprise entre $90^\circ$ et $180^\circ$ est un angle \ldots & aigu. & droit. & obtus. &3,%
$\widehat{RST}=180^\circ$. On peut affirmer que \ldots & $R$ est un point de la droite $(ST)$. & les points $R\text{,}$ $S$ et $T$ sont alignés. & l'angle $\widehat{RST}$ est plat. &1%2,3
}
\normalsize{}
\begin{center}
	\begin{Geometrie}[CoinHD={(4u,3u)}]
		pair A,B,C;
		B=u*(1,1);
		C=u*(3,0.5);
		A=rotation(C,B,37);
		trace demidroite(B,C);
		trace demidroite(B,A);
		marque_p:="croix";
		pointe(A,C);
		trace Codeangle(C,B,A,0,btex \textcolor{white}{\ang{37}} etex); 
		label.lft(btex B etex, B);
		label.bot(btex C etex, C);
		label.top(btex A etex, A);
	\end{Geometrie}\\\color{black}
	\textbf{\underline{Figure 1}}
\end{center}



\color{Green}
\section{Auto-évaluation}
\color{black}

\begin{tabular}{|>{\columncolor{darkgray}\color{white}}m{8cm}|>{\color{white}}m{0.6cm}|m{0.6cm}|m{0.6cm}|m{5cm}|} \hline

\rowcolor{darkgray}\centering{} Compétences & \centering{}\color{white} Oui & \centering{}\color{white}\scriptsize{}\rotatebox{45}{Moyen} & \centering{}\color{white}Non & \centering{}\color{white}Pour réviser \tabularnewline \hline \hline

\cellcolor{gray}Je connais le vocabulaire lié aux angles. & & & & \textbf{\underline{Leçon}} : Partie I \smallskip \newline  \textbf{\underline{Exercices}} : 1 à 3 - \tabularnewline \hline \hline

\cellcolor{gray}Je suis capable de résoudre des problèmes sur les angles. & & & & \textbf{\underline{Leçon}} : Partie II \smallskip \newline  \textbf{\underline{Exercices}} : 4 à 7 - 9 \tabularnewline \hline \hline

\cellcolor{gray}Je sais lire la mesure d'un angle avec mon rapporteur. & & & & \textbf{\underline{Leçon}} : Partie III \smallskip \newline  \textbf{\underline{Exercices}} : 8 - 9 - 15  \tabularnewline \hline \hline

\cellcolor{gray}Je suis capable de construire un angle de mesure donnée à l'aide de mon rapporteur. & & & & \textbf{\underline{Leçon}} : Partie III \smallskip \newline  \textbf{\underline{Exercices}} :  10 à 14 - 16 - 17 - 18 \tabularnewline \hline 
\end{tabular}


%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------- Impression -----------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\newpage
\begin{center}\begin{tabular}{|c|c|c|} \hline
\textbf{Figure} & \textbf{Nom de l'angle $\widehat{BAC}$} & \textbf{Mesure} \tabularnewline \hline
& & \tabularnewline
\begin{Geometrie}[CoinHD={(5u,3u)}]
	pair A,C,B;
	A=u*(1,1);
	B=u*(2,1);
	C=u*(4,1);
	marque_s:=marque_s/3;
	trace demidroite(A,B);
	trace marquedemidroite(A,B);
	trace marquedemidroite(B,C);
	trace marquedemidroite(C,A);
	label.bot(btex A etex, A);
	label.bot(btex B etex, B);
	label.bot(btex C etex, C);
\end{Geometrie} & \pointilles[4cm] &\pointilles[4cm] \tabularnewline \hline
\begin{Geometrie}[CoinHD={(4u,3.5u)}]
	pair A,C,B;
	A=u*(1,1);
	B=u*(3,1);
	C=u*(2,3);
	marque_s:=marque_s/3;
	trace demidroite(B,A);
	trace demidroite(C,A);
	trace marquedemidroite(A,B);
	trace Codeangle(B,A,C,0,btex \hspace{0.5cm} etex);
	label.bot(btex A etex, A);
	label.bot(btex B etex, B);
	label.lft(btex C etex, C);
\end{Geometrie} & \pointilles[4cm] & \pointilles[4cm] \tabularnewline \hline
\begin{Geometrie}[CoinHD={(4u,3.5u)}]
	pair A,C,B;
	A=u*(1,1);
	B=u*(3,1);
	C=u*(1,3);
	marque_s:=marque_s/3;
	trace demidroite(A,B);
	trace demidroite(A,C);
	trace codeperp(C,A,B,10);
	label.bot(btex A etex, A);
	label.bot(btex B etex, B);
	label.llft(btex C etex, C);
\end{Geometrie} & \pointilles[4cm] & \pointilles[4cm] \tabularnewline \hline
\begin{Geometrie}[CoinHD={(4u,3.5u)}]
	pair A,C,B;
	A=u*(2,1);
	B=u*(3,1);
	C=u*(1,3);
	marque_s:=marque_s/3;
	trace demidroite(A,B);
	trace demidroite(A,C);
	trace marquedemidroite(A,B);
	trace Codeangle(B,A,C,0,btex \hspace{0.5cm} etex);
	label.bot(btex A etex, A);
	label.bot(btex B etex, B);
	label.lft(btex C etex, C);
\end{Geometrie} & \pointilles[4cm] & \pointilles[4cm]  \tabularnewline \hline
& & \tabularnewline
\begin{Geometrie}[CoinHD={(5u,3u)}]
	pair A,C,B;
	C=u*(1,1);
	A=u*(2,1);
	B=u*(4,1);
	trace droite(A,B);
	marque_s:=marque_s/3;
	trace marquedemidroite(A,B);
	trace marquedemidroite(B,C);
	trace marquedemidroite(C,A);
	label.bot(btex A etex, A);
	label.bot(btex B etex, B);
	label.bot(btex C etex, C);
	\end{Geometrie} & \pointilles[4cm] & \pointilles[4cm] \tabularnewline \hline
\end{tabular}
\end{center}
\begin{center}
    \includegraphics[width=15cm]{6_C12_Rapporteur2.png}
\end{center}

QCM (plusieurs réponses possibles)\\\footnotesize{}
\QCM[Alterne,Stretch=2,Reponses=3, Titre, Nom=, AlphT, Largeur=3cm]{%
L'angle sur la figure $1$ s'appelle \ldots & $\widehat{ABC}$. & $\widehat{BAC}$. & $\widehat{ACB}$. &1,%
Le sommet de l'angle $\widehat{TRS}$ est le point \ldots & $T$. & $R$. & $S$. &2,%
Un angle droit mesure \ldots & $360^\circ$ & $90^\circ$ & $180^\circ$ &2,%
Un angle dont la mesure est comprise entre $90^\circ$ et $180^\circ$ est un angle \ldots & aigu. & droit. & obtus. &3,%
$\widehat{RST}=180^\circ$. On peut affirmer que \ldots & $R$ est un point de la droite $(ST)$. & les points $R\text{,}$ $S$ et $T$ sont alignés. & l'angle $\widehat{RST}$ est plat. &1%2,3
}
\normalsize{}
\begin{center}
	\begin{Geometrie}[CoinHD={(4u,3u)}]
		pair A,B,C;
		B=u*(1,1);
		C=u*(3,0.5);
		A=rotation(C,B,37);
		trace demidroite(B,C);
		trace demidroite(B,A);
		marque_p:="croix";
		pointe(A,C);
		trace Codeangle(C,B,A,0,btex \textcolor{white}{\ang{37}} etex); 
		label.lft(btex B etex, B);
		label.bot(btex C etex, C);
		label.top(btex A etex, A);
	\end{Geometrie}\\\color{black}
	\textbf{\underline{Figure 1}}
\end{center}

%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%--------------------------------------------------------------- Histoire des mathématiques -------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\newpage
\begin{tcolorbox}[colframe=black, colback=blue!10, boxrule=2pt, arc=6pt]
	\centering{}\LARGE{\textbf{Histoire des mathématiques \faBook}}
\end{tcolorbox}
\begin{tcolorbox}[colframe=black!30, colback=blue!0, boxrule=2pt, arc=6pt, title=?, coltitle=black]

\end{tcolorbox}

\begin{tcolorbox}[colframe=black!10, colback=blue!0, boxrule=1pt, arc=1pt, title=Quelques anecdotes, coltitle=black]

\end{tcolorbox}

\textbf{\underline{Sources}}
\begin{itemize}[label=\textbullet]
	\item Hauchecorne, B. et Suratteau, D. (2008), \textit{Des Mathématiciens de A à Z} ($3^e$ édition), Ellipses
	\item Byrne, O. (2022), \textit{The first six books of the elements of Euclid}, Taschen
	\item Houlou-Garcia, A. (2019), \textit{Mathematikos} (2\ieme\, tirage), Belles lettres
	\item Barbin, E. (2019), \textit{Faire des mathématiques avec l'histoire au lycée}, Ellipses
	\item Dahan-Dalmedico, A. et Peiffer, J. (1986), \textit{Une histoire des mathématiques : Routes et dédales}, Points
	\item Freiberger, M. et Thomas, R. (2018), \textit{Dans le secret des nombres}, Ekho
\end{itemize}

%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%-------------------------------------------------------------------------- Fiche de route ------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\newpage
\begin{tcolorbox}[colframe=black, colback=Khaki!50, boxrule=2pt, arc=6pt]
	\centering{}\LARGE{\textbf{Fiche de route}}
\end{tcolorbox}
\begin{itemize}[label=\textbullet]
	\item \color{Tomato} \textbf{Introduction} \color{black} : Titre, prérequis, objectifs, rappels
	\item \color{RoyalBlue}\underline{Leçon} \color{black} : Partie I : 
	\item \color{ForestGreen}\underline{Exercices}  \color{black} : 
	\item \color{RoyalBlue}\underline{Leçon}  \color{black} : Parties IV et V : Bilan + Auto-évaluation
	\item \color{ForestGreen}\underline{Exercices bilan}  \color{black} : 
	\item \color{Coral} \underline{Histoire des mathématiques}
\end{itemize}

%\end{comment}
\end{document}
