---
author: Anthony Salze
title: Activités
hide :
    -footer
---
# <span style="color:Tomato"> Activités

## <span style="color:LightSalmon"> Histoire des mathématiques
??? tip "Activités <span style=color:red> :material-file-pdf-box: <span style=color:blue>:material-text-box-edit-outline: "
    === "Act_01 - Découverte de la numération chinoise"
        
        | [Sujet PDF (à venir)](a_telecharger/6_Act){:target="_blank"} | [Sujet LaTeX (à venir)](a_telecharger/6_Act) |  

        <span style=color:CornflowerBlue> _Dernière mise à jour : le 07/01/25_

    === "Act_02 - Découverte de l'écriture cunéiforme (phase 1)"

        | [Sujet PDF (à venir)](a_telecharger/6_Act){:target="_blank"} | [Sujet LaTeX (à venir)](a_telecharger/6_Act) |  

        <span style=color:CornflowerBlue> _Dernière mise à jour : le 07/01/25_
    
    === "Act_03 - Découverte de l'écriture cunéiforme (phase 2) - Jeu vidéo Mathêma"

        | [Jeu vidéo - Mathêma](https://anthony_salze.forge.apps.education.fr/mathema/){:target="_blank"} |

        <span style=color:CornflowerBlue> _Dernière mise à jour : le 07/01/25_

    === "Act_04 - Découverte de la numération maya"

        | [Sujet PDF (à venir)](a_telecharger/6_Act){:target="_blank"} | [Sujet LaTeX (à venir)](a_telecharger/6_Act) |  

        <span style=color:CornflowerBlue> _Dernière mise à jour : le 07/01/25_


